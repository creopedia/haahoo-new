package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

import creo.com.haahooapp.Adapters.SlidingContentAdapter;
import creo.com.haahooapp.Modal.StoryPojo;

public class Parallax extends AppCompatActivity {

    private SlidingUpPanelLayout slidingLayout;
    private Button btnShow;
    private Button btnHide;
    private TextView textView;
    LinearLayout dragView;
    RecyclerView recyclerView;
    SlidingContentAdapter slidingContentAdapter;
    Context context = this;
    SearchView searchView;
    ArrayList<StoryPojo> storyPojos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        // will hide the title
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parallax);
        btnShow = (Button)findViewById(R.id.btn_show);
        btnHide = (Button)findViewById(R.id.btn_hide);
        searchView = findViewById(R.id.search);
        searchView.setQueryHint("Search for members");
        dragView = findViewById(R.id.dragView);
        textView = (TextView)findViewById(R.id.text);
        recyclerView = findViewById(R.id.recyclerview);
        slidingContentAdapter = new SlidingContentAdapter(context,storyPojos);
        recyclerView.setAdapter(slidingContentAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        //set layout slide listener
        slidingLayout = (SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        btnShow.setVisibility(View.VISIBLE);
        //some "demo" event
        slidingLayout.setPanelSlideListener(onSlideListener());
        btnHide.setOnClickListener(onHideListener());
        btnShow.setOnClickListener(onShowListener());
    }

    /**
     * Request show sliding layout when clicked
     * @return
     */
    private View.OnClickListener onShowListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show sliding layout in bottom of screen (not expand it)
                //dragView.setVisibility(View.VISIBLE);
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                btnShow.setVisibility(View.GONE);
            }
        };
    }

    /**
     * Hide sliding layout when click button
     * @return
     */
    private View.OnClickListener onHideListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide sliding layout
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                btnShow.setVisibility(View.VISIBLE);
            }
        };
    }

    private SlidingUpPanelLayout.PanelSlideListener onSlideListener() {
        return new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
                textView.setText("panel is sliding");
            }

            @Override
            public void onPanelCollapsed(View view) {
                textView.setText("panel Collapse");
            }

            @Override
            public void onPanelExpanded(View view) {
                textView.setText("panel expand");
            }

            @Override
            public void onPanelAnchored(View view) {
                textView.setText("panel anchored");
            }

            @Override
            public void onPanelHidden(View view) {
                textView.setText("panel is Hidden");
            }
        };
    }
}