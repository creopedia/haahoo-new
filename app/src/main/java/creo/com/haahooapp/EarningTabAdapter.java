package creo.com.haahooapp;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import creo.com.haahoo.AddShopEarnings;

public class EarningTabAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public EarningTabAdapter(Context context, FragmentManager fm, int totalTabs) {

        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;

    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ReferralEarning referralEarning = new ReferralEarning();
                return referralEarning;
            case 1:
                DirectSellEarning directSellEarning = new DirectSellEarning();
                return directSellEarning;
            case 2:
                AddShopEarnings addShopEarnings = new AddShopEarnings();
                return addShopEarnings;

            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}