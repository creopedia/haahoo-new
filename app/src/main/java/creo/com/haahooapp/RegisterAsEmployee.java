package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.config.ApiClient;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class RegisterAsEmployee extends AppCompatActivity {

    ImageView back;
    Context context = this;
    CardView card;
    ProgressDialog progressDialog;
    RadioGroup radioGroup;
    RadioButton radioButton,radioButton2,radioButton3;
    int radiobuttonid = 0;
    Activity activity = this;
    String user_type = "null";
    TextView interest1,interest,head;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_as_employee);
        Window window = activity.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        Bundle bundle = getIntent().getExtras();
        radioGroup = findViewById(R.id.radioGroup);
        radiobuttonid = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(R.id.radioButton);
        radioButton2 = findViewById(R.id.radioButton2);
        radioButton3 = findViewById(R.id.radioButton3);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_type = "employee";
            }
        });

        radioButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_type = "freelancer";
            }
        });

        radioButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_type = "e Distributor";
            }
        });

        String status = bundle.getString("status");
        card = findViewById(R.id.card);
        interest1 = findViewById(R.id.interest1);
        interest = findViewById(R.id.interest);
        head = findViewById(R.id.head);
        if(status.equals("Failed")){
            head.setVisibility(View.VISIBLE);
            interest.setVisibility(View.VISIBLE);
            card.setVisibility(View.VISIBLE);
            radioGroup.setVisibility(View.VISIBLE);
        }
        if (status.equals("Pending")){
            head.setVisibility(View.GONE);
            interest.setVisibility(View.GONE);
            card.setVisibility(View.GONE);
            radioGroup.setVisibility(View.GONE);
            interest1.setVisibility(View.VISIBLE);
        }

        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(user_type.equals("null")){
                    Toast.makeText(RegisterAsEmployee.this,"Please Select Type",Toast.LENGTH_SHORT).show();
                }
                if (!(user_type.equals("null"))) {
                    registeremployee();
                }
            }
        });

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterAsEmployee.super.onBackPressed();
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(RegisterAsEmployee.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(RegisterAsEmployee.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(RegisterAsEmployee.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(RegisterAsEmployee.this, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
    }

    public void registeremployee(){
        showProgressDialogWithTitle("Submitting your application... Please Wait..");
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"customer_employee/con_cus/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            if(jsonObject1.optString("message").equals("Success")){
                                head.setVisibility(View.GONE);
                                interest.setVisibility(View.GONE);
                                card.setVisibility(View.GONE);
                                radioGroup.setVisibility(View.GONE);
                                interest1.setVisibility(View.VISIBLE);
                                hideProgressDialogWithTitle();
                            }
                            if(jsonObject1.optString("message").equals("Failed")){
                                head.setVisibility(View.VISIBLE);
                                radioGroup.setVisibility(View.VISIBLE);
                                interest.setVisibility(View.VISIBLE);
                                card.setVisibility(View.VISIBLE);
                                hideProgressDialogWithTitle();
                                Toast.makeText(RegisterAsEmployee.this,"Some error occured",Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterAsEmployee.this,"Some error occured",Toast.LENGTH_SHORT).show();
                        hideProgressDialogWithTitle();
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("convert",user_type);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }
}
