package creo.com.haahooapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahooapp.Adapters.AddShopEarningsAdapter;
import creo.com.haahooapp.Adapters.EarningAdapter;
import creo.com.haahooapp.Adapters.TransactionHistoryAdapter;
import creo.com.haahooapp.Modal.AddShopData;
import creo.com.haahooapp.Modal.EarningAddShop;
import creo.com.haahooapp.Modal.EarningPojo;
import creo.com.haahooapp.Modal.TransactionPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


public class ReferralEarning extends Fragment {

    ArrayList<String>name = new ArrayList<>();
    ArrayList<String>earnings = new ArrayList<>();
    ArrayList<String>date = new ArrayList<>();
    RecyclerView recyclerView;
    TextView names , price;
    String token = "null";
    ArrayList<AddShopData> newArrayList;
    public ReferralEarning() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_referral_earning,container,false);
//        TextView description = view.findViewById(R.id.description);
//        description.setText(ApiClient.product_description);

        recyclerView = view.findViewById(R.id.listview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        getEarnings();

//         names = view.findViewById(R.id.name);
//         price = view.findViewById(R.id.price);


        // Inflate the layout for this fragment
        return view;
    }



    public void getEarnings(){
        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        Call<EarningAddShop> call = RetrofitClient
                .getInstance()
                .getApi()
                .getReferalEarnings("Token " + token);
        call.enqueue(new Callback<EarningAddShop>() {
            @Override
            public void onResponse(Call<EarningAddShop> call, Response<EarningAddShop> response) {
                if(response.isSuccessful()) {
                    EarningAddShop earningAddShop = response.body();
                    if(response.body()!=null){
                        String success = response.body().getMessage();
                        if (success != null && success.equals("Success")) {
                            List<AddShopData> addShopData = earningAddShop.getAddShopData();
                            newArrayList =(ArrayList<AddShopData>) addShopData;
                            EarningAdapter earningAdapter = new EarningAdapter((ArrayList<AddShopData>) addShopData,getContext());
                            recyclerView.setAdapter(earningAdapter);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<EarningAddShop> call, Throwable t) {

            }
        });
    }

   /* public void getEarnings(){

        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/login_ref_earnings/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String total = jsonObject.optString("total");
                            if(total.equals("0")){
                                //total_earnings.setText("0");
                               // Toast.makeText(getActivity(),"No earnings yet",Toast.LENGTH_SHORT).show();
                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                for(int i=0 ; i<jsonArray.length();i++) {
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                    name.add(jsonObject1.optString("name"));
                                    date.add(jsonObject1.optString("date"));
                                    earnings.add(jsonObject1.optString("earnings"));
                                }
                                final List<EarningPojo> earningPojos = new ArrayList<>();
                                for(int k =0;k<earnings.size();k++){
                                    EarningPojo downloadPojo = new EarningPojo(name.get(k),date.get(k),earnings.get(k));
                                    earningPojos.add(downloadPojo);
                                }

                                EarningAdapter earningAdapter = new EarningAdapter(earningPojos, getContext());
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                recyclerView.setAdapter(earningAdapter);

                            }

                            if(!(total.equals("0"))){
                                //total_earnings.setText(total);
                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                for(int i=0 ; i<jsonArray.length();i++) {
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                    name.add(jsonObject1.optString("name"));
                                    date.add(jsonObject1.optString("date"));
                                    earnings.add(jsonObject1.optString("earnings"));
//                                    Log.d("dfgh","fgh"+earnings.get(0)+earnings.get(1));

                                }

                                final List<EarningPojo> earningPojos = new ArrayList<>();
                                for(int k =0;k<earnings.size();k++){
                                    EarningPojo downloadPojo = new EarningPojo(name.get(k),date.get(k),earnings.get(k));
                                    earningPojos.add(downloadPojo);
                                }

                                EarningAdapter earningAdapter = new EarningAdapter(earningPojos, getContext());
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                recyclerView.setAdapter(earningAdapter);

                            }


                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(Earnings.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>params = new HashMap<String,String>();
                params.put("Authorization", "Token "+token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }*/

}