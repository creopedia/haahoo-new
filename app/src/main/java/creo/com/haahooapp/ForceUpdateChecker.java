package creo.com.haahooapp;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import androidx.annotation.NonNull;

public class ForceUpdateChecker {

    public String versionCode = BuildConfig.VERSION_NAME;
    private static final String TAG = ForceUpdateChecker.class.getSimpleName();
    public static  String KEY_UPDATE_URL = "update_url";
    public static final String KEY_CURRENT_VERSION = "version_code";
    private OnUpdateNeededListener onUpdateNeededListener;
    private Context context;

    public interface OnUpdateNeededListener {

        void onUpdateNeeded(String updateUrl);
    }

    public static Builder with(@NonNull Context context) {
        return new Builder(context);
    }

    public ForceUpdateChecker(@NonNull Context context,
                              OnUpdateNeededListener onUpdateNeededListener) {
        this.context = context;
        this.onUpdateNeededListener = onUpdateNeededListener;
    }

    public void check() {
        final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();
        String updateUrl = remoteConfig.getString(KEY_UPDATE_URL);
//
//        String playStoreVersionCode = remoteConfig.getString(
//                KEY_UPDATE_URL);
//
//       if(!(versionCode).equals("1.0.2")){
//
          onUpdateNeededListener.onUpdateNeeded(updateUrl);
//       }
            String currentVersion = remoteConfig.getString(KEY_CURRENT_VERSION);
            String appVersion = getAppVersion(context);

        Log.d("vhhgghv","code"+currentVersion+appVersion);
            if (!TextUtils.equals(currentVersion, appVersion)
                    && onUpdateNeededListener != null) {

        }
    }

    private String getAppVersion(Context context) {
        String result = "";

        try {
            result = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0)
                    .versionName;
            result = result.replaceAll("[a-zA-Z]|-", "");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }

        return result;
    }

    public static class Builder {

        private Context context;
        private OnUpdateNeededListener onUpdateNeededListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder onUpdateNeeded(OnUpdateNeededListener onUpdateNeededListener) {
            this.onUpdateNeededListener = onUpdateNeededListener;
            return this;
        }

        public ForceUpdateChecker build() {
            return new ForceUpdateChecker(context, onUpdateNeededListener);
        }

        public ForceUpdateChecker check() {
            ForceUpdateChecker forceUpdateChecker = build();
            forceUpdateChecker.check();

            return forceUpdateChecker;
        }
    }

}