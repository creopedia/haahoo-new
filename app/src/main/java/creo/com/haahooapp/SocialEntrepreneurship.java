package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import creo.com.haahooapp.config.ApiClient;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class SocialEntrepreneurship extends AppCompatActivity {
    Activity activity = this;
    ImageView back;
    ImageView whatspp,logo;
    TextView pay;
    ImageView active,inactive,premium;
    ProgressDialog progressDialog;
    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_entrepreneurship);
        logo = findViewById(R.id.logo);
        Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/social_logo.png").into(logo);

        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        active = findViewById(R.id.active);
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        inactive = findViewById(R.id.inactive);
        premium = findViewById(R.id.premiumc);
        getStatus();
        back = findViewById(R.id.back);
        pay = findViewById(R.id.pay);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(SocialEntrepreneurship.this,SocialMakePayment.class));
            }
        });
        whatspp = findViewById(R.id.whatsapp);
        whatspp.setVisibility(View.GONE);

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(SocialEntrepreneurship.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(SocialEntrepreneurship.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(SocialEntrepreneurship.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context,MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context,AllStories.class));
                                break;

                        }
                        return true;
                    }
                });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SocialEntrepreneurship.super.onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }


    void getStatus(){
        showProgressDialogWithTitle("Loading, Please Wait...");
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"events/club_status_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.optString("status").equals("Inactive")){
                                inactive.setVisibility(View.VISIBLE);
                                Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/inactive.png").into(inactive);
                                hideProgressDialogWithTitle();
                            }
                            if(jsonObject.optString("status").equals("Classic")){
                                active.setVisibility(View.VISIBLE);
                                Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/classic.png").into(active);
                                hideProgressDialogWithTitle();
                            }
                            if (jsonObject.optString("status").equals("Premium")){
                                premium.setVisibility(View.VISIBLE);
                                Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/premium.png").into(premium);
                                hideProgressDialogWithTitle();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("event_id","1");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }
}
