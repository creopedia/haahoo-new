package creo.com.haahooapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.json.JSONObject;
import java.util.ArrayList;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import creo.com.haahooapp.Adapters.FeatureAdapter;
import creo.com.haahooapp.Modal.ProductFeaturepojo;
import creo.com.haahooapp.config.ApiClient;

public class Description extends Fragment {
ArrayList<ProductFeaturepojo> productFeaturepojos = new ArrayList<>();
ArrayList<String>features = new ArrayList<>();

    public Description() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

     View view = inflater.inflate(R.layout.fragment_description,container,false);

        RecyclerView listView = view.findViewById(R.id.listview);
        for (int i = 0; i< ApiClient.product_features.size();i++){
            try {
                JSONObject jsonObject = new JSONObject(ApiClient.product_features.get(i));
                features.add(jsonObject.optString(String.valueOf(i+1)));
                //Log.d("feaddttt","feature"+features.get(0));
                ProductFeaturepojo productFeaturepojo = new ProductFeaturepojo(features.get(i));
                productFeaturepojos.add(productFeaturepojo);
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        FeatureAdapter featureAdapter = new FeatureAdapter(productFeaturepojos,getActivity());
        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(getActivity()));
        listView.setAdapter(featureAdapter);
        // Inflate the layout for this fragment
        return view;
    }

}