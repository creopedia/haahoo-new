package creo.com.haahooapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.cardview.widget.CardView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import creo.com.haahooapp.Implementation.RegisterPresenterImpl;
import creo.com.haahooapp.Modal.RegisterPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.RegisterCallBack;
import creo.com.haahooapp.interfaces.RegisterPresenter;
import creo.com.haahooapp.interfaces.RegisterView;

public class Register extends AppCompatActivity implements RegisterCallBack, RegisterView, AdapterView.OnItemSelectedListener {

    EditText docID, dob;
    TextInputEditText referal, referal_name;
    TextInputEditText name, email, mobile;
    TextInputEditText password;
    TextInputEditText confirmpass;
    DatePickerDialog datePickerDialog;
    TextView register;
    int year, month, day;
    String kyc_type = "Choose document";
    Activity activity = this;
    RegisterPresenter registerPresenter;
    TextView gotoLogin;
    String calender_date = null;
    Context context = this;
    Calendar calendar;
    String token = "null";
    String device_id = null;
    ProgressDialog progressDialog;
    Pattern p = Pattern.compile("[0-9 ]");
    ImageView logo;
    Pattern alphabets = Pattern.compile("[A-Za-z ]");
    Pattern special = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
    String specialCharacters = " !#$%&'()*+,-./:;<=>?@[]^_`{|}~";
    String[] options = {"Choose document", "PAN Card", "Aadhar Card"};
    Spinner spinner;
    CardView cardView;
    private FirebaseAnalytics analytics;
    String refresh = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        logo = findViewById(R.id.logo);
        Glide.with(context).load(ApiClient.BASE_URL + "media/files/events_add/extra_pic/haahoo_logo1.png").into(logo);
        Window window = activity.getWindow();
        analytics = FirebaseAnalytics.getInstance(Register.this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        refresh = refreshedToken;
        Log.d("refreshloginwithpin", "kjbhjb" + refresh);
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        gotoLogin = findViewById(R.id.gotoLogin);
        cardView = findViewById(R.id.already);
        referal_name = findViewById(R.id.referal_name);
        referal_name.setFocusable(false);
        confirmpass = findViewById(R.id.confirmpass);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register.this, MainUI.class));
            }
        });
        docID = findViewById(R.id.docID);
        dob = findViewById(R.id.dob);
        referal = findViewById(R.id.referal);
        //Log.d("referalfff", "id" + ApiClient.referal_id + ApiClient.study_referal_id);
        if (ApiClient.referal_id.equals("0") && ApiClient.study_referal_id.equals("0")) {
            referal.setText("172");
            referal.setFocusableInTouchMode(true);
        }
        if (!(ApiClient.referal_id.equals("0"))) {
            referal.setFocusableInTouchMode(true);
            referal.setText(ApiClient.referal_id);
        }
        if (!(ApiClient.study_referal_id.equals("0"))) {
            referal.setFocusable(false);
            referal.setText(ApiClient.study_referal_id);
        }

        referal.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                getNewUserName();
            }
        });

        device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//        Log.d("derviceid","device"+device_id);
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, options);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        register = findViewById(R.id.register);

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Register.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dob.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                calender_date = dob.getText().toString();

                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });

        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        mobile = findViewById(R.id.mobile);
        mobile.setText(pref.getString("phone_no", null));
        getNewUserName();
        gotoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register.this, MainUI.class));
            }
        });
        mobile.setEnabled(false);

        registerPresenter = new RegisterPresenterImpl(this);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().length() < 3) {
                    name.setError("Name must include atleast three alphabets");
                }
                if (email.getText().toString().length() == 0) {
                    email.setError("Email cannot be empty");
                }
                if (referal.getText().toString().length() == 0) {
                    Toast.makeText(context, "Referal Id cannot be empty", Toast.LENGTH_SHORT).show();
                }
                if (referal_name.getText().toString().length() == 0) {
                    getNewUserName();
                }
                if (referal_name.getText().toString().length() > 0) {
                    if (confirmpass.getText().toString().length() == 0) {
                        confirmpass.setError("Please confirm the password");
                    }
                    if ((email.getText().toString().length() > 0) && (name.getText().toString().length() > 3) && (referal.getText().toString().length() > 0)) {
                        if (password.getText().toString().length() >= 4) {
                            if (password.getText().toString().equals(confirmpass.getText().toString())) {
                                if (emailValidator(email.getText().toString())) {
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString("membership", "0");
                                    editor.commit();
                                    getUsername();
                                }
                                if (!(emailValidator(email.getText().toString()))) {
                                    email.setError("Invalid format");
                                }
                            } else {
                                confirmpass.setError("Both passwords should be same");
                            }
                        }
                        if (password.getText().toString().length() < 4) {
                            password.setError("Password should be minimum 4 characters ");
                        }
                    }
                }

            }
        });

    }

    public void registerSkillQ(final String email, final String username, final String password, final String phone_no, String tok) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.haahoo.in/api_login/api_haahoo_mobile_signup/ ",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(Register.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("username", username);
                params.put("password", password);
                params.put("phone_no", phone_no);
                params.put("token", tok);
                params.put("device_id", device_id);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{6,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public void getNewUserName() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/id_name/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if (message.equals("Success")) {
                                referal_name.setText(jsonObject.optString("data"));
                            }
                            if (!(message.equals("Success"))) {
                                // referal_name.setText("haahoo");
                                Toast.makeText(context, "Invalid Referal Id", Toast.LENGTH_SHORT).show();
                                referal.setText("");
                                referal_name.setText("");
                                referal.setFocusableInTouchMode(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(Register.this, "//error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", referal.getText().toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public void getUsername() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/id_name/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if (message.equals("Success")) {
                                referal_name.setText(jsonObject.optString("data"));
                                final RegisterPojo registerPojo = new RegisterPojo(name.getText().toString(), email.getText().toString(), mobile.getText().toString(), password.getText().toString(), device_id, referal.getText().toString(), kyc_type, docID.getText().toString(), calender_date, refresh);
                                registerPresenter.register(registerPojo);
                                showProgressDialogWithTitle("Submitting");
                            }
                            if (!(message.equals("Success"))) {
                                Toast.makeText(context, "Invalid Referal Id", Toast.LENGTH_SHORT).show();
                                referal.setText("");
                                referal_name.setText("");
                                referal.setFocusableInTouchMode(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(Register.this, "//error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", referal.getText().toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(Register.this, MainUI.class));
    }

    @Override
    public void onVerifySuccess(String data) {

    }

    @Override
    public void onVerifyFailed(String msg) {

    }

    @Override
    public void onSuccess(String response) {
        if (!(response.equals("null"))) {
            Toast.makeText(Register.this, "Successfully Registered", Toast.LENGTH_LONG).show();
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("token", response);
            editor.commit();
            token = response;
            registerSkillQ(email.getText().toString(), name.getText().toString(), password.getText().toString(), mobile.getText().toString(), token);
            hideProgressDialogWithTitle();
            startActivity(new Intent(Register.this, SetPin.class));
        }
        if (response.equals("null")) {
            hideProgressDialogWithTitle();
            Toast.makeText(Register.this, "Already Exists..", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailed(String response) {
        //Toast.makeText(Register.this,"Server error..",Toast.LENGTH_LONG).show();
        hideProgressDialogWithTitle();
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (options[position].equals("PAN Card")) {
            setEditTextMaxLength(docID, 10);
            kyc_type = "PAN Card";
        }
        if (options[position].equals("Aadhar Card")) {
            setEditTextMaxLength(docID, 12);
            kyc_type = "Aadhar Card";
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public static void setEditTextMaxLength(EditText dd, int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        dd.setFilters(FilterArray);
    }


}
