package creo.com.haahooapp;

import android.app.Activity;
import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ai.api.AIServiceException;
import ai.api.RequestExtras;
import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.android.GsonFactory;
import ai.api.model.AIContext;
import ai.api.model.AIError;
import ai.api.model.AIEvent;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import ai.api.model.Status;
import creo.com.haahooapp.Adapters.ChatAdapter;
import creo.com.haahooapp.Modal.ChatPojo;
import creo.com.haahooapp.Modal.User;
import creo.com.haahooapp.config.LanguageConfig;

public class ChatBot extends AppCompatActivity {
  public static final String TAG = ChatBot.class.getName();
  private Gson gson = GsonFactory.getGson();
  private AIDataService aiDataService;
  User[] users =null;
  Context context = this;
  EditText editText ;
  ImageView imageView,back;
  Activity activity = this;
  private List<ChatPojo> pojo = new ArrayList<>();
  private List<ChatPojo> chatPojos = new ArrayList<>();
  RecyclerView recyclerView;
  ImageView textView;
  boolean ismine = true;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
    getSupportActionBar().hide();
    super.onCreate(savedInstanceState);
    final LanguageConfig config = new LanguageConfig("en", "edb7a0423fe648e8a34e453f384deeb7");
    initService(config);
    setContentView(R.layout.activity_chat_bot);
    Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
    window.setStatusBarColor(activity.getResources().getColor(R.color.black));
    recyclerView = findViewById(R.id.recyclerView);
    editText = findViewById(R.id.msg);
    imageView = findViewById(R.id.send);
    back = findViewById(R.id.back);
    back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ChatBot.super.onBackPressed();
      }
    });
    final ChatAdapter chatAdapter = new ChatAdapter(pojo , this);
    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    chatAdapter.notifyDataSetChanged();
    recyclerView.setAdapter(chatAdapter);
    imageView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(editText.getText().toString().length()!=0) {
          ChatPojo chatPojo = new ChatPojo(editText.getText().toString(), true);
          pojo.add(chatPojo);
          chatAdapter.notifyDataSetChanged();
          new AiTask().execute(editText.getText().toString(), null, null);
          editText.setText("");
          recyclerView.scrollToPosition(pojo.size() - 1);
        }
      }
    });



  }

  public class AiTask extends AsyncTask<String, Void, AIResponse> {
    private AIError aiError;

    @Override
    protected AIResponse doInBackground(final String... params) {
      final AIRequest request = new AIRequest();
      String query = params[0];
      String event = params[1];
      String context = params[2];

      if (!TextUtils.isEmpty(query)){
        request.setQuery(query);
      }

      if (!TextUtils.isEmpty(event)){
        request.setEvent(new AIEvent(event));
      }

      RequestExtras requestExtras = null;
      if (!TextUtils.isEmpty(context)) {
        final List<AIContext> contexts = Collections.singletonList(new AIContext(context));
        requestExtras = new RequestExtras(contexts, null);
      }

      try {
        return aiDataService.request(request, requestExtras);
      } catch (final AIServiceException e) {
        aiError = new AIError(e);
        return null;
      }
    }

    @Override
    protected void onPostExecute(final AIResponse response) {
      if (response != null) {
        onResult(response);
      } else {
        onError(aiError);
      }
    }
  }
  private void onError(final AIError error) {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        Log.e(TAG,error.toString());
      }
    });
  }


  private void onResult(final AIResponse response) {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        // Variables
        gson.toJson(response);
        final Status status = response.getStatus();
        final Result result = response.getResult();
        final String speech = result.getFulfillment().getSpeech();
        final Metadata metadata = result.getMetadata();
        final HashMap<String, JsonElement> params = result.getParameters();

        // Logging
        Log.d(TAG, "onResult");
        Log.i(TAG, "Received success response");
        Log.i(TAG, "Status code: " + status.getCode());
        Log.i(TAG, "Status type: " + status.getErrorType());
        Log.i(TAG, "Resolved query: " + result.getResolvedQuery());
        Log.i(TAG, "Action: " + result.getAction());
        ChatPojo chatPojo = new ChatPojo(speech,false);

        pojo.add(chatPojo);
        ChatAdapter chatAdapter = new ChatAdapter(pojo , context);
        chatAdapter.notifyDataSetChanged();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(chatAdapter);
        recyclerView.scrollToPosition(pojo.size()-1);

        Log.i(TAG, "Speech: " + pojo.toArray());


        if (metadata != null) {
          Log.i(TAG, "Intent id: " + metadata.getIntentId());
          Log.i(TAG, "Intent name: " + metadata.getIntentName());
        }

        if (params != null && !params.isEmpty()) {
          Log.i(TAG, "Parameters: ");
          for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
            Log.i(TAG, String.format("%s: %s",
                    entry.getKey(), entry.getValue().toString()));
          }
        }

        //Update view to bot says

      }
    });
  }

  private void initService(final LanguageConfig languageConfig) {
    final AIConfiguration.SupportedLanguages lang =
            AIConfiguration.SupportedLanguages.fromLanguageTag(languageConfig.getLanguageCode());
    final AIConfiguration config = new AIConfiguration(languageConfig.getAccessToken(),
            lang,
            AIConfiguration.RecognitionEngine.System);
    aiDataService = new AIDataService(this, config);
  }





}
