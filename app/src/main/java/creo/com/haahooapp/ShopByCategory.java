package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.Adapters.ConnectedShopsAdapter;
import creo.com.haahooapp.Adapters.NewShopByCategory;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.Modal.ConnectedShops;
import creo.com.haahooapp.config.ApiClient;

public class ShopByCategory extends AppCompatActivity {

    ImageView back, cart;
    ArrayList<ConnectedShops> connectedShops = new ArrayList<>();
    Activity activity = this;
    Context context = this;
    RecyclerView gridView;
    String token = null;
    ProgressDialog progressDialog;
    ArrayList<Category> categories = new ArrayList<>();
    ConnectedShopsAdapter connectedShopsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_by_category);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        gridView = findViewById(R.id.gridview);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Loading Connected Shops..");
        progressDialog.show();
        getConnectedShops();
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShopByCategory.super.onBackPressed();
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.shop:

//                                startActivity(new Intent(context, MyShopNew.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(context, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
        cart = findViewById(R.id.cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShopByCategory.this, Cart.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void getConnectedShops() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "virtual_shop/connected_shop_det/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("connectionstatus", "hhhh" + jsonObject);

                            JSONArray data = jsonObject.optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject1 = data.optJSONObject(i);
                                ConnectedShops connectedShops1 = new ConnectedShops();
                                connectedShops1.setId(jsonObject1.optString("shop_id"));
                                connectedShops1.setName(jsonObject1.optString("shop_name"));
                                connectedShops1.setRating(jsonObject1.optString("rating_count"));
                                connectedShops1.setImage(ApiClient.BASE_URL + jsonObject1.optString("shop_img").replace("[", "").replace("]", ""));
                                connectedShops.add(connectedShops1);
                            }

                            connectedShopsAdapter = new ConnectedShopsAdapter(context, connectedShops);
//                            gridView.setAdapter(connectedShopsAdapter);
//                            gridView.setNestedScrollingEnabled(false);
//                            gridView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                            gridView.setLayoutManager(new GridLayoutManager(context, 2));
                            gridView.setAdapter(new ConnectedShopsAdapter(context, connectedShops));
                            gridView.setNestedScrollingEnabled(false);
                            progressDialog.dismiss();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public void getcategories() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL + "api_shop_app/list_shop_cat/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                Category category = new Category();
                                category.setId(jsonObject.optString("id"));
                                category.setName(jsonObject.optString("name"));
                                String images1 = jsonObject.optString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "").replace("]", "");
                                category.setImage(ApiClient.BASE_URL + split);
                                categories.add(category);
                            }
                            gridView.setLayoutManager(new GridLayoutManager(context, 2));
                            gridView.setAdapter(new NewShopByCategory(context, categories));
                            gridView.setNestedScrollingEnabled(false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}
