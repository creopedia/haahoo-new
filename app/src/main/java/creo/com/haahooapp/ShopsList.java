package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.ShopListAdapter;
import creo.com.haahooapp.Modal.ShopPojo;
import creo.com.haahooapp.config.ApiClient;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShopsList extends AppCompatActivity {

    RecyclerView recyclerView;
    Activity activity = this;
    FloatingActionButton fab;
    ImageView back;
    private ShopListAdapter orderAdapter;
    ArrayList<ShopPojo> dataModelArrayList;
    String from = "null";
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops_list);
        recyclerView = findViewById(R.id.recyclerView);
        back = findViewById(R.id.back);
        Bundle bundle = getIntent().getExtras();
         from = bundle.getString("fromactivity");
        fab = findViewById(R.id.fabd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(ShopsList.this,RegisterShop.class));
                Intent intent = new Intent(ShopsList.this,RegisterShop.class);
                intent.putExtra("fromactivity","list");
                startActivity(intent);
            }
        });
        fetchingJSON();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             // startActivity(new Intent(ShopsList.this,NewHome.class));
                if (from.equals("register")){
                    startActivity(new Intent(ShopsList.this,NewHome.class));
                }
                if (from.equals("jobs")){
                    ShopsList.super.onBackPressed();
                }
            }
        });
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(ShopsList.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(ShopsList.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(ShopsList.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(ShopsList.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });

    }

    private void fetchingJSON() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(ShopsList.this);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"shop/shop_list/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("ssssd", "resp" + obj);
//                            amount.setText(obj.optString("total"));
                            dataModelArrayList = new ArrayList<>();
                            JSONArray dataArray  = obj.getJSONArray("data");

                            if(dataArray.length() == 0){
                                Toast.makeText(ShopsList.this,"Nothing to display",Toast.LENGTH_SHORT).show();
                            }

                            for (int i = 0; i < dataArray.length(); i++) {

                                ShopPojo playerModel = new ShopPojo();
                                JSONObject dataobj = dataArray.getJSONObject(i);
//                                String images1 = dataobj.getString("image");
//                                String[] seperated = images1.split(",");
//                                String split = seperated[0].replace("[", "");
                                  playerModel.setName(dataobj.optString("name"));
                               // ApiClient.productids.add(dataobj.optString("id"));
                                  playerModel.setPhone(dataobj.optString("phone"));
                                  playerModel.setGst(dataobj.optString("gst_no"));
//                                playerModel.(dataobj.optString("earnings"));
//                                playerModel.setStatus(dataobj.optString("status"));
//                                playerModel.setImg(ApiClient.BASE_URL+"media/" + split);

                                dataModelArrayList.add(playerModel);

                            }

                            setupRecycler();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ShopsList.this,"//Error",Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    private void setupRecycler(){
        orderAdapter = new ShopListAdapter(dataModelArrayList,this);
        recyclerView.setAdapter(orderAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onBackPressed() {
        if (from.equals("register")){
            startActivity(new Intent(ShopsList.this,NewHome.class));
        }
        if (from.equals("jobs")){
            ShopsList.super.onBackPressed();
        }
    }

}
