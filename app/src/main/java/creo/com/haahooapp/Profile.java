package creo.com.haahooapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONObject;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Implementation.ProfilePresenterImpl;
import creo.com.haahooapp.interfaces.ProfileCallback;
import creo.com.haahooapp.interfaces.ProfilePresenter;
import creo.com.haahooapp.interfaces.ProfileView;

public class Profile extends AppCompatActivity implements ProfileCallback,ProfileView {

    String token = null;
    JSONArray jsonArray = new JSONArray();
    ProfilePresenter profilePresenter;
    TextView emailtext,mobiletext,nametext,login;
    RelativeLayout relativeLayout;
    Activity activity = this;
    ImageView back;
    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile.super.onBackPressed();
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Profile.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Profile.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Profile.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(Profile.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
        relativeLayout = findViewById(R.id.logout);
        emailtext = findViewById(R.id.email);
        mobiletext = findViewById(R.id.mobile);
        nametext = findViewById(R.id.name);
        login = findViewById(R.id.login);
        profilePresenter = new ProfilePresenterImpl(this);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        Log.d("Token","nn"+token);
        if(token.equals("out")){
            relativeLayout.setVisibility(View.VISIBLE);
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Profile.this,MainUI.class));
                }
            });
        }
        if(!(token).equals("out")) {
            profilePresenter.getProfile(token);
        }

    }

    @Override
    public void profileSuccess(JSONArray jsonArray) {

    }

    @Override
    public void profileFailure(String msg) {

    }

    @Override
    public void onSuccess(JSONArray jsonArray1) {
        jsonArray = jsonArray1;
        try {

            JSONObject jsonObject = jsonArray.getJSONObject(0);
            JSONObject jsonObject1 = jsonObject.getJSONObject("fields");
            String phone_no = jsonObject1.getString("phone_no");
            String email = jsonObject1.getString("email");
            String name = jsonObject1.getString("name");
            emailtext.setText(email);
            mobiletext.setText(phone_no);
            nametext.setText(name);

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void onFailed(String response) {

    }
}
