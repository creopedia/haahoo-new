package creo.com.haahooapp.interfaces;

public interface NumberVerifyPresenter {

  void verifyNumber(String number);

}
