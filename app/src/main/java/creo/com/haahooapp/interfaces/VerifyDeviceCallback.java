package creo.com.haahooapp.interfaces;

public interface VerifyDeviceCallback {

    void onVerifySuccess(String data);
    void onVerifyFailed(String msg);

}
