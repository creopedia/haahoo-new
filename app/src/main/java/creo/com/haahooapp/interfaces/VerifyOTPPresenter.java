package creo.com.haahooapp.interfaces;

public interface VerifyOTPPresenter {
    void verifyOTP(String otp , String number);
}
