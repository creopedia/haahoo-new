package creo.com.haahooapp.interfaces;

public interface ProfilePresenter {

  void getProfile(String token);

}
