package creo.com.haahooapp.interfaces;

public interface ProductDetailPresenter {

  void getProduct(String token ,String id);

}
