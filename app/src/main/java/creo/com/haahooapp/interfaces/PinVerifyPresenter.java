package creo.com.haahooapp.interfaces;

public interface PinVerifyPresenter {

  void verifyPin(String device_id , String pin,String fire_token);
  void getProducts(String url);

}
