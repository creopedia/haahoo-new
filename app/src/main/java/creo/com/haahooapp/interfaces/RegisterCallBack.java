package creo.com.haahooapp.interfaces;

public interface RegisterCallBack {

  void onVerifySuccess(String data);
  void onVerifyFailed(String msg);

}
