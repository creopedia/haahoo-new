package creo.com.haahooapp.interfaces;

public interface SetPinCallBack {

  void onVerifySuccess(String data);
  void onVerifyFailed(String msg);

}
