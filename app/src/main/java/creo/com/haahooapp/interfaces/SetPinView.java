package creo.com.haahooapp.interfaces;

public interface SetPinView extends BaseView {
  void onSuccess(String response);
  void onFailed(String response);

}
