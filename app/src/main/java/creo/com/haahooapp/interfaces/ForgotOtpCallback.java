package creo.com.haahooapp.interfaces;

public interface ForgotOtpCallback {

    void onVerifySuccessOtp(String data);
    void onVerifyFailedOtp(String msg);
}
