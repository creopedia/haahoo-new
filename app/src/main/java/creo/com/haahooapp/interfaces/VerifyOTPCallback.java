package creo.com.haahooapp.interfaces;

public interface VerifyOTPCallback {

    void onVerifySuccess(String data);
    void onVerifyFailed(String msg);

}
