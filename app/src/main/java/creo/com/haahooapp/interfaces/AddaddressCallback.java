package creo.com.haahooapp.interfaces;

import org.json.JSONArray;

public interface AddaddressCallback {
    void onVerifySuccess(String data);
    void onVerifyFailed(String msg);

    void viewAddress(JSONArray data);
    void viewFailed(String data);
}