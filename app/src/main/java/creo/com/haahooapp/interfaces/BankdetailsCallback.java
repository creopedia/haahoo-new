package creo.com.haahooapp.interfaces;

public interface BankdetailsCallback {

    void onVerifySuccess(String data);
    void onVerifyFailed(String msg);
}