package creo.com.haahooapp.interfaces;

public interface PasswordView extends BaseView {
    void onSuccess(String response);
    void onFailed(String response);

}
