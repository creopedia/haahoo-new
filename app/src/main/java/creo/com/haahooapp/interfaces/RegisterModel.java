package creo.com.haahooapp.interfaces;

import creo.com.haahooapp.Modal.RegisterPojo;

public interface RegisterModel extends BaseModel {

  void submitDetails(RegisterPojo registerPojo , RegisterCallBack registerCallBack);

}
