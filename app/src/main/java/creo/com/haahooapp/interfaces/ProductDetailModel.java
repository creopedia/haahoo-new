package creo.com.haahooapp.interfaces;

public interface ProductDetailModel {

  void getProductDetail(String token , String id , ProductDetailCallback productDetailCallback);

}
