package creo.com.haahooapp.interfaces;

import org.json.JSONArray;
import org.json.JSONException;

public interface ProductDetailView {

    void success(JSONArray jsonArray) throws JSONException;
    void failure(String data);

}
