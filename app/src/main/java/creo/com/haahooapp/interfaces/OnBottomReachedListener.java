package creo.com.haahooapp.interfaces;

public interface OnBottomReachedListener {

  void onBottomReached(int position);
}
