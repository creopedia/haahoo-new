package creo.com.haahooapp.interfaces;

public interface VerifyOTPView extends BaseView {

    void onSuccess(String response);
    void onFailed(String response);

}
