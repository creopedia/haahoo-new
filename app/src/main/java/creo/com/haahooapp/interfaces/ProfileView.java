package creo.com.haahooapp.interfaces;

import org.json.JSONArray;

public interface ProfileView {
  void onSuccess(JSONArray jsonArray);
  void onFailed(String response);

}
