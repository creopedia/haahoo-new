package creo.com.haahooapp.interfaces;

public interface VerifyNumberCallBack extends BaseCallBack {

  void onVerifySuccess(String data);
  void onVerifyFailed(String msg);

}
