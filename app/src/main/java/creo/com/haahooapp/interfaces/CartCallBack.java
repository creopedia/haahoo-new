package creo.com.haahooapp.interfaces;

import org.json.JSONArray;

public interface CartCallBack {

  void successcart(String data);
  void failedcart(String msg);

  void viewsuccess(JSONArray data);
  void viewfail(String msg);
}
