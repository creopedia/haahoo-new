package creo.com.haahooapp.interfaces;

public interface BankdetailsView  extends BaseView{


    void onSuccess(String response);
    void onFailed(String response);

}