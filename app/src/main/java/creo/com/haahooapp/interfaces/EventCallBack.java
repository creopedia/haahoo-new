package creo.com.haahooapp.interfaces;

import org.json.JSONArray;

public interface EventCallBack {

  void success(JSONArray jsonArray);
  void failed(String msg);


}
