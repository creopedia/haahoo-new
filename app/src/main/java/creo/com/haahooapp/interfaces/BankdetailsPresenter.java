package creo.com.haahooapp.interfaces;

import creo.com.haahooapp.Modal.BankdetailsPojo;

public interface BankdetailsPresenter {

  void bankdetails(BankdetailsPojo bankdetailsPojo, String token);
}