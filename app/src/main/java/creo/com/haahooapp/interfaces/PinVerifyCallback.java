package creo.com.haahooapp.interfaces;

import org.json.JSONArray;
import org.json.JSONObject;

public interface PinVerifyCallback {

  void onVerifySuccess(String data,String mem);
  void onVerifyFailed(String msg);
  void jsonObjectSuccess(JSONArray jsonObject,String next);
}
