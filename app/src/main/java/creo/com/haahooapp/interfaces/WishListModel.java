package creo.com.haahooapp.interfaces;

public interface WishListModel {

  void addToWishlist(String id,String token, WishListCallBack wishListCallBack);
  void removeFromWishList(String id, String token , WishListCallBack wishListCallBack);
  void view_wishlist(String token , WishListCallBack wishListCallBack);

}
