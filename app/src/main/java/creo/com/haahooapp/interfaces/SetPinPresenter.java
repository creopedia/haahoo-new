package creo.com.haahooapp.interfaces;

public interface SetPinPresenter {

  void setPin(String device_id, String pin,String token);

}
