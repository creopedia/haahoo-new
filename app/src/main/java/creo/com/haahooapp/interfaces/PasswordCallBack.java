package creo.com.haahooapp.interfaces;

public interface PasswordCallBack {

    void onVerifySuccesschangepass(String data);
    void onVerifyFailedchangepass(String msg);

}
