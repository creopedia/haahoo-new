package creo.com.haahooapp.interfaces;

public interface VerifyDevicePresenter {

  void verifyDevice(String deviceId);

}
