package creo.com.haahooapp.interfaces;

import creo.com.haahooapp.Modal.BankdetailsPojo;

public interface BankdetailsModel extends BaseModel {

  void submitDetails(BankdetailsPojo bankdetailsPojo, BankdetailsCallback bankdetailsCallback, String token);

}