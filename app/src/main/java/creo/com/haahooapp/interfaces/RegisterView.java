package creo.com.haahooapp.interfaces;

public interface RegisterView extends BaseView{


  void onSuccess(String response);
  void onFailed(String response);

}
