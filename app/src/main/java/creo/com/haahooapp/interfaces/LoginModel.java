package creo.com.haahooapp.interfaces;

import creo.com.haahooapp.Modal.LoginPojo;

public interface LoginModel extends BaseModel {

  void login(LoginPojo loginPojo , LoginCallBack loginCallBack);

}
