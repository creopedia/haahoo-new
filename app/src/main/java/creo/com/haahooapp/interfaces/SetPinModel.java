package creo.com.haahooapp.interfaces;

public interface SetPinModel {

  void setPin(String device_id , String pin , String token, SetPinCallBack setPinCallBack);

}
