package creo.com.haahooapp.interfaces;

public interface WishListPresenter {

    void addToWishList(String id,String token);
    void removeFromWishList(String id , String token);
    void viewWishList(String token);

}
