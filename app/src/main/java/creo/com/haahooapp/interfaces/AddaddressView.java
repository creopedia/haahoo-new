package creo.com.haahooapp.interfaces;

import org.json.JSONArray;

public interface AddaddressView extends BaseView{


    void onSuccess(String response);
    void onFailed(String response);

    void addressview(JSONArray data);
    void addressfail(String data);

}