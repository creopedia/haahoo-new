package creo.com.haahooapp.interfaces;

public interface VerifyDeviceView extends BaseView {
    void onSuccess(String response);
    void onFailed(String response);
}
