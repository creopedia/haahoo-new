package creo.com.haahooapp.interfaces;

import org.json.JSONArray;
import org.json.JSONException;

public interface ProductDetailCallback {

    void success123(JSONArray jsonArray) throws JSONException;
    void failed(String data);

}
