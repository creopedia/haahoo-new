package creo.com.haahooapp.interfaces;

public interface EventViewPresenter {

  void getEvents();
  void getEventDetail(String id);

}
