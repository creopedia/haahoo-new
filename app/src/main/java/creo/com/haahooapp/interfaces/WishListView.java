package creo.com.haahooapp.interfaces;

public interface WishListView extends BaseView {

    void onSuccessWish(String response);
    void onRemove(String response);
    void onFailedWish(String response);
    void wishView(String response);
    void wishFailed(String response);

}
