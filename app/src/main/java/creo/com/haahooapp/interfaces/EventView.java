package creo.com.haahooapp.interfaces;

import org.json.JSONArray;

public interface EventView extends BaseView {

  void onSuccess(JSONArray data);
  void onFailed(String data);

}
