package creo.com.haahooapp.interfaces;

public interface PasswordPresenter {

  void change_password(String current_password, String new_password, String token);

}
