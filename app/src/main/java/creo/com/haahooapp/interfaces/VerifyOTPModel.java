package creo.com.haahooapp.interfaces;

public interface VerifyOTPModel extends BaseModel {

    void submitOTP(String otp ,String number, VerifyOTPCallback verifyOTPCallback);

}
