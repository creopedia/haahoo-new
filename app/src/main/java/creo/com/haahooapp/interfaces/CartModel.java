package creo.com.haahooapp.interfaces;

public interface CartModel {

  void addToCart(String id,String count, String token , CartCallBack cartCallBack);
  void viewCart(String token, CartCallBack cartCallBack);
}
