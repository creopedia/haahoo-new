package creo.com.haahooapp.interfaces;

public interface LoginPresenter {

    void login(String phone_no, String password, String device_id,String token);

}
