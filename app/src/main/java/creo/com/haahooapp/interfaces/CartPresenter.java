package creo.com.haahooapp.interfaces;

public interface CartPresenter {

  void addToCart(String id ,String count, String token);
  void viewCart(String token);

}
