package creo.com.haahooapp.interfaces;

import creo.com.haahooapp.Modal.PinVerifyPojo;

public interface PinVerifyModel extends BaseModel {

  void verifyPin(PinVerifyPojo pinVerifyPojo, PinVerifyCallback pinVerifyCallback);
  void getProducts(String url,PinVerifyCallback pinVerifyCallback);

}
