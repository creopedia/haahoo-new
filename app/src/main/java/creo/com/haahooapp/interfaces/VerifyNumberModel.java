package creo.com.haahooapp.interfaces;

public interface VerifyNumberModel extends BaseModel {

  void submitNumber(String number , VerifyNumberCallBack verifyNumberCallBack);

}
