package creo.com.haahooapp.interfaces;

public interface ForgotOtpPresenter {

    void forgotOtp( String number);
    void verifyOtp( String number , String otp);
    void resetPassword( String number , String password);

}
