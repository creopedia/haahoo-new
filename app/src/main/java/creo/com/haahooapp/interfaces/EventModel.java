package creo.com.haahooapp.interfaces;

public interface EventModel extends BaseModel {

  void getEvents(EventCallBack eventCallBack);
  void getEventDetails(String id , EventCallBack eventCallBack);

}
