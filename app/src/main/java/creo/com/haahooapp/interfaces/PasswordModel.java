package creo.com.haahooapp.interfaces;

public interface PasswordModel {

  void change_password(String currentpass , String newpass , String token , PasswordCallBack passwordCallBack);

}
