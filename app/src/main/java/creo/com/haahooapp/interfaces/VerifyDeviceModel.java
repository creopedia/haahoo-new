package creo.com.haahooapp.interfaces;

public interface VerifyDeviceModel {

  void submitDevice(String deviceid, VerifyDeviceCallback verifyDeviceCallback);

}
