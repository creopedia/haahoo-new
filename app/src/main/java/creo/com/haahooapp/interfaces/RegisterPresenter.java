package creo.com.haahooapp.interfaces;

import creo.com.haahooapp.Modal.RegisterPojo;

public interface RegisterPresenter {

    void register(RegisterPojo registerPojo);

}
