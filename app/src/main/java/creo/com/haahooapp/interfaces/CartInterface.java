package creo.com.haahooapp.interfaces;

public interface CartInterface {
    void sendData(String productid,String count,String isVirtual,String shop_id,String variant);
}