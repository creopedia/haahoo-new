package creo.com.haahooapp.interfaces;

public interface ProfileModel {

    void getProfile(String token,ProfileCallback profileCallback);

}
