package creo.com.haahooapp.interfaces;

import creo.com.haahooapp.Modal.AddaddressPojo;

public interface AddaddressPresenter {

  void addaddress(AddaddressPojo addaddressPojo,String token);
  void viewAddress1(String token);
}