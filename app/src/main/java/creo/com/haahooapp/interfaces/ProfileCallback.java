package creo.com.haahooapp.interfaces;

import org.json.JSONArray;

public interface ProfileCallback {

  void profileSuccess(JSONArray jsonArray);
  void profileFailure(String msg);

}
