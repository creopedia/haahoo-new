package creo.com.haahooapp.interfaces;

import org.json.JSONArray;
import org.json.JSONObject;

public interface PinVerifyView extends BaseView {

    void onSuccess(String response,String mem);
    void onFailed(String response);
    void jsonSUccess(JSONArray jsonObject,String next);

}
