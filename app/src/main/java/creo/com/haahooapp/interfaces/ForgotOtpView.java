package creo.com.haahooapp.interfaces;

public interface ForgotOtpView extends BaseView {

  void onSuccessOtp(String response);
  void onFailedOtp(String response);

}
