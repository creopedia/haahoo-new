package creo.com.haahooapp.interfaces;

import creo.com.haahooapp.Modal.ForgotOTPPojo;
import creo.com.haahooapp.Modal.VerifyOtpPojo;

public interface ForgotOtpModel extends BaseModel {

  void forgotOtp(ForgotOTPPojo forgotOTPPojo, ForgotOtpCallback forgotOtpCallback);
  void verifyOtp(VerifyOtpPojo verifyOtpPojo , ForgotOtpCallback forgotOtpCallback);
  void resetPassword(String phone_no , String password , ForgotOtpCallback forgotOtpCallback);

}
