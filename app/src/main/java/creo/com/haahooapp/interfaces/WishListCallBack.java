package creo.com.haahooapp.interfaces;

public interface WishListCallBack {
    void onVerifySuccessWish(String data);
    void onVerifySuccessWishRemove(String data);
    void onVerifyFailedWish(String msg);
    void wishlistviewsuccess(String msg);
    void wishlistviewfailed(String data);

}
