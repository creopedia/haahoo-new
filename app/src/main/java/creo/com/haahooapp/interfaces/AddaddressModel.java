package creo.com.haahooapp.interfaces;

import creo.com.haahooapp.Modal.AddaddressPojo;

public interface AddaddressModel extends BaseModel {

    void submitDetails(AddaddressPojo addaddressPojo,AddaddressCallback addaddressCallback,String token);
    void view_address(AddaddressCallback addaddressCallback,String token);

}