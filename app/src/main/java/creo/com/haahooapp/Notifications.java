package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.NotificationAdapter;
import creo.com.haahooapp.Adapters.SlidingContentAdapterProduct;
import creo.com.haahooapp.Modal.NotificationPojo;
import creo.com.haahooapp.Modal.StoryPojo;
import creo.com.haahooapp.config.ApiClient;

public class Notifications extends AppCompatActivity {
    ImageView back;
    RecyclerView recyclerView;
    NotificationAdapter notificationAdapter;
    Activity activity = this;
    Context context = this;
    String url1 =  ApiClient.BASE_URL+"virtual_shop/list_invitations/";
    ArrayList<NotificationPojo> notificationPojos = new ArrayList<>();
    TextView loadmore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        loadmore = findViewById(R.id.loadmore);
        back = findViewById(R.id.back);
        recyclerView = findViewById(R.id.recyclerview);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        loadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNotificationsList(url1);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Notifications.super.onBackPressed();
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
        getNotificationsList(url1);
    }

    public void getNotificationsList(String url){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("notifyapi","refff"+jsonObject);
                            url1 = jsonObject.optString("next");
                            JSONArray results = jsonObject.optJSONArray("results");
                            if (results.length()==0){
                                Toast.makeText(context,"No more notifications",Toast.LENGTH_SHORT).show();
                            }
                            if (results.length()>0) {
                                for (int i = 0; i < results.length(); i++) {
                                    JSONObject jsonObject1 = results.optJSONObject(i);
                                    NotificationPojo notificationPojo = new NotificationPojo();
                                    notificationPojo.setName(jsonObject1.optString("message"));
                                    notificationPojo.setDate(jsonObject1.optString("date"));
                                    notificationPojos.add(notificationPojo);
                                }
                            }
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setNestedScrollingEnabled(false);
                                notificationAdapter = new NotificationAdapter(context,notificationPojos);
                                recyclerView.setAdapter(notificationAdapter);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Downloads.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){


            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String,String>();
                params.put("Authorization", "Token "+token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
