package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import creo.com.haahooapp.config.ApiClient;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class TransactionDetails extends AppCompatActivity {

    ImageView image;
    TextView amount,payment,date,txn,account_no,ifsc,name;
    Activity activity = this;
    Context context = this;
    String id = "null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_details);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        image = findViewById(R.id.image);
        name = findViewById(R.id.name);
        Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/haahoo_logo1.png").into(image);
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        Log.d("hgfdsghjk","utrewty"+id);
        amount = findViewById(R.id.amount);
        payment = findViewById(R.id.payment);
        date = findViewById(R.id.date);
        txn = findViewById(R.id.txn);
        account_no = findViewById(R.id.account_no);
        ifsc = findViewById(R.id.ifsc);
        getTxnDetails();
    }


    public  void getTxnDetails(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"bank_details/withdraw_list_id/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            Log.d("details","dzsxfcgh"+jsonObject1);
                            JSONArray jsonArray = jsonObject1.optJSONArray("message");
                            for (int i =0 ; i <jsonArray.length() ; i++){
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                String withdrawal = jsonObject.optString("withdrawal");
                                String amount1 = jsonObject.optString("amount");

                                if (withdrawal.equals("0")){
                                    payment.setText("Debited from Wallet");
                                    amount.setText(" - ₹"+amount1);
                                }
                                if (withdrawal.equals("1")){
                                    payment.setText("Credited to wallet");
                                    amount.setText(" + ₹"+amount1);
                                }
                                String date1 = jsonObject.optString("date");
                                date.setText(date1);

                                String Bank_acc = jsonObject.optString("Bank_acc");
                                account_no.setText(Bank_acc);
                                String ifsc1 = jsonObject.optString("ifsc");
                                ifsc.setText(ifsc1);
                                String tranid = jsonObject.optString("transfer_id");
                                txn.setText(tranid);
                                String acc_name = jsonObject.optString("Acc_name");
                                name.setText(acc_name);
                            }
                           // String message = jsonObject1.optString("message");

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id", id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
