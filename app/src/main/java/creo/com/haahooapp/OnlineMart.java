package creo.com.haahooapp;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahooapp.Adapters.OnlineMartProductAdapter;
import creo.com.haahooapp.Adapters.ResellingCategoryAdapter;
import creo.com.haahooapp.Adapters.ResellingProductsAdapter;
import creo.com.haahooapp.Modal.HorizontalPojo;
import creo.com.haahooapp.Modal.ResellingPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.ItemClick;
import creo.com.haahooapp.interfaces.MyViewModel;

import static android.content.Context.MODE_PRIVATE;

public class OnlineMart extends Fragment implements ResellingProducts.OnDataPassedListener, ItemClick {

    TextView loadmore;
    private MyViewModel mViewModel;
    List<HorizontalPojo> productFeaturepojos = new ArrayList<>();
    ResellingCategoryAdapter resellingCategoryAdapter;
    //String nexturl = ApiClient.BASE_URL+"api_shop_app/no_resl_pdt/";
    String nexturl = ApiClient.BASE_URL + "api_shop_app/view_bazar/";
    RecyclerView recyclerview, subcategories;
    ArrayList<ResellingPojo> pojos = new ArrayList<>();
    OnlineMartProductAdapter resellingAdapter;
    CardView card;
    ProgressDialog progressDialog;
    String url1 = ApiClient.BASE_URL + "api_shop_app/category_based_nonresel_pdt/";

    public OnlineMart() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MyViewModel.class);

        ViewModelProviders.of(getActivity()).get(MyViewModel.class).getMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String message) {
                //textView.setText(message);
                // Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
                // loadProducts(message);

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_online_mart, container, false);
        loadmore = view.findViewById(R.id.laodmore);
        recyclerview = view.findViewById(R.id.recyclerview);
        card = view.findViewById(R.id.card);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait while the data is loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(true);
        progressDialog.show();
        subcategories = view.findViewById(R.id.subcategories);
        card.setVisibility(View.GONE);
        resellingCategoryAdapter = new ResellingCategoryAdapter(productFeaturepojos, getContext(), this);
        getcategories();
        getResellingProducts();


        return view;
    }

    public void getcategories() {
        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/list_shop_cat/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("categories", "bncgbc" + obj);
                            progressDialog.dismiss();
                            JSONArray data = obj.optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = data.optJSONObject(i);
                                HorizontalPojo horizontalPojo = new HorizontalPojo();
                                horizontalPojo.setId(jsonObject.optString("id"));
                                horizontalPojo.setName(jsonObject.optString("name"));
                                horizontalPojo.setImage(ApiClient.BASE_URL + jsonObject.optString("image").replace("[", "").replace("]", "").trim());
                                productFeaturepojos.add(horizontalPojo);
                            }

                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                            subcategories.setLayoutManager(horizontalLayoutManagaer);
                            subcategories.setAdapter(resellingCategoryAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                //

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("resell_value", "0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void getResellingProducts() {
        //Toast.makeText(getContext(),"started",Toast.LENGTH_SHORT).show();

        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/reselling_pdts/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, nexturl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("response220", "resp" + response);
                            // Toast.makeText(getContext(),"jhvcgfx",Toast.LENGTH_SHORT).show();
                            nexturl = obj.optString("next");

                            if (!(nexturl.equals("null"))) {
                                card.setVisibility(View.VISIBLE);
                            }
                            loadmore.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (!(nexturl.equals("null"))) {
                                        getResellingProducts();
                                    }
                                    if (nexturl.equals("null")) {
                                        Toast.makeText(getContext(), "No more data", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                ResellingPojo resellingPojo = new ResellingPojo();
                                resellingPojo.setId(jsonObject.optString("product_id"));
                                resellingPojo.setShop_id(jsonObject.optString("shop_id"));
                                String images = jsonObject.optString("pdt_image");
                                String[] seperated = images.split(",");
                                resellingPojo.setImage(seperated[0].replace("[", "").replace("]", ""));
                                resellingPojo.setProduct_name(jsonObject.optString("pdt_name"));
                                resellingPojo.setWholesale_price(jsonObject.optString("wholesale_prize").trim());
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setResell(jsonObject.optString("resell"));
                                resellingPojo.setResell_count(jsonObject.optString("resell_count"));
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setMin_wholesale_qty(jsonObject.optString("minimum_wholesale_quantity"));
                                resellingPojo.setDelivery_expense(jsonObject.optString("delivery_expense"));
                                pojos.add(resellingPojo);
                            }
                            resellingAdapter = new OnlineMartProductAdapter(getContext(), pojos);
                            recyclerview.setAdapter(resellingAdapter);
                            recyclerview.setNestedScrollingEnabled(false);
                            recyclerview.setLayoutManager(new GridLayoutManager(getContext(), 2));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public java.util.Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onDataPassed(String text) {
        Log.d("hgfg", "received");
    }

    @Override
    public void addProduct(String productid, String price1, String edited_price) {

    }

    @Override
    public void sendData(String id) {
        //Toast.makeText(getContext(),id,Toast.LENGTH_SHORT).show();
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait while the data is loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(true);
        progressDialog.show();
        loadProducts(id);
    }

    public void loadProducts(String categoryid) {


        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/view_reselling_products_under_category/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("response555", "resp" + response);
                            pojos.clear();
                            // url1 = obj.optString("next");
                            JSONArray jsonArray = obj.optJSONArray("data");
                            if (jsonArray.length() == 0) {
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), "No products under this category", Toast.LENGTH_SHORT).show();
                            }
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                ResellingPojo resellingPojo = new ResellingPojo();
                                resellingPojo.setId(jsonObject.optString("product_id"));
                                resellingPojo.setShop_id(jsonObject.optString("shop_id"));
                                String images = jsonObject.optString("pdt_image");
                                String[] seperated = images.split(",");
                                resellingPojo.setImage(seperated[0].replace("[", "").replace("]", ""));
                                resellingPojo.setProduct_name(jsonObject.optString("pdt_name"));
                                resellingPojo.setWholesale_price(jsonObject.optString("wholesale_prize").trim());
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setResell(jsonObject.optString("resell"));
                                resellingPojo.setResell_count(jsonObject.optString("resell_count"));
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setMin_wholesale_qty(jsonObject.optString("minimum_wholesale_quantity"));
                                resellingPojo.setDelivery_expense(jsonObject.optString("delivery_expense"));
                                pojos.add(resellingPojo);
                            }
                            resellingAdapter = new OnlineMartProductAdapter(getContext(), pojos);
                            recyclerview.setAdapter(resellingAdapter);
                            recyclerview.setNestedScrollingEnabled(false);
                            recyclerview.setLayoutManager(new GridLayoutManager(getContext(), 2));
                            progressDialog.dismiss();

                        } catch (JSONException | NullPointerException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
                progressDialog.dismiss();
            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("category_id", categoryid);
                params.put("resell_value", "0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };


        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}