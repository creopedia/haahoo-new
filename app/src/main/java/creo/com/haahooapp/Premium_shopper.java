package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import creo.com.haahooapp.camera.ResultObject;
import creo.com.haahooapp.camera.VideoInterface;
import creo.com.haahooapp.config.ApiClient;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Premium_shopper extends AppCompatActivity {
    private MediaRecorder mRecorder;
    TextView record,stop,recording;
    Activity activity = this;
    Context context = this;
    ImageView imageView;
    EditText edit;
    private File mOutputFile;
    private long mStartTime = 0;
    ImageView img_record,img_stop,send,second_record,second_stop,second_send;
    private int[] amplitudes = new int[100];
    private int i = 0;

    private Handler mHandler = new Handler();
    private Runnable mTickExecutor = new Runnable() {
        @Override
        public void run() {
            tick();
            mHandler.postDelayed(mTickExecutor,100);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium_shopper);
        img_record = findViewById(R.id.img_record);
        send = findViewById(R.id.send);
        imageView = findViewById(R.id.image);
        edit = findViewById(R.id.input);

        recording = findViewById(R.id.recording);
        Glide.with(context).load(ApiClient.BASE_URL+"media/files/product_add/premium_img.png").into(imageView);
        img_stop = findViewById(R.id.img_stop);
        second_record = findViewById(R.id.second_record);
        second_stop = findViewById(R.id.second_stop);
        second_send = findViewById(R.id.second_send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit.getText().toString().length()==0){
                   Toast.makeText(context,"Please enter your query",Toast.LENGTH_SHORT).show();
                }
                if (edit.getText().toString().length()>0){
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                    String token = (pref.getString("token", ""));

                    RequestQueue queue = Volley.newRequestQueue(context);
                    //this is the url where you want to send the request
                    String url = ApiClient.BASE_URL+"premium_shopper/add_txt/";
                    // Request a string response from the provided URL.
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                            new com.android.volley.Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // Display the response string.

                                    try {
                                        JSONObject obj = new JSONObject(response);
                                        edit.setText("");
                                        Log.d("jhgghf", "jsonarray" + obj);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }, new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("type", "text");
                            params.put("data",edit.getText().toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders()  {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Authorization", "Token " + token);
                            return params;
                        }
                    };

                    // Add the request to the RequestQueue.
                    queue.add(stringRequest);
                }
            }
        });
        second_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStoragePermissionGranted()){
                    startRecording();
                    second_record.setVisibility(View.GONE);
                    second_stop.setVisibility(View.VISIBLE);
                    recording.setVisibility(View.VISIBLE);
                }
            }
        });
        second_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording(true);
                second_stop.setVisibility(View.GONE);
                second_record.setVisibility(View.GONE);
                second_send.setVisibility(View.VISIBLE);
                recording.setVisibility(View.GONE);
            }
        });
        second_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestBody videoBody = RequestBody.create(MediaType.parse("audio/*"), mOutputFile);
                MultipartBody.Part vFile = MultipartBody.Part.createFormData("data", mOutputFile.getName(), videoBody);
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiClient.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                VideoInterface vInterface = retrofit.create(VideoInterface.class);
                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                final String token = (pref.getString("token", ""));
                RequestBody filetype = RequestBody.create(MediaType.parse("text/plain"),"audio");
                Call<ResultObject> serverCom = vInterface.addFiles("Token "+token,vFile,filetype);
                serverCom.enqueue(new Callback<ResultObject>() {
                    @Override
                    public void onResponse(Call<ResultObject> call, Response<ResultObject> response) {
                        Log.d("outputshopper11","jhbj"+response);
                        second_send.setVisibility(View.GONE);
                        second_record.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFailure(Call<ResultObject> call, Throwable t) {
                        Toast.makeText(context,"Some error occured",Toast.LENGTH_SHORT).show();
                        second_send.setVisibility(View.GONE);
                        second_record.setVisibility(View.VISIBLE);
                    }
                });

            }
        });
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        record = findViewById(R.id.record);
        stop = findViewById(R.id.stop);
        img_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStoragePermissionGranted()) {
                    startRecording();
                    img_record.setVisibility(View.GONE);
                    img_stop.setVisibility(View.VISIBLE);
                }
            }
        });
        img_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording(true);
                img_stop.setVisibility(View.GONE);
                img_record.setVisibility(View.GONE);
                send.setVisibility(View.VISIBLE);

            }
        });

//        send.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("audio_file","filesss"+mOutputFile.getAbsolutePath());
//                RequestBody videoBody = RequestBody.create(MediaType.parse("audio/*"), mOutputFile);
//                MultipartBody.Part vFile = MultipartBody.Part.createFormData("data", mOutputFile.getName(), videoBody);
//                Retrofit retrofit = new Retrofit.Builder()
//                        .baseUrl(ApiClient.BASE_URL)
//                        .addConverterFactory(GsonConverterFactory.create())
//                        .build();
//                VideoInterface vInterface = retrofit.create(VideoInterface.class);
//                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                final String token = (pref.getString("token", ""));
//                RequestBody filetype = RequestBody.create(MediaType.parse("text/plain"),"audio");
//                Call<ResultObject> serverCom = vInterface.addFiles("Token "+token,vFile,filetype);
//                serverCom.enqueue(new Callback<ResultObject>() {
//                    @Override
//                    public void onResponse(Call<ResultObject> call, Response<ResultObject> response) {
//                        Log.d("outputshopper11","jhbj"+response);
//                        send.setVisibility(View.GONE);
//                        img_record.setVisibility(View.VISIBLE);
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResultObject> call, Throwable t) {
//                        Toast.makeText(context,"Some error occured",Toast.LENGTH_SHORT).show();
//                        send.setVisibility(View.GONE);
//                        img_record.setVisibility(View.VISIBLE);
//                    }
//                });
//            }
//        });
    }

    protected  void stopRecording(boolean saveFile) {
        mRecorder.stop();
        mRecorder.reset();
        mRecorder.release();
        mRecorder = null;
        mStartTime = 0;
        mHandler.removeCallbacks(mTickExecutor);
        if (!saveFile && mOutputFile != null) {
            mOutputFile.delete();
        }
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
            mRecorder.setAudioEncodingBitRate(48000);
        } else {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setAudioEncodingBitRate(64000);
        }
        mRecorder.setAudioSamplingRate(16000);
        mOutputFile = getOutputFile();
        mOutputFile.getParentFile().mkdirs();
        Log.d("mkdirs","dirs"+mOutputFile.getParentFile().mkdirs());
        mRecorder.setOutputFile(mOutputFile.getAbsolutePath());

        try {
            mRecorder.prepare();
            mRecorder.start();
            mStartTime = SystemClock.elapsedRealtime();
            mHandler.postDelayed(mTickExecutor, 100);
            Log.d("Voice Recorder","started recording to "+mOutputFile.getAbsolutePath());
        } catch (IOException e) {
            Log.e("Voice Recorder", "prepare() failed "+e.getMessage());
        }
    }
    private void tick() {
        long time = (mStartTime < 0) ? 0 : (SystemClock.elapsedRealtime() - mStartTime);
        int minutes = (int) (time / 60000);
        int seconds = (int) (time / 1000) % 60;
        int milliseconds = (int) (time / 100) % 10;
       // mTimerTextView.setText(minutes+":"+(seconds < 10 ? "0"+seconds : seconds)+"."+milliseconds);
        if (mRecorder != null) {
            amplitudes[i] = mRecorder.getMaxAmplitude();
            //Log.d("Voice Recorder","amplitude: "+(amplitudes[i] * 100 / 32767));
            if (i >= amplitudes.length -1) {
                i = 0;
            } else {
                ++i;
            }
        }
    }

    private File getOutputFile() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US);
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString()
                + "/Voice Recorder/RECORDING_"
                + dateFormat.format(new Date())
                + ".m4a");
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)  {
                Log.v("dfdgdfg","Permission is granted");
                if ((checkSelfPermission(Manifest.permission.RECORD_AUDIO)
                        == PackageManager.PERMISSION_GRANTED)){
                    return true;
                }
                return false;

            } else {

                Log.v("dfdgdfg","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("dfdgdfg","Permission is granted");
            return true;
        }
    }
}
