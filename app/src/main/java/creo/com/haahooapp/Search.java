package creo.com.haahooapp;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.ImageAdapter;
import creo.com.haahooapp.Adapters.InfiniteScrolling;
import creo.com.haahooapp.Adapters.LeadsAdapter;
import creo.com.haahooapp.Adapters.ListViewAdapter;
import creo.com.haahooapp.Adapters.PopularAdapter;
import creo.com.haahooapp.Adapters.ResellingProductsAdapter;
import creo.com.haahooapp.Adapters.SearchAdapter;
import creo.com.haahooapp.Adapters.SearchNewAdapter;
import creo.com.haahooapp.Adapters.TrendingAdapter;
import creo.com.haahooapp.Modal.LeadsPojo;
import creo.com.haahooapp.Modal.PopularPojo;
import creo.com.haahooapp.Modal.ProductPojo;
import creo.com.haahooapp.Modal.ResellingPojo;
import creo.com.haahooapp.Modal.SearchPojo;
import creo.com.haahooapp.Modal.TrendingPojo;
import creo.com.haahooapp.config.ApiClient;

public class Search extends AppCompatActivity {

    public SearchView searchView;
    CardView cardView;
    List<SearchPojo> searchPojos = new ArrayList<>();
    Context context = this;
    RecyclerView recyclerView;
    Activity activity = this;
    ImageView back;
    RecyclerView searchlist;
    SearchNewAdapter searchNewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchlist = findViewById(R.id.searchlist);
        Window window = activity.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back = findViewById(R.id.back);
        recyclerView = findViewById(R.id.recyclerView);
        cardView = findViewById(R.id.card);
        searchView = findViewById(R.id.search);
        searchView.onActionViewExpanded();
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.clearFocus();
        //searchView.setQueryHint("Search for products...");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Search.super.onBackPressed();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() == 0) {
                    searchlist.setVisibility(View.GONE);
                }
                if (query.length() > 0) {
                    Log.d("fgxgxg","hgchxghg"+query);
                    suggestions(query);
                }
                //getProducts123(next_api);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d("fgxgxg","hgchxghg"+newText);
              if (newText.length()>0){
                  suggestions(newText);
              }

                return false;
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Search.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Search.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Search.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(Search.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
    }

    @Override
    public void onBackPressed() {
        Search.super.onBackPressed();
    }

    public void suggestions(String key) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);

        String token = (pref.getString("token", ""));

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "api_shop_app/search_shops/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            searchPojos.clear();
                            if (jsonObject1.optJSONArray("data").length() >= 1) {
                                searchlist.setVisibility(View.VISIBLE);
                                JSONArray data = jsonObject1.optJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject jsonObject = data.optJSONObject(i);
                                    SearchPojo searchPojo = new SearchPojo();
                                    searchPojo.setShop_id(jsonObject.optString("shop_id"));
                                    searchPojo.setShop_name(jsonObject.optString("shop_name"));
                                    searchPojo.setPdt_shop_id(jsonObject.optString("pdt_shop_id"));
                                    searchPojo.setProduct_id(jsonObject.optString("pdt_id"));
                                    searchPojo.setProduct_name(jsonObject.optString("pdt_name"));
                                    searchPojos.add(searchPojo);
                                }

                                searchNewAdapter = new SearchNewAdapter(searchPojos, context);
                                searchlist.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                searchlist.setAdapter(searchNewAdapter);
                                searchlist.setNestedScrollingEnabled(false);

                            }

                            if (jsonObject1.optJSONArray("data").length() == 0){
                                searchPojos.clear();
                                searchNewAdapter = new SearchNewAdapter(searchPojos, context);
                                searchlist.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                searchlist.setAdapter(searchNewAdapter);
                                searchlist.setNestedScrollingEnabled(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("search", key);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}