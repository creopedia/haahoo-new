package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahooapp.Adapters.MyShopAdapter;
import creo.com.haahooapp.Adapters.ResellingCategoryAdapter;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.Modal.HorizontalPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ItemClickSupport;
import creo.com.haahooapp.interfaces.ItemClick;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.bluetooth.BluetoothGattDescriptor.PERMISSION_WRITE;

public class MyShopNew extends AppCompatActivity implements ItemClick {

    ImageView image, edit, share, back, cart;
    TextView shopname;
    RecyclerView categories, products;
    Context context = this;
    ProgressDialog progressDialog;
    Activity activity = this;
    List<HorizontalPojo> productFeaturepojos = new ArrayList<>();
    ResellingCategoryAdapter resellingCategoryAdapter;
    ArrayList<Category> categories1 = new ArrayList<>();
    CardView card, more;
    TextView edit1;
    String next = ApiClient.BASE_URL + "virtual_shop/list_virtual_shop/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shop_new);
        image = findViewById(R.id.image);
        more = findViewById(R.id.more);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (next.equals("null")) {
                    Toast.makeText(context, "That's all for now", Toast.LENGTH_SHORT).show();
                }
                if (!(next.equals("null"))) {
                    getProducts();
                }
            }
        });
        edit = findViewById(R.id.edit);
        back = findViewById(R.id.back);
        edit1 = findViewById(R.id.edit1);
        card = findViewById(R.id.card);
        progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        resellingCategoryAdapter = new ResellingCategoryAdapter(productFeaturepojos, context, this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading products.. Please Wait..");
        progressDialog.show();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyShopNew.super.onBackPressed();
            }
        });
        // checkPermission();
        share = findViewById(R.id.share);
        shopname = findViewById(R.id.shopname);
        categories = findViewById(R.id.categories);
        products = findViewById(R.id.products);
        Window window = activity.getWindow();
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setTitle("Loading..");
                progressDialog.setMessage("Loading apps..");
                progressDialog.setIcon(R.drawable.haahoologo);
                progressDialog.show();
                shareShop();
            }
        });
        Glide.with(context).load("https://www.invensis.net/blog/wp-content/uploads/2015/11/5-Tips-for-Faster-Product-Data-Upload-in-Magento-Store-Invensis.jpg").into(image);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareShop();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                Log.d("hjgdgsgs", "uytrewq");
            } else {
                requestPermission();
            }
        }
        cart = findViewById(R.id.cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyShopNew.this, Cart.class);
                startActivity(intent);
            }
        });

        edit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                View view = layoutInflater.inflate(R.layout.edit_name, null);
                final AlertDialog dialog = new AlertDialog.Builder(context)
                        .setView(view)
                        .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                        .setNegativeButton(android.R.string.cancel, null)
                        .create();
                final EditText shopname1 = (EditText) view
                        .findViewById(R.id.editTextDialogUserInput);
                // userInput.setText(price1.trim());
                shopname1.setText(shopname.getText().toString());
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {

                    @Override
                    public void onShow(DialogInterface dialogInterface) {

                        Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);

                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (shopname1.getText().toString().length() <= 3) {
                                    shopname1.setError("Shopname must be atleast three characters");
                                }
                                if (shopname1.getText().toString().length() > 3) {
                                    dialog.dismiss();
                                    editShopname(shopname1.getText().toString());
                                }
                            }
                        });

                    }
                });
                dialog.show();
            }
        });
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        getProducts();
        getcategories();
        shareShop1();

    }

    public void getProducts() {
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL + "virtual_shop/list_virtual_shop/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, next,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {
                            Log.d("GFGFGFZZGF", "HJGDTS" + response);
                            JSONObject obj = new JSONObject(response);
                            next = obj.optString("next");
                            JSONArray jsonArray = obj.optJSONArray("results");
                            if (jsonArray.length() == 0) {
                                progressDialog.dismiss();
                                Toast.makeText(context, "No products available", Toast.LENGTH_SHORT).show();
                            }
                            if (jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    Category category = new Category();
                                    category.setId(jsonObject.optString("id"));
                                    category.setProduct_id(jsonObject.optString("product_id"));
                                    category.setName(jsonObject.optString("name"));
                                    category.setDescription(jsonObject.optString("description"));
                                    category.setOriginal_price(jsonObject.optString("orginal_price"));
                                    category.setEdited_price(jsonObject.optString("changed_price"));
                                    category.setReselling_max(jsonObject.optString("re_max_price"));
                                    String images1 = jsonObject.optString("image");
                                    String[] seperated = images1.split(",");
                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    category.setImage(ApiClient.BASE_URL + split);
                                    categories1.add(category);
                                }
                                products.setAdapter(new MyShopAdapter(context, categories1));
                                products.setNestedScrollingEnabled(false);
                                progressDialog.dismiss();
                                products.setLayoutManager(new GridLayoutManager(context, 2));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    protected void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            }
        }
    }

    public void editShopname(String name) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL + "virtual_shop/add_virtual_name/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("jhgghf", "jsonarray" + obj);
                            String message = obj.optString("message");
                            if (message.equals("Success")) {
                                //startActivity(new Intent(context, MyShopNew.class));
                                shopname.setText(name);
                            }
                            if (!(message.equals("Success"))) {
                                Toast.makeText(context, "Failed to edit", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public boolean checkPermission() {
        int READ_EXTERNAL_PERMISSION = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if ((READ_EXTERNAL_PERMISSION != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_WRITE);
            return false;
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_WRITE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //do somethings
            getcategories();
        }
        if (requestCode == 100) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //do your work
                Log.d("hjgfghvj", "Permission Denied, You cannot use local drive .");
            } else {
                Log.e("value", "Permission Denied, You cannot use local drive .");
            }


        }
    }

    public void shareShop1() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL + "virtual_shop/virtual_shop_share/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("jhgghf", "jsonarray" + obj);
                            String data = obj.optString("data");
                            String shareBody = data;
                            String shop_name = obj.optString("shop_name");
                            shopname.setText(shop_name);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getcategories() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/list_shop_cat/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("categories", "bncgbc" + obj);

                            JSONArray data = obj.optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = data.optJSONObject(i);
                                HorizontalPojo horizontalPojo = new HorizontalPojo();
                                horizontalPojo.setId(jsonObject.optString("id"));
                                horizontalPojo.setName(jsonObject.optString("name"));
                                horizontalPojo.setImage(ApiClient.BASE_URL + jsonObject.optString("image").replace("[", "").replace("]", "").trim());
                                productFeaturepojos.add(horizontalPojo);
                            }

                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                            categories.setLayoutManager(horizontalLayoutManagaer);
                            categories.setAdapter(resellingCategoryAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(context,"//Error",Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }

    public void shareShop() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL + "virtual_shop/virtual_shop_share/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("jhgghf", "jsonarray" + obj);
                            String data = obj.optString("data");
                            String shareBody = data;
                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
//                //sharingIntent.putExtra(Intent.EXTRA_SUBJECT,"esrdfgvhbnhrtdfcg rdtfghbjnmjbhgytfr rdtfghjnhytfr");
                            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                            context.startActivity(Intent.createChooser(sharingIntent, "Share to"));
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void showproducts(String category_id) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "virtual_shop/virtual_shop_products_by_category/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            categories1.clear();
                            Log.d("MYSHOOOpppcat", "ghfdg" + obj);
                            if (obj.optString("message").equals("no products under this category")) {
                                categories1.clear();
                                progressDialog.dismiss();
                                Toast.makeText(context, "No products under this category", Toast.LENGTH_SHORT).show();
                            }
                            if (obj.optString("message").equals("Success")) {
                                JSONArray jsonArray = obj.optJSONArray("data");
                                if (jsonArray.length() == 0) {
                                    progressDialog.dismiss();
                                    categories1.clear();
                                    Toast.makeText(context, "No products under this category", Toast.LENGTH_SHORT).show();
                                }

                                progressDialog.dismiss();

                                categories1.clear();
                                // Log.d("jhgghf", "jsonarray" + obj);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    Category category = new Category();
                                    category.setId(jsonObject.optString("id"));
                                    category.setProduct_id(jsonObject.optString("product_id"));
                                    category.setName(jsonObject.optString("name"));
                                    category.setDescription(jsonObject.optString("description"));
                                    category.setOriginal_price(jsonObject.optString("orginal_price"));
                                    category.setEdited_price(jsonObject.optString("changed_price"));
                                    category.setReselling_max(jsonObject.optString("re_max_price"));
                                    String images1 = jsonObject.optString("image");
                                    String[] seperated = images1.split(",");
                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    category.setImage(ApiClient.BASE_URL + split);
                                    categories1.add(category);
                                    //Log.d("response2wa", "resp" + ApiClient.BASE_URL + split);
                                }
                                // Log.d("ddddddd", "resp" + ApiClient.BASE_URL);
                                products.setAdapter(new MyShopAdapter(context, categories1));
                                products.setNestedScrollingEnabled(false);
                                //products.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                                products.setLayoutManager(new GridLayoutManager(context, 2));
                                progressDialog.dismiss();
                            }

                        } catch (JSONException | NullPointerException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context,"//Error",Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("category_id", category_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    @Override
    public void addProduct(String productid, String price1, String edited_price) {

    }

    @Override
    public void sendData(String id) {
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait while the data is loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(true);
        progressDialog.show();
        //Toast.makeText(context, "" + id, Toast.LENGTH_SHORT).show();
        showproducts(id);
    }
}