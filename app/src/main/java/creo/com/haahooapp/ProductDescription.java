package creo.com.haahooapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import creo.com.haahooapp.Adapters.FeatureAdapter;
import creo.com.haahooapp.Modal.ProductFeaturepojo;


public class ProductDescription extends Fragment {
    RecyclerView features;
    TextView description;
    ArrayList<ProductFeaturepojo> productFeaturepojos = new ArrayList<>();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    public ProductDescription() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_description,container,false);
        description = view.findViewById(R.id.description);
        description.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        features = view.findViewById(R.id.features);
        for (int i = 0 ; i < 5 ; i++) {
            ProductFeaturepojo productFeaturepojo = new ProductFeaturepojo("Hi this is a test sample for checking whether data i scoming or not");
            productFeaturepojos.add(productFeaturepojo);
        }
        FeatureAdapter featureAdapter = new FeatureAdapter(productFeaturepojos,getActivity());
        features.setHasFixedSize(true);
        features.setLayoutManager(new LinearLayoutManager(getActivity()));
        features.setAdapter(featureAdapter);
        return view;
    }

}