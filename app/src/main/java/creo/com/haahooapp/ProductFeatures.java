package creo.com.haahooapp;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ProductFeatures extends Fragment {
RecyclerView recyclerView;
    public ProductFeatures() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_product_features,container,false);
        // Inflate the layout for this fragment
        recyclerView = view.findViewById(R.id.recyclerview);
        return view;
    }


}
