package creo.com.haahooapp.camera;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import androidx.core.content.ContextCompat;
import creo.com.haahooapp.Navigation;
import creo.com.haahooapp.NewHome;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailedImageVideo extends Activity {
    com.jsibbold.zoomage.ZoomageView image_frame;
    String image_or_video_path ;
    ProgressDialog progressDialog ;
    private final String STATE_RESUME_WINDOW = "resumeWindow";
    private final String STATE_RESUME_POSITION = "resumePosition";
    private final String STATE_PLAYER_FULLSCREEN = "playerFullscreen";
    ImageView image;
    File file = null;
    private SimpleExoPlayerView mExoPlayerView;
    private MediaSource mVideoSource;
    private String mInputPath;
    private boolean mExoPlayerFullscreen = false;
    private FrameLayout mFullScreenButton;
    private ImageView mFullScreenIcon;
    String type = "null";
    private Dialog mFullScreenDialog;
    Activity context = this;
    private int mResumeWindow;
    private long mResumePosition;
    private FrameLayout main_media_frame;
    TextView text;
    ProgressBar loading_progress;
    private VideoCompressor mVideoCompressor = new VideoCompressor(this);
    FFmpeg fFmpeg;
    String message_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_image_video);
        image = findViewById(R.id.image);
        text = findViewById(R.id.send);
        if (getIntent().hasExtra("filepath")){
            file = new File(getIntent().getStringExtra("filepath"));
        }
        progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    if (type.equals("video")) {
                        progressDialog.setMessage("Adding Story ...");
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                        Log.d("videooofikle", "hjgfds" + file + type);
                        RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), file);

                        MultipartBody.Part vFile = MultipartBody.Part.createFormData("story", file.getName(), videoBody);
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ApiClient.BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        VideoInterface vInterface = retrofit.create(VideoInterface.class);
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                        final String token = (pref.getString("token", ""));
                        RequestBody filetype = RequestBody.create(MediaType.parse("text/plain"), type);
                        Call<ResponseBody> serverCom = vInterface.uploadVideoToServer("Token " + token, vFile, filetype);
                        serverCom.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                progressDialog.dismiss();
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setMessage("Your story will be added soon..");
                                alertDialogBuilder.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                finish();
                                               // Toast.makeText(DetailedImageVideo.this, "Failed to add story , Please try again.." + call.request(), Toast.LENGTH_SHORT).show();
                                                startActivity(new Intent(DetailedImageVideo.this, NewHome.class));
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.setCancelable(false);
                                alertDialog.show();

                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(DetailedImageVideo.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                    if (type.equals("image")) {

                        progressDialog.setMessage("Adding Story ...");
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                        Log.d("videooofikle", "hjgfds" + file +image_or_video_path+ type);
                        RequestBody videoBody = RequestBody.create(MediaType.parse("image/*"), file);

                        MultipartBody.Part vFile = MultipartBody.Part.createFormData("story", file.getName(), videoBody);
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ApiClient.BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        Retrofit retrofit1 = ApiClient.getRetrofitClient(context);
                        VideoInterface vInterface = retrofit1.create(VideoInterface.class);
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                        final String token = (pref.getString("token", ""));
                        RequestBody filetype = RequestBody.create(MediaType.parse("text/plain"), type);
                        Call<ResponseBody> serverCom = vInterface.uploadVideoToServer("Token " + token, vFile, filetype);
                        serverCom.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                progressDialog.dismiss();
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setMessage("Your story will be added soon..");
                                alertDialogBuilder.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                finish();
                                                //Toast.makeText(DetailedImageVideo.this, "Failed to add story , Please try again.." + call.request(), Toast.LENGTH_SHORT).show();
                                                startActivity(new Intent(DetailedImageVideo.this, NewHome.class));
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.setCancelable(false);
                                alertDialog.show();

                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(DetailedImageVideo.this, "Check Your Internet Connection" , Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        image_or_video_path=getIntent().getStringExtra(Config.KeyName.FILEPATH);
        setUI();
        if (savedInstanceState != null) {
            mResumeWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW);
            mResumePosition = savedInstanceState.getLong(STATE_RESUME_POSITION);
            mExoPlayerFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN);
        }
    }

    private void setUI() {

        loading_progress=findViewById(R.id.loading_progress);
        loading_progress.setVisibility(View.VISIBLE);
        image_frame=findViewById(R.id.image_frame);
        main_media_frame=findViewById(R.id.main_media_frame);
        if (image_or_video_path.contains(".mp4")){
            message_type= Config.MessageType.VIDEO;
            image_frame.setVisibility(View.GONE);
            main_media_frame.setVisibility(View.VISIBLE);
            fFmpeg = FFmpeg.getInstance(context);
            try {
                fFmpeg.loadBinary(new LoadBinaryResponseHandler() {

                    @Override
                    public void onStart() {
                        Log.d("started","ffmpeg load binary started...");
                    }

                    @Override
                    public void onFailure() {
                        Log.e("failed","ffmpeg load binary failure...");
                    }

                    @Override
                    public void onSuccess() {
                        Log.d("success","ffmpeg load binary success...");
                    }

                    @Override
                    public void onFinish() {
                        Log.d("finished","ffmpeg load binary finish...");
                    }
                });
            } catch (FFmpegNotSupportedException e) {
                // Handle if FFmpeg is not supported by device
            }
            addVideo(new File(image_or_video_path));
            startMediaCompression();
        }else {
            Uri uri = null;
            try {
                uri = addImageToGallery(new File(image_or_video_path));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(context,"imggg",Toast.LENGTH_SHORT).show();
            message_type= Config.MessageType.IMAGE;
            image_frame.setVisibility(View.VISIBLE);
            image.setVisibility(View.VISIBLE);
            main_media_frame.setVisibility(View.GONE);
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

//            Glide.with(context)
//                    .asBitmap()
//                    .load(image_or_video_path)
//                    .apply( new RequestOptions().signature(new ObjectKey(Calendar.getInstance().getTime()))
//                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                    )
//                    .into(image_frame);
            setImage();

            image.setImageURI(uri);
        }

        File file = new File(image_or_video_path);
        int file_size = Integer.parseInt(String.valueOf((file.length()/1024)/1024));
     //   image_frame.setOnTouchListener(this);
       // Toast.makeText(DetailedImageVideo.this,"File is "+file_size +" mb",Toast.LENGTH_SHORT).show();
    }
    private void setImage(){
        loading_progress.setVisibility(View.GONE);
        //mExoPlayerView.setVisibility(View.GONE);


    }
    public void onSaveInstanceState(Bundle outState) {

        outState.putInt(STATE_RESUME_WINDOW, mResumeWindow);
        outState.putLong(STATE_RESUME_POSITION, mResumePosition);
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, mExoPlayerFullscreen);

        super.onSaveInstanceState(outState);
    }


    public Uri addVideo(File videoFile) {
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.Video.Media.TITLE, "My video title");
        Log.d("jihuygtfr","ertyui"+videoFile.getAbsolutePath());

        type = "video";
        mInputPath = videoFile.getAbsolutePath();
        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
        values.put(MediaStore.Video.Media.DATA, videoFile.getAbsolutePath());

        return getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
    }

    private void startMediaCompression() {

        mVideoCompressor.startCompressing(mInputPath, new VideoCompressor.CompressionListener() {
            @Override
            public void compressionFinished(int status, boolean isVideo, String fileOutputPath) {

                if (mVideoCompressor.isDone()) {
                    File outputFile = new File(fileOutputPath);
                    long outputCompressVideosize = outputFile.length();
                    long fileSizeInKB = outputCompressVideosize / 1024;
                    long fileSizeInMB = fileSizeInKB / 1024;
                    file = outputFile;

                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onProgress(final int progress) {

                progressDialog.setMessage("Compressing your video before sending to server "+progress+"%");
                progressDialog.show();
                progressDialog.setCancelable(false);
                Log.d("progress222","jhgfd"+progress);
            }
        });
    }

    public Uri addImageToGallery(File filePath) throws IOException {
        file = filePath;
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        type = "image";
        values.put(MediaStore.MediaColumns.DATA, filePath.getAbsolutePath());
        Bitmap bitmap = null;

        return getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }
    public void writeToFile(byte[] data, File file) throws IOException {
        BufferedOutputStream bufferedInputStream = null;
        try{
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bufferedInputStream = new BufferedOutputStream(fileOutputStream);
            bufferedInputStream.write(data);

        }
        finally {
            if (bufferedInputStream != null){
                try{
                    bufferedInputStream.flush();
                    bufferedInputStream.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }


    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog() {

        ((ViewGroup) mExoPlayerView.getParent()).removeView(mExoPlayerView);
        mFullScreenDialog.addContentView(mExoPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
       // mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(DetailedImageVideo.this, R.drawable.ic_fullscreen_exit_white_24dp));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
    }


    private void closeFullscreenDialog() {

        ((ViewGroup) mExoPlayerView.getParent()).removeView(mExoPlayerView);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(mExoPlayerView);
        mExoPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(DetailedImageVideo.this, R.drawable.ic_fullscreen));
    }


    private void initFullscreenButton() {

        PlaybackControlView controlView = mExoPlayerView.findViewById(R.id.exo_controller);
        mFullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mExoPlayerFullscreen)
                    openFullscreenDialog();
                else
                    closeFullscreenDialog();
            }
        });
    }


    private void initExoPlayer() {
        if (loading_progress!=null) {
            loading_progress.setVisibility(View.GONE);
        }
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
        SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);
        mExoPlayerView.setPlayer(player);

        boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;

        if (haveResumePosition) {
            mExoPlayerView.getPlayer().seekTo(mResumeWindow, mResumePosition);
        }

        player.prepare(mVideoSource);
        mExoPlayerView.getPlayer().setPlayWhenReady(true);
    }


    @Override
    protected void onResume() {

        super.onResume();
        if (image_or_video_path.contains(".mp4")){

            if (mExoPlayerView == null) {

                mExoPlayerView = (SimpleExoPlayerView) findViewById(R.id.exoplayer);
                // initFullscreenDialog();
                //initFullscreenButton();

                // String streamUrl = "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8";
                String userAgent = Util.getUserAgent(DetailedImageVideo.this, getApplicationContext().getApplicationInfo().packageName);
                DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(userAgent, null, DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
                DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(DetailedImageVideo.this, null, httpDataSourceFactory);
                //   Uri daUri = Uri.parse(streamUrl);

                // mVideoSource = new HlsMediaSource(daUri, dataSourceFactory, 1, null, null);
                DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                mVideoSource = new ExtractorMediaSource(Uri.parse(image_or_video_path),
                        dataSourceFactory, extractorsFactory, null, null);
                initExoPlayer();
            }

            else {
                mExoPlayerView.getPlayer().setPlayWhenReady(true);
            }
            //  initExoPlayer();
/*
            if (mExoPlayerFullscreen) {
                ((ViewGroup) mExoPlayerView.getParent()).removeView(mExoPlayerView);
                mFullScreenDialog.addContentView(mExoPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(UploadFeedActivity.this, R.drawable.exo_controls_next));
                mFullScreenDialog.show();
            }*/
        }
    }

    @Override
    protected void onPause() {

        super.onPause();

        if (mExoPlayerView != null && mExoPlayerView.getPlayer() != null) {
            mExoPlayerView.getPlayer().setPlayWhenReady(false);
        }

    }

}