package creo.com.haahooapp.camera;

import creo.com.haahooapp.Modal.StoryModal;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface VideoInterface {
    @Multipart
    @POST("stories/story_add/")
    Call<ResponseBody> uploadVideoToServer(@Header("Authorization")String token , @Part MultipartBody.Part story, @Part("type") RequestBody type);

    @Multipart
    @POST("premium_shopper/add_files/")
    Call<ResultObject> addFiles(@Header("Authorization")String token , @Part MultipartBody.Part data , @Part("type") RequestBody requestBody);
}