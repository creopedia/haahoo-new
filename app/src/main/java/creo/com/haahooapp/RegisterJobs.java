package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.config.ApiClient;

public class RegisterJobs extends AppCompatActivity {
    Activity activity = this;
    ImageView back,whatsapp;
    EditText name,phone,job_name,job_descripition;
    TextView textView;
    Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_jobs);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        whatsapp = findViewById(R.id.whatsapp);
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String url = "https://chat.whatsapp.com/GhZRE8alvnzHaT6uZEKVJx";
                intent.setData(Uri.parse(url));
                intent.setPackage("com.whatsapp");
                startActivity(intent);
            }
        });
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        job_name = findViewById(R.id.job_name);
        back = findViewById(R.id.back);
        job_descripition = findViewById(R.id.description);
        textView = findViewById(R.id.submit);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterJobs.super.onBackPressed();
            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(name.getText().toString().length()!=3){
                    name.setError("Enter a valid name");
                }
                if(phone.getText().toString().length()!=10){
                    phone.setError("Enter valid number");
                }
                if(job_name.getText().toString().length()==0){
                    job_name.setError("Enter job name");
                }
                if(job_descripition.getText().length()==0){
                    job_descripition.setError("Enter valid description");
                }
                if(name.getText().toString().length()>3 && phone.getText().toString().length()==10 && job_name.getText().toString().length()!=0 && job_descripition.getText().toString().length()!=0) {
                    registerJob();
                }
            }
        });


    }

    public void registerJob(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/reg_jobs/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.optString("message");
                            if(message.equals("Success")){
                                Toast.makeText(RegisterJobs.this,"Your job details has been submitted for further verification",Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(RegisterJobs.this,NewHome.class));
                            }
                            if(message.equals("Failed")){
                                Toast.makeText(RegisterJobs.this,"Failed to submit your job details",Toast.LENGTH_SHORT).show();

                            }

                            Log.d("asdfgh","result222"+jsonObject);

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterJobs.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("name",name.getText().toString());
                params.put("pdt_name",job_name.getText().toString());
                params.put("pdt_contact",phone.getText().toString());
                params.put("pdt_description",job_descripition.getText().toString());
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }

}
