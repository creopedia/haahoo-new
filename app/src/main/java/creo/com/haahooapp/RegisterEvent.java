package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.config.ApiClient;

public class RegisterEvent extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    Activity activity = this;
    TextView register;
    EditText name , phone , email;
    Spinner profession;
    String image_url = "null";
    Context context = this;
    RelativeLayout job ,course;
    ImageView back;
    EditText jobtype,coursetype;
    String event_id = null;
    String profession_name = "null";
    String profession_type = "null";
    String[] options = {"Choose profession" ,"Student",  "Employed" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_event);

        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        jobtype = findViewById(R.id.jobtype);
        coursetype = findViewById(R.id.coursetype);
        back = findViewById(R.id.back);
        Bundle bundle = getIntent().getExtras();
        event_id = bundle.getString("event_id");
        image_url = bundle.getString("image");
        job = findViewById(R.id.jb);
        course = findViewById(R.id.crse);
        profession = findViewById(R.id.profession);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, options);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        profession.setAdapter(adapter);
        profession.setOnItemSelectedListener(this);
        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(RegisterEvent.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(RegisterEvent.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(RegisterEvent.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(RegisterEvent.this, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterEvent.this,NewHome.class));
            }
        });
        profession = findViewById(R.id.profession);
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        email = findViewById(R.id.email);
        email.setText(ApiClient.email);
        name.setText(ApiClient.username);
        phone.setText(ApiClient.user_contact_no);
        register = findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean email_valid = isValidEmailId(email.getText().toString());
                if (!(isValidEmailId(email.getText().toString()))){
                    email.setError("Enter valid email");
                }
                if(name.getText().toString().length() < 3){
                    name.setError("Enter a valid name");
                }
                if(phone.getText().toString().length()<10){
                    phone.setError("Enter a valid number");
                }
//                if (profession_name.equals("null")){
//
//                }
                if(name.getText().toString().length()>=3 && phone.getText().toString().length() == 10 && email_valid && !(profession_name.equals("null")) && !(profession_type.equals("null"))){
//                    registerDetails();
                    Intent intent = new Intent(RegisterEvent.this,EventNew.class);
                    intent.putExtra("number",phone.getText().toString());
                    intent.putExtra("email",email.getText().toString());
                    intent.putExtra("event_id",event_id);
                    intent.putExtra("image",image_url);
                    intent.putExtra("name",name.getText().toString());
                    intent.putExtra("proffession",profession_name);
                    intent.putExtra("proffession_type",profession_type);
                    startActivity(intent);
                }

            }
        });

    }

    public void registerDetails(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        Log.d("toke","token"+token);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"events/event_reg/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if(message.equals("Success")){
//                                Intent intent = new Intent(RegisterEvent.this,MakePaymentEvents.class);
//                                intent.putExtra("number",phone.getText().toString());
//                                intent.putExtra("email",email.getText().toString());
//                                intent.putExtra("event_id",event_id);
//                                startActivity(intent);

                            }
                            if(!(message.equals("Success"))){
                                Toast.makeText(RegisterEvent.this,"Some error occured",Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      //  Toast.makeText(RegisterEvent.this, "//error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id",event_id);
                params.put("proffession",profession_name);
                params.put("proffession_type",profession_type);
                params.put("name", name.getText().toString());
                params.put("phone_no", phone.getText().toString());
                params.put("email",email.getText().toString());
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(options[position].equals("Student")){
            profession_type = "Student";
            job.setVisibility(View.GONE);
            course.setVisibility(View.VISIBLE);
            profession_name = coursetype.getText().toString();
        }
        if(options[position].equals("Employed")){
            profession_type = "Employed";
            course.setVisibility(View.GONE);
            job.setVisibility(View.VISIBLE);
            profession_name = jobtype.getText().toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private boolean isValidEmailId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }
}
