package creo.com.haahooapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.config.ApiClient;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class ProfileFragmentKYC extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    Spinner spinner;
    EditText doc;
    RelativeLayout sub,submitted;
    TextView submit;
    String[] options = {"Choose document type" ,"PAN Card",  "Aadhar Card" };
    String selecteddoc = "Choose document type";
    public ProfileFragmentKYC() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragmentKYC.
     */


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_fragment_kyc,container,false);

        spinner = view.findViewById(R.id.spinner);
        doc = view.findViewById(R.id.doc);
        submit = view.findViewById(R.id.submit);
        sub = view.findViewById(R.id.sub);
        submitted = view.findViewById(R.id.submitted);
        if (ApiClient.proof.equals("Choose document")){
            sub.setVisibility(View.VISIBLE);
            submitted.setVisibility(View.GONE);
        }
        if (!(ApiClient.proof).equals("Choose document")){
            sub.setVisibility(View.GONE);
            submitted.setVisibility(View.VISIBLE);
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selecteddoc.equals("Choose document type")){
                    Toast.makeText(getContext(),"Select document type",Toast.LENGTH_SHORT).show();
                }
                if (!(selecteddoc.equals("Choose document type"))) {
                    updateProfile();
                }
            }
        });
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, options);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
       // spinner.setOnItemSelectedListener(getActivity());
       spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

               selecteddoc = options[position];
               if (selecteddoc.equals("PAN Card")){
                   int maxLength = 10;
                   doc.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
                   //setEditTextMaxLength(doc,10);
               }
               if (selecteddoc.equals("Aadhar Card")){
                   int maxLength = 12;
                   doc.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
                   // setEditTextMaxLength(doc,12);
               }
//               Toast.makeText(getContext(),"selected"+selecteddoc,Toast.LENGTH_SHORT).show();
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });
        return view;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public void updateProfile(){
        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
       String  token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/user_details_edit/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.optString("message");
                            if (message.equals("Success")){
//                                ApiClient.username = name.getText().toString();
//                                ApiClient.email = email.getText().toString();
                                Toast.makeText(getContext(),"Successfully updated profile",Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getContext(),MyAccount.class);
                                intent.putExtra("fromactivity","update");
                                startActivity(intent);
                            }
                            if (message.equals("Failed")){
                                Toast.makeText(getContext(),"Failed to update",Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),"Some error occured",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("name",ApiClient.username);
                params.put("email",ApiClient.email);
                params.put("proof_id",selecteddoc);
                params.put("proof_id_no",doc.getText().toString());
                params.put("date_of_birth","");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }
//    public static void setEditTextMaxLength(EditText dd, int length) {
//        InputFilter[] FilterArray = new InputFilter[1];
//        FilterArray[0] = new InputFilter.LengthFilter(length);
//        dd.setFilters(FilterArray);
//    }


}
