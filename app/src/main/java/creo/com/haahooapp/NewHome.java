package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.CombinedCartActivity;
import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahoo.ViewCouponActivity;
import creo.com.haahooapp.Adapters.ConnectedShopHorizontalAdapter;
import creo.com.haahooapp.Adapters.NewHomeCat;
import creo.com.haahooapp.Adapters.NewHomeDeals;
import creo.com.haahooapp.Adapters.NewHomeGrocery;
import creo.com.haahooapp.Adapters.NewHomeHouseAdapter;
import creo.com.haahooapp.Adapters.NewHomeOffAdapter;
import creo.com.haahooapp.Adapters.NewHomeOffs;
import creo.com.haahooapp.Adapters.SliderAdapterExample;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.Modal.ConnectedShops;
import creo.com.haahooapp.Modal.Grocery;
import creo.com.haahooapp.Modal.PopularPojo;
import creo.com.haahooapp.Modal.TrendingPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class NewHome extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    boolean doubleBackToExitPressedOnce = false;
    Activity activity = this;
    ImageView logo;
    RecyclerView categories;
    List<TrendingPojo> trendingPojos = new ArrayList<>();
    RecyclerView secondrel, thirdrel, offs, fourthrel, grocery, connectedlist;
    List<String> stringList = new ArrayList<>();
    ImageView banner, bann, start, secondbanner, thirdban, fourthbann;
    Toolbar tool;
    DrawerLayout drawer;
    ActionBarDrawerToggle mDrawerToggle;
    NavigationView navigationView;
    Context context = this;
    RelativeLayout seeall, allnew, connectrel, connected;
    CardView firstcard, fourthcard;
    ProgressDialog progressDialog;
    TextView name, email, phone, referralid, more_events, active, count;
    CardView myshop, bazaar, morefish, fruits, grocerymore;
    ImageView cart, refer, startbiz;
    String link = "null";
    RelativeLayout connectall;
    SwipeRefreshLayout swipe;
    //LOCATION LOGIC
    private FusedLocationProviderClient mFusedLocationClient;
    private int locationRequestCode = 1000;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private LocationManager locationManager;
    private boolean isContinue = false;
    private LocationSettingsRequest mLocationSettingsRequest;
    private SettingsClient mSettingsClient;
    String latitude = "null";
    String longitude = "null";
    CardView premiumcard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_hom);
        categories = findViewById(R.id.categories);
        secondrel = findViewById(R.id.secondrel);
        myshop = findViewById(R.id.myshop);
        connectall = findViewById(R.id.connectall);
        grocerymore = findViewById(R.id.grocerymore);
        swipe = findViewById(R.id.swipe);
        premiumcard = findViewById(R.id.premiumcard);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String membershipval = (pref.getString("membership", "null"));
        if (membershipval.equals("1")) {
            premiumcard.setVisibility(View.VISIBLE);
        }
        if (membershipval.equals("0")) {
            premiumcard.setVisibility(View.GONE);
        }
        swipe.setColorSchemeResources(R.color.theme);
        startbiz = findViewById(R.id.startbiz);
        Glide.with(context).load("https://testapi.creopedia.com/media/android/startgif.gif").into(startbiz);
        //Glide.with(context).load(R.drawable.startgif).into(startbiz);
        startbiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MemberShipActivity.class));
            }
        });
        connectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShopByCategory.class);
                startActivity(intent);
            }
        });
        connected = findViewById(R.id.connected);
        connectrel = findViewById(R.id.connectrel);
        bazaar = findViewById(R.id.bazaar);
        connectedlist = findViewById(R.id.connectedlist);
        progressDialog = new ProgressDialog(this);
        morefish = findViewById(R.id.morefish);
        fruits = findViewById(R.id.fruits);
        cart = findViewById(R.id.cart);
        refer = findViewById(R.id.refer);
        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setTitle("Loading..");
                progressDialog.setIcon(R.drawable.haahoologo);
                progressDialog.setMessage("Finding Apps...");
                progressDialog.show();
                shareLongDynamicLink();
            }
        });
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CombinedCartActivity.class);
                intent.putExtra("from", "home");
                startActivity(intent);
            }
        });
        count = findViewById(R.id.count);
        count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CombinedCartActivity.class);
                intent.putExtra("from", "home");
                startActivity(intent);
            }
        });
        count.setText(String.valueOf(ApiClient.count));
        grocerymore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MoreProduct.class);
                intent.putExtra("from", "grocery");
                startActivity(intent);
            }
        });
        fruits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MoreProduct.class);
                intent.putExtra("from", "fruits");
                startActivity(intent);
            }
        });
        morefish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MoreProduct.class);
                intent.putExtra("from", "fish");
                startActivity(intent);
            }
        });
        ApiClient.bazaar = "null";
        bazaar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(context, ResellingProducts.class));
//                startActivity(new Intent(context,OnlineMart.class));
//                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                String membershipval = (pref.getString("membership", "null"));
//                if (membershipval.equals("1")) {
//                    startActivity(new Intent(context, BazaarProducts.class));
//                }
//                if (!(membershipval.equals("1"))) {
//                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                            .setTitle("Become Our Premium Member")
//                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                startActivity(new Intent(context, MemberShipActivity.class));
//                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                dialog.dismiss();
//                            });
//// Show the alert
//                    builder.show();
//                }
                Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();

            }
        });
        myshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                String membershipval = (pref.getString("membership", "null"));
//                if (membershipval.equals("1")) {
//                    startActivity(new Intent(context, ResellProducts.class));
//                }
//                if (!(membershipval.equals("1"))) {
//                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                            .setTitle("Become Our Premium Member")
//                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                startActivity(new Intent(context, MemberShipActivity.class));
//                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                dialog.dismiss();
//                            });
//// Show the alert
//                    builder.show();
//                }

                Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });
        thirdrel = findViewById(R.id.thirdrel);
        tool = findViewById(R.id.tool);
        logo = tool.findViewById(R.id.logo);
        Glide.with(context).load(ApiClient.BASE_URL + "media/android/haahoologo.png").into(logo);
        progressDialog.setTitle("Loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Fetching Products...");
        progressDialog.show();
        allnew = findViewById(R.id.allnew);
        allnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewProducts.class);
                intent.putExtra("lat", latitude);
                intent.putExtra("log", longitude);
                startActivity(intent);
            }
        });
        seeall = findViewById(R.id.seeall);
        firstcard = findViewById(R.id.firstcard);
        fourthcard = findViewById(R.id.fourthcard);
        fourthcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(context, Jobs.class));
                Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });
        firstcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Downloads.class));
            }
        });
        seeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AllCategories.class));
            }
        });
//        setSupportActionBar(tool);
        drawer = findViewById(R.id.drawer);
        secondbanner = findViewById(R.id.secondbanner);
        thirdban = findViewById(R.id.thirdban);
        fourthbann = findViewById(R.id.fourthbann);
        grocery = findViewById(R.id.grocery);
        grocery();
        Glide.with(context).load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRbwEWUO5ajpDXjNL64hpDP0gd_Fg-PTO92-A&usqp=CAU").into(fourthbann);
        Glide.with(context).load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT7JIEgGnHH5cXwPpnit_tbHFPiNmm4FnHVBA&usqp=CAU").into(thirdban);
        Glide.with(context).load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQv0YrvG5GDQ8MZY8rp3hxDsRm5ReIuqt7i8w&usqp=CAU").into(secondbanner);
        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        View view = navigationView.getHeaderView(0);
        name = view.findViewById(R.id.name);
        email = view.findViewById(R.id.email);
        phone = view.findViewById(R.id.phone);
        referralid = view.findViewById(R.id.referralid);
        active = view.findViewById(R.id.active);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, tool, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        toggle.syncState();
        bann = findViewById(R.id.bann);
        start = findViewById(R.id.start);
        Glide.with(getApplicationContext()).load("https://testapi.creopedia.com/media/android/startbiz.png").into(start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MemberShipActivity.class));
            }
        });
        Glide.with(getApplicationContext()).load("https://testapi.creopedia.com/media/android/homebanner.jpg").into(bann);
        fourthrel = findViewById(R.id.fourthrel);
        banner = findViewById(R.id.banner);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.action_home:
                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:
                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:
                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:
                                //startActivity(new Intent(context, AllStories.class));
                                Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }

                                Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();

                                break;

                        }
                        return true;
                    }
                });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.profile:
                        startActivity(new Intent(context, ProfileNew.class));
                        break;

                    case R.id.cart:
                        startActivity(new Intent(context, CombinedCartActivity.class));
                        break;

                    case R.id.wishlist:
                        startActivity(new Intent(context, WishList.class));
                        break;

                    case R.id.orders:
                        Intent intent = new Intent(context, Orders.class);
                        intent.putExtra("fromactivity", "home");
                        startActivity(intent);
                        break;

                    case R.id.address:
                        startActivity(new Intent(context, ChooseAddress.class));
                        break;
                    case R.id.memebership:
                        startActivity(new Intent(context, MemberShipActivity.class));
                        break;

                    case R.id.wallet:
                        startActivity(new Intent(context, Earnings.class));
                        break;

                    case R.id.rewards:
                        startActivity(new Intent(context, RewardsUI.class));
                        break;

                    case R.id.coupons:
                        startActivity(new Intent(context, ViewCouponActivity.class));
                        break;
                    case R.id.events:
                        startActivity(new Intent(context, Events.class));
                        break;

                    case R.id.about:
                        startActivity(new Intent(context, AboutUs.class));
                        break;

                    case R.id.sell:
                        //checkRole();
                        break;

                    case R.id.shop:
                        Intent inten = new Intent(context, MyShopNew.class);
                        startActivity(inten);
                        break;

                    case R.id.sell_list:
                        // checkRoleList();
                        break;

                    case R.id.withdraw:
//                        startActivity(new Intent(Navigation.this,MyAccount.class));
                        Intent intent1 = new Intent(context, MyAccount.class);
                        intent1.putExtra("fromactivity", "home");
                        startActivity(intent1);
                        break;

                    case R.id.shop_bycategory:
                        Intent intent2 = new Intent(context, ShopByCategory.class);
                        startActivity(intent2);
                        break;

                    case R.id.active:
                        Intent intent3 = new Intent(context, SubscriptionList.class);
                        intent3.putExtra("fromactivity", "home");
                        startActivity(intent3);
                        break;


                    case R.id.leads:
                        Intent intent4 = new Intent(context, Leads.class);
                        startActivity(intent4);
                        break;

                    case R.id.notification:
                        Intent intent5 = new Intent(context, Notifications.class);
                        startActivity(intent5);
                        break;

                }
                return true;
            }
        });
        Glide.with(getApplicationContext()).load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRGNeVAX81cYQiJ3g2BfvpNDad-0OTddBEnqw&usqp=CAU").into(banner);
        offs = findViewById(R.id.offs);
        offs.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        offs.setAdapter(new NewHomeOffs(getApplicationContext(), stringList));
        offs.setNestedScrollingEnabled(false);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        SliderView sliderView = findViewById(R.id.imageSlider);
        SliderAdapterExample adapter = new SliderAdapterExample(this);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setScrollTimeInSec(10); //set scroll delay in seconds :
        sliderView.startAutoCycle();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                progressDialog.show();
                swipe.setRefreshing(false);
                getProfileDetails();
                getConnectedShops();
                getcategories();
                fishandmeat();
                trendingPojos.clear();
                req();
                fruits();
                //newAPI();
                getRefrralDetails();
                grocery();
                cartCount("");
            }
        });
        getProfileDetails();
        getConnectedShops();
        getcategories();

        Runnable runnable44 = new Runnable() {
            @Override
            public void run() {
                try {
                    cartCount("");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread cartc = new Thread(runnable44);
        cartc.start();
        Runnable runnable3 = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        fishandmeat();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread fish = new Thread(runnable3);
        fish.start();

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        fruits();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread threadfruits = new Thread(runnable2);
        threadfruits.start();

        // newAPI();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        getRefrralDetails();
                    } catch (Exception e) {

                    }
                }
            }
        };
        Thread threadref = new Thread(runnable);
        threadref.start();

        //LOCATION LOGIC

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                List<Location> locationList = locationResult.getLocations();
                if (locationList.size() > 0) {
                    Location location = locationList.get(locationList.size() - 1);
                    //WORKING PORTION
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());
                    if (trendingPojos.size() > 0) {

                    }
                    if (trendingPojos.size() == 0) {
                        newAPI();
                    }
                    // Toast.makeText(context,"Latitude"+location.getLatitude()+location.getLongitude(),Toast.LENGTH_SHORT).show();
                    locationManager.removeUpdates(mLocationListener);
                    //getLocation(location.getLatitude(),location.getLongitude());
                }
            }
        };
        Places.initialize(getApplicationContext(), "AIzaSyB66smBcn9YjDIWCdmhym66YJN7IGKdJII");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    locationRequestCode);
        }
        req();
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (checkLocationPermission()) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                    0, mLocationListener);
        }
    }

    public void req() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY));
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();

        mSettingsClient = LocationServices.getSettingsClient(NewHome.this);

        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                        // Success Perform Task Here
                        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        if (checkLocationPermission()) {
                            if (ContextCompat.checkSelfPermission(NewHome.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions(NewHome.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                            }
                            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
                                    0, mLocationListener);
                        }

                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(NewHome.this, 1000);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.e("GPS", "Unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e("GPS", "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                        }
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        Log.e("GPS", "checkLocationSettings -> onCanceled");
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            switch (resultCode) {
                case Activity
                        .RESULT_OK:
                    req();
                    break;
                case Activity.RESULT_CANCELED:
                    req();
                    break;
            }

        }

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (isContinue) {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    } else {
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(NewHome.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    //Toast.makeText(context, "JKBF" + location.getLongitude(), Toast.LENGTH_SHORT).show();
                                } else {
                                    mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            //WORKING PORTION
            // Toast.makeText(context,"hjhfghf"+location.getLongitude(),Toast.LENGTH_SHORT).show();
            locationManager.removeUpdates(mLocationListener);

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public boolean checkLocationPermission() {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void cartCount(final String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token1 = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "cart_view/cart_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            String total_count = jsonObject1.optString("total all count");
                            if (total_count.equals("")) {
                                total_count = "0";
                                ApiClient.count = Integer.valueOf(total_count);
                            }
                            if (!(total_count.equals(""))) {
                                ApiClient.count = Integer.valueOf(total_count);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(LoginWithPin.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token1);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void getConnectedShops() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "virtual_shop/connected_shop_det/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            ArrayList<ConnectedShops> connectedShops = new ArrayList<>();
                            Log.d("connectionstatus", "hhhh" + jsonObject.optJSONArray("data").length());
                            if (jsonObject.optJSONArray("data") != null) {

                                JSONArray data = jsonObject.optJSONArray("data");

                                if (data.length() < 5) {
                                    if (data.length() == 0) {
                                        connected.setVisibility(View.GONE);
                                        connectrel.setVisibility(View.GONE);
                                    } else {
                                        connected.setVisibility(View.VISIBLE);
                                        connectrel.setVisibility(View.VISIBLE);
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject jsonObject1 = data.optJSONObject(i);
                                            ConnectedShops connectedShops1 = new ConnectedShops();
                                            connectedShops1.setId(jsonObject1.optString("shop_id"));
                                            connectedShops1.setName(jsonObject1.optString("shop_name"));
                                            connectedShops1.setRating(jsonObject1.optString("rating_count"));
                                            connectedShops1.setImage(ApiClient.BASE_URL + jsonObject1.optString("shop_img").replace("[", "").replace("]", ""));
                                            connectedShops.add(connectedShops1);
                                        }
                                        connectedlist.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                        connectedlist.setAdapter(new ConnectedShopHorizontalAdapter(context, connectedShops));
                                        connectedlist.setNestedScrollingEnabled(true);
                                    }
                                }
                                if (data.length() >= 5) {
                                    connected.setVisibility(View.VISIBLE);
                                    connectrel.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < 5; i++) {
                                        JSONObject jsonObject1 = data.optJSONObject(i);
                                        ConnectedShops connectedShops1 = new ConnectedShops();
                                        connectedShops1.setId(jsonObject1.optString("shop_id"));
                                        connectedShops1.setName(jsonObject1.optString("shop_name"));
                                        connectedShops1.setRating(jsonObject1.optString("rating_count"));
                                        connectedShops1.setImage(ApiClient.BASE_URL + jsonObject1.optString("shop_img").replace("[", "").replace("]", ""));
                                        connectedShops.add(connectedShops1);
                                    }
                                    connectedlist.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    connectedlist.setAdapter(new ConnectedShopHorizontalAdapter(context, connectedShops));
                                    connectedlist.setNestedScrollingEnabled(true);
                                }
                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public void shareLongDynamicLink() {

        final String path = "https://haahooapp.page.link/?" +
                "link=" + /*link*/
                "https://haahoo.in/?" +
                "id=" +
                referralid.getText().toString() +
                "&apn=" + /*getPackageName()*/
                "creo.com.haahooapp" +
                "&st=" + /*titleSocial*/
                "Download+Haahoo" +
                "&sd=" + /*description*/
                "Install+Haahoo+to+get+a+social+entrepreneurship+venture+experience" +
                "&utm_source=" + /*source*/
                "AndroidApp";
        Log.d("linkkkk", "linkpath" + path);
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(path))
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            link = shortLink.toString();
                            Intent intent = new Intent();
                            progressDialog.dismiss();
                            Log.d("actual link", "linkkk" + link);
                            String msg = "visit my awesome website: " + link;

                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT, link);

                            Intent shareintent = Intent.createChooser(intent, null);
                            startActivity(shareintent);

                        } else {
                            progressDialog.dismiss();
                        }
                    }
                });

    }

    public void getRefrralDetails() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/login_count/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            // Log.d("jsonobjj","hhhh"+jsonObject);
                            String data = jsonObject.optJSONArray("data").optString(0);
                            JSONObject jsonObject1 = new JSONObject(data);
//                            referrals.setText(jsonObject1.optString("referal_count"));
//                            orders.setText(jsonObject1.optString("order_count"));
                            referralid.setText(jsonObject1.optString("id"));
                            ApiClient.own_id = jsonObject1.optString("id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            finishAffinity();

            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(this, " Click back again to exit from the app", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;

            }
        }, 2000);
    }

    void getProfileDetails() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/user_details/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("jkbjbb", "response" + response);
                            JSONObject jsonObject = new JSONObject(response);
                            String data = jsonObject.optString("data");
                            String activity_status = jsonObject.optString("activity_status");
                            JSONArray jsonObject1 = new JSONArray(data);
                            String fields = jsonObject1.optJSONObject(0).optString("fields");
                            JSONObject jsonObject2 = new JSONObject(fields);
                            String name1 = jsonObject2.optString("name");
                            String email1 = jsonObject2.optString("email");
                            String phone_no1 = jsonObject2.optString("phone_no");
                            ApiClient.mobile = phone_no1;
                            ApiClient.proof = jsonObject2.optString("proof_id");
                            ApiClient.activity_status = activity_status;
                            if (ApiClient.activity_status.equals("0")) {
                                active.setText("Inactive");
                                active.setTextColor(Color.parseColor("#FF0000"));
                            }
                            if (ApiClient.activity_status.equals("1")) {
                                active.setText("Active");
                                active.setTextColor(Color.parseColor("#00ff00"));

                            }
                            ApiClient.email = email1;
                            name.setText(name1);
                            ApiClient.username = name1;
                            ApiClient.user_contact_no = phone_no1;
                            email.setText(email1);
                            phone.setText(phone_no1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public void getcategories() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/list_shop_cat/";

        // Request a string response from the provided URL.


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("CGHCHGCGH", "HJVHJVHJ" + response);
                            List<Category> categories = new ArrayList<>();
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                Category category = new Category();
                                category.setId(jsonObject.optString("id"));
                                category.setName(jsonObject.optString("name"));
                                String images1 = jsonObject.optString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "").replace("]", "");
                                category.setImage(ApiClient.BASE_URL + split);
                                categories.add(category);
                            }
                            secondrel.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                            secondrel.setAdapter(new NewHomeOffAdapter(getApplicationContext(), categories));
                            secondrel.setNestedScrollingEnabled(false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
            }
        }) {
            @Override
            public java.util.Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void newAPI() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        //NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        Call<ResponseBody> callregister = apiInterface.getProductslatest("Token " + token, latitude, longitude);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    //progressDialog.dismiss();
                    trendingPojos.clear();
                    JSONArray jsonArray = obj.optJSONArray("data");
                    if (jsonArray.length() == 0) {
                        // Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {

                        for (int i = 0; i < 4; i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            TrendingPojo category = new TrendingPojo();
                            category.setId(jsonObject.optString("id"));
                            category.setName(jsonObject.optString("name"));
                            category.setShop_id(jsonObject.optString("shop_id"));
                            String images1 = jsonObject.optString("image");
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            trendingPojos.add(category);
                        }
                    }
                    categories.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                    categories.setAdapter(new NewHomeCat(getApplicationContext(), trendingPojos));
                    categories.setNestedScrollingEnabled(false);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void fishandmeat() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        //NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        Call<ResponseBody> callregister = apiInterface.getProductsFish("meat", "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    List<PopularPojo> trendingPojos = new ArrayList<>();
                    JSONArray jsonArray = obj.optJSONArray("data");
                    if (jsonArray != null) {
                        if (jsonArray.length() == 0) {
                            Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                        }
                        if (jsonArray.length() != 0) {
                            if (jsonArray.length() > 4) {
                                for (int i = 0; i < 4; i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    PopularPojo category = new PopularPojo();
                                    category.setName(jsonObject.optString("product_name"));
                                    String images1 = jsonObject.optString("pdt_image");
                                    category.setPrice("Rs. " + jsonObject.optString("discount"));
                                    String[] seperated = images1.split(",");
                                    category.setShop_name(jsonObject.optString("shop_name"));
                                    category.setShop_id(jsonObject.optString("shop_id"));
                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    category.setImage(ApiClient.BASE_URL + split);
                                    trendingPojos.add(category);
                                }
                            }
                            if (jsonArray.length() <= 4) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    PopularPojo category = new PopularPojo();
                                    category.setName(jsonObject.optString("product_name"));
                                    String images1 = jsonObject.optString("pdt_image");
                                    category.setPrice("Rs. " + jsonObject.optString("discount"));
                                    String[] seperated = images1.split(",");
                                    category.setShop_name(jsonObject.optString("shop_name"));
                                    category.setShop_id(jsonObject.optString("shop_id"));
                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    category.setImage(ApiClient.BASE_URL + split);
                                    trendingPojos.add(category);
                                }
                            }

                        }
                        thirdrel.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                        thirdrel.setAdapter(new NewHomeDeals(getApplicationContext(), trendingPojos));
                        thirdrel.setNestedScrollingEnabled(false);
                    }
                    if (jsonArray == null) {
                        Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    public void grocery() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        Call<ResponseBody> callregister = apiInterface.getProductsFish("grocery", "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    List<Grocery> trendingPojos = new ArrayList<>();
                    JSONArray jsonArray = obj.optJSONArray("data");
                    Log.d("JSSSSS", "JJJJJ" + jsonArray);
                    if (jsonArray.length() > 6) {
                        for (int i = 0; i < 6; i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Grocery category = new Grocery();
                            category.setName(jsonObject.optString("product_name"));
                            String images1 = jsonObject.optString("pdt_image");
                            String[] seperated = images1.split(",");
                            category.setShop_id(jsonObject.optString("shop_id"));
                            category.setShop_name(jsonObject.optString("shop_name"));
                            category.setPrice("From Rs. " + jsonObject.optString("discount"));
                            if (jsonObject.optString("pdt_image").length() != 0) {
                                String split = seperated[0].replace("[", "").replace("]", "");
                                category.setImage(ApiClient.BASE_URL + split);
                            }
                            if (jsonObject.optString("pdt_image").length() == 0) {
                                category.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRUN4SeftFS5bldudKAcO3aqQEbs-RXMz9sXw&usqp=CAU");
                            }
                            trendingPojos.add(category);
                        }
                    }
                    if (jsonArray.length() <= 6) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Grocery category = new Grocery();
                            category.setName(jsonObject.optString("product_name"));
                            String images1 = jsonObject.optString("pdt_image");
                            String[] seperated = images1.split(",");
                            category.setShop_id(jsonObject.optString("shop_id"));
                            category.setShop_name(jsonObject.optString("shop_name"));
                            category.setPrice("From Rs. " + jsonObject.optString("discount"));

                            if (jsonObject.optString("pdt_image").length() != 0) {
                                String split = seperated[0].replace("[", "").replace("]", "");
                                category.setImage(ApiClient.BASE_URL + split);
                            }
                            if (jsonObject.optString("pdt_image").length() == 0) {
                                category.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRUN4SeftFS5bldudKAcO3aqQEbs-RXMz9sXw&usqp=CAU");
                            }

                            trendingPojos.add(category);
                        }
                    }
                    grocery.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
                    grocery.setAdapter(new NewHomeGrocery(getApplicationContext(), trendingPojos));
                    grocery.setNestedScrollingEnabled(true);
                    //   progressDialog.dismiss();

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    public void fruits() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        Call<ResponseBody> callregister = apiInterface.getProductsFish("fruits", "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    List<PopularPojo> trendingPojos = new ArrayList<>();
                    JSONArray jsonArray = obj.optJSONArray("data");
                    if (jsonArray.length() == 0) {
                        Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {
                        if (jsonArray.length() > 4) {
                            for (int i = 0; i < 6; i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                PopularPojo category = new PopularPojo();
                                category.setName(jsonObject.optString("product_name"));
                                String images1 = jsonObject.optString("pdt_image");
                                category.setShop_name(jsonObject.optString("shop_name"));
                                category.setShop_id(jsonObject.optString("shop_id"));
                                String[] seperated = images1.split(",");
                                if (jsonObject.optString("pdt_image").length() != 0 && !(jsonObject.optString("pdt_image").equals("null"))) {
                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    category.setImage(ApiClient.BASE_URL + split);
                                }
                                if (jsonObject.optString("pdt_image").length() == 0 || jsonObject.optString("pdt_image").equals("null")) {
                                    category.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRUN4SeftFS5bldudKAcO3aqQEbs-RXMz9sXw&usqp=CAU");
                                }
                                trendingPojos.add(category);
                            }
                        }
                        if (jsonArray.length() <= 4) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                PopularPojo category = new PopularPojo();
                                category.setName(jsonObject.optString("product_name"));
                                String images1 = jsonObject.optString("pdt_image");
                                category.setShop_name(jsonObject.optString("shop_name"));
                                category.setShop_id(jsonObject.optString("shop_id"));
                                String[] seperated = images1.split(",");
                                if (jsonObject.optString("pdt_image").length() != 0 && !(jsonObject.optString("pdt_image").equals("null"))) {
                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    category.setImage(ApiClient.BASE_URL + split);
                                }
                                if (jsonObject.optString("pdt_image").length() == 0 || jsonObject.optString("pdt_image").equals("null")) {
                                    category.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRUN4SeftFS5bldudKAcO3aqQEbs-RXMz9sXw&usqp=CAU");
                                }
                                trendingPojos.add(category);
                            }
                        }
                    }
                    fourthrel.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
                    fourthrel.setAdapter(new NewHomeHouseAdapter(getApplicationContext(), trendingPojos));
                    fourthrel.setNestedScrollingEnabled(false);

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
