package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import com.google.android.material.tabs.TabLayout;
import com.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import creo.com.haahooapp.Adapters.ProductTabsAdapter;
import creo.com.haahooapp.Adapters.SlidingImage_Adapter;

public class TestActivity extends AppCompatActivity {

    Context context = this;
    Activity activity = this;
    private static int NUM_PAGES = 0;
    TabLayout tabLayout;
    ViewPager viewPager,pager;
    private static int currentPage = 0;
    ArrayList<String>images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productdetailsshop);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        pager = findViewById(R.id.pager);
        Window window = activity.getWindow();
        images.add("https://5.imimg.com/data5/WE/XU/MY-29477425/latest-fancy-shirts-500x500.jpg");
        images.add("https://contents.mediadecathlon.com/p1416456/k$713efbfe382d4401ce01ef763b50c64f/men-backpacking-shirt-travel-100.jpg?&f=800x800");
        images.add("https://rukminim1.flixcart.com/image/332/398/jlsc58w0/shirt/c/x/z/38-sh701-navy-blue-36-zombom-original-imaf8syzzhmhvccz.jpeg?q=50");
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        tabLayout.addTab(tabLayout.newTab().setText("Details"));
        tabLayout.addTab(tabLayout.newTab().setText("Delivery"));
        tabLayout.addTab(tabLayout.newTab().setText("Options"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        pager.setAdapter(new SlidingImage_Adapter(context,images));

        CirclePageIndicator indicator =
                findViewById(R.id.indicator);

        indicator.setViewPager(pager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle1 indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES =images.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                pager.setCurrentItem(currentPage++, true);
            }
        };
//
        //Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }

        });
        ProductTabsAdapter productTabsAdapter = new ProductTabsAdapter(context,getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(productTabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}
