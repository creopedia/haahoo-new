package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.EventsList;
import creo.com.haahooapp.Modal.DownloadPojo;
import creo.com.haahooapp.config.ApiClient;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Events extends AppCompatActivity {
    ImageView back;
    Activity activity = this;
    RecyclerView recyclerView;
    private List<DownloadPojo> pojo = new ArrayList<>();
    Context context = this;
    ArrayList<String> event_name = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        Window window = activity.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        recyclerView = findViewById(R.id.recyclerView);
        back = findViewById(R.id.back);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Events.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Events.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Events.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(Events.this, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Events.this,NewHome.class));
            }
        });

        fetchEvents();

    }

    public  void fetchEvents(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));

        RequestQueue queue = Volley.newRequestQueue(Events.this);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"events/event_order_list/";

        // Request a string response from the provided URL.


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("ordddd", "resp" + obj);
                            JSONArray jsonArray = obj.getJSONArray("data");
                            for(int i=0 ;i<jsonArray.length();i++) {
                               // Log.d("ordddd", "resp" + jsonObject);
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            event_name.add(jsonObject.optString("name"));
                                String images1 = jsonObject.optString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "");
                                ApiClient.evt_img=split;
                                String location = jsonObject.optString("location");
                                String date = jsonObject.optString("register_date");
                                ApiClient.evt_location = location;
                                ApiClient.evt_date = date;
//                                images.add(split);
                            }
                            for(int j=0;j<event_name.size();j++) {
                                DownloadPojo downloadPojo = new DownloadPojo(event_name.get(j));
                                pojo.add(downloadPojo);
                            }
                            EventsList downloadAdapter = new EventsList(pojo,context);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(context));
                            recyclerView.setAdapter(downloadAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Log.d("responseerror", "resp" + error.networkResponse.statusCode);
                //Toast.makeText(Events.this,"//Error",Toast.LENGTH_LONG).show();
                String action;


            }
        }) {
            //adding parameters to the request
           /* @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();
                Bundle extras=getIntent().getExtras();
                String name=extras.getString("order_id");
                Log.d("orderid","mm"+name);
                params.put("order_id", name);
                return params;
            }*/

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };


        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
