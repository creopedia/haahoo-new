package creo.com.haahooapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.Adapters.MyShopAdapter;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.config.ApiClient;
import static android.content.Context.MODE_PRIVATE;

public class MyShopProductsFragment extends Fragment {

    RecyclerView recyclerView;
    ArrayList<Category> categories = new ArrayList<>();
    ProgressDialog progressDialog;
    RelativeLayout noprod;


    public MyShopProductsFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_shop_products,container,false);
       // Toast.makeText(getContext(),"not loading",Toast.LENGTH_SHORT).show();
        recyclerView = view.findViewById(R.id.listview);
        noprod = view.findViewById(R.id.noprod);// getProducts();
        progressDialog = new ProgressDialog(getContext(),R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Loading ..");
        progressDialog.show();
        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL+"virtual_shop/list_virtual_shop/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            //Log.d("jhgghf", "jsonarray" + obj);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            if (jsonArray.length() == 0){
                                progressDialog.dismiss();
                                noprod.setVisibility(View.VISIBLE);
                                noprod.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startActivity(new Intent(getContext(),ShopByCategory.class));
                                    }
                                });
                            }

                            if (jsonArray.length() != 0) {
                                progressDialog.dismiss();
                                noprod.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                               // Log.d("jhgghf", "jsonarray" + obj);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    Category category = new Category();
                                    category.setId(jsonObject.optString("id"));
                                    category.setProduct_id(jsonObject.optString("product_id"));
                                    category.setName(jsonObject.optString("name"));
                                    category.setOriginal_price(jsonObject.optString("orginal_price"));
                                    category.setEdited_price(jsonObject.optString("changed_price"));
                                    category.setReselling_max(jsonObject.optString("re_max_price"));
                                    String images1 = jsonObject.optString("image");
                                    String[] seperated = images1.split(",");
                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    category.setImage(ApiClient.BASE_URL + split);
                                    categories.add(category);
                                    //Log.d("response2wa", "resp" + ApiClient.BASE_URL + split);
                                }
                               // Log.d("ddddddd", "resp" + ApiClient.BASE_URL);
                                recyclerView.setAdapter(new MyShopAdapter(getContext(), categories));
                                recyclerView.setNestedScrollingEnabled(false);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        // Inflate the layout for this fragment
        return view;
    }



    public void getProducts(){


    }
}
