package creo.com.haahooapp;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import creo.com.haahooapp.config.ApiClient;

public class BasicDetails extends Fragment {


    public BasicDetails() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_basic_details,container,false);
        TextView description = view.findViewById(R.id.description);
        description.setText(ApiClient.product_description);
        TextView name = view.findViewById(R.id.name);
        TextView price = view.findViewById(R.id.price);
        name.setText(ApiClient.product_name);
        price.setText(ApiClient.product_price);
        // Inflate the layout for this fragment
        return view;
    }

}