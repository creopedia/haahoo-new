package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.config.ApiClient;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputEditText;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class EditAddress extends AppCompatActivity {
    TextInputEditText name,address,phone_no,landmark,city,state,pincode,area;
    ImageView imageView,back;
    Activity activity = this;
    TextView save;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        Window window = activity.getWindow();
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditAddress.super.onBackPressed();
            }
        });

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        name=findViewById(R.id.name);
        address=findViewById(R.id.address);
        area = findViewById(R.id.area);
        phone_no=findViewById(R.id.phone);
        landmark=findViewById(R.id.landmark);
        city=findViewById(R.id.city);
        state=findViewById(R.id.state);
        pincode=findViewById(R.id.pincode);
        save=findViewById(R.id.save);

        save.setText("Save & Continue");

        getAddressDetails();
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(EditAddress.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(EditAddress.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(EditAddress.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(EditAddress.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().length() < 3){
                    name.setError("Enter a valid name");
                }
                if(phone_no.getText().toString().length() < 10){
                    phone_no.setError("Enter a valid number");
                }
                if(pincode.getText().toString().length() <6 ){
                    pincode.setError("Pincode must be of six digits");
                }
                if(name.getText().toString().length() == 0){
                    name.setError("Name cannot be empty");
                }

                if(address.getText().toString().length() == 0 ){
                    address.setError("Address cannot be empty");
                }
                if (city.getText().toString().length() == 0){
                    city.setError("City cannot be empty");
                }
                if(state.getText().toString().length() == 0){
                    state.setError("State name cannot be empty");
                }
                if(name.getText().toString().length()>=3 && phone_no.getText().toString().length() == 10 && pincode.getText().toString().length() == 6
                        && address.getText().toString().length()!=0 && city.getText().toString().length() !=0 && state.getText().toString().length() !=0)
                {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                  final  String token = (pref.getString("token", ""));
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"booking/address_edit/",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject1 = new JSONObject(response);
                                        Log.d("dsdfgfd","erws"+jsonObject1);
                                        String message = jsonObject1.getString("message");
                                        if(message.equals("Success")){
                                            Toast.makeText(EditAddress.this,"Successfully updated address",Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(EditAddress.this,ChooseAddress.class));
                                        }
                                        if(!(message.equals("Success"))){
                                            Toast.makeText(EditAddress.this,"Some error occured",Toast.LENGTH_SHORT).show();
                                        }

                                    }catch (JSONException e){
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                   // Toast.makeText(EditAddress.this,error.toString(),Toast.LENGTH_LONG).show();
                                }
                            }){
                        @Override
                        protected Map<String,String> getParams(){
                            Map<String,String> params = new HashMap<String, String>();
                            params.put("id",ApiClient.address);
                            params.put("name",name.getText().toString());
                            params.put("house_no",address.getText().toString());
                            params.put("pincode",pincode.getText().toString());
                            params.put("landmark",landmark.getText().toString());
                            params.put("area",area.getText().toString());
                            params.put("city",city.getText().toString());
                            params.put("state",state.getText().toString());
                            params.put("phone_no",phone_no.getText().toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {

                            Map<String, String>  params = new HashMap<String, String>();
                            params.put("Authorization","Token "+token);
                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    requestQueue.add(stringRequest);

                }

            }
        });

    }


    public void getAddressDetails(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
       final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"booking/part_address_view/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);

                            JSONArray jsonObject = jsonObject1.getJSONArray("data");
                            JSONArray jsonObject2 = jsonObject.getJSONArray(0);
                            JSONObject  jsonObject3 = jsonObject2.getJSONObject(0);
                            String fields = jsonObject3.getString("fields");
                            JSONObject jsonObject4 = new JSONObject(fields);
                            name.setText(jsonObject4.optString("name"));
                            phone_no.setText(jsonObject4.optString("phone_no"));
                            address.setText(jsonObject4.optString("house_no"));
                            pincode.setText(jsonObject4.optString("pincode"));
                            landmark.setText(jsonObject4.optString("landmark"));
                            city.setText(jsonObject4.optString("city"));
                            state.setText(jsonObject4.optString("state"));
                            area.setText(jsonObject4.optString("area"));
                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                     //   Toast.makeText(EditAddress.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("address_id",ApiClient.address);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
