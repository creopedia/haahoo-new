package creo.com.haahooapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import creo.com.haahooapp.Adapters.CallAdapter;
import creo.com.haahooapp.Modal.Downloadd;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ItemClickSupport;
import static android.content.Context.MODE_PRIVATE;

public class CallFragment extends Fragment {

    RecyclerView recyclerView;
    List<String> list = new ArrayList<>();
    List<String> name = new ArrayList<>();
    List<String> date = new ArrayList<>();
    List<String> number = new ArrayList<>();
    CallAdapter calladapter;

    public CallFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_call,container,false);
        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setNestedScrollingEnabled(false);
        getDownloads();

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+number.get(position)));
                startActivity(intent);
            }
        });

        return view;
    }

    public void getDownloads(){

        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/login_ref_count/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("refcount","refff"+jsonObject);
                            name.clear();
                            date.clear();
                            String message = jsonObject.optString("message");
                            if(message.equals("Success")){
                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                for(int i=0 ;i<jsonArray.length();i++){
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                    name.add(jsonObject1.optString("name"));
                                    date.add(jsonObject1.optString("join_date"));
                                    number.add(jsonObject1.optString("phone_no"));
                                }

                                final List<Downloadd> downloadPojos = new ArrayList<>();
                                for(int k=0;k<name.size();k++){
                                    Downloadd downloadPojo = new Downloadd(name.get(k),date.get(k),number.get(k));
                                    downloadPojos.add(downloadPojo);
                                }
                                calladapter = new CallAdapter(downloadPojos,getContext());
                                recyclerView.setNestedScrollingEnabled(false);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                recyclerView.setAdapter(calladapter);
                            }
                            if(!(message.equals("Success"))){
                                Toast.makeText(getContext(),"No data found",Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Downloads.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Authorization","Token "+token);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }

}
