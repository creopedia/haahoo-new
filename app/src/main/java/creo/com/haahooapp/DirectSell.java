package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.config.ApiClient;

public class DirectSell extends AppCompatActivity {

    ImageView back;
    EditText name,phone_no;
    TextView proceed;
    Context context = this;
    Activity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direct_sell);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back = findViewById(R.id.back);
        name = findViewById(R.id.name);
        phone_no = findViewById(R.id.phone);
        proceed = findViewById(R.id.continu);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(DirectSell.this,NewHome.class));
            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(name.getText().toString().length() < 3){
                    name.setError("Name must be at least three characters");
                }
                if(phone_no.getText().toString().length() <10){
                    phone_no.setError("Phone number must be 10 digit");
                }

                if(name.getText().toString().length() >=3 && phone_no.getText().toString().length()==10) {
                    ApiClient.sell_phone = phone_no.getText().toString();
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                    final String token = (pref.getString("token", ""));
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"direct_selling/add_buyer/",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String data = jsonObject.getString("data");
                                        ApiClient.directid = data;
                                        String message = jsonObject.optString("message");
                                        if(message.equals("Success")){
//
//                                            Intent intent =new Intent(DirectSell.this,MoreProductSell.class);
//                                            intent.putExtra("id",message);
//                                            startActivity(intent);
                                                //startActivity(new Intent(DirectSell.this,MoreProductSell.class));
                                            Intent intent = new Intent(DirectSell.this,B2B.class);
                                            intent.putExtra("customer_name",name.getText().toString());
                                            startActivity(intent);
                                        }
                                        if(message.equals("Failed")){
                                            Toast.makeText(DirectSell.this,"Unexpected error occured",Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                  //  Toast.makeText(DirectSell.this, error.toString(), Toast.LENGTH_LONG).show();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("name", name.getText().toString());
                            params.put("phone_no", phone_no.getText().toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Authorization", "Token " + token);

                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    requestQueue.add(stringRequest);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(DirectSell.this,NewHome.class));
    }
}
