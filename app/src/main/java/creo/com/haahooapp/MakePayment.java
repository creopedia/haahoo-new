package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import creo.com.haahooapp.config.ApiClient;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class MakePayment extends AppCompatActivity implements PaymentResultListener {

    String products = null;
    String address = null;
    String payment_mode = null;
    String token = null;
    Activity activity = this;
    ProgressDialog progressDialog;
    String bazaar_status = "0";
    String amount="null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_payment);
        Window window = activity.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading...");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Saving Payment Details..");
        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        Bundle bundle = getIntent().getExtras();
        products = bundle.get("products").toString();
        address = bundle.get("address").toString();
        payment_mode = bundle.get("payment_method").toString();
        bazaar_status = bundle.get("bazaar_status").toString();
        token = bundle.get("token").toString();
        startPayment();
    }

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Haahoo");
            options.put("description", "Product Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("currency", "INR");
            if (ApiClient.price.contains("₹ ")) {
                String price[] = ApiClient.price.split("₹ ");

                if (price.length > 1) {

                    options.put("amount", price[1] + "00");
                    amount = price[1];
                }
                if (price.length == 1) {
                    options.put("amount", price[0] + "00");
                    amount = price[0];
                }
            }
            if (ApiClient.price.contains("Rs.")){
                String price[] = ApiClient.price.split("Rs. ");
                if (price.length > 1) {
                    options.put("amount", price[1] + "00");
                    amount = price[1];
                }
                if (price.length == 1) {
                    options.put("amount", price[0] + "00");
                    amount = price[0];
                }
            }
            if (!((ApiClient.price.contains("Rs. ")) || (ApiClient.price.contains("₹ ")))){
               // Log.d("Thejsonstringis ", "dsfs" + ApiClient.price);
                String price[] = ApiClient.price.split("Rs. ");
                if (price.length > 1) {
                    options.put("amount", price[1] + "00");
                    amount = price[1];
                }
                if (price.length == 1) {
                    options.put("amount", price[0] + "00");
                    amount = price[0];
                }
            }
            JSONObject preFill = new JSONObject();
            preFill.put("email", ApiClient.email);
            preFill.put("contact", ApiClient.mobile);
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        progressDialog.show();
        create_order(s);
    }

    @Override
    public void onPaymentError(int i, String s) {
       // Toast.makeText(MakePayment.this, "Cancelled Payment"+s, Toast.LENGTH_LONG).show();
        startActivity(new Intent(MakePayment.this, NewHome.class));
    }

    public void create_order(final String payment_mode) {
        String price[] = ApiClient.price.split("₹ ");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "order_details/place_order/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog.dismiss();
                            startActivity(new Intent(MakePayment.this, OrderSuccess.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(MakePayment.this, "Some error occured", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product", products.toString());
                params.put("address_id", ApiClient.address);
                params.put("payment_method", "ONLINE");
                params.put("payment_status", "1");
                params.put("payment_id", payment_mode);
                params.put("delivery_mode", "HaaHoo Delivery");
                params.put("payment_amount", amount + "00");
                params.put("pdt_refer", ApiClient.referal_id);
                params.put("pdt_refer_id", ApiClient.product_referral_id);
                params.put("bazar_key", bazaar_status);
                Log.d("hvgcgfxgfx", "ghdytdd" + products.toString() + ApiClient.address + bazaar_status + amount);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}