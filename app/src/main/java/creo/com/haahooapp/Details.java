package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONObject;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Implementation.EventViewPresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.EventCallBack;
import creo.com.haahooapp.interfaces.EventView;
import creo.com.haahooapp.interfaces.EventViewPresenter;

public class Details extends AppCompatActivity implements EventCallBack,EventView {
    EventViewPresenter eventViewPresenter;
    Context context=this;
    ImageView imageView,back;
    String image_url = "null";
    ProgressDialog progressDialog;
    TextView title,description,date,place,time,book,price;
    Activity activity = this;
    String event_id = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back = findViewById(R.id.back);
        book = findViewById(R.id.book);
        price = findViewById(R.id.price);
        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Details.this,RegisterEvent.class);
                intent.putExtra("event_id",event_id);
                intent.putExtra("image",image_url);
                startActivity(intent);

                //startActivity(new Intent(Details.this,SeatArrangement.class));

            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Details.super.onBackPressed();
            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Details.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Details.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Details.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(Details.this, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });

        progressDialog = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        showProgressDialogWithTitle("Loading");
        eventViewPresenter = new EventViewPresenterImpl(this);
        imageView = findViewById(R.id.image);
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        date = findViewById(R.id.date);
        //time = findViewById(R.id.time);
        place = findViewById(R.id.place);


//
        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("id");
        event_id = bundle.getString("id");
        eventViewPresenter.getEventDetail(id);

    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }
    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void success(JSONArray jsonArray) {

    }

    @Override
    public void failed(String msg) {

    }

    @Override
    public void onSuccess(JSONArray data) {
        try{

            JSONArray jsonArray = data;
            Log.d("dataaa","datas"+jsonArray);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            JSONObject jsonObject1  = jsonObject.getJSONObject("fields");
            String name = jsonObject1.getString("name");
            String image = jsonObject1.getString("image");
            String description1 = jsonObject1.getString("description");
            String date1 = jsonObject1.getString("date");
            String time1 = jsonObject1.getString("time");
            String place1 = jsonObject1.getString("location");
            String price1 = jsonObject1.optString("price");
            ApiClient.eventprice = price1;
            ApiClient.event_price = price1+"00";
            String[] seperated = image.split(",");
            String split = seperated[0].replace("[","");
            title.setText(name);
            ApiClient.event_name = name;
            description.setText(description1);
            date.setText("Event Date : "+date1);
            price.setText("Amount : "+price1);
            ApiClient.event_time = time1;
            ApiClient.event_location = "Venue : "+place1;
            //time.setText(time1);
            place.setText("Venue : "+place1);
            Glide.with(context).load(ApiClient.BASE_URL+"media/" + split).into(imageView);
            image_url = ApiClient.BASE_URL+"media/"+split;
            hideProgressDialogWithTitle();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onFailed(String data) {
       // Toast.makeText(Details.this,"Server failed",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
