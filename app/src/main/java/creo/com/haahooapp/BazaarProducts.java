package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.OnlineMartProductAdapter;
import creo.com.haahooapp.Adapters.ResellingCategoryAdapter;
import creo.com.haahooapp.Modal.HorizontalPojo;
import creo.com.haahooapp.Modal.ResellingPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.ItemClick;

public class BazaarProducts extends AppCompatActivity implements ResellingProducts.OnDataPassedListener, ItemClick {

    ImageView back;
    Activity activity = this;
    RecyclerView recyclerview, subcategories;
    Context context = this;
    ArrayList<ResellingPojo> pojos = new ArrayList<>();
    OnlineMartProductAdapter resellingAdapter;
    CardView card;
    ProgressDialog progressDialog;
    ResellingCategoryAdapter resellingCategoryAdapter;
    List<HorizontalPojo> productFeaturepojos = new ArrayList<>();
    String nexturl = ApiClient.BASE_URL + "api_shop_app/view_bazar/";
    TextView loadmore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bazaar_products);
        back = findViewById(R.id.back);
        subcategories = findViewById(R.id.subcategories);
        recyclerview = findViewById(R.id.recyclerview);
        Window window = activity.getWindow();
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait while the data is loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        loadmore = findViewById(R.id.laodmore);
        progressDialog.setCancelable(true);
        progressDialog.show();
        card = findViewById(R.id.card);
        resellingCategoryAdapter = new ResellingCategoryAdapter(productFeaturepojos, context, this);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              BazaarProducts.super.onBackPressed();
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
        getcategories();
        getResellingProducts();
    }
    public void getcategories() {
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/list_shop_cat/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("categories", "bncgbc" + obj);
                            progressDialog.dismiss();
                            JSONArray data = obj.optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = data.optJSONObject(i);
                                HorizontalPojo horizontalPojo = new HorizontalPojo();
                                horizontalPojo.setId(jsonObject.optString("id"));
                                horizontalPojo.setName(jsonObject.optString("name"));
                                horizontalPojo.setImage(ApiClient.BASE_URL + jsonObject.optString("image").replace("[", "").replace("]", "").trim());
                                productFeaturepojos.add(horizontalPojo);
                            }

                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                            subcategories.setLayoutManager(horizontalLayoutManagaer);
                            subcategories.setAdapter(resellingCategoryAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                //

            }
        }) {

            @Override
            public java.util.Map<String, String> getHeaders() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void getResellingProducts() {

        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/reselling_pdts/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, nexturl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            progressDialog.dismiss();
                            nexturl = obj.optString("next");
                            Log.d("RESELLLL","bjvjhc"+response);
                            if (!(nexturl.equals("null"))) {
                                card.setVisibility(View.VISIBLE);
                            }
                            loadmore.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (!(nexturl.equals("null"))) {
                                        progressDialog.setTitle("Loading");
                                        progressDialog.setMessage("Please wait while the data is loading");
                                        progressDialog.setIcon(R.drawable.haahoologo);
                                        progressDialog.setCancelable(true);
                                        progressDialog.show();
                                        getResellingProducts();
                                    }
                                    if (nexturl.equals("null")) {
                                        Toast.makeText(context, "No more data", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            JSONArray jsonArray = obj.optJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                ResellingPojo resellingPojo = new ResellingPojo();
                                resellingPojo.setId(jsonObject.optString("product_id"));
                                resellingPojo.setShop_id(jsonObject.optString("shop_id"));
                                String images = jsonObject.optString("pdt_image");
                                String[] seperated = images.split(",");
                                resellingPojo.setImage(seperated[0].replace("[", "").replace("]", ""));
                                resellingPojo.setProduct_name(jsonObject.optString("pdt_name"));
                                resellingPojo.setWholesale_price(jsonObject.optString("baazar_amount").trim());
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setResell(jsonObject.optString("resell"));
                                resellingPojo.setResell_count(jsonObject.optString("resell_count"));
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setMin_wholesale_qty(jsonObject.optString("minimum_baazar_quantity"));
                                resellingPojo.setDelivery_expense(jsonObject.optString("delivery_expense"));
                                pojos.add(resellingPojo);
                            }
                            resellingAdapter = new OnlineMartProductAdapter(context, pojos);
                            recyclerview.setAdapter(resellingAdapter);
                            recyclerview.setNestedScrollingEnabled(false);
                            recyclerview.setLayoutManager(new GridLayoutManager(context, 2));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() throws AuthFailureError {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("resell_value","0");
                return params;
            }

            @Override
            public java.util.Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onDataPassed(String text) {

    }

    @Override
    public void addProduct(String productid, String price1, String edited_price) {

    }

    @Override
    public void sendData(String id) {
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait while the data is loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(true);
        progressDialog.show();
        loadProducts(id);
    }
    public void loadProducts(String categoryid) {

        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/view_reselling_products_under_category/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("response555", "resp" + response);
                            pojos.clear();
                            // url1 = obj.optString("next");
                            JSONArray jsonArray = obj.optJSONArray("data");
                            if (jsonArray.length() == 0) {
                                progressDialog.dismiss();
                                Toast.makeText(context, "No products under this category", Toast.LENGTH_SHORT).show();
                            }
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                ResellingPojo resellingPojo = new ResellingPojo();
                                resellingPojo.setId(jsonObject.optString("product_id"));
                                resellingPojo.setShop_id(jsonObject.optString("shop_id"));
                                String images = jsonObject.optString("pdt_image");
                                String[] seperated = images.split(",");
                                resellingPojo.setImage(seperated[0].replace("[", "").replace("]", ""));
                                resellingPojo.setProduct_name(jsonObject.optString("pdt_name"));
                                resellingPojo.setWholesale_price(jsonObject.optString("baazar_amount").trim());
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setResell(jsonObject.optString("resell"));
                                resellingPojo.setResell_count(jsonObject.optString("resell_count"));
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setMin_wholesale_qty(jsonObject.optString("minimum_baazar_quantity"));
                                resellingPojo.setDelivery_expense(jsonObject.optString("delivery_expense"));
                                pojos.add(resellingPojo);
                            }
                            resellingAdapter = new OnlineMartProductAdapter(context, pojos);
                            recyclerview.setAdapter(resellingAdapter);
                            recyclerview.setNestedScrollingEnabled(false);
                            recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
                            progressDialog.dismiss();

                        } catch (JSONException | NullPointerException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
                progressDialog.dismiss();
            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("category_id", categoryid);
                params.put("resell_value", "0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };


        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}