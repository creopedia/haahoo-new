package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.List;
import creo.com.haahooapp.Modal.ProductPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class ImageAdapter extends BaseAdapter {
    public List<ProductPojo> productPojos;
    Context context1 ;

    public ImageAdapter(Context context, List<ProductPojo> productPojo) {
        this.context1 = context;
        this.productPojos = productPojo;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context1
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;
        gridView = new View(context1);
        // get layout from mobile.xml
        gridView = (View) convertView;
        gridView = inflater.inflate(R.layout.more_products_adapter, null);
        // set value into textview
        TextView name = (TextView) gridView
                .findViewById(R.id.name);
        TextView price = gridView.findViewById(R.id.price);
        // set image based on selected text
        ImageView imageView = (ImageView) gridView
                .findViewById(R.id.icon);
        String pdt_name = productPojos.get(position).getName();
        name.setText(pdt_name);
        price.setText(productPojos.get(position).getDescription());
        Glide.with(context1).load(ApiClient.BASE_URL+"media/"+productPojos.get(position).getImage()).into(imageView);

        return gridView;
    }

    @Override
    public int getCount() {
        return productPojos.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    public void updateData(List<ProductPojo> viewModels) {
        productPojos.clear();
        productPojos.addAll(viewModels);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return productPojos.size();
    }

}