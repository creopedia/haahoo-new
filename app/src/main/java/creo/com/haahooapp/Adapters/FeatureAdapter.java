package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;
import creo.com.haahooapp.Modal.ProductFeaturepojo;
import creo.com.haahooapp.R;

public class FeatureAdapter extends RecyclerView.Adapter<FeatureAdapter.ViewHolder> {

    public List<ProductFeaturepojo> productFeaturepojos;
    Context context1 ;

    public FeatureAdapter(List<ProductFeaturepojo> productFeaturepojos,Context context) {
        this.productFeaturepojos = productFeaturepojos;
        this.context1 = context;
    }

    @Override
    public FeatureAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.product_key_features, parent, false);
        final FeatureAdapter.ViewHolder viewHolder = new FeatureAdapter.ViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FeatureAdapter.ViewHolder holder, int position) {

        holder.key1.setText(productFeaturepojos.get(position).getFeature_name());

    }

    @Override
    public int getItemCount() {
        return productFeaturepojos.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView dot;
        public TextView key1;

        public ViewHolder(View itemView) {
            super(itemView);
            this.key1 =  itemView.findViewById(R.id.key1);
            this.dot = itemView.findViewById(R.id.dot2);
        }

    }

}
