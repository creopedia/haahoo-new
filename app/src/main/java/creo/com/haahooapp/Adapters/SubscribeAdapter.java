package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Response;

import java.util.ArrayList;

import creo.com.haahooapp.Modal.SubscriptionPlanPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.SubscriptionPlans;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.Recycler;

public class SubscribeAdapter extends RecyclerView.Adapter<SubscribeAdapter.MyViewHolder>  {

    private LayoutInflater inflater;
    private ArrayList<SubscriptionPlanPojo> dataModelArrayList;
    private Recycler recycler;
    public Context context1 ;
    public boolean clicked;
    private int index=-1;

    public SubscribeAdapter(Context ctx, ArrayList<SubscriptionPlanPojo> dataModelArrayList, Recycler callback){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        this.recycler=callback;

    }


    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    @Override
    public SubscribeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.subscribe_adapter, parent, false);
        SubscribeAdapter.MyViewHolder holder = new SubscribeAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(SubscribeAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(dataModelArrayList.get(position).getName());
        holder.amount.setText("INR "+dataModelArrayList.get(position).getAmount());
        holder.benefits.setText("Benefits : "+dataModelArrayList.get(position).getBenefits());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index=position;
                setClicked(true);
                notifyDataSetChanged();
            }
        });

        if(index==position){
            Resources res = context1.getResources();
            Drawable drawable = res.getDrawable(R.drawable.subscribe_border);
            ApiClient.subscribe_amount = dataModelArrayList.get(position).getAmount();
            recycler.onClick(dataModelArrayList.get(position).getName());
           // SubscriptionPlans.view_visible(dataModelArrayList.get(position).getName());
            holder.relativeLayout.setBackground(drawable);
        }

        if(index!=position){
            Resources res = context1.getResources();
            Drawable drawable = res.getDrawable(R.drawable.border_relative_complete);
            holder.relativeLayout.setBackground(drawable);
        }

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder{

    RelativeLayout relativeLayout;
    TextView name,amount,benefits;

        public MyViewHolder(View itemView) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.relative);
            name = itemView.findViewById(R.id.save);
            amount = itemView.findViewById(R.id.price);
            benefits = itemView.findViewById(R.id.refund);
        }

    }





  }