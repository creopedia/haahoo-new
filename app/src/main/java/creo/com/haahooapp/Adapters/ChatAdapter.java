package creo.com.haahooapp.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import creo.com.haahooapp.Modal.ChatPojo;
import creo.com.haahooapp.R;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    public List<ChatPojo> productPojo;
    Context context1 ;

    public ChatAdapter(List<ChatPojo> productPojo,Context context) {
        this.productPojo = productPojo;
        this.context1 = context;
    }

    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_message_sent, parent, false);
        ChatAdapter.ViewHolder viewHolder = new ChatAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChatAdapter.ViewHolder holder, int position) {
        if(productPojo.get(position).isIsmine() == true) {
            holder.name.setVisibility(View.VISIBLE);
            holder.name.setText(productPojo.get(position).getMessage());
            holder.price.setVisibility(View.GONE);
        }
        if(productPojo.get(position).isIsmine() == false){
            holder.price.setVisibility(View.VISIBLE);
            holder.price.setText(productPojo.get(position).getMessage());
            holder.name.setVisibility(View.GONE);
        }
//        holder.imageView.setImageResource((Glide.with(context).load(productPojo[position].getImage())));
        //  Glide.with(context1).load("https://hahooapi.herokuapp.com/media/"+productPojo.get(position).getImage()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return productPojo.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name,price;

        public ViewHolder(View itemView) {
            super(itemView);

            this.name =  itemView.findViewById(R.id.msg);
            this.price = itemView.findViewById(R.id.msg1);

        }

    }

}
