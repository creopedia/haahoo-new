package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

import creo.com.haahooapp.Modal.DownloadPojo;
import creo.com.haahooapp.Modal.Downloadd;
import creo.com.haahooapp.R;

public class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.ViewHolder> {

    public List<Downloadd> downloadPojos;
    Context context1 ;

    public CoinAdapter(List<Downloadd> productPojo, Context context) {
        this.downloadPojos = productPojo;
        this.context1 = context;
    }

    @Override
    public CoinAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.coin_adapter, parent, false);
        CoinAdapter.ViewHolder viewHolder = new CoinAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CoinAdapter.ViewHolder holder, int position) {

        holder.textView.setText(downloadPojos.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return downloadPojos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView,date,amount;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView =  itemView.findViewById(R.id.text);
            this.date = itemView.findViewById(R.id.date);
            this.amount = itemView.findViewById(R.id.amount);
        }

    }

}