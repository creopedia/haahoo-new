package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Api;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import creo.com.haahooapp.B2B;
import creo.com.haahooapp.Cart;
import creo.com.haahooapp.Modal.CartPojo;
import creo.com.haahooapp.Modal.ProductB2BPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class B2BListAdapter extends RecyclerView.Adapter<B2BListAdapter.ViewHolder>{

    public int quantity = 0;
    public List<ProductB2BPojo> productPojo;
    public Context context1 ;

    public B2BListAdapter(List<ProductB2BPojo> productPojo,Context context) {
        this.productPojo = productPojo;
        this.context1 = context;
    }

    @Override
    public B2BListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final View listItem= layoutInflater.inflate(R.layout.b2blist_adapter, parent, false);
        final B2BListAdapter.ViewHolder viewHolder = new B2BListAdapter.ViewHolder(listItem);
//        viewHolder.plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                viewHolder.minus.setVisibility(View.VISIBLE);
//                quantity++;
//                viewHolder.add.setText(String.valueOf(quantity));
//            }
//        });
//        viewHolder.minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                quantity--;
//                viewHolder.add.setText(String.valueOf(quantity));
//            }
//        });

        return viewHolder;
    }



    @Override
    public void onBindViewHolder(B2BListAdapter.ViewHolder holder, int position) {

       holder.product_name.setText(productPojo.get(position).getProduct_name());
       quantity = Integer.parseInt(productPojo.get(position).getQuantity());
       holder.price.setText(productPojo.get(position).getPrice());
       ApiClient.directsell_cartcount = String.valueOf(quantity);
       Glide.with(context1).load(productPojo.get(position).getImage()).into(holder.imageView);
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quantity>=0) {

                    holder.minus.setVisibility(View.VISIBLE);
                    quantity = Integer.parseInt(productPojo.get(position).getQuantity()) + 1;
                    ApiClient.directsell_cartcount = String.valueOf(quantity);
                    productPojo.get(position).setQuantity(String.valueOf(quantity));
                    holder.add.setText(productPojo.get(position).getQuantity());
                    ApiClient.productB2BPojo = productPojo;
//                ApiClient.ids.add(productPojo.get(position).getId());
//                ApiClient.quantity.add(holder.add.getText().toString());
//                Log.d("beffrrr","hgghfgh"+ApiClient.quantity.get(position));
                    // if (!(ApiClient.ids.contains(productPojo.get(position).getId()))){

                    ApiClient.ids.add(productPojo.get(position).getId());
                    ApiClient.quantity.add(holder.add.getText().toString());
                }
                   // Toast.makeText(context1,"gfghf"+ApiClient.productB2BPojo.get(position).getPrice()+productPojo.get(position).getQuantity(),Toast.LENGTH_SHORT).show();
               // }
//                Log.d("hbhjbvjhbjh","jjbhjbbhj"+ApiClient.quantity.get(position)+productPojo.get(position).getQuantity());
//                if (ApiClient.ids.contains(productPojo.get(position).getId())){
//                    ApiClient.ids.remove(productPojo.get(position).getId());
//                    ApiClient.prices.remove(productPojo.get(position).getPrice());
//                    ApiClient.quantity.remove(productPojo.get(position).getQuantity());
//                    ApiClient.name.remove(productPojo.get(position).getProduct_name());
//                    ApiClient.images.remove(productPojo.get(position).getImage());
//
//                    ApiClient.ids.add(productPojo.get(position).getId());
//                    ApiClient.prices.add(productPojo.get(position).getPrice());
//                    ApiClient.quantity.add(productPojo.get(position).getQuantity());
//                    ApiClient.name.add(productPojo.get(position).getProduct_name());
//                    ApiClient.images.add(productPojo.get(position).getImage());
//                    Toast.makeText(context1,"Already present"+productPojo.get(position).getQuantity(),Toast.LENGTH_SHORT).show();
//                }
//                if (!(ApiClient.ids.contains(productPojo.get(position).getId()))) {
//                    ApiClient.ids.add(productPojo.get(position).getId());
//                    ApiClient.prices.add(productPojo.get(position).getPrice());
//                    ApiClient.quantity.add(productPojo.get(position).getQuantity());
//                    ApiClient.name.add(productPojo.get(position).getProduct_name());
//                    ApiClient.images.add(productPojo.get(position).getImage());
//                }
               // Log.d("positionnnn","posss"+position+productPojo.get(position).getQuantity());
            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quantity>0) {

                    quantity = Integer.parseInt(productPojo.get(position).getQuantity())-1;
                    ApiClient.directsell_cartcount = String.valueOf(quantity);
                    productPojo.get(position).setQuantity(String.valueOf(quantity));
                    holder.add.setText(productPojo.get(position).getQuantity());
                    ApiClient.productB2BPojo = productPojo;

                }
                if (quantity==0){
                    ApiClient.directsell_cartcount = String.valueOf(quantity);
                    holder.add.setText("ADD");
                    holder.minus.setVisibility(View.GONE);
                    productPojo.get(position).setQuantity("0");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return productPojo.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        TextView product_name,minus,add,plus;
        public TextView name,price;

        public ViewHolder(final View itemView) {
            super(itemView);
            this.imageView =  itemView.findViewById(R.id.image);
            this.price = itemView.findViewById(R.id.price);
            this.product_name = itemView.findViewById(R.id.product_name);
            this.minus = itemView.findViewById(R.id.minus);
            this.add = itemView.findViewById(R.id.add);
            this.plus = itemView.findViewById(R.id.plus);


        }

    }

}
