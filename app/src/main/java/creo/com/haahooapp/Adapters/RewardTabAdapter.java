package creo.com.haahooapp.Adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import creo.com.haahooapp.EarnedFragment;
import creo.com.haahooapp.OfferFragment;

public class RewardTabAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public RewardTabAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                EarnedFragment homeFragment = new EarnedFragment();
                return homeFragment;

            case 1:
                OfferFragment sportFragment = new OfferFragment();
                return sportFragment;

            default:
                return null;

        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}