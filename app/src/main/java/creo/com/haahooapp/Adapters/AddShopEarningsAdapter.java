package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import creo.com.haahooapp.Modal.AddShopData;
import creo.com.haahooapp.R;

public class AddShopEarningsAdapter extends RecyclerView.Adapter<AddShopEarningsAdapter.ViewHolder> {
    ArrayList<AddShopData> newArrayList;
    Context context;

    public AddShopEarningsAdapter(ArrayList<AddShopData> newArrayList, Context context) {
        this.newArrayList = newArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AddShopEarningsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.earning_adapter, parent, false);
        AddShopEarningsAdapter.ViewHolder viewHolder = new AddShopEarningsAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AddShopEarningsAdapter.ViewHolder holder, int position) {
        final AddShopData addShopData = newArrayList.get(position);
        holder.name.setText(addShopData.getUser_name());
        holder.amount.setText(addShopData.getWallet_amount());
        holder.status.setText(addShopData.getEarning_status());
    }

    @Override
    public int getItemCount() {
        return newArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, amount, status;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            amount = itemView.findViewById(R.id.amount);
            status = itemView.findViewById(R.id.date);
        }
    }
}
