package creo.com.haahooapp.Adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import creo.com.haahooapp.OnlineMart;
import creo.com.haahooapp.ResaleMart;

public class ResellingTabAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public ResellingTabAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                OnlineMart onlineMart = new OnlineMart();
                return onlineMart;

            case 1:
                ResaleMart resaleMart = new ResaleMart();
                return resaleMart;

            default:
                return null;

        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}