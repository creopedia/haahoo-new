package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import creo.com.haahooapp.Modal.HorizontalPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.interfaces.ItemClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ResellingCategoryAdapter extends RecyclerView.Adapter<ResellingCategoryAdapter.ViewHolder> {

    public List<HorizontalPojo> productFeaturepojos;
    Context context1;
    ItemClick itemClick;

    public ResellingCategoryAdapter(List<HorizontalPojo> productFeaturepojos, Context context, ItemClick itemClick) {
        this.productFeaturepojos = productFeaturepojos;
        this.context1 = context;
        this.itemClick = itemClick;
    }

    @Override
    public ResellingCategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.horizontal_item, parent, false);
        final ResellingCategoryAdapter.ViewHolder viewHolder = new ResellingCategoryAdapter.ViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ResellingCategoryAdapter.ViewHolder holder, int position) {
        try {
            holder.name.setText(productFeaturepojos.get(position).getName());
            Glide.with(context1).load(productFeaturepojos.get(position).getImage()).into(holder.image);
            holder.base.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.sendData(productFeaturepojos.get(position).getId());
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return productFeaturepojos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        RelativeLayout base;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            base = itemView.findViewById(R.id.base);

        }

    }


}