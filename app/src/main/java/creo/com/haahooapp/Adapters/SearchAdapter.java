package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.List;
import creo.com.haahooapp.Modal.ProductPojo;
import creo.com.haahooapp.ProductDetails;
import creo.com.haahooapp.ProductDetailsShop;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    public List<ProductPojo> productPojos;
    Context context1 ;

    public SearchAdapter(Context context, List<ProductPojo> productPojo) {
        this.context1 = context;
        this.productPojos = productPojo;
    }

    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.search_adapter, parent, false);
        SearchAdapter.ViewHolder viewHolder = new SearchAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SearchAdapter.ViewHolder holder, int position) {

        holder.name.setText(productPojos.get(position).getName());
        holder.price.setText(productPojos.get(position).getDescription());
        holder.rating.setRating(Float.parseFloat(productPojos.get(position).getRating()));
        holder.percent.setText(productPojos.get(position).getPercent());
        holder.discount.setText(productPojos.get(position).getDiscount());

//        if (ApiClient.cart_status.get(position).equals("1")){
//            holder.ft.setVisibility(View.GONE);
//            holder.viewrelative.setVisibility(View.VISIBLE);
//            holder.first.setVisibility(View.GONE);
//            holder.divider22.setVisibility(View.VISIBLE);
//            holder.divider19.setVisibility(View.GONE);
//        }
//
//        if (ApiClient.cart_status.get(position).equals("0")){
//            holder.ft.setVisibility(View.VISIBLE);
//            holder.viewrelative.setVisibility(View.GONE);
//            holder.first.setVisibility(View.VISIBLE);
//            holder.divider19.setVisibility(View.VISIBLE);
//            holder.divider22.setVisibility(View.GONE);
//        }

        Glide.with(context1).load(ApiClient.BASE_URL+productPojos.get(position).getImage()).into(holder.icon);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApiClient.featured.get(position).equals("1")){
                    Intent intent = new Intent(context1,ProductDetails.class);
                    intent.putExtra("id",ApiClient.searchids.get(position));
                    context1.startActivity(intent);
                }
                if (ApiClient.featured.get(position).equals("0")){
                    Intent  intent = new Intent(context1, ProductDetailsShop.class);
                    intent.putExtra("shopid",ApiClient.shop_id.get(position));
                    intent.putExtra("productid",ApiClient.searchids.get(position));
                    context1.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return productPojos.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name,price;
        public RelativeLayout relativeLayout,ft,viewrelative,first;
        public ImageView icon;
        public View divider19,divider22;
        public RatingBar rating;
        public TextView percent,discount;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name =  itemView.findViewById(R.id.name);
            this.price = itemView.findViewById(R.id.price);
            this.relativeLayout = itemView.findViewById(R.id.linear);
            this.icon = itemView.findViewById(R.id.icon);
            this.ft = itemView.findViewById(R.id.ft);
            this.viewrelative = itemView.findViewById(R.id.viewrelative);
            this.percent = itemView.findViewById(R.id.off);
            this.discount = itemView.findViewById(R.id.original_price);
            this.first = itemView.findViewById(R.id.first);
            this.divider19 = itemView.findViewById(R.id.divider19);
            this.divider22 = itemView.findViewById(R.id.divider22);
            this.rating = itemView.findViewById(R.id.rating);
        }

    }

}