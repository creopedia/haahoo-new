package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import creo.com.haahooapp.Modal.FeaturePojoValues;
import creo.com.haahooapp.Modal.Spec_header_values;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class FilterValueAdapter extends RecyclerView.Adapter<FilterValueAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<FeaturePojoValues> dataModelArrayList;
    public boolean clicked;
    public Context context1 ;
    private int index=-1;
    JSONArray arr = new JSONArray();
    JSONObject products = new JSONObject();

    public FilterValueAdapter(Context ctx, ArrayList<FeaturePojoValues> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;

    }

    public boolean isClicked() {
        return clicked;
    }

    @Override
    public FilterValueAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.filter_value_adapter, parent, false);
        FilterValueAdapter.MyViewHolder holder = new FilterValueAdapter.MyViewHolder(view);

        return holder;
    }
    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    @Override
    public void onBindViewHolder(FilterValueAdapter.MyViewHolder holder, final int position) {
//
        holder.category.setText(dataModelArrayList.get(position).getName().replace("\"",""));
        Spec_header_values spec_header_values = new Spec_header_values();
        spec_header_values.getSpec_value();

        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.checkbox.isChecked()){
                    ApiClient.hashMap.put(dataModelArrayList.get(position).getHeader()+String.valueOf(position),dataModelArrayList.get(position).getName().replace("\"",""));
                    try {

                        List<String> list = new ArrayList<String>();

                        if (!(ApiClient.map.containsKey(dataModelArrayList.get(position).getHeader()))){
                            ApiClient.map.put(dataModelArrayList.get(position).getHeader(), list);
                        }

                        if (ApiClient.map.containsKey(dataModelArrayList.get(position).getHeader())) {
                            ApiClient.map.get(dataModelArrayList.get(position).getHeader()).add(dataModelArrayList.get(position).getName().replace("\"",""));
                        }
                        JSONObject jsonObject = new JSONObject(ApiClient.map);
//                    JSONObject obj=new JSONObject(ApiClient.listdata_head,ApiClient.listdata);
//
//                        JSONArray array=new JSONArray("["+obj.toString()+"]");
                        Log.d("Thejsonstringis ", "dsfs" +jsonObject);
                    }catch (Exception f){
                        f.printStackTrace();
                    }
                    HashMap<String, JSONObject> map = new HashMap<String, JSONObject>();
                   try{
                            JSONObject json = new JSONObject();
                            json.put(dataModelArrayList.get(position).getHeader(),ApiClient.hashMap.get(dataModelArrayList.get(position).getHeader()));

                            map.put("json" + position, json);
                            arr.put(map.get("json" + position));

                        products.put("product", arr);

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
//                if (!(holder.checkbox.isChecked())){
//                   // Toast.makeText(context1,"untick",Toast.LENGTH_SHORT).show();
//                    for (Map.Entry<String, List<String>> entry : ApiClient.map.entrySet()) {
//                        entry.getValue().remove(dataModelArrayList.get(position).getName());
//                    }
//                    JSONObject jsonObject = new JSONObject(ApiClient.map);
//                    Log.d("Thejsonstringistrety ", "dsfs" + jsonObject);
//                }

//                if (!(holder.checkbox.isChecked())){
//                    ApiClient.listdata.remove(dataModelArrayList.get(position).getName().replace("\"",""));
//                    ApiClient.listdata_head.remove(dataModelArrayList.get(position).getHeader());
//
//                    HashMap<String, JSONObject> map = new HashMap<String, JSONObject>();
//                    try {
//                        for (int i = 0; i < ApiClient.listdata.size(); i++) {
//                            JSONObject json = new JSONObject();
//                            json.put(ApiClient.listdata_head.get(i),ApiClient.listdata.get(i));
//                            map.put("json" + i, json);
//                            arr.put(map.get("json" + i));
//                        }
//                        products.put("product", arr);
//
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//                if (!holder.checkbox.isChecked()){
//                   // ApiClient.hashMap.remove(dataModelArrayList.get(position).getHeader()+String.valueOf(position),dataModelArrayList.get(position).getName());
//                    Log.d("hashmap","hjhf"+new Gson().toJson(ApiClient.hashMap));
//                }
            }
        });
        if (ApiClient.hashMap.containsKey(dataModelArrayList.get(position).getHeader()+String.valueOf(position))){
            Log.d("hjvghf","mhvgcxgfx");
            holder.checkbox.setChecked(true);
        }


    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView category;
        CheckBox checkbox;

        public MyViewHolder(View itemView) {
            super(itemView);
            category = itemView.findViewById(R.id.category);
            checkbox = itemView.findViewById(R.id.checkbox);

        }

    }
}