package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import creo.com.haahooapp.Modal.ViewersPojo;
import creo.com.haahooapp.R;

public class ViewersAdapter extends RecyclerView.Adapter<ViewersAdapter.ViewHolder> {
    public List<ViewersPojo> downloadPojos;
    Context context1;

    public ViewersAdapter(List<ViewersPojo> productPojo, Context context) {
        this.downloadPojos = productPojo;
        this.context1 = context;
    }

    @Override
    public ViewersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.viewers_adapter, parent, false);
        ViewersAdapter.ViewHolder viewHolder = new ViewersAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewersAdapter.ViewHolder holder, int position) {

        holder.textView.setText(downloadPojos.get(position).getName());
        holder.time.setText(downloadPojos.get(position).getTime());
    }

    @Override
    public int getItemCount() {
        return downloadPojos.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView, time;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(R.id.name);
            this.time = itemView.findViewById(R.id.time);
        }

    }
}
