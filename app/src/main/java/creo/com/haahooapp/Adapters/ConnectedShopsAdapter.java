package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import creo.com.haahooapp.Modal.ConnectedShops;
import creo.com.haahooapp.ProductsInShop;
import creo.com.haahooapp.R;

public class ConnectedShopsAdapter extends RecyclerView.Adapter<ConnectedShopsAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ConnectedShops> dataModelArrayList;
    public Context context1 ;

    public ConnectedShopsAdapter(Context ctx, ArrayList<ConnectedShops> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;

    }

    @Override
    public ConnectedShopsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.connected_shops_adapter, parent, false);
        ConnectedShopsAdapter.MyViewHolder holder = new ConnectedShopsAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ConnectedShopsAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(dataModelArrayList.get(position).getName());
        Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);
        holder.rating.setText(dataModelArrayList.get(position).getRating());
        holder.rating1.setRating(Float.parseFloat(dataModelArrayList.get(position).getRating()));
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1, ProductsInShop.class);
                intent.putExtra("categoryid",dataModelArrayList.get(position).getId());
                intent.putExtra("categoryname",dataModelArrayList.get(position).getName());
                context1.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView  name, rating;
        CardView card;
        ImageView iv;
        RatingBar rating1;

        public MyViewHolder(View itemView) {
            super(itemView);
          iv = itemView.findViewById(R.id.image);
          name = itemView.findViewById(R.id.name);
          rating = itemView.findViewById(R.id.rating);
          card = itemView.findViewById(R.id.card);
          rating1 = itemView.findViewById(R.id.rating1);
        }

    }
}