package creo.com.haahooapp.Adapters;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;

import java.util.List;

import creo.com.haahooapp.Modal.Grocery;
import creo.com.haahooapp.ProductsInShop;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class NewHomeGrocery extends RecyclerView.Adapter<NewHomeGrocery.MyViewHolder> {

    private LayoutInflater inflater;
    private List<Grocery> dataModelArrayList;
    public Context context1;
    ClipData myClip;
    ClipboardManager clipboard;

    public NewHomeGrocery(Context ctx, List<Grocery> dataModelArrayList) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        clipboard = (ClipboardManager) context1.getSystemService(Context.CLIPBOARD_SERVICE);
    }

    @Override
    public NewHomeGrocery.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.home_grocery, parent, false);
        NewHomeGrocery.MyViewHolder holder = new NewHomeGrocery.MyViewHolder(view);

        return holder;

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(NewHomeGrocery.MyViewHolder holder, final int position) {

        Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);
        holder.name.setText(dataModelArrayList.get(position).getName().substring(0,1).toUpperCase()+dataModelArrayList.get(position).getName()
        .substring(1).toLowerCase());
        holder.price.setText(dataModelArrayList.get(position).getPrice());
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1, ProductsInShop.class);
                intent.putExtra("categoryid", dataModelArrayList.get(position).getShop_id());
                intent.putExtra("categoryname", dataModelArrayList.get(position).getShop_name());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context1.startActivity(intent);
            }
        });
        holder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1, ProductsInShop.class);
                intent.putExtra("categoryid", dataModelArrayList.get(position).getShop_id());
                intent.putExtra("categoryname", dataModelArrayList.get(position).getShop_name());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context1.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView price, name, qty, delivery;
        LinearLayout linearLayout;
        ImageView iv, copy;
        MaterialCardView rel,card;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            rel = itemView.findViewById(R.id.rel);
            card = itemView.findViewById(R.id.card);

        }

    }
}