package creo.com.haahooapp.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.List;
import creo.com.haahooapp.Modal.CartPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.WishList;
import creo.com.haahooapp.config.ApiClient;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.ViewHolder> {

    public List<CartPojo> productPojo;
    Context context1 ;

    public WishListAdapter(List<CartPojo> productPojo,Context context) {
        this.productPojo = productPojo;
        this.context1 = context;
    }

    @Override
    public WishListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.wishlist_adapter, parent, false);
        final WishListAdapter.ViewHolder viewHolder = new WishListAdapter.ViewHolder(listItem);
        viewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context1 instanceof WishList) {
                    ((WishList)context1).wishlist_remove(ApiClient.wishlist_ids.get(viewHolder.getAdapterPosition()));
                }
            }
        });

        viewHolder.movetocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context1 instanceof  WishList){
                    ((WishList)context1).movetocart(ApiClient.wishlist_ids.get(viewHolder.getAdapterPosition()));
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(WishListAdapter.ViewHolder holder, int position) {


        holder.name.setText(productPojo.get(position).getName());
        holder.price.setText(productPojo.get(position).getPrice());
        holder.quantity.setText(productPojo.get(position).getQuantity());
//        holder.imageView.setImageResource((Glide.with(context).load(productPojo[position].getImage())));
        Glide.with(context1).load(productPojo.get(position).getImage()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return productPojo.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView name,price,movetocart,quantity,remove;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView =  itemView.findViewById(R.id.img);
            this.remove = itemView.findViewById(R.id.remove);
            this.name =  itemView.findViewById(R.id.title);
            this.price = itemView.findViewById(R.id.price);
            this.quantity = itemView.findViewById(R.id.quantity);
            this.movetocart = itemView.findViewById(R.id.moveTocart);
        }

    }

}
