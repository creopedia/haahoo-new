package creo.com.haahooapp.Adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import creo.com.haahooapp.Modal.Coupon;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.ItemClick;
import nl.dionsegijn.konfetti.KonfettiView;

public class NewRewardAdapter extends RecyclerView.Adapter<NewRewardAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Coupon> dataModelArrayList;
    public Context context1;
    ClipData myClip;
    ClipboardManager clipboard;
    ItemClick itemClick;

    public NewRewardAdapter(Context ctx, ArrayList<Coupon> dataModelArrayList,ItemClick itemClick) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        clipboard = (ClipboardManager) context1.getSystemService(Context.CLIPBOARD_SERVICE);
        this.itemClick = itemClick;

    }

    @Override
    public NewRewardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.reward_adapter, parent, false);
        NewRewardAdapter.MyViewHolder holder = new NewRewardAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(NewRewardAdapter.MyViewHolder holder, final int position) {

        Glide.with(context1).load(ApiClient.BASE_URL + "media/files/events_add/reward2.png").into(holder.iv);
        holder.rela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               itemClick.sendData("jhhcgghc");
            }
        });

    }

    @Override
    public int getItemCount() {
        return 16;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView price, name, qty, delivery;
        LinearLayout linearLayout;
        ImageView iv, copy;
        RelativeLayout rela;
        KonfettiView konfettiView;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.text);
            copy = itemView.findViewById(R.id.copy);
            rela = itemView.findViewById(R.id.rela);
            konfettiView = itemView.findViewById(R.id.viewKonfetti);
//            qty = (TextView) itemView.findViewById(R.id.non);
        }

    }
}
