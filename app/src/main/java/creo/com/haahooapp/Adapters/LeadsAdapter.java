package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import creo.com.haahooapp.Modal.LeadsPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import de.hdodenhof.circleimageview.CircleImageView;

public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<LeadsPojo> dataModelArrayList;
    public Context context1 ;

    public LeadsAdapter(Context ctx, ArrayList<LeadsPojo> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public LeadsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.leads_adapter, parent, false);
        LeadsAdapter.MyViewHolder holder = new LeadsAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(LeadsAdapter.MyViewHolder holder, final int position) {
        holder.service_name.setText(dataModelArrayList.get(position).getService_name());
        Glide.with(context1).load(ApiClient.BASE_URL+"media/" +dataModelArrayList.get(position).getImage()).into(holder.image);
        //Picasso.get().load(dataModelArrayList.get(position).getImgURL()).into(holder.iv);
        holder.username.setText(dataModelArrayList.get(position).getUsername());
        holder.number.setText(dataModelArrayList.get(position).getPhone_number());
        holder.status.setText(dataModelArrayList.get(position).getStatus());

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

    TextView service_name,username,number,status;
    CircleImageView image;

        public MyViewHolder(View itemView) {
            super(itemView);

            service_name = itemView.findViewById(R.id.service_name);
            image = itemView.findViewById(R.id.image);
            username = itemView.findViewById(R.id.name);
            number = itemView.findViewById(R.id.number);
            status = itemView.findViewById(R.id.status);

//            qty = (TextView) itemView.findViewById(R.id.non);
        }

    }
}
