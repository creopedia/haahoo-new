package creo.com.haahooapp.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import creo.com.haahooapp.Modal.ResellingPojo;
import creo.com.haahooapp.MyShopNew;
import creo.com.haahooapp.ProductDetailsShop;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.ItemClick;

import static android.content.Context.MODE_PRIVATE;

public class ResellingProductsAdapter extends RecyclerView.Adapter<ResellingProductsAdapter.MyViewHolder> {
    int quantity = 0;
    private LayoutInflater inflater;
    private ArrayList<ResellingPojo> dataModelArrayList;
    public Context context1;
    ItemClick itemClick;
    int a = 0;
    int total = 0;

    public ResellingProductsAdapter(Context ctx, ArrayList<ResellingPojo> dataModelArrayList, ItemClick itemClick) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        this.itemClick = itemClick;

    }

    @Override
    public ResellingProductsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.reselling_grid, parent, false);
        ResellingProductsAdapter.MyViewHolder holder = new ResellingProductsAdapter.MyViewHolder(view);
        return holder;

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ResellingProductsAdapter.MyViewHolder holder, final int position) {
        try {
            Glide.with(context1).load(ApiClient.BASE_URL + dataModelArrayList.get(position).getImage()).into(holder.image);
            if (dataModelArrayList.get(position).getDelivery_expense().length() == 0 || dataModelArrayList.get(position).getDelivery_expense().equals("null")) {
                a = Integer.parseInt(dataModelArrayList.get(position).getWholesale_price());
                holder.price.setText("Rs. " + String.valueOf(Integer.parseInt(dataModelArrayList.get(position).getWholesale_price())));
                //total = Integer.parseInt(dataModelArrayList.get(position).getPdt_price());
            }
            if (!(dataModelArrayList.get(position).getDelivery_expense().equals("null")) || dataModelArrayList.get(position).getDelivery_expense().length() != 0) {
                int b = Integer.parseInt(dataModelArrayList.get(position).getWholesale_price()) + Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense().trim());
                holder.price.setText("Rs. " + b);
                int total = Integer.parseInt(dataModelArrayList.get(position).getPdt_price()) + Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense());
            }
            holder.name.setText(dataModelArrayList.get(position).getProduct_name().substring(0, 1).toUpperCase() + dataModelArrayList.get(position).getProduct_name()
                    .substring(1).toLowerCase());
            holder.minqty.setVisibility(View.GONE);
            if (dataModelArrayList.get(position).getStatus().equals("0")) {
                holder.buyNow.setText("Add To MyShop");
            }
            if (dataModelArrayList.get(position).getStatus().equals("1")) {
                holder.buyNow.setText("View in MyShop");
            }
            holder.minus.setVisibility(View.GONE);
            holder.plus.setVisibility(View.GONE);
            holder.qty.setVisibility(View.GONE);
            holder.buyNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.buyNow.getText().toString().equals("Add To MyShop")) {
                        LayoutInflater layoutInflater = LayoutInflater.from(context1);
                        View view = layoutInflater.inflate(R.layout.edit_price, null);
                        final AlertDialog dialog = new AlertDialog.Builder(context1)
                                .setView(view)
                                .create();
                        final EditText userInput = (EditText) view
                                .findViewById(R.id.editTextDialogUserInput);
                        final ImageView image = view.findViewById(R.id.image);
                        TextView range = view.findViewById(R.id.range);
                        Glide.with(context1).load(ApiClient.BASE_URL + dataModelArrayList.get(position).getImage()).into(image);
                        try {
                            int max = Integer.parseInt(dataModelArrayList.get(position).getPdt_price()) + Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense());
                            int min = Integer.parseInt(dataModelArrayList.get(position).getWholesale_price()) + Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense());
                            userInput.setText(String.valueOf(min));
                            range.setText("You can set a price between " + String.valueOf(min) + " and " + String.valueOf(max));
                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialogInterface) {

                                    CardView okcard = view.findViewById(R.id.okcard);
                                    CardView cancelcard = view.findViewById(R.id.cancelcard);
                                    cancelcard.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });
                                    okcard.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            // TODO Do something
                                            if (userInput.getText().toString().length() == 0) {
                                                userInput.setError("Please enter valid price");
                                            }
                                            if (userInput.getText().toString().length() != 0) {
                                                if (Integer.parseInt(userInput.getText().toString().trim()) < a) {
                                                    userInput.setError("Price cannot be lesser than original amount");
                                                }
                                                if (Integer.parseInt(userInput.getText().toString().trim()) > (Integer.parseInt(dataModelArrayList.get(position).getPdt_price()) + Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense()))) {
                                                    int maxx = Integer.parseInt(dataModelArrayList.get(position).getPdt_price()) + Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense());
                                                    userInput.setError("Maximum price is " + String.valueOf(maxx));
                                                }
                                                if ((Integer.parseInt(userInput.getText().toString().trim()) <= (Integer.parseInt(dataModelArrayList.get(position).getPdt_price()) + Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense()))) && (Integer.parseInt(userInput.getText().toString().trim()) >= (Integer.parseInt(dataModelArrayList.get(position).getWholesale_price()) + Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense())))) {
                                                    dialog.dismiss();
                                                    holder.buyNow.setText("View in MyShop");
                                                    itemClick.addProduct(dataModelArrayList.get(position).getId(), dataModelArrayList.get(position).getPdt_price(), userInput.getText().toString());
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            dialog.show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    if (holder.buyNow.getText().toString().equals("View in MyShop")) {
                        Intent intent = new Intent(context1, MyShopNew.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context1.startActivity(intent);
                    }
                }
            });
            holder.card1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context1, ProductDetailsShop.class);
                    intent.putExtra("shopid", dataModelArrayList.get(position).getShop_id());
                    intent.putExtra("productid", dataModelArrayList.get(position).getId());
                    intent.putExtra("from", "reselling");
                    context1.startActivity(intent);
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }




    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name, price, original_price, minqty, plus, minus, qty;
        CardView card, card1;
        Button buyNow;

        public MyViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            card = itemView.findViewById(R.id.card);
            card1 = itemView.findViewById(R.id.card1);
            original_price = itemView.findViewById(R.id.original_price);
            minqty = itemView.findViewById(R.id.minqty);
            buyNow = itemView.findViewById(R.id.btn);
            plus = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);
            qty = itemView.findViewById(R.id.qty);
            buyNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

    }
}