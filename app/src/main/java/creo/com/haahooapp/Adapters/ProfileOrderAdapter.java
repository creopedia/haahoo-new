package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import creo.com.haahooapp.Modal.ProfileOrderPojo;
import creo.com.haahooapp.Orderdetails;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class ProfileOrderAdapter extends RecyclerView.Adapter<ProfileOrderAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ProfileOrderPojo> dataModelArrayList;
    public Context context1 ;

    public ProfileOrderAdapter(Context ctx, ArrayList<ProfileOrderPojo> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public ProfileOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.profile_fragment_adapter, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ProfileOrderAdapter.MyViewHolder holder, final int position) {

        //Picasso.get().load(dataModelArrayList.get(position).getImgURL()).into(holder.iv);
//        holder.name.setText(dataModelArrayList.get(position).getName());
//        holder.delivery.setText(dataModelArrayList.get(position).getPrice());
        Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);
        holder.name.setText(dataModelArrayList.get(position).getName());
        holder.iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1, Orderdetails.class);
                intent.putExtra("order_id",dataModelArrayList.get(position).getId());
                ApiClient.sh_order_id = ApiClient.shids.get(position);
                 Log.d("djbsjbshdb","jhgugug"+dataModelArrayList.get(position).getImage());
                context1.startActivity(intent);
            }
        });
//        holder.qty.setText(dataModelArrayList.get(position).getQty());
        // holder.country.setText(dataModelArrayList.get(position).getCountry());
        // holder.city.setText(dataModelArrayList.get(position).getCity());
    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

//        TextView price, name, qty,delivery;
//        LinearLayout linearLayout;
        ImageView iv;
        TextView name;

        public MyViewHolder(View itemView) {
            super(itemView);
//            delivery = itemView.findViewById(R.id.delivery);
//            linearLayout = itemView.findViewById(R.id.layout);
//            name = (TextView) itemView.findViewById(R.id.title);
//            price = (TextView) itemView.findViewById(R.id.price);
            iv = itemView.findViewById(R.id.img);
            name = itemView.findViewById(R.id.name);
//            qty = (TextView) itemView.findViewById(R.id.non);
        }

    }
}