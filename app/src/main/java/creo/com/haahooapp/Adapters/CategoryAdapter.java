package creo.com.haahooapp.Adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import creo.com.haahooapp.CategoryShop;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.R;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private List<Category> dataModelArrayList;
    public Context context1 ;
    ClipData myClip;
    ClipboardManager clipboard;
    public CategoryAdapter(Context ctx, List<Category> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        clipboard = (ClipboardManager) context1.getSystemService(Context.CLIPBOARD_SERVICE);
    }

    @Override
    public CategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.allcat, parent, false);
        CategoryAdapter.MyViewHolder holder = new CategoryAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.MyViewHolder holder, final int position) {

        Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);
        holder.name.setText(dataModelArrayList.get(position).getName());
        if (position%2==0) {
            RelativeLayout.LayoutParams params =
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            holder.name.setLayoutParams(params);
        }
        if (position%2==1){
            RelativeLayout.LayoutParams params =
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            holder.name.setLayoutParams(params);
        }
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1, CategoryShop.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("categoryid",dataModelArrayList.get(position).getId());
                intent.putExtra("categoryname",dataModelArrayList.get(position).getName());
                context1.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView price, name, qty,delivery;
        LinearLayout linearLayout;
        ImageView iv, copy;
        CardView card;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            card = itemView.findViewById(R.id.card);

        }

    }
}