package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import creo.com.haahooapp.ChooseAddress;
import creo.com.haahooapp.ImageZoom;
import creo.com.haahooapp.Modal.ResellingPojo;
import creo.com.haahooapp.ProductDetailsShop;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

import static android.content.Context.MODE_PRIVATE;

public class OnlineMartProductAdapter extends RecyclerView.Adapter<OnlineMartProductAdapter.MyViewHolder> {
    int quantity = 0;
    private LayoutInflater inflater;
    private ArrayList<ResellingPojo> dataModelArrayList;
    public Context context1;

    public OnlineMartProductAdapter(Context ctx, ArrayList<ResellingPojo> dataModelArrayList) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public OnlineMartProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.reselling_grid, parent, false);
        OnlineMartProductAdapter.MyViewHolder holder = new OnlineMartProductAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(OnlineMartProductAdapter.MyViewHolder holder, final int position) {
        try {

            Glide.with(context1).load(ApiClient.BASE_URL + dataModelArrayList.get(position).getImage()).into(holder.image);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context1, ImageZoom.class);
                    intent.putExtra("image",ApiClient.BASE_URL+dataModelArrayList.get(position).getImage());
                    context1.startActivity(intent);
                }
            });
            int a = Integer.parseInt(dataModelArrayList.get(position).getWholesale_price());
            int b = Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense());
            int c = a + b;
            holder.name.setText(dataModelArrayList.get(position).getProduct_name());
            holder.price.setText("Rs. " + c);
            holder.minqty.setText("Minimum Qty:  " + dataModelArrayList.get(position).getMin_wholesale_qty());
            holder.qty.setText(dataModelArrayList.get(position).getMin_wholesale_qty());
            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // holder.price.setText(String.valueOf(Integer.parseInt(holder.price.getText().toString().split("Rs. ")[0].trim())+Integer.parseInt(dataModelArrayList.get(position).getWholesale_price())+Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense())));
                    //Log.d("hvcgxrtyxc","tydtydty"+(Integer.parseInt(holder.price.getText().toString().split("Rs. ")[1])+Integer.parseInt(dataModelArrayList.get(position).getWholesale_price().trim())+Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense().trim())));
                    holder.price.setText("Rs. "+String.valueOf(Integer.parseInt(holder.price.getText().toString().split("Rs. ")[1])+Integer.parseInt(dataModelArrayList.get(position).getWholesale_price().trim())+Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense().trim())));
                    String quantity = holder.qty.getText().toString();
                    int added = Integer.parseInt(quantity) + Integer.parseInt(dataModelArrayList.get(position).getMin_wholesale_qty());
                    holder.qty.setText(String.valueOf(added));
                }
            });
            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.qty.getText().toString().equals(dataModelArrayList.get(position).getMin_wholesale_qty())){

                    }else {
                        if (Integer.parseInt(holder.qty.getText().toString()) > 1) {
                            String quantity = holder.qty.getText().toString();
                            holder.price.setText("Rs. "+String.valueOf(Integer.parseInt(holder.price.getText().toString().split("Rs. ")[1])-(Integer.parseInt(dataModelArrayList.get(position).getWholesale_price().trim())+Integer.parseInt(dataModelArrayList.get(position).getDelivery_expense().trim()))));
                            int decrease = Integer.parseInt(quantity) - Integer.parseInt(dataModelArrayList.get(position).getMin_wholesale_qty());
                            holder.qty.setText(String.valueOf(decrease));
                        }
                    }
                }
            });
            holder.buyNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences pref = context1.getSharedPreferences("MyPref", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("shop_id", dataModelArrayList.get(position).getShop_id());
                    editor.commit();
                    ApiClient.ids.clear();
                    ApiClient.prices.clear();
                    ApiClient.quantity.clear();
                    ApiClient.name.clear();
                    ApiClient.images.clear();
                    ApiClient.virtual.clear();
                    ApiClient.ids.add(dataModelArrayList.get(position).getId());
                    ApiClient.prices.add(holder.price.getText().toString().split("Rs. ")[1]);
                    ApiClient.price = "₹ "+holder.price.getText().toString().split("Rs. ")[1];
                    ApiClient.quantity.add(holder.qty.getText().toString());
                    ApiClient.name.add(dataModelArrayList.get(position).getProduct_name());
                    ApiClient.images.add(ApiClient.BASE_URL + dataModelArrayList.get(position).getImage());
                    ApiClient.virtual.add("0");
                    ApiClient.bazaar = "true";
//                    Intent intent = new Intent(context1, ProductDetailsShop.class);
//                    intent.putExtra("shopid", dataModelArrayList.get(position).getShop_id());
//                    intent.putExtra("productid", dataModelArrayList.get(position).getId());
//                    intent.putExtra("from", "noresell");
//                    context1.startActivity(intent);

                    Intent intent = new Intent(context1, ChooseAddress.class);
                    intent.putExtra("from", "details");
                    context1.startActivity(intent);
                }
            });
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name, price, original_price, minqty, plus, minus, qty;
        CardView card, card1;
        Button buyNow;

        public MyViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            card = itemView.findViewById(R.id.card);
            card1 = itemView.findViewById(R.id.card1);
            original_price = itemView.findViewById(R.id.original_price);
            minqty = itemView.findViewById(R.id.minqty);
            buyNow = itemView.findViewById(R.id.btn);
            plus = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);
            qty = itemView.findViewById(R.id.qty);
        }

    }
}