package creo.com.haahooapp.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import creo.com.haahooapp.EventFirstFragment;
import creo.com.haahooapp.EventSecondFragment;
import creo.com.haahooapp.EventThirdFragment;

public class EventNewAdapter  extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public EventNewAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                EventFirstFragment eventFirstFragment = new EventFirstFragment();
                return eventFirstFragment;
            case 1:
                EventSecondFragment eventSecondFragment = new EventSecondFragment();
                return eventSecondFragment;
            case 2:
                EventThirdFragment eventThirdFragment = new EventThirdFragment();
                return eventThirdFragment;
            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }

}
