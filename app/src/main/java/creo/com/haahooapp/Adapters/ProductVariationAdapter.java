package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import creo.com.haahooapp.Modal.ProductVariationPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.Recycler;

public class ProductVariationAdapter extends RecyclerView.Adapter<ProductVariationAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ProductVariationPojo> dataModelArrayList;
    public Context context1 ;
    private Recycler recycler;
    private int index=-1;
    public boolean clicked;


    public ProductVariationAdapter(Context ctx, ArrayList<ProductVariationPojo> dataModelArrayList, Recycler callback){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        this.recycler = callback;
    }

    @Override
    public ProductVariationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.product_variation, parent, false);
        ProductVariationAdapter.MyViewHolder holder = new ProductVariationAdapter.MyViewHolder(view);

        return holder;
    }


    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }
    @Override
    public void onBindViewHolder(ProductVariationAdapter.MyViewHolder holder, final int position) {
    holder.name.setText(dataModelArrayList.get(position).getName());
        Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.image);
        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                index=position;
                setClicked(true);
                notifyDataSetChanged();
            }
        });

        if(index==position){
            Resources res = context1.getResources();
            Drawable drawable = res.getDrawable(R.drawable.border_relative_available);
            //ApiClient.subscribe_amount = dataModelArrayList.get(position).getAmount();
//            recycler.onClick(dataModelArrayList.get(position).getName());
            recycler.variationClick(dataModelArrayList.get(position).getId());
            // SubscriptionPlans.view_visible(dataModelArrayList.get(position).getName());
            holder.relative.setBackground(drawable);
        }

        if(index!=position){
            Resources res = context1.getResources();
            //Drawable drawable = res.getDrawable(R.drawable.border_relative_complete);
            holder.relative.setBackgroundResource(0);
        }

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout relative;
        TextView name;
        ImageView image;

        public MyViewHolder(View itemView) {
            super(itemView);
            relative = itemView.findViewById(R.id.relative);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);

        }

    }
}