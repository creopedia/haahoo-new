package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import creo.com.haahooapp.Modal.Downloadd;
import creo.com.haahooapp.R;

public class CallAdapter extends RecyclerView.Adapter<CallAdapter.ViewHolder> {

    public List<Downloadd> downloadPojos;
    Context context1 ;

    public CallAdapter(List<Downloadd> productPojo, Context context) {
        this.downloadPojos = productPojo;
        this.context1 = context;
    }

    @Override
    public CallAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.call_adapter, parent, false);
        CallAdapter.ViewHolder viewHolder = new CallAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CallAdapter.ViewHolder holder, int position) {

        holder.textView.setText(downloadPojos.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return downloadPojos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView,date,amount;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView =  itemView.findViewById(R.id.name);
            this.date = itemView.findViewById(R.id.date);
            this.amount = itemView.findViewById(R.id.amount);
        }

    }

}