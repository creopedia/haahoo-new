package creo.com.haahooapp.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import creo.com.haahooapp.ProductAdditionalOptions;
import creo.com.haahooapp.ProductDescription;
import creo.com.haahooapp.ProductFeatures;

public class ProductTabsAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public ProductTabsAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                ProductDescription homeFragment = new ProductDescription();
                return homeFragment;

            case 1:
                ProductFeatures sportFragment = new ProductFeatures();
                return sportFragment;

            case 2:
                ProductAdditionalOptions productAdditionalOptions = new ProductAdditionalOptions();
                return productAdditionalOptions;

            default:
                return null;

        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}
