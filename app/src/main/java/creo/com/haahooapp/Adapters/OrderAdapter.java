package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import androidx.recyclerview.widget.RecyclerView;
import creo.com.haahooapp.Modal.OrderPojo;
import creo.com.haahooapp.Orderdetails;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<OrderPojo> dataModelArrayList;
    public Context context1 ;

    public OrderAdapter(Context ctx, ArrayList<OrderPojo> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;

    }

    @Override
    public OrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.order_adapter, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(OrderAdapter.MyViewHolder holder, final int position) {
        try {
            holder.name.setText(dataModelArrayList.get(position).getProductid());
            holder.delivery.setText(dataModelArrayList.get(position).getPrice());
            holder.date.setText(dataModelArrayList.get(position).getDate());
            holder.shopname.setText(dataModelArrayList.get(position).getShop_name());
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context1, Orderdetails.class);
                    intent.putExtra("order_id", dataModelArrayList.get(position).getProductid());
                    context1.startActivity(intent);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView price, name, qty,delivery,date,shopname;
        LinearLayout linearLayout;
        ImageView iv;

        public MyViewHolder(View itemView) {
            super(itemView);
            delivery = itemView.findViewById(R.id.delivery);
            linearLayout = itemView.findViewById(R.id.layout);
            name = (TextView) itemView.findViewById(R.id.title);
            price = (TextView) itemView.findViewById(R.id.price);
            iv = itemView.findViewById(R.id.image);
            date = itemView.findViewById(R.id.date);
            shopname = itemView.findViewById(R.id.shopname);
//            qty = (TextView) itemView.findViewById(R.id.non);
        }

    }
}
