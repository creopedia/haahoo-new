package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.List;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.OnClickMenu;

public class DeliveryModeAdapter extends  RecyclerView.Adapter<DeliveryModeAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private List<String> dataModelArrayList;
    public Context context1;
    private int selectedPosition = -1;// no selection by default
    OnClickMenu onClickMenu;

    public DeliveryModeAdapter(Context ctx, List<String> dataModelArrayList,OnClickMenu onClickMenu) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        this.onClickMenu = onClickMenu;
    }

    @Override
    public DeliveryModeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.delivery_mode_adapter, parent, false);
        DeliveryModeAdapter.MyViewHolder holder = new DeliveryModeAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(DeliveryModeAdapter.MyViewHolder holder, final int position) {
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickMenu.onClick(dataModelArrayList.get(position));
                selectedPosition = position;
                notifyDataSetChanged();
            }
        });
     holder.radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
         @Override
         public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (buttonView.isPressed()){
                onClickMenu.onClick(dataModelArrayList.get(position));
                selectedPosition = position;
                notifyDataSetChanged();
            }
         }
     });
        if (selectedPosition == position){
            holder.radio.setChecked(true);
        }
        else{
            holder.radio.setChecked(false);
        }
        holder.type.setText(dataModelArrayList.get(position));
        if (dataModelArrayList.get(position).contains("Express Delivery")) {
            Glide.with(context1).load(R.drawable.flash).into(holder.img);
        }
        if (dataModelArrayList.get(position).toLowerCase().contains("haahoo")){
            holder.description.setText("Delivered Within 3 hours");
            Glide.with(context1).load(ApiClient.BASE_URL+"media/android/haahoologo.png").into(holder.img);
        }
        if (dataModelArrayList.get(position).contains("Custom Delivery")){
            Glide.with(context1).load(R.drawable.custom).into(holder.img);
        }
        if (dataModelArrayList.get(position).contains("Pick from Shop")){
            Glide.with(context1).load(R.drawable.pickshop).into(holder.img);
        }

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView type, name, qty, delivery, earning, customer,del,description;
        LinearLayout linearLayout;
        ImageView img;
        CardView card;
        RadioButton radio;

        public MyViewHolder(View itemView) {
            super(itemView);
            type = itemView.findViewById(R.id.type);
            img = itemView.findViewById(R.id.img);
            card = itemView.findViewById(R.id.card);
            radio = itemView.findViewById(R.id.radio);
            description = itemView.findViewById(R.id.description);
        }

    }
}

