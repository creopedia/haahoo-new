package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.BusinessDetails;
import creo.com.haahooapp.Modal.BusinessPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import static android.content.Context.MODE_PRIVATE;

public class BusinessAdapter extends RecyclerView.Adapter<BusinessAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<BusinessPojo> dataModelArrayList;
    public Context context1 ;

    public BusinessAdapter(Context ctx, ArrayList<BusinessPojo> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;

    }

    @Override
    public BusinessAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.business_adapter, parent, false);
        BusinessAdapter.MyViewHolder holder = new BusinessAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(BusinessAdapter.MyViewHolder holder,  int position) {

        Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.imageView);

        holder.title.setText(dataModelArrayList.get(position).getName());

        holder.detailsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1,BusinessDetails.class);

                intent.putExtra("id",dataModelArrayList.get(position).getId());
                context1.startActivity(intent);
            }
        });
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context1,BusinessDetails.class);
                intent.putExtra("id",dataModelArrayList.get(position).getId());
                context1.startActivity(intent);
               // Toast.makeText(context1,"id"+dataModelArrayList.get(position).getId()+position,Toast.LENGTH_SHORT).show();
            }
        });

        holder.sharebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = context1.getSharedPreferences("MyPref", MODE_PRIVATE);
                String token = (pref.getString("token", ""));
                RequestQueue queue = Volley.newRequestQueue(context1);

                //this is the url where you want to send the request

                // Request a string response from the provided URL.

                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"bussiness/bussiness_url/",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Display the response string.
                                try {
                                    JSONObject obj = new JSONObject(response);
                                   // Log.d("urlbusiness","buzzz"+obj);
                                    String shareable_url = obj.optString("data");
                                    String shareBody = shareable_url;
                                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                    sharingIntent.setType("text/plain");
                                    //sharingIntent.putExtra(Intent.EXTRA_SUBJECT,"esrdfgvhbnhrtdfcg rdtfghbjnmjbhgytfr rdtfghjnhytfr");
                                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                                    context1.startActivity(Intent.createChooser(sharingIntent, "Share using"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {


                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("image_id",dataModelArrayList.get(position).getId());
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders()  {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", "Token " + token);
                        return params;
                    }
                };

                // Add the request to the RequestQueue.
                queue.add(stringRequest);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView title;
        RelativeLayout details,count,share,card;
        Button detailsbtn,sharebtn;
        public MyViewHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card);
            imageView = itemView.findViewById(R.id.image);
            details = itemView.findViewById(R.id.details);
            count = itemView.findViewById(R.id.count);
            share = itemView.findViewById(R.id.share);
            title = itemView.findViewById(R.id.title);
            detailsbtn = itemView.findViewById(R.id.detailsbtn);
            sharebtn = itemView.findViewById(R.id.sharebtn);

        }

    }
}