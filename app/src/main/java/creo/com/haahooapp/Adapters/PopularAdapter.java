package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.List;
import creo.com.haahooapp.Modal.PopularPojo;
import creo.com.haahooapp.ProductDetailsShop;
import creo.com.haahooapp.R;

public class PopularAdapter extends RecyclerView.Adapter<PopularAdapter.ViewHolder> {

    public List<PopularPojo> downloadPojos;
    Context context1 ;

    public PopularAdapter(List<PopularPojo> productPojo, Context context) {
        this.downloadPojos = productPojo;
        this.context1 = context;
    }

    @Override
    public PopularAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.popular_layout, parent, false);
        PopularAdapter.ViewHolder viewHolder = new PopularAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PopularAdapter.ViewHolder holder, int position) {

        holder.name.setText(downloadPojos.get(position).getName());
        Glide.with(context1).load(downloadPojos.get(position).getImage()).into(holder.image);
        holder.category.setText(downloadPojos.get(position).getCategory());
        holder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1, ProductDetailsShop.class);
                intent.putExtra("shopid",downloadPojos.get(position).getShop_id());
                intent.putExtra("productid",downloadPojos.get(position).getId());
                context1.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return downloadPojos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name,category;
        public ImageView image;
        public RelativeLayout rel;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name =  itemView.findViewById(R.id.name);
            this.image = itemView.findViewById(R.id.image);
            this.category = itemView.findViewById(R.id.category);
            this.rel = itemView.findViewById(R.id.rel);
        }

    }

}