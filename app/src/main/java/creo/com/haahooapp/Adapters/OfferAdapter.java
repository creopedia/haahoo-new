package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class OfferAdapter extends BaseAdapter {
    private Context ctx;

    private LayoutInflater inflater;

    private ArrayList<String> ratingPojos;

    public OfferAdapter(Context context,ArrayList<String> ratingPojos) {
        this.ratingPojos = ratingPojos;
        this.ctx = context;
        inflater = (LayoutInflater.from(context));
    }


    @Override
    public int getCount() {
        return 5;
    }
    @Override
    public Object getItem(int i) {
        return null;
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }
//    @Override
//    public View getView(int i, View view, ViewGroup viewGroup) {
//        view = inflater.inflate(R.layout.reward_offers_adapter, null); // inflate the layout
//        ImageView icon = (ImageView) view.findViewById(R.id.icon); // get the reference of ImageView
//        //icon.setImageResource(logos[i]); // set logo images
//        Glide.with(ctx).load(ApiClient.BASE_URL+"media/files/events_add/reward2.jpg").into(icon);
//        return view;
//    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.reward_offers_adapter, null);
        //TextView textView = (TextView) v.findViewById(R.id.textView);
        ImageView imageView = (ImageView) v.findViewById(R.id.image);
       // textView.setText(birdList.get(position).getbirdName());
       // imageView.setImageResource(birdList.get(position).getbirdImage());
        Glide.with(ctx).load(ApiClient.BASE_URL+"media/files/events_add/reward2.jpg").into(imageView);
        return v;

    }
}