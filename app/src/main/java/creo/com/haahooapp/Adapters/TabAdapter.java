package creo.com.haahooapp.Adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import creo.com.haahooapp.BasicDetails;
import creo.com.haahooapp.Description;
import creo.com.haahooapp.Queries;

public class TabAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public TabAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                BasicDetails basicDetails = new BasicDetails();
                return basicDetails;
            case 1:
                Description description = new Description();
                return description;
            case 2:
                Queries queries = new Queries();
                return queries;
            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}