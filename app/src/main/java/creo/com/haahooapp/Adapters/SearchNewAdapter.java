package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import creo.com.haahooapp.Modal.SearchPojo;
import creo.com.haahooapp.ProductDetailsShop;
import creo.com.haahooapp.ProductsInShop;
import creo.com.haahooapp.R;

public class SearchNewAdapter extends RecyclerView.Adapter<SearchNewAdapter.ViewHolder> {

    public List<SearchPojo> downloadPojos;
    Context context1;

    public SearchNewAdapter(List<SearchPojo> productPojo, Context context) {
        this.downloadPojos = productPojo;
        this.context1 = context;
    }

    @Override
    public SearchNewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.search_new_adapter, parent, false);
        SearchNewAdapter.ViewHolder viewHolder = new SearchNewAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SearchNewAdapter.ViewHolder holder, int position) {

        if (downloadPojos.get(position).getProduct_name().length()==0) {
            holder.name.setText(downloadPojos.get(position).getShop_name());
        }
        if (downloadPojos.get(position).getShop_name().length()==0) {
            holder.name.setText(downloadPojos.get(position).getProduct_name());
        }
        holder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    if (downloadPojos.get(position).getShop_id().length() != 0) {
                        Intent intent = new Intent(context1, ProductsInShop.class);
                        intent.putExtra("categoryid", downloadPojos.get(position).getShop_id());
                        intent.putExtra("categoryname", downloadPojos.get(position).getShop_name());
                        context1.startActivity(intent);
                    }
                    if (downloadPojos.get(position).getProduct_id().length() != 0) {
                        Intent intent = new Intent(context1, ProductDetailsShop.class);
                        intent.putExtra("productid", downloadPojos.get(position).getProduct_id());
                        intent.putExtra("shopid", downloadPojos.get(position).getPdt_shop_id());
                        context1.startActivity(intent);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return downloadPojos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, price, offer, percent;
        public ImageView image;
        public RelativeLayout rel;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.item);
            this.rel = itemView.findViewById(R.id.rel);
        }

    }

}