package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;
import androidx.recyclerview.widget.RecyclerView;
import creo.com.haahooapp.Modal.ShopPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ViewHolder> {

    public List<ShopPojo> productFeaturepojos;
    Context context1 ;

    public ShopListAdapter(List<ShopPojo> productFeaturepojos,Context context) {
        this.productFeaturepojos = productFeaturepojos;
        this.context1 = context;
    }

    @Override
    public ShopListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.shop_list_adapter, parent, false);
        final ShopListAdapter.ViewHolder viewHolder = new ShopListAdapter.ViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShopListAdapter.ViewHolder holder, int position) {

        holder.name.setText(productFeaturepojos.get(position).getName());
        holder.phone.setText(productFeaturepojos.get(position).getPhone());
        holder.gst.setText(productFeaturepojos.get(position).getGst());
        Glide.with(context1).load(ApiClient.BASE_URL+"media/icon_product/shop_logo.png").into(holder.dot);

    }

    @Override
    public int getItemCount() {
        return productFeaturepojos.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView dot;
        public TextView name,phone,gst;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name =  itemView.findViewById(R.id.name);
            this.dot = itemView.findViewById(R.id.img);
            this.phone = itemView.findViewById(R.id.phone);
            this.gst = itemView.findViewById(R.id.gst);
        }

    }

}