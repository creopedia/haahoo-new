package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import creo.com.haahooapp.Modal.AddShopData;
import creo.com.haahooapp.Modal.EarningPojo;
import creo.com.haahooapp.R;

public class EarningAdapter extends RecyclerView.Adapter<EarningAdapter.ViewHolder> {

    ArrayList<AddShopData> newArrayList;
    Context context;

    public EarningAdapter(ArrayList<AddShopData> newArrayList, Context context) {
        this.newArrayList = newArrayList;
        this.context = context;
    }

    @Override
    public EarningAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.earning_adapter, parent, false);
        EarningAdapter.ViewHolder viewHolder = new EarningAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EarningAdapter.ViewHolder holder, int position) {

        final AddShopData addShopData = newArrayList.get(position);
        holder.textView.setText(addShopData.getName());
        holder.amount.setText(addShopData.getCoupon_reward());
    }

    @Override
    public int getItemCount() {
        return newArrayList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView,date,amount;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView =  itemView.findViewById(R.id.name);
            this.date = itemView.findViewById(R.id.date);
            this.amount = itemView.findViewById(R.id.amount);
        }

    }

}
