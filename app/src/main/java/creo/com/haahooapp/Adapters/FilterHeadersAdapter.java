package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

import creo.com.haahooapp.Modal.FeaturePojoHeader;
import creo.com.haahooapp.Modal.LeadsPojo;
import creo.com.haahooapp.Modal.Spec_header_values;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.FilterInterface;

public class FilterHeadersAdapter extends RecyclerView.Adapter<FilterHeadersAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<FeaturePojoHeader> dataModelArrayList;
    public boolean clicked;
    public Context context1 ;
    private int index=-1;
    FilterInterface filterInterface;
    ArrayList<Spec_header_values> values = new ArrayList<>();

    public FilterHeadersAdapter(Context ctx, ArrayList<FeaturePojoHeader> dataModelArrayList,FilterInterface filterInterface){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        this.filterInterface = filterInterface;

    }

    public boolean isClicked() {
        return clicked;
    }

    @Override
    public FilterHeadersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.filter_header_adapter, parent, false);
        FilterHeadersAdapter.MyViewHolder holder = new FilterHeadersAdapter.MyViewHolder(view);

        return holder;
    }
    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    @Override
    public void onBindViewHolder(FilterHeadersAdapter.MyViewHolder holder, final int position) {
        holder.category.setText(dataModelArrayList.get(position).getName());
        holder.category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index=position;
                setClicked(true);
                notifyDataSetChanged();
                filterInterface.getFilterId(dataModelArrayList.get(position).getId(),dataModelArrayList.get(position).getName());

            }
        });

        if (index == position){
            Resources resources = context1.getResources();
            Drawable drawable = resources.getDrawable(R.drawable.border_relative_available);
            holder.category.setBackground(drawable);
        }
        if (index != position){
            Resources resources = context1.getResources();
            Drawable drawable = resources.getDrawable(R.color.white);
            holder.category.setBackground(drawable);
        }
    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView category;

        public MyViewHolder(View itemView) {
            super(itemView);
            category = itemView.findViewById(R.id.category);


        }

    }
}