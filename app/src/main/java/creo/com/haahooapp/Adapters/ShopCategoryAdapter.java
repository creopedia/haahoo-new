package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.R;

public class ShopCategoryAdapter extends BaseAdapter {

    public Context context;
    public ArrayList<Category> categories = new ArrayList<>();

    public ShopCategoryAdapter(Context context, ArrayList<Category> categories) {
        this.context = context;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        if (convertView == null){

            gridView = new View(context);
            gridView = inflater.inflate(R.layout.shop_by_category_adapter , null);

            ImageView imageView = gridView.findViewById(R.id.img);
            TextView textView = gridView.findViewById(R.id.title);
            Glide.with(context).load(categories.get(position).getImage()).into(imageView);
            textView.setText(categories.get(position).getName());

        }
        else {
            gridView = (View) convertView;
        }

        return gridView;
    }
}