package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.net.URLEncoder;
import java.util.List;
import creo.com.haahooapp.Modal.Downloadd;
import creo.com.haahooapp.R;

public class DownloadAdapter extends RecyclerView.Adapter<DownloadAdapter.ViewHolder>{
    public List<Downloadd> downloadPojos;
    Context context1 ;

    public DownloadAdapter(List<Downloadd> productPojo,Context context) {
        this.downloadPojos = productPojo;
        this.context1 = context;
    }

    @Override
    public DownloadAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.dowloads_list, parent, false);
        DownloadAdapter.ViewHolder viewHolder = new DownloadAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DownloadAdapter.ViewHolder holder, int position) {

        holder.textView.setText(downloadPojos.get(position).getName());
        holder.date.setText("Member since "+downloadPojos.get(position).getDate());
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+downloadPojos.get(position).getNumber()));
                context1.startActivity(intent);
            }
        });
        holder.whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessageToWhatsAppContact(downloadPojos.get(position).getNumber());
            }
        });
    }

    private void sendMessageToWhatsAppContact(String number) {
        PackageManager packageManager = context1.getPackageManager();
        boolean status = isPackageInstalled("com.whatsapp",packageManager);
        if (status) {

            Intent i = new Intent(Intent.ACTION_VIEW);
            try {
                String url = "https://api.whatsapp.com/send?phone=+91" + number + "&text=" + URLEncoder.encode("Hii", "UTF-8");
                i.setPackage("com.whatsapp");
                i.setData(Uri.parse(url));
                if (i.resolveActivity(packageManager) != null) {
                    context1.startActivity(i);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!status){
            Toast.makeText(context1,"App not installed",Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return downloadPojos.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView,date;
        public ImageView call,whatsapp;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView =  itemView.findViewById(R.id.name);
            this.date = itemView.findViewById(R.id.date);
            this.call = itemView.findViewById(R.id.call);
            this.whatsapp = itemView.findViewById(R.id.whatsapp);
        }

    }

}


