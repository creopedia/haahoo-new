package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import creo.com.haahooapp.Modal.ResellingPojo;
import creo.com.haahooapp.ProductDetailsShop;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import de.hdodenhof.circleimageview.CircleImageView;

public class ResellingAdapter extends RecyclerView.Adapter<ResellingAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ResellingPojo> dataModelArrayList;
    public Context context1 ;

    public ResellingAdapter( ArrayList<ResellingPojo> dataModelArrayList, Context context1) {
        this.dataModelArrayList = dataModelArrayList;
        inflater = LayoutInflater.from(context1);
        this.context1 = context1;
    }

    @Override
    public ResellingAdapter.MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.reselling_adapter,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ResellingAdapter.MyViewHolder holder, int position) {

        holder.title.setText(dataModelArrayList.get(position).getProduct_name());
        /*holder.actual.setText("Actual price is MRP Rs. "+dataModelArrayList.get(position).getActual_price());
        holder.maximum.setText("Max reselling price MRP Rs. "+dataModelArrayList.get(position).getResell_price());*/
        Glide.with(context1).load(ApiClient.BASE_URL+dataModelArrayList.get(position).getImage()).into(holder.imageView);

        holder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1, ProductDetailsShop.class);
                intent.putExtra("shopid",dataModelArrayList.get(position).getShop_id());
                intent.putExtra("productid",dataModelArrayList.get(position).getId());
                context1.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        CircleImageView imageView;
        TextView title,actual,maximum;
        RelativeLayout main;

        public MyViewHolder(View itemView) {
            super(itemView);
            main = itemView.findViewById(R.id.main);
            imageView = itemView.findViewById(R.id.img);
            title = itemView.findViewById(R.id.title);
            actual = itemView.findViewById(R.id.actual);
            maximum = itemView.findViewById(R.id.maximum);
        }
    }
}