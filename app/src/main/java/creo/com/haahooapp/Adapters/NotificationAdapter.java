package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import creo.com.haahooapp.Modal.NotificationPojo;
import creo.com.haahooapp.R;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<NotificationPojo> dataModelArrayList;
    public Context context1 ;

    public NotificationAdapter(Context ctx, ArrayList<NotificationPojo> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;

    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.notification_adapter, parent, false);
        NotificationAdapter.MyViewHolder holder = new NotificationAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.MyViewHolder holder, final int position) {

        holder.notification.setText(dataModelArrayList.get(position).getName());
        holder.date.setText(dataModelArrayList.get(position).getDate());


    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView notification, date;

        public MyViewHolder(View itemView) {

            super(itemView);
            notification = itemView.findViewById(R.id.notification);
            date = itemView.findViewById(R.id.date);

        }

    }
}
