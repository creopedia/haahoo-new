package creo.com.haahooapp.Adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.List;
import creo.com.haahooapp.Modal.PopularPojo;
import creo.com.haahooapp.ProductsInShop;
import creo.com.haahooapp.R;

public class NewHomeHouseAdapter extends RecyclerView.Adapter<NewHomeHouseAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private List<PopularPojo> dataModelArrayList;
    public Context context1;
    ClipData myClip;
    ClipboardManager clipboard;

    public NewHomeHouseAdapter(Context ctx, List<PopularPojo> dataModelArrayList) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        clipboard = (ClipboardManager) context1.getSystemService(Context.CLIPBOARD_SERVICE);

    }

    @Override
    public NewHomeHouseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.househol, parent, false);
        NewHomeHouseAdapter.MyViewHolder holder = new NewHomeHouseAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(NewHomeHouseAdapter.MyViewHolder holder, final int position) {

        Log.d("IMHHHH","buvuv"+dataModelArrayList.get(position).getImage());

        Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);
        holder.name.setText(dataModelArrayList.get(position).getName());
        holder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1, ProductsInShop.class);
                intent.putExtra("categoryid", dataModelArrayList.get(position).getShop_id());
                intent.putExtra("categoryname", dataModelArrayList.get(position).getShop_name());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context1.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView price, name, qty, delivery;
        LinearLayout linearLayout;
        ImageView iv, copy;
        RelativeLayout rel;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            rel = itemView.findViewById(R.id.rel);

        }

    }
}