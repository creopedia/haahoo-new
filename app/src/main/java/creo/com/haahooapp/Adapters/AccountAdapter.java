package creo.com.haahooapp.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import creo.com.haahooapp.ProfileAccountFragment;
import creo.com.haahooapp.ProfileFragment;
import creo.com.haahooapp.DirectSellEarning;
import creo.com.haahooapp.ProfileFragmentKYC;
import creo.com.haahooapp.ProfileWithdrawalFragment;

public class AccountAdapter extends FragmentPagerAdapter {

    private Context context;
    private int num_of_tabs;

    public AccountAdapter(Context context, FragmentManager fm, int totalTabs) {

        super(fm);
        this.context = context;
        this.num_of_tabs = totalTabs;

    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ProfileFragment referralEarning = new ProfileFragment();
                return referralEarning;
            case 1:
                ProfileAccountFragment directSellEarning = new ProfileAccountFragment();
                return directSellEarning;
            case 2:
                ProfileWithdrawalFragment directSellEarning2 = new ProfileWithdrawalFragment();
                return directSellEarning2;
            case 3:
                ProfileFragmentKYC directSellEarning1 = new ProfileFragmentKYC();
                return directSellEarning1;

            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return num_of_tabs;
    }

}