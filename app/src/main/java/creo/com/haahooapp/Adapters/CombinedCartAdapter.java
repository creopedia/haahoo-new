package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import creo.com.haahooapp.Modal.CombinedCartList;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class CombinedCartAdapter extends RecyclerView.Adapter<CombinedCartAdapter.ViewHolder> {

    ArrayList<CombinedCartList> newArrayList;
    Context context;
    private CombinedCartAdapter.OnClickListener onClickListener;

    public CombinedCartAdapter(ArrayList<CombinedCartList> newArrayList, Context context, OnClickListener onClickListener) {
        this.newArrayList = newArrayList;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.view_coupon_list, parent, false);
        return new CombinedCartAdapter.ViewHolder(listItem, onClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            final CombinedCartList combinedCartList = newArrayList.get(position);
            holder.shopName.setText("Shop Name : " + combinedCartList.getShop_name());
            holder.shopId.setText("Shop ID : " + combinedCartList.getShop_id());
            holder.count.setText(combinedCartList.getCart_count() + " Products");
            Glide.with(context).load(ApiClient.BASE_URL+combinedCartList.getShop_img().replace("[","").replace("]","")).into(holder.shopimg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return newArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView shopName, shopId, count;
        ImageView shopimg;
        private OnClickListener onClickListener;

        public ViewHolder(@NonNull View itemView, OnClickListener onClickListener) {
            super(itemView);
            shopName = itemView.findViewById(R.id.shopName);
            shopId = itemView.findViewById(R.id.cartid1);
            count = itemView.findViewById(R.id.cartNo);
            shopimg = itemView.findViewById(R.id.shopimg);
            this.onClickListener = onClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListener.OnClickDetail(getAdapterPosition());
        }
    }

    public interface OnClickListener {
        void OnClickDetail(int position);
    }
}
