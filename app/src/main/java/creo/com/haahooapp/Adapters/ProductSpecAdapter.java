package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import creo.com.haahooapp.Modal.ProductFeaturepojo;
import creo.com.haahooapp.Modal.RatingPojo;
import creo.com.haahooapp.R;

public class ProductSpecAdapter extends RecyclerView.Adapter<ProductSpecAdapter.MyViewHolder> {
    private Context ctx;

    private LayoutInflater inflater;

    private List<ProductFeaturepojo> ratingPojos;

    public ProductSpecAdapter(List<ProductFeaturepojo> ratingPojos,Context context) {
        this.ratingPojos = ratingPojos;
        this.ctx = context;
    }


    @Override
    public ProductSpecAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.spec_adapter, parent, false);
        ProductSpecAdapter.MyViewHolder viewHolder = new ProductSpecAdapter.MyViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProductSpecAdapter.MyViewHolder holder, int position) {
        holder.name.setText(ratingPojos.get(position).getFeature_name());
    }

    @Override
    public int getItemCount() {
        return ratingPojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,review;
        public MyViewHolder(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.spec);
//            review=(TextView)itemView.findViewById(R.id.review);

        }
    }
}