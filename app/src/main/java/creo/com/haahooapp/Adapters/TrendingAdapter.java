package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.List;
import creo.com.haahooapp.Modal.TrendingPojo;
import creo.com.haahooapp.ProductDetailsShop;
import creo.com.haahooapp.R;

public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.ViewHolder> {

    public List<TrendingPojo> downloadPojos;
    Context context1 ;

    public TrendingAdapter(List<TrendingPojo> productPojo, Context context) {
        this.downloadPojos = productPojo;
        this.context1 = context;
    }

    @Override
    public TrendingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.trending_layout, parent, false);
        TrendingAdapter.ViewHolder viewHolder = new TrendingAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TrendingAdapter.ViewHolder holder, int position) {

        holder.name.setText(downloadPojos.get(position).getName());
        holder.price.setText("₹ "+downloadPojos.get(position).getPrice());
        Glide.with(context1).load(downloadPojos.get(position).getImage()).into(holder.image);
        holder.offer.setText("₹ "+downloadPojos.get(position).getOriginal_price());
        holder.percent.setText(downloadPojos.get(position).getPercent()+"%");
        holder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context1, ProductDetailsShop.class);
                intent.putExtra("shopid",downloadPojos.get(position).getShop_id());
                intent.putExtra("productid",downloadPojos.get(position).getId());
                context1.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return downloadPojos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name,price,offer,percent;
        public ImageView image;
        public RelativeLayout rel;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name =  itemView.findViewById(R.id.name);
            this.price = itemView.findViewById(R.id.price);
            this.image = itemView.findViewById(R.id.image);
            this.offer = itemView.findViewById(R.id.offer);
            this.percent = itemView.findViewById(R.id.percent);
            this.rel = itemView.findViewById(R.id.rel);
        }

    }

}