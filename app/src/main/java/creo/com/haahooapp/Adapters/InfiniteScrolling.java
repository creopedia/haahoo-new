package creo.com.haahooapp.Adapters;

import android.widget.AbsListView;

public final class InfiniteScrolling implements AbsListView.OnScrollListener {
  private int prevLastItemIndex = 0;
  private boolean isDataAvailable = true;
  private OnReachEndListener listener;

  @Override
  public void onScrollStateChanged(AbsListView absListView, int state) {
    if (state == SCROLL_STATE_IDLE) {
      int lastVisibleItemIndex = absListView.getLastVisiblePosition();
      int lastItemIndex = absListView.getCount() - 1;
      boolean isReachedEnd = lastVisibleItemIndex == lastItemIndex && lastItemIndex != prevLastItemIndex;
      if (isReachedEnd) {
        prevLastItemIndex = lastItemIndex;
        if (isDataAvailable) {
          if (null != listener) {
            listener.onEndReached();
          }
        }
      }
    }
  }

  @Override
  public void onScroll(AbsListView absListView, int firstVisible, int visibleCount, int totalCount) { }

  public void notifyItemsCountChanged() {
    prevLastItemIndex = 0;
  }

  public void setDataAvalable(boolean isDataAvailable) {
    this.isDataAvailable = isDataAvailable;
  }

  public void setOnReachEndListener(OnReachEndListener listener) {
    this.listener = listener;
  }

  public interface OnReachEndListener {
    void onEndReached();
  }
}