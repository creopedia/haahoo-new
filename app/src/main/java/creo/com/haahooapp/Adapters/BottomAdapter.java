package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import creo.com.haahooapp.Modal.ProductVariant;
import creo.com.haahooapp.R;
import creo.com.haahooapp.interfaces.ItemClick;

public class BottomAdapter extends RecyclerView.Adapter<BottomAdapter.GroceryViewHolder> {
    private List<ProductVariant> horizontalGrocderyList;
    Context context;
    ItemClick itemClick;

    public BottomAdapter(List<ProductVariant> horizontalGrocderyList, Context context, ItemClick itemClick) {
        this.horizontalGrocderyList = horizontalGrocderyList;
        this.context = context;
        this.itemClick = itemClick;
    }

    @Override
    public BottomAdapter.GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bottom_adapter, parent, false);
        BottomAdapter.GroceryViewHolder gvh = new BottomAdapter.GroceryViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(BottomAdapter.GroceryViewHolder holder, final int position) {
        try {
            Glide.with(context).load(horizontalGrocderyList.get(position).getImage()).into(holder.imageView);
            holder.name.setText(horizontalGrocderyList.get(position).getName());
            holder.price.setText(horizontalGrocderyList.get(position).getPrice());
            holder.variant.setText(horizontalGrocderyList.get(position).getVariant_value());
            holder.rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.sendData(horizontalGrocderyList.get(position).getId());
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return horizontalGrocderyList.size();
    }

    public class GroceryViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name, price, variant;
        RelativeLayout rel;

        public GroceryViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            price = view.findViewById(R.id.price);
            variant = view.findViewById(R.id.variant);
            rel = view.findViewById(R.id.rel);

        }
    }
}