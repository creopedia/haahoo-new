package creo.com.haahooapp.Adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import creo.com.haahooapp.Modal.Coupon;
import creo.com.haahooapp.Modal.ViewCoupon;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class CouponListAdapter extends RecyclerView.Adapter<CouponListAdapter.ViewHolder> {

    ArrayList<Coupon> newArrayList;
    Context context;
    ClipData myClip;
    ClipboardManager clipboard;

    public CouponListAdapter(ArrayList<Coupon> newArrayList, Context context) {
        this.newArrayList = newArrayList;
        this.context = context;
        clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
    }

    @NonNull
    @Override
    public CouponListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.reward_adapter, parent, false);
        return new CouponListAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull CouponListAdapter.ViewHolder holder, int position) {
        final Coupon coupon = newArrayList.get(position);
        Glide.with(context).load("https://testapi.creopedia.com/media/files/events_add/reward2.jpg").into(holder.iv);
        holder.name.setText(coupon.getCoupon_code());
        holder.website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://www.study4career.com/"));
                context.startActivity(viewIntent);
            }
        });
        holder.copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = holder.name.getText().toString();
                myClip = ClipData.newPlainText("text", text);

                clipboard.setPrimaryClip(myClip);

                Toast.makeText(context, "Text Copied", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return newArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView iv, copy, website;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.text);
            copy = itemView.findViewById(R.id.copy);
            website = itemView.findViewById(R.id.website);
        }
    }
}
