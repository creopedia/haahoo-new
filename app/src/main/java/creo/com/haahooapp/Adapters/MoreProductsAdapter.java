package creo.com.haahooapp.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.List;
import creo.com.haahooapp.Modal.ProductPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.OnBottomReachedListener;

public class MoreProductsAdapter extends RecyclerView.Adapter<MoreProductsAdapter.ViewHolder> {

  public List<ProductPojo> productPojo;
  Context context1 ;
  OnBottomReachedListener onBottomReachedListener;

  public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){

    this.onBottomReachedListener = onBottomReachedListener;
  }

  public MoreProductsAdapter(List<ProductPojo> productPojo,Context context) {
    this.productPojo = productPojo;
    this.context1 = context;
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public int getItemViewType(int position) {
    return position;
  }



  @Override
  public MoreProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View listItem= layoutInflater.inflate(R.layout.more_products_adapter, parent, false);
    MoreProductsAdapter.ViewHolder viewHolder = new MoreProductsAdapter.ViewHolder(listItem);
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(MoreProductsAdapter.ViewHolder holder, int position) {
    if (position == productPojo.size() - 1){

      onBottomReachedListener.onBottomReached(position);

    }

    holder.name.setText(productPojo.get(position).getName());
    holder.price.setText(productPojo.get(position).getDescription());
//        holder.imageView.setImageResource((Glide.with(context).load(productPojo[position].getImage())));
    Glide.with(context1).load(ApiClient.BASE_URL+"media/"+productPojo.get(position).getImage()).into(holder.imageView);

  }

  @Override
  public int getItemCount() {
    return productPojo.size();
  }


  public static class ViewHolder extends RecyclerView.ViewHolder {
    public ImageView imageView;
    public TextView name,price;

    public ViewHolder(View itemView) {
      super(itemView);
      this.imageView =  itemView.findViewById(R.id.icon);
      this.name =  itemView.findViewById(R.id.name);
      this.price = itemView.findViewById(R.id.price);
    }

  }
}
