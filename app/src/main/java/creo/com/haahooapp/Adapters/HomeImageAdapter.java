package creo.com.haahooapp.Adapters;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.util.ArrayList;
import creo.com.haahooapp.R;

public class HomeImageAdapter extends PagerAdapter {

  private ArrayList<Integer> IMAGES;
  private LayoutInflater inflater;
  private Context context;

  public HomeImageAdapter(Context context,ArrayList<Integer> IMAGES) {
    this.IMAGES = IMAGES;
    inflater = LayoutInflater.from(context);
    this.context = context;
  }

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((View) object);
  }

  @Override
  public Object instantiateItem(ViewGroup view, int position) {
    View imageLayout = inflater.inflate(R.layout.sliding_images, view, false);

    assert imageLayout != null;
    final ImageView imageView = (ImageView) imageLayout
            .findViewById(R.id.image);


    imageView.setImageResource(IMAGES.get(position));

    view.addView(imageLayout, 0);

    return imageLayout;
  }

  @Override
  public int getCount() {
    return IMAGES.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view.equals(object);
  }
}
