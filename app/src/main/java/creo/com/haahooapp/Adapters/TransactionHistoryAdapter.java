package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import creo.com.haahooapp.Modal.CartPojo;
import creo.com.haahooapp.Modal.TransactionPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.TransactionDetails;
import creo.com.haahooapp.WishList;
import creo.com.haahooapp.config.ApiClient;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.ViewHolder> {

    public List<TransactionPojo> productPojo;
    Context context1 ;

    public TransactionHistoryAdapter(List<TransactionPojo> productPojo,Context context) {
        this.productPojo = productPojo;
        this.context1 = context;
    }

    @Override
    public TransactionHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.transaction_history, parent, false);
        final TransactionHistoryAdapter.ViewHolder viewHolder = new TransactionHistoryAdapter.ViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TransactionHistoryAdapter.ViewHolder holder, int position) {

        holder.name.setText(productPojo.get(position).getName());

        holder.date.setText(productPojo.get(position).getDate());
        Glide.with(context1).load(ApiClient.BASE_URL+"media/icon_product/cash.jpg").into(holder.imageView);
        holder.amount.setText("Rs. "+productPojo.get(position).getAmount());
        //holder.amount.setTextColor(Color.parseColor("#1aab00"));

    }

    @Override
    public int getItemCount() {
        return productPojo.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView name,amount,date;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView =  itemView.findViewById(R.id.image);
            this.name =  itemView.findViewById(R.id.name);
            this.amount = itemView.findViewById(R.id.amount);
            this.date = itemView.findViewById(R.id.date);
            this.relativeLayout = itemView.findViewById(R.id.relative);
        }

    }

}

