package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import creo.com.haahooapp.CategoryShop;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.R;

public class NewShopByCategory extends RecyclerView.Adapter<NewShopByCategory.GroceryViewHolder> {

    public Context context;
    public ArrayList<Category> categories = new ArrayList<>();

    public NewShopByCategory(Context context, ArrayList<Category> categories) {
        this.context = context;
        this.categories = categories;
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }


    @Override
    public NewShopByCategory.GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_shop_by_category, parent, false);
        NewShopByCategory.GroceryViewHolder gvh = new NewShopByCategory.GroceryViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(NewShopByCategory.GroceryViewHolder holder, final int position) {

        Glide.with(context).load(categories.get(position).getImage()).into(holder.imageView);
        holder.name.setText(categories.get(position).getName());
        holder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CategoryShop.class);
                intent.putExtra("categoryid",categories.get(position).getId());
                intent.putExtra("categoryname",categories.get(position).getName());
                context.startActivity(intent);
            }
        });

    }


    public class GroceryViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name, price, variant;
        RelativeLayout rel;

        public GroceryViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.img);
            name = view.findViewById(R.id.title);
            rel = view.findViewById(R.id.rel);


        }
    }
}