package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import creo.com.haahooapp.Modal.StoryPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.StoryActivityOthers;
import creo.com.haahooapp.config.ApiClient;

public class AllStoriesAdapter extends RecyclerView.Adapter<AllStoriesAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<StoryPojo> dataModelArrayList;
    public Context context1 ;

    public AllStoriesAdapter(Context ctx, ArrayList<StoryPojo> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public AllStoriesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.all_stories_adapter, parent, false);
        AllStoriesAdapter.MyViewHolder holder = new AllStoriesAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(AllStoriesAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(dataModelArrayList.get(position).name);
        Glide
                .with( context1 )
                .load( dataModelArrayList.get(position).getImage() )
                .into( holder.iv );
        holder.name.setText(dataModelArrayList.get(position).getName());
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dt = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String check = dateFormat.format(dt);
            if (check.equals(dataModelArrayList.get(position).getDate())) {
                holder.time.setText("Today at "+dataModelArrayList.get(position).getTime());
            }
            if (!(check.equals(dataModelArrayList.get(position).getDate()))){
                holder.time.setText("Yesterday at "+dataModelArrayList.get(position).getTime());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiClient.story_id = dataModelArrayList.get(position).getStory_id();
                ApiClient.story_id_name = dataModelArrayList.get(position).getName();
                context1.startActivity(new Intent(context1, StoryActivityOthers.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView iv;
        TextView name,time;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            time = itemView.findViewById(R.id.time);
            cardView = itemView.findViewById(R.id.card);
        }

    }
}
