package creo.com.haahooapp.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahooapp.ChooseAddress;
import creo.com.haahooapp.EditAddress;
import creo.com.haahooapp.Modal.AddressPojo;
import creo.com.haahooapp.OrderSummary;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class ChooseAddressShopAdapter extends RecyclerView.Adapter<ChooseAddressShopAdapter.ViewHolder>{


    public List<AddressPojo> productPojo;
    Context context1 ;
    private AlertDialog mDialog;
    public int lastSelectedPosition = -1;

    public ChooseAddressShopAdapter(List<AddressPojo> productPojo,Context context) {
        this.productPojo = productPojo;
        this.context1 = context;
    }

    @Override
    public ChooseAddressShopAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.choose_address_row, parent, false);
        ChooseAddressShopAdapter.ViewHolder viewHolder = new ChooseAddressShopAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChooseAddressShopAdapter.ViewHolder holder, final int position) {

        holder.name.setText(productPojo.get(position).getName());
        holder.address.setText(productPojo.get(position).getAddress());
        holder.phone.setText("Phone No : "+productPojo.get(position).getPhone_no());
        if(productPojo.get(position).getDef().equals("1")){
            holder.def.setText("Default");
        }
        if(productPojo.get(position).getDef().equals("0")){
            holder.def.setText("Set as default");
        }
        holder.pincode.setText("Pincode : "+productPojo.get(position).getPincode());
        holder.select.setChecked(lastSelectedPosition==position);
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiClient.address = ApiClient.addressids.get(position);
                context1.startActivity(new Intent(context1, EditAddress.class));
            }
        });

        holder.def.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiClient.address = ApiClient.addressids.get(position);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"booking/set_default_address/",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject1 = new JSONObject(response);
                                    Log.d("dsdfgfd","erws"+jsonObject1);
                                    String message = jsonObject1.optString("message");
                                    if(message.equals("Success")){
                                        // Toast.makeText(context1,"Address set successfully",Toast.LENGTH_SHORT).show();
                                        holder.def.setText("Default");
                                        context1.startActivity(new Intent(context1, ChooseAddress.class));
                                        // context1.startActivity(new Intent(context1,ChooseAddress.class));
                                    }
                                    if(!(message.equals("Success"))){
                                        Toast.makeText(context1,"Failed to set default address",Toast.LENGTH_SHORT).show();
                                    }

                                }catch (JSONException e){
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context1,error.toString(),Toast.LENGTH_LONG).show();
                            }
                        }){
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("address_id",ApiClient.address);

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Authorization","Token "+ApiClient.token);
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(context1);
                requestQueue.add(stringRequest);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context1);
                builder.setTitle("Confirm Delete");
                builder.setMessage("Do you really want to delete this address ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ApiClient.address = ApiClient.addressids.get(position);
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"booking/address_del/",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject1 = new JSONObject(response);
                                            Log.d("dsdfgfd","erws"+jsonObject1);
                                            String message = jsonObject1.optString("message");
                                            if(message.equals("Success")){
                                                Toast.makeText(context1,"Address deleted successfully",Toast.LENGTH_SHORT).show();
                                                context1.startActivity(new Intent(context1,ChooseAddress.class));
                                            }
                                            if(!(message.equals("Success"))){
                                                Toast.makeText(context1,"Failed to delete address",Toast.LENGTH_SHORT).show();
                                            }


                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(context1,error.toString(),Toast.LENGTH_LONG).show();
                                    }
                                }){
                            @Override
                            protected Map<String,String> getParams(){
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("address_id",ApiClient.address);

                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {

                                Map<String, String>  params = new HashMap<String, String>();
                                params.put("Authorization","Token "+ApiClient.token);
                                return params;
                            }
                        };

                        RequestQueue requestQueue = Volley.newRequestQueue(context1);
                        requestQueue.add(stringRequest);
                        // Toast.makeText(context1, "You've choosen to delete all records", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(context1, "You've changed your mind to delete all records", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.show();
//

//
//            }
//        });
            }
        });

        //Log.d("Thejsonstringis " ,"dsfs"+ApiClient.addressids.get(position));

    }

    @Override
    public int getItemCount() {
        return productPojo.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView,remove;
        public TextView name,address,phone,pincode,edit,delete,def;
        public RadioButton select;

        public ViewHolder(View itemView) {
            super(itemView);
            this.select = itemView.findViewById(R.id.radio);
            this.name =  itemView.findViewById(R.id.name);
            this.address = itemView.findViewById(R.id.address);
            this.pincode = itemView.findViewById(R.id.pincode);
            this.phone = itemView.findViewById(R.id.phone);
            this.edit = itemView.findViewById(R.id.edit);
            this.delete = itemView.findViewById(R.id.delete);
            this.def = itemView.findViewById(R.id.def);


            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    ApiClient.address = ApiClient.addressids.get(lastSelectedPosition);
                    //  Log.d("possss","posiion"+lastSelectedPosition+getAdapterPosition());
                    notifyDataSetChanged();

                    if(ApiClient.ids.size()>0) {
                        context1.startActivity(new Intent(context1, OrderSummary.class));
                    }
                }
            });
        }

    }

}