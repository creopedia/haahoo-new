package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import creo.com.haahooapp.Modal.StoryPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.StoryActivityOthers;
import creo.com.haahooapp.camera.MainActivity;
import creo.com.haahooapp.config.ApiClient;

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<StoryPojo> dataModelArrayList;
    public Context context1 ;

    public StoryAdapter(Context ctx, ArrayList<StoryPojo> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public StoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.story_ui, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(StoryAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(dataModelArrayList.get(position).name);
        Glide
                .with( context1 )
                .load( dataModelArrayList.get(position).getImage() )
                .into( holder.iv );
        holder.name.setText(dataModelArrayList.get(position).getName());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiClient.story_id = dataModelArrayList.get(position).getStory_id();
                ApiClient.story_id_name = dataModelArrayList.get(position).getName();
                context1.startActivity(new Intent(context1, StoryActivityOthers.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView iv;
        TextView name;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            cardView = itemView.findViewById(R.id.card);
        }

    }
}
