package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.Modal.StoryPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class SlidingContentAdapter extends RecyclerView.Adapter<SlidingContentAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<StoryPojo> dataModelArrayList;
    public Context context1 ;

    public SlidingContentAdapter(Context ctx, ArrayList<StoryPojo> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public SlidingContentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.sliding_content, parent, false);
        SlidingContentAdapter.MyViewHolder holder = new SlidingContentAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(SlidingContentAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(dataModelArrayList.get(position).getName());
        holder.send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sendNotification(dataModelArrayList.get(position).getToken(),dataModelArrayList.get(position).getId(),dataModelArrayList.get(position).getShop_id());

                RequestQueue queue = Volley.newRequestQueue(context1);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"virtual_shop/invitation_sending_shop/",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Display the response string.
                                try {
                                    JSONObject obj = new JSONObject(response);
                                    Log.d("notifications","shopprods"+obj);
                                    String message = obj.optString("message");
                                    if (message.equals("success")){
                                        holder.send.setVisibility(View.GONE);
                                        holder.tick.setVisibility(View.VISIBLE);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("shop_id", dataModelArrayList.get(position).getShop_id());
                        params.put("id",dataModelArrayList.get(position).getId());
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders()  {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", "Token " + dataModelArrayList.get(position).getToken());
                        return params;
                    }
                };

                // Add the request to the RequestQueue.
                queue.add(stringRequest);
            }
        });

    }





    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
       public TextView name,send;
       public ImageView tick;
        public MyViewHolder(View itemView) {
            super(itemView);

            this.name = itemView.findViewById(R.id.name);
            send = itemView.findViewById(R.id.send);
            tick = itemView.findViewById(R.id.tick);
        }

    }

    public void updateList(ArrayList<StoryPojo> storyPojos){
        dataModelArrayList = new ArrayList<>();
        dataModelArrayList.addAll(storyPojos);
        notifyDataSetChanged();
    }
}
