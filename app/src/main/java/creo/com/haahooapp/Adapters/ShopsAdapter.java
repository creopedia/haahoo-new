package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.R;

public class ShopsAdapter extends RecyclerView.Adapter<ShopsAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    public Context context;
    public ArrayList<Category> categories = new ArrayList<>();

    public ShopsAdapter(Context context, ArrayList<Category> categories) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.categories = categories;
    }

    @Override
    public ShopsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.shops_adapter, parent, false);
        ShopsAdapter.MyViewHolder holder = new ShopsAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ShopsAdapter.MyViewHolder holder, final int position) {

        Glide.with(context).load(categories.get(position).getImage()).into(holder.imageView);
        String name = categories.get(position).getName().substring(0,1).toUpperCase();
        String title = name+categories.get(position).getName().substring(1);
        holder.title.setText(title);
        if (position==0||position==1)
        {
            holder.imageView.setVisibility(View.VISIBLE);
            holder.img1.setVisibility(View.GONE);
            holder.card.setVisibility(View.GONE);
        }
        else{
            holder.imageView.setVisibility(View.GONE);
            holder.img1.setVisibility(View.VISIBLE);
            Glide.with(context).load(categories.get(position).getImage()).into(holder.img1);

        }

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        CircularImageView imageView;
        ImageView img1;
        TextView title;
        CardView card;

        public MyViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.img);
            title = itemView.findViewById(R.id.title);
            img1 = itemView.findViewById(R.id.img1);
            card = itemView.findViewById(R.id.card);

        }

    }

//    @Override
//    public int getCount() {
//        return categories.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return null;
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return 0;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        LayoutInflater inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View gridView;
//
//        if (convertView == null){
//
//            gridView = new View(context);
//            gridView = inflater.inflate(R.layout.shops_adapter ,parent, false);
//
//            ImageView imageView = gridView.findViewById(R.id.img);
//            TextView textView = gridView.findViewById(R.id.title);
//            Glide.with(context).load(categories.get(position).getImage()).into(imageView);
//            textView.setText(categories.get(position).getName());
//
//        }
//        else {
//            gridView = (View) convertView;
//        }
//
//        return gridView;
//    }
}
