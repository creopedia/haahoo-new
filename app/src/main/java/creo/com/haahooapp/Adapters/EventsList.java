package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;
import androidx.recyclerview.widget.RecyclerView;
import creo.com.haahooapp.Modal.DownloadPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class EventsList extends RecyclerView.Adapter<EventsList.ViewHolder>{

    public List<DownloadPojo> downloadPojos;
    Context context1 ;

    public EventsList(List<DownloadPojo> productPojo,Context context) {
        this.downloadPojos = productPojo;
        this.context1 = context;
    }

    @Override
    public EventsList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.events_list, parent, false);
        EventsList.ViewHolder viewHolder = new EventsList.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EventsList.ViewHolder holder, int position) {

        holder.textView.setText(downloadPojos.get(position).getName());
        Glide.with(context1).load(ApiClient.BASE_URL+"media/"+ApiClient.evt_img).into(holder.img);
        holder.location.setText(ApiClient.evt_location);
        holder.date.setText(ApiClient.evt_date);

    }

    @Override
    public int getItemCount() {
        return downloadPojos.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView,location,date;
        public ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView =  itemView.findViewById(R.id.name);
            this.img = itemView.findViewById(R.id.img);
            this.location = itemView.findViewById(R.id.location);
            this.date = itemView.findViewById(R.id.date);
        }

    }
}
