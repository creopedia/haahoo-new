package creo.com.haahooapp.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.EditReview;
import creo.com.haahooapp.Modal.Reviewpojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

import static android.content.Context.MODE_PRIVATE;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Reviewpojo> dataModelArrayList;
    public Context context1 ;

    public ReviewAdapter(Context ctx, ArrayList<Reviewpojo> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public ReviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.review_adapter, parent, false);
        ReviewAdapter.MyViewHolder holder = new ReviewAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ReviewAdapter.MyViewHolder holder, final int position) {

        holder.name.setText(dataModelArrayList.get(position).getName());
        holder.ratingBar.setRating(Float.parseFloat(dataModelArrayList.get(position).getRating()));
        holder.date.setText(dataModelArrayList.get(position).getDate());
        holder.desc.setText(dataModelArrayList.get(position).getReview());
        if (dataModelArrayList.get(position).getName().equals("Your Review")){
            holder.edit.setVisibility(View.VISIBLE);
            holder.delete.setVisibility(View.VISIBLE);
        }

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(context1, EditReview.class);
            intent.putExtra("productrating",dataModelArrayList.get(position).getRating());
            intent.putExtra("description",dataModelArrayList.get(position).getReview());
            intent.putExtra("shoprating",dataModelArrayList.get(position).getShop_rating());
            intent.putExtra("productname",dataModelArrayList.get(position).getProductName());
            intent.putExtra("productimage",dataModelArrayList.get(position).getProductImage());
            intent.putExtra("productid",dataModelArrayList.get(position).getProductid());
            intent.putExtra("productprice",dataModelArrayList.get(position).getPrice());
            intent.putExtra("reviewid",dataModelArrayList.get(position).getId());
            context1.startActivity(intent);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:

                                deleteReview(dataModelArrayList.get(position).getId());
                                //Yes button clicked
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context1);
                builder.setMessage("Confirm delete?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

TextView name,date,desc,edit,delete;
RatingBar ratingBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            ratingBar = itemView.findViewById(R.id.rating);
            date = itemView.findViewById(R.id.date);
            desc = itemView.findViewById(R.id.desc);
            edit = itemView.findViewById(R.id.edit);
            delete = itemView.findViewById(R.id.delete);

        }

    }


    public void deleteReview(String review_id){

        SharedPreferences pref = context1.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context1);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"api_shop_app/pdt_rating_remove/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("delllll", "jsonarray" + jsonObject);
                            String message = jsonObject.optString("message");
                            if (message.equals("success")){
                                ((Activity)context1).finish();
                            }
                            if (!(message.equals("success"))){
                                Toast.makeText(context1,"Failed to delete review",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Toast.makeText(context1,"//Error",Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() throws AuthFailureError {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("id", review_id);
                return params;
            }

            @Override
            public java.util.Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}