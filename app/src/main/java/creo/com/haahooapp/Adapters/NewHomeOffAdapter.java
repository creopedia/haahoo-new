package creo.com.haahooapp.Adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;

import java.util.List;

import creo.com.haahooapp.CategoryShop;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.R;

public class NewHomeOffAdapter extends RecyclerView.Adapter<NewHomeOffAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private List<Category> dataModelArrayList;
    public Context context1;
    ClipData myClip;
    ClipboardManager clipboard;
    boolean flag = false;

    public NewHomeOffAdapter(Context ctx, List<Category> dataModelArrayList) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        clipboard = (ClipboardManager) context1.getSystemService(Context.CLIPBOARD_SERVICE);
    }

    @Override
    public NewHomeOffAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.off_adapter, parent, false);
        NewHomeOffAdapter.MyViewHolder holder = new NewHomeOffAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(NewHomeOffAdapter.MyViewHolder holder, final int position) {
        try {
            //Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);
            Glide.with(context1)
                    .load(dataModelArrayList.get(position).getImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            flag = true;
                            try {
                                Log.d("JHGCGHCGH","jhghhgc");
                            }catch (Exception e1){
                                e1.printStackTrace();
                            }
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).apply(new RequestOptions().error(R.drawable.noimage)).into(holder.iv);
            if (flag) {

                Glide.with(context1).load("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRUN4SeftFS5bldudKAcO3aqQEbs-RXMz9sXw&usqp=CAU").into(holder.iv);
            }
            holder.name.setText(dataModelArrayList.get(position).getName());
            holder.rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context1, CategoryShop.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("categoryid", dataModelArrayList.get(position).getId());
                    intent.putExtra("categoryname", dataModelArrayList.get(position).getName());
                    context1.startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView price, name, qty, delivery;
        LinearLayout linearLayout;
        ImageView iv, copy;
        RelativeLayout rel;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            rel = itemView.findViewById(R.id.rel);

        }

    }
}