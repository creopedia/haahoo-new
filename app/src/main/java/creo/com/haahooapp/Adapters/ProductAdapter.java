package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.List;

import creo.com.haahooapp.Modal.ProdPojo;
import creo.com.haahooapp.Modal.ProductPojo;
import creo.com.haahooapp.R;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.GroceryViewHolder> {
    private List<ProdPojo> horizontalGrocderyList;
    Context context;

    public ProductAdapter(List<ProdPojo> horizontalGrocderyList, Context context) {
        this.horizontalGrocderyList = horizontalGrocderyList;
        this.context = context;
    }

    @Override
    public ProductAdapter.GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_details_adapter, parent, false);
        ProductAdapter.GroceryViewHolder gvh = new ProductAdapter.GroceryViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(ProductAdapter.GroceryViewHolder holder, final int position) {
        Glide.with(context).load(horizontalGrocderyList.get(position).getImage()).into(holder.imageView);
        holder.name.setText(horizontalGrocderyList.get(position).getName());
        holder.price.setText(horizontalGrocderyList.get(position).getPrice());
        holder.qty.setText(horizontalGrocderyList.get(position).getQty());
        holder.status.setText(horizontalGrocderyList.get(position).getStatus());
    }

    @Override
    public int getItemCount() {
        return horizontalGrocderyList.size();
    }

    public class GroceryViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView name, price, status,qty;

        public GroceryViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            price = view.findViewById(R.id.price);
            status = view.findViewById(R.id.status);
            qty = view.findViewById(R.id.qty);
        }
    }
}