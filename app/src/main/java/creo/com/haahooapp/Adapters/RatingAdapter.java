package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import creo.com.haahooapp.Modal.RatingPojo;
import creo.com.haahooapp.R;

public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.MyViewHolder> {
  private Context ctx;

  private LayoutInflater inflater;

  private  RatingPojo[]ratingPojos;

  public RatingAdapter(RatingPojo[] ratingPojos) {
    this.ratingPojos = ratingPojos;
  }


  @Override
  public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View listItem= layoutInflater.inflate(R.layout.search_adapter, parent, false);
    RatingAdapter.MyViewHolder viewHolder = new RatingAdapter.MyViewHolder(listItem);
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(MyViewHolder holder, int position) {
    final RatingPojo ratingPojo = ratingPojos[position];
    holder.name.setText(ratingPojos[position].getName());
    holder.review.setText(ratingPojos[position].getReviews());

  }

  @Override
  public int getItemCount() {
    return ratingPojos.length;
  }

  public class MyViewHolder extends RecyclerView.ViewHolder {
    TextView name,review;
    public MyViewHolder(View itemView) {
      super(itemView);
      name=(TextView)itemView.findViewById(R.id.name);
//            review=(TextView)itemView.findViewById(R.id.review);

    }
  }
}