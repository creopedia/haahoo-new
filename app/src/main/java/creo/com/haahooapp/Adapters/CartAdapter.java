package creo.com.haahooapp.Adapters;

import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahooapp.Cart;
import creo.com.haahooapp.Modal.CartPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.CartInterface;
import creo.com.haahooapp.interfaces.ItemClick;

import static android.content.Context.MODE_PRIVATE;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    public List<CartPojo> productPojo;
    public Context context1;
    public CartInterface itemClick;

    public CartAdapter(List<CartPojo> productPojo, Context context, CartInterface itemClick) {
        this.productPojo = productPojo;
        this.context1 = context;
        this.itemClick = itemClick;
    }

    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final View listItem = layoutInflater.inflate(R.layout.cart_adapter, parent, false);
        final CartAdapter.ViewHolder viewHolder = new CartAdapter.ViewHolder(listItem);
        viewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context1 instanceof Cart) {
                    ApiClient.count = ApiClient.count - Integer.parseInt(productPojo.get(viewHolder.getAdapterPosition()).getQuantity().trim());
                    ((Cart) context1).remove(ApiClient.ids.get(viewHolder.getAdapterPosition()), ApiClient.cart_id.get(viewHolder.getAdapterPosition()));
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CartAdapter.ViewHolder holder, int position) {

        holder.name.setText(productPojo.get(position).getName());
        int vv = Integer.parseInt(productPojo.get(position).getPrice()) * Integer.parseInt(productPojo.get(position).getQuantity());
        holder.price.setText("₹ " + vv);
        holder.quantity.setText("Quantity : " + productPojo.get(position).getQuantity());
        Glide.with(context1).load(productPojo.get(position).getImage()).into(holder.imageView);
        holder.qty.setText(productPojo.get(position).getQuantity());
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(holder.qty.getText().toString()) >= 1) {
                    if (Integer.parseInt(holder.qty.getText().toString()) >= Integer.parseInt(productPojo.get(position).getStock())){
                        Toast.makeText(context1,"No more stock",Toast.LENGTH_SHORT).show();
                    }
                    if (Integer.parseInt(holder.qty.getText().toString()) < Integer.parseInt(productPojo.get(position).getStock())) {
                        String items = holder.qty.getText().toString();
                        int value = Integer.parseInt(items);
                        String added = String.valueOf(value + 1);
                        ApiClient.count = ApiClient.count++;
                        itemClick.sendData(productPojo.get(position).getProductid(), added, ApiClient.virtual.get(position), productPojo.get(position).getShop_id(), productPojo.get(position).getVariant());
                    }
                }
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(holder.qty.getText().toString()) > 1) {
                    String items = holder.qty.getText().toString();
                    int value = Integer.parseInt(items);
                    String calc = String.valueOf(value - 1);
                    ApiClient.count = ApiClient.count--;
                    itemClick.sendData(productPojo.get(position).getProductid(), calc, ApiClient.virtual.get(position), productPojo.get(position).getShop_id(), productPojo.get(position).getVariant());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return productPojo.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        TextView remove, qty, id;
        public TextView name, price, quantity, minus, plus, delivery;
        CardView qty1, increase, decrease;

        public ViewHolder(final View itemView) {

            super(itemView);
            this.imageView = itemView.findViewById(R.id.img);
            this.remove = itemView.findViewById(R.id.remove);
            this.name = itemView.findViewById(R.id.title);
            this.quantity = itemView.findViewById(R.id.quantity);
            this.price = itemView.findViewById(R.id.price);
            this.qty = itemView.findViewById(R.id.qty);
            this.minus = itemView.findViewById(R.id.minus);
            this.plus = itemView.findViewById(R.id.plus);
            this.delivery = itemView.findViewById(R.id.delivery);

        }

    }

}