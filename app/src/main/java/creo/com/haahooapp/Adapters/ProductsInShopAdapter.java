package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.R;

public class ProductsInShopAdapter extends RecyclerView.Adapter<ProductsInShopAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Category> dataModelArrayList;
    public Context context1 ;

    public ProductsInShopAdapter(Context ctx, ArrayList<Category> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public ProductsInShopAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.products_in_shop_adapter, parent, false);
        ProductsInShopAdapter.MyViewHolder holder = new ProductsInShopAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductsInShopAdapter.MyViewHolder holder, final int position) {

        holder.name.setText(dataModelArrayList.get(position).getName());
        holder.price.setText("₹ "+dataModelArrayList.get(position).getPrice().trim());
        Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView price, name, qty,delivery;
        LinearLayout linearLayout;
        ImageView iv;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.title);
            price = (TextView) itemView.findViewById(R.id.price);
            iv = itemView.findViewById(R.id.image);
        }

    }
}
