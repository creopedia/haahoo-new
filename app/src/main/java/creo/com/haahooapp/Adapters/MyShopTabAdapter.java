package creo.com.haahooapp.Adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import creo.com.haahooapp.BusinessFragment;
import creo.com.haahooapp.MyShopProductsFragment;

public class MyShopTabAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public MyShopTabAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                MyShopProductsFragment homeFragment = new MyShopProductsFragment();
                return homeFragment;

            case 1:
                BusinessFragment sportFragment = new BusinessFragment();
                return sportFragment;

            default:
                return null;

        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}