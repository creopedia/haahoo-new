package creo.com.haahooapp.Adapters;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.MyShopNew;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

import static android.bluetooth.BluetoothGattDescriptor.PERMISSION_WRITE;
import static android.content.Context.MODE_PRIVATE;

public class MyShopAdapter extends RecyclerView.Adapter<MyShopAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Category> dataModelArrayList;
    public Context context1;
    public String fileUri;

    public MyShopAdapter(Context ctx, ArrayList<Category> dataModelArrayList) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;

    }

    @Override
    public MyShopAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.myshopadapter, parent, false);
        MyShopAdapter.MyViewHolder holder = new MyShopAdapter.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyShopAdapter.MyViewHolder holder, final int position) {

        Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);
        holder.name.setText(dataModelArrayList.get(position).getName());
        holder.old_price.setText("Rs. " + dataModelArrayList.get(position).getOriginal_price().trim());
        holder.newprice.setText("Rs. " + dataModelArrayList.get(position).getEdited_price().trim());
        int margin = (Integer.parseInt(dataModelArrayList.get(position).getEdited_price().trim())) - Integer.parseInt(dataModelArrayList.get(position).getOriginal_price().trim());
        holder.margin_price.setText("Earnings Rs. " + String.valueOf(margin));
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getShareURL(dataModelArrayList.get(position).getProduct_id(), dataModelArrayList.get(position).getImage(), "Rs. " + dataModelArrayList.get(position).getEdited_price(), dataModelArrayList.get(position).getName());
            }
        });
        holder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context1);
                builder.setTitle("Confirm remove");
                builder.setMessage("Are you sure you want to delete ?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        removeProduct(dataModelArrayList.get(position).getId());
                        dataModelArrayList.remove(position);
                        notifyItemRemoved(position);
                        notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = LayoutInflater.from(context1);
                View view = layoutInflater.inflate(R.layout.edit_price, null);
                final AlertDialog dialog = new AlertDialog.Builder(context1)
                        .setView(view)
                        .create();
                final EditText userInput = (EditText) view
                        .findViewById(R.id.editTextDialogUserInput);
                userInput.setText(dataModelArrayList.get(position).getEdited_price().trim());
                ImageView image = view.findViewById(R.id.image);
                TextView range = view.findViewById(R.id.range);
                Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(image);
                range.setText("You can set a price between "+dataModelArrayList.get(position).getOriginal_price()+" and "+dataModelArrayList.get(position).getReselling_max());
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        CardView okcard = view.findViewById(R.id.okcard);
                        CardView cancelcard = view.findViewById(R.id.cancelcard);
                        cancelcard.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        okcard.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                if (userInput.getText().toString().length() == 0) {
                                    userInput.setError("Please enter valid price");
                                }
                                if (userInput.getText().toString().length() != 0) {
                                    if (Integer.parseInt(userInput.getText().toString().trim()) < Integer.parseInt(dataModelArrayList.get(position).getOriginal_price().trim())) {
                                        userInput.setError("Price cannot be lesser than original amount");
                                    }
                                    if (Integer.parseInt(userInput.getText().toString().trim()) > Integer.parseInt(dataModelArrayList.get(position).getReselling_max())) {
                                        userInput.setError("Maximum reselling price is " + dataModelArrayList.get(position).getReselling_max());
                                    }
                                    if ((Integer.parseInt(userInput.getText().toString().trim()) <= Integer.parseInt(dataModelArrayList.get(position).getReselling_max().trim())) && (Integer.parseInt(userInput.getText().toString().trim()) >= Integer.parseInt(dataModelArrayList.get(position).getOriginal_price().trim()))) {
                                        holder.newprice.setText("Rs. " + userInput.getText().toString());
                                        editprice(dataModelArrayList.get(position).getId(), userInput.getText().toString());
                                        dialog.dismiss();
                                    }
                                }
                            }
                        });
                    }
                });
                dialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, old_price, newprice, margin_price;
        LinearLayout linearLayout;
        ImageView iv, remove, close;
        RelativeLayout share, edit;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            iv = itemView.findViewById(R.id.image);
            old_price = itemView.findViewById(R.id.oldprice);
            newprice = itemView.findViewById(R.id.newprice);
            margin_price = itemView.findViewById(R.id.margin_price);
            share = itemView.findViewById(R.id.share);
            edit = itemView.findViewById(R.id.edit);
            close = itemView.findViewById(R.id.close);
        }

    }

    public void getShareURL(String product_id, String image, String description, String prodcutname) {
        SharedPreferences pref = context1.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context1);
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL + "virtual_shop/virtual_pdt_share/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject obj = new JSONObject(response);
                            String data = obj.optString("data");
                            String shareBody = data;
//                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                            sharingIntent.setType("text/plain");
//                            PermissionListener permissionlistener = new PermissionListener() {
//                                @Override
//                                public void onPermissionGranted() {
//                                    sharingIntent.putExtra(Intent.EXTRA_TEXT, "Product Name : " + prodcutname + "\n" + "Product Price : " + description + "\n" + "Product URL : " + shareBody);
//                                    Picasso.with(context1).load(image).into(new Target() {
//                                        @Override
//                                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                                            File mydir = new File(Environment.getExternalStorageDirectory() + "/.HaaHoo");
//                                            if (!mydir.exists()) {
//                                                mydir.mkdirs();
//                                            }
//                                            try {
//                                                fileUri = mydir.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".jpg";
//                                                FileOutputStream outputStream = new FileOutputStream(fileUri);
//                                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
//                                                outputStream.flush();
//                                                outputStream.close();
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                            }
//                                        }
//
//                                        @Override
//                                        public void onBitmapFailed(Drawable errorDrawable) {
//
//                                        }
//
//                                        @Override
//                                        public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                                        }
//                                    });
//                                    try {
//                                        Uri uri = Uri.parse(fileUri);
//                                        sharingIntent.setType("image/*");
//                                        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                                        context1.startActivity(Intent.createChooser(sharingIntent, "Share to"));
//                                    } catch (IllegalStateException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//
//                                @Override
//                                public void onPermissionDenied(List<String> deniedPermissions) {
//
//                                }
//
//
//                            };
//                            TedPermission.with(context1)
//                                    .setPermissionListener(permissionlistener)
//                                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                                    .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
//                                    .check();

                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
                            PermissionListener permissionlistener = new PermissionListener() {
                                @Override
                                public void onPermissionGranted() {
                                    sharingIntent.putExtra(Intent.EXTRA_TEXT, "Product Name : " +prodcutname + "\n" + "Product Price : " + description + "\n" + "Product URL : " + shareBody);
                                    Picasso.with(context1).load(image).into(new Target() {
                                        @Override
                                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                            File mydir = new File(Environment.getExternalStorageDirectory() + "/.HaaHoo");
                                            if (!mydir.exists()) {
                                                mydir.mkdirs();
                                            }
                                            try {
                                                fileUri = mydir.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".jpg";
                                                FileOutputStream outputStream = new FileOutputStream(fileUri);
                                                //Log.d("FILEEEURIII", "jkhgf" + fileUri.toString());
                                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                                                outputStream.flush();
                                                outputStream.close();
                                                try {
//                                Uri uri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeFile(fileUri), null, null));
                                                    Uri uri = Uri.parse(fileUri);
                                                    //progressDialog.dismiss();
                                                    sharingIntent.setType("image/*");
                                                    sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
                                                    context1.startActivity(Intent.createChooser(sharingIntent, "Share to"));
                                                } catch (IllegalStateException e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onBitmapFailed(Drawable errorDrawable) {

                                        }

                                        @Override
                                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                                        }
                                    });

                                }

                                @Override
                                public void onPermissionDenied(List<String> deniedPermissions) {

                                }


                            };
                            TedPermission.with(context1)
                                    .setPermissionListener(permissionlistener)
                                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                                    .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                                    .check();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


//                        Dexter.withActivity((Activity) context1)
//                                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                                .withListener(new PermissionListener() {
//                                    @Override
//                                    public void onPermissionGranted(PermissionGrantedResponse response) {
//                                        File mydir = new File(Environment.getExternalStorageDirectory() + "/.HaaHoo");
//                                        mydir.mkdirs();
//                                    }
//
//                                    @Override
//                                    public void onPermissionDenied(PermissionDeniedResponse response) {
//
//                                    }
//
//                                    @Override
//                                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
//
//                                    }
//                                })
//                                .check();

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pdt_id", product_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode==PERMISSION_WRITE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            //do somethings
//            getcategories();
//        }
//    }

    public void editprice(String id, String editedprice) {


        SharedPreferences pref = context1.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context1);
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL + "virtual_shop/edit_price/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject obj = new JSONObject(response);
                            String message = obj.optString("message");
                            if (message.equals("Success")) {
                                Toast.makeText(context1, "Price edited successfully", Toast.LENGTH_SHORT).show();
                                //context1.startActivity(new Intent(context1, MyShopNew.class));
                            }
                            if (message.equals("Failed")) {
                                Toast.makeText(context1, "Failed to edit price", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", id);
                params.put("price", editedprice);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void removeProduct(String product_id) {

        SharedPreferences pref = context1.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context1);
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL + "virtual_shop/remove_virtual_pdt/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("jhgghf", "jsonarray" + obj);
                            String message = obj.optString("message");
                            if (message.equals("Success")) {
                                Toast.makeText(context1, "Product removed successfully", Toast.LENGTH_SHORT).show();
                                //context1.startActivity(new Intent(context1, MyShopNew.class));
                            }
                            if (message.equals("Failed")) {
                                Toast.makeText(context1, "Failed to remove product", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", product_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}