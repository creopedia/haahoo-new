package creo.com.haahooapp.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.haozhang.lib.SlantedTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.Cart;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.ProductDetailsShop;
import creo.com.haahooapp.ProductsInShop;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.SendData;
import creo.com.haahooapp.interfaces.SubCategoryInterface;

import static android.content.Context.MODE_PRIVATE;

public class ProductsnewAdapter extends RecyclerView.Adapter<ProductsnewAdapter.MyViewHolder> {
    int quantity = 0;
    private LayoutInflater inflater;
    private ArrayList<Category> dataModelArrayList;
    public Context context1;
    SubCategoryInterface anInterface;
    SendData sendData;

    public ProductsnewAdapter(Context ctx, ArrayList<Category> dataModelArrayList, SubCategoryInterface subCategoryInterface, SendData sendData ) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        this.anInterface = subCategoryInterface;
        this.sendData = sendData;
    }

    @Override
    public ProductsnewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.products_new_adapter, parent, false);
        ProductsnewAdapter.MyViewHolder holder = new ProductsnewAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductsnewAdapter.MyViewHolder holder, final int position) {
        try {
            String name1 = dataModelArrayList.get(position).getName().substring(0, 1).toUpperCase();
            String title1 = name1 + dataModelArrayList.get(position).getName().substring(1);
            if (dataModelArrayList.get(position).getVariant_count() != null) {
                if (dataModelArrayList.get(position).getVariant_count().equals("0")) {
                    holder.variantcard.setVisibility(View.GONE);
                }
                if (!(dataModelArrayList.get(position).getVariant_count().equals("0"))) {
                    holder.variantcard.setVisibility(View.VISIBLE);
                    holder.varcount.setText(dataModelArrayList.get(position).getVariant_count() + " variants");
                }
                holder.variantcard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anInterface.variant(dataModelArrayList.get(position).getId());
                    }
                });
            }
            if (dataModelArrayList.get(position).getVariant_count() == null) {
                holder.variantcard.setVisibility(View.GONE);
            }
            int stock = 0;
            holder.name.setText(title1);
            holder.price.setText("Rs. " + dataModelArrayList.get(position).getPrice().trim());
            Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);
            holder.original_price.setText("Rs. " + dataModelArrayList.get(position).getOriginal_price());
            holder.rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(dataModelArrayList.get(position).getStock()) > 0){
                    Intent intent = new Intent(context1, ProductDetailsShop.class);
                    intent.putExtra("productid", dataModelArrayList.get(position).getId());
                    intent.putExtra("shopid", dataModelArrayList.get(position).getShopid());
                    context1.startActivity(intent);
                    }
                }
            });
            if (dataModelArrayList.get(position).getStock() != null) {
                stock = Integer.parseInt(dataModelArrayList.get(position).getStock());
                if (stock <= 0) {
                    holder.slant.setText("Out of stock");
                    holder.slant.setSlantedBackgroundColor(Color.parseColor("#fc0015"));
                    holder.ft.setVisibility(View.GONE);
                    holder.first.setVisibility(View.GONE);
                }
                if (stock > 0) {
                    holder.slant.setText("In stock");
                    holder.slant.setSlantedBackgroundColor(Color.parseColor("#38be55"));
                }
            }
            try {
                holder.rating.setRating(Float.parseFloat(dataModelArrayList.get(position).getRating()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if ((dataModelArrayList.get(position).getOriginal_price().trim().equals("null")) || dataModelArrayList.get(position).getPrice().equals("null")) {

            } else {
                try {
                    int offe = Integer.parseInt(dataModelArrayList.get(position).getOriginal_price().trim()) - Integer.parseInt(dataModelArrayList.get(position).getPrice().trim());
                    holder.off.setText("Rs. " + String.valueOf(offe) + " off");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            if (dataModelArrayList.get(position).getCart_status().equals("1")) {

                if (stock <= 0) {
                    holder.first.setVisibility(View.GONE);
                    holder.ft.setVisibility(View.GONE);
                    holder.view1.setVisibility(View.VISIBLE);
                    holder.viewrelative.setVisibility(View.GONE);
                    holder.view2.setVisibility(View.GONE);
                }
                if (stock > 0) {
                    holder.first.setVisibility(View.GONE);
                    holder.ft.setVisibility(View.GONE);
                    holder.view1.setVisibility(View.GONE);
                    holder.viewrelative.setVisibility(View.VISIBLE);
                    holder.view2.setVisibility(View.VISIBLE);
                }
                holder.viewrelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context1, Cart.class);
                        intent.putExtra("from", "productsinshop");
                        context1.startActivity(intent);

                    }
                });
            }

            if (dataModelArrayList.get(position).getCart_status().equals("0")) {
                if (stock > 0) {
                    holder.first.setVisibility(View.VISIBLE);
                    holder.ft.setVisibility(View.VISIBLE);
                    holder.view1.setVisibility(View.VISIBLE);
                    holder.viewrelative.setVisibility(View.GONE);
                    holder.view2.setVisibility(View.GONE);
                }
                if (stock <= 0) {
                    holder.first.setVisibility(View.GONE);
                    holder.ft.setVisibility(View.GONE);
                    holder.view1.setVisibility(View.VISIBLE);
                    holder.viewrelative.setVisibility(View.GONE);
                    holder.view2.setVisibility(View.GONE);
                }
            }
            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Integer.parseInt(holder.qty.getText().toString()) >= 1) {
                        String items = holder.qty.getText().toString();
                        int value = Integer.parseInt(items);
                        String added = String.valueOf(value + 1);
                        //quantity = Integer.parseInt(added);
                        holder.qty.setText(added);
//                    Toast.makeText(context1,quantity,Toast.LENGTH_SHORT).show();
                    }

                }
            });
            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(holder.qty.getText().toString()) > 1) {
                        String items = holder.qty.getText().toString();
                        int value = Integer.parseInt(items);
                        String calc = String.valueOf(value - 1);
                        //quantity = Integer.parseInt(calc);
                        holder.qty.setText(calc);
                        // Toast.makeText(context1,quantity,Toast.LENGTH_SHORT).show();
                    }
                }
            });
            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Toast.makeText(context1,holder.qty.getText().toString(),Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder = new AlertDialog.Builder(context1);
                    builder.setTitle("Add to cart");
                    builder.setMessage("Do you wish to add this product to cart ?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog

                            SharedPreferences pref = context1.getSharedPreferences("MyPref", MODE_PRIVATE);
                            String token = (pref.getString("token", ""));
                            String shop_id = (pref.getString("shop_id", ""));
                            RequestQueue queue = Volley.newRequestQueue(context1);

                            //this is the url where you want to send the request

                            String url = ApiClient.BASE_URL + "cart_view/cart_add/";

                            // Request a string response from the provided URL.

                            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            // Display the response string.
                                            try {

                                                JSONObject obj = new JSONObject(response);
                                                Log.d("addtocart", "cartadd" + obj);
                                                String message = obj.optString("message");
                                                if (message.equals("Success")) {
                                                    //  Toast.makeText(context1,"Product added to your cart",Toast.LENGTH_SHORT).show();
                                                    holder.first.setVisibility(View.GONE);
                                                    holder.ft.setVisibility(View.GONE);
                                                    holder.view1.setVisibility(View.GONE);
                                                    holder.viewrelative.setVisibility(View.VISIBLE);
                                                    holder.view2.setVisibility(View.VISIBLE);
                                                    holder.viewrelative.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            Intent intent = new Intent(context1, Cart.class);
                                                            intent.putExtra("from", "productsinshop");
                                                            context1.startActivity(intent);
                                                        }
                                                    });
                                                    sendData.sendData(ApiClient.count);
                                                    // buynow.setText("GO TO CART");
                                                }
                                                if (message.equals("Failed")) {
                                                    Toast.makeText(context1, "Failed adding product to cart", Toast.LENGTH_SHORT).show();
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //  Toast.makeText(context1, "//Error", Toast.LENGTH_LONG).show();
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("productid", dataModelArrayList.get(position).getId());
                                    if (holder.qty.getText().toString().equals("0")) {
                                        params.put("pro_count", "1");
                                        ApiClient.count++;
                                    }
                                    if (!(holder.qty.getText().toString().equals("0"))) {
                                        params.put("pro_count", holder.qty.getText().toString());
                                        ApiClient.count = ApiClient.count+Integer.parseInt(holder.qty.getText().toString());
                                    }
                                    params.put("virtual", "1");
                                    params.put("variant", "0");
                                    params.put("shop_id", shop_id);
                                    return params;
                                }

                                @Override
                                public Map<String, String> getHeaders() {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("Authorization", "Token " + token);
                                    return params;
                                }
                            };

                            // Add the request to the RequestQueue.
                            queue.add(stringRequest);
                            // addproductTocart(dataModelArrayList.get(position).getId(),String.valueOf(holder.qty.getText().toString()));
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addproductTocart(String productid, String count) {


    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView price, name, qty, plus, minus, add, original_price, off, varcount;
        LinearLayout linearLayout;
        ImageView iv;
        RelativeLayout first, ft, viewrelative, rel;
        View view1, view2;
        RatingBar rating;
        SlantedTextView slant;
        CardView variantcard;

        public MyViewHolder(View itemView) {
            super(itemView);
            add = itemView.findViewById(R.id.add);
            rating = itemView.findViewById(R.id.rating);
            plus = itemView.findViewById(R.id.plus);
            variantcard = itemView.findViewById(R.id.variantcard);
            varcount = itemView.findViewById(R.id.varcount);
            minus = itemView.findViewById(R.id.minus);
            name = (TextView) itemView.findViewById(R.id.title);
            original_price = itemView.findViewById(R.id.original_price);
            price = (TextView) itemView.findViewById(R.id.price);
            iv = itemView.findViewById(R.id.img);
            rel = itemView.findViewById(R.id.rel);
            off = itemView.findViewById(R.id.off);
            qty = itemView.findViewById(R.id.qty);
            first = itemView.findViewById(R.id.first);
            ft = itemView.findViewById(R.id.ft);
            viewrelative = itemView.findViewById(R.id.viewrelative);
            view1 = itemView.findViewById(R.id.divider19);
            view2 = itemView.findViewById(R.id.divider22);
            slant = itemView.findViewById(R.id.slant);
        }
    }

    public void filteredlist(ArrayList<Category> hospitalsPojos) {

        dataModelArrayList = hospitalsPojos;
        notifyDataSetChanged();

    }

}