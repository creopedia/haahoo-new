package creo.com.haahooapp.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.List;
import creo.com.haahooapp.Modal.ProductPojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    public List<ProductPojo> productPojo;
    Context context1 ;

    public EventAdapter(List<ProductPojo> productPojo,Context context) {
        this.productPojo = productPojo;
        this.context1 = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.events_list_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.textView.setText(productPojo.get(position).getName());
        holder.textView2.setText(productPojo.get(position).getDescription());
//        holder.imageView.setImageResource((Glide.with(context).load(productPojo[position].getImage())));
        Glide.with(context1).load(ApiClient.BASE_URL+"media/"+productPojo.get(position).getImage()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return productPojo.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView,textView2;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView =  itemView.findViewById(R.id.img);
            this.textView =  itemView.findViewById(R.id.title);
            this.textView2 = itemView.findViewById(R.id.place);
        }

    }
}
