package creo.com.haahooapp.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import creo.com.haahooapp.CallFragment;
import creo.com.haahooapp.CoinFragment;
import creo.com.haahooapp.WhatsappFragment;

public class DownloadPager extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public DownloadPager(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                CoinFragment footballFragment = new CoinFragment();
                return footballFragment;
            case 1:
                WhatsappFragment cricketFragment = new WhatsappFragment();
                return cricketFragment;
            case 2:
                CallFragment nbaFragment = new CallFragment();
                return nbaFragment;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}