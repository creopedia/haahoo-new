package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import creo.com.haahooapp.Modal.HorizontalPojo;
import creo.com.haahooapp.Modal.ProductFeaturepojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.interfaces.SubCategoryInterface;
import de.hdodenhof.circleimageview.CircleImageView;

public class Horizontal_item_adapter extends RecyclerView.Adapter<Horizontal_item_adapter.ViewHolder> {

    public List<HorizontalPojo> productFeaturepojos;
    Context context1 ;
    SubCategoryInterface subCategoryInterface;

    public Horizontal_item_adapter(List<HorizontalPojo> productFeaturepojos, Context context,SubCategoryInterface subCategoryInterface) {
        this.productFeaturepojos = productFeaturepojos;
        this.context1 = context;
        this.subCategoryInterface = subCategoryInterface;
    }

    @Override
    public Horizontal_item_adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.horizontal_item, parent, false);
        final Horizontal_item_adapter.ViewHolder viewHolder = new Horizontal_item_adapter.ViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Horizontal_item_adapter.ViewHolder holder, int position) {

        holder.name.setText(productFeaturepojos.get(position).getName());
        //Glide.with(context1).load(productFeaturepojos.get(position).getImage()).into(holder.image);
        holder.base.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                subCategoryInterface.setsubcategoryId(productFeaturepojos.get(position).getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return productFeaturepojos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        RelativeLayout base;
        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            base = itemView.findViewById(R.id.base);
        }

    }


}
