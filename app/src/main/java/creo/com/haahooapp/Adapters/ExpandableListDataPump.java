package creo.com.haahooapp.Adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump {

  public static HashMap<String, List<String>> getData() {
    HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

    List<String> cricket = new ArrayList<String>();
    cricket.add("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");

    List<String> football = new ArrayList<String>();
    football.add("It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).");

    List<String> data1 = new ArrayList<>();
    data1.add("You could see different prices for the same product, as it could be listed by many Sellers.");

    List<String> data2 = new ArrayList<>();
    data2.add("An item is marked as 'Out of stock' when it is not available with any sellers at the moment; you won't be able to buy it now.");

    List<String> data3 = new ArrayList<>();
    data3.add("Products sold by Sellers with Fabinsane Assured badge are shipped in packages with waterproof plastic wrap.\n" +
            "\n" +
            "Fragile items are safely secured with bubble wrap. Other Sellers also follow standard packing procedure. Sellers are rated on packaging quality and affects overall seller rating.");


    expandableListDetail.put("What are the services offered ?", football);
    expandableListDetail.put("Why do I see different prices for the same product?",data1);
    expandableListDetail.put("What does 'Out of Stock' mean?",data2);
    expandableListDetail.put("How are items packaged?",data3);
    expandableListDetail.put("What is haahoo ?", cricket);

    return expandableListDetail;
  }
}
