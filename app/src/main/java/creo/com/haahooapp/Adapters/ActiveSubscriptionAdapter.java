package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import creo.com.haahooapp.Modal.Subscribedpojo;
import creo.com.haahooapp.R;
import creo.com.haahooapp.SubscriptionDetails;
import creo.com.haahooapp.config.ApiClient;
import de.hdodenhof.circleimageview.CircleImageView;

public class ActiveSubscriptionAdapter extends RecyclerView.Adapter<ActiveSubscriptionAdapter.ViewHolder>{

    public ArrayList<Subscribedpojo> productPojo;
    public Context context1 ;

    public ActiveSubscriptionAdapter(ArrayList<Subscribedpojo> productPojo,Context context) {
        this.productPojo = productPojo;
        this.context1 = context;
    }

    @Override
    public ActiveSubscriptionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final View listItem= layoutInflater.inflate(R.layout.active_subscription_adapter, parent, false);
        final ActiveSubscriptionAdapter.ViewHolder viewHolder = new ActiveSubscriptionAdapter.ViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ActiveSubscriptionAdapter.ViewHolder holder, int position) {

        holder.pdt_name.setText(productPojo.get(position).getPdt_name());
        holder.type.setText(productPojo.get(position).getMode()+" based subscription");
        Glide.with(context1).load(ApiClient.BASE_URL+productPojo.get(position).getPdt_image()).into(holder.img);
        holder.amount.setText("₹ "+productPojo.get(position).getAmount());
        holder.until.setText(productPojo.get(position).getUntil());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context1,SubscriptionDetails.class);
                intent.putExtra("pdt_id",productPojo.get(position).getPdt_id());
                intent.putExtra("until",productPojo.get(position).getUntil());
                intent.putExtra("name",productPojo.get(position).getPdt_name());
                intent.putExtra("image",productPojo.get(position).getPdt_image());
                intent.putExtra("order_id",productPojo.get(position).getOrder_id());
                intent.putExtra("days",productPojo.get(position).getDays());
                intent.putExtra("benefits",productPojo.get(position).getBenefits());
                context1.startActivity(intent);
                //context1.startActivity(new Intent(context1, SubscriptionDetails.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return productPojo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView pdt_name,type,until,amount;
        CircleImageView img;
        CardView cardView;

        public ViewHolder(final View itemView) {

            super(itemView);

            pdt_name = itemView.findViewById(R.id.pdt_name);
            type = itemView.findViewById(R.id.type);
            until = itemView.findViewById(R.id.until);
            img = itemView.findViewById(R.id.img);
            amount = itemView.findViewById(R.id.amount);
            cardView = itemView.findViewById(R.id.card);

        }

    }

}