package creo.com.haahooapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.List;
import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class NewHomeOffs extends RecyclerView.Adapter<NewHomeOffs.MyViewHolder> {

    private LayoutInflater inflater;
    private List<String> dataModelArrayList;
    public Context context1;


    public NewHomeOffs(Context ctx, List<String> dataModelArrayList) {

        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;

    }

    @Override
    public NewHomeOffs.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.offs_adapter, parent, false);
        NewHomeOffs.MyViewHolder holder = new NewHomeOffs.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(NewHomeOffs.MyViewHolder holder, final int position) {
        try {

            Glide.with(context1).load(ApiClient.BASE_URL+"media/android/studyfor.png").into(holder.iv);
            holder.iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context1, CouponDetailsActivity.class);
                    intent.putExtra("from", "referal");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context1.startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView price, name, qty, delivery;
        LinearLayout linearLayout;
        ImageView iv, copy;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.image);

        }

    }
}