package creo.com.haahooapp.Adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import creo.com.haahooapp.Modal.Coupon;
import creo.com.haahooapp.Modal.TrendingPojo;
import creo.com.haahooapp.ProductDetailsShop;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;

public class NewHomeCat extends RecyclerView.Adapter<NewHomeCat.MyViewHolder> {

    private LayoutInflater inflater;
    private List<TrendingPojo> dataModelArrayList;
    public Context context1;
    ClipData myClip;
    ClipboardManager clipboard;

    public NewHomeCat(Context ctx, List<TrendingPojo> dataModelArrayList) {
        inflater = LayoutInflater.from(ctx);
        this.context1 = ctx;
        this.dataModelArrayList = dataModelArrayList;
        clipboard = (ClipboardManager) context1.getSystemService(Context.CLIPBOARD_SERVICE);
    }

    @Override
    public NewHomeCat.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.new_home_cat, parent, false);
        NewHomeCat.MyViewHolder holder = new NewHomeCat.MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(NewHomeCat.MyViewHolder holder, final int position) {
        try {
            Glide.with(context1).load(dataModelArrayList.get(position).getImage()).into(holder.iv);
            holder.name.setText(dataModelArrayList.get(position).getName());
            holder.card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context1, ProductDetailsShop.class);
                    intent.putExtra("productid", dataModelArrayList.get(position).getId());
                    intent.putExtra("shopid", dataModelArrayList.get(position).getShop_id());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context1.startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView price, name, qty, delivery;
        LinearLayout linearLayout;
        ImageView iv, copy;
        RelativeLayout rel;
        CardView card;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.img);
            name = itemView.findViewById(R.id.name);
            rel = itemView.findViewById(R.id.rel);
            card = itemView.findViewById(R.id.card);

        }
    }
}