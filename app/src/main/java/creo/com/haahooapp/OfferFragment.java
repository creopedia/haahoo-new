package creo.com.haahooapp;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;

import creo.com.haahooapp.Adapters.OfferAdapter;
import creo.com.haahooapp.Adapters.RewardAdapter;

public class OfferFragment extends Fragment {

GridView recyclerView;
ArrayList<String> dataModelArrayList = new ArrayList<>();
OfferAdapter offerAdapter;

    public OfferFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_offer,container,false);

        recyclerView = view.findViewById(R.id.offer);

        offerAdapter = new OfferAdapter(getContext(),dataModelArrayList);
        recyclerView.setAdapter(offerAdapter);
//        recyclerView.setNestedScrollingEnabled(false);
//        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));

        return view;
    }

}