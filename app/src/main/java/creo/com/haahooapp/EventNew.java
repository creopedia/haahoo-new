package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import creo.com.haahooapp.Adapters.EventNewAdapter;
import creo.com.haahooapp.config.ApiClient;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventNew extends AppCompatActivity {

    Activity activity = this;
    String number,email,event_id,image,name,proffession,proffession_type;
    TabLayout tabLayout;
    ViewPager viewPager;
    ImageView imageView;
    List<String> dates = new ArrayList<>();
    List<String> available_dates = new ArrayList<>();
    List<String> seats = new ArrayList<>();
    TextView eventname,price;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_new);
        Window window = activity.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        Bundle bundle = getIntent().getExtras();
        imageView = findViewById(R.id.image);
        number = bundle.getString("number");
        eventname = findViewById(R.id.name);
        price = findViewById(R.id.price);
        eventname.setText(ApiClient.event_name);
        price.setText("₹ "+ApiClient.eventprice);
        email = bundle.getString("email");
        event_id = bundle.getString("event_id");
        getAvailableSeats();
        image = bundle.getString("image");
        Glide.with(context).load(image).into(imageView);
        name = bundle.getString("name");
        proffession = bundle.getString("proffession");
        proffession_type = bundle.getString("proffession_type");
        ApiClient.event_id = event_id;
        ApiClient.event_email = email;
        ApiClient.proffession_name = proffession;
        ApiClient.proffession_type = proffession_type;
        ApiClient.event_name = name;
        ApiClient.event_number = number;
        ApiClient.event_user_name= name;
        tabLayout=(TabLayout)findViewById(R.id.tabLayout);
        viewPager=(ViewPager)findViewById(R.id.viewPager);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(EventNew.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(EventNew.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(EventNew.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context, MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                        }
                        return true;
                    }
                });

    }

    public void getAvailableSeats(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));

        RequestQueue queue = Volley.newRequestQueue(EventNew.this);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"events/seat_availability/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String date = jsonObject.optString("date");
                                String remaining = jsonObject.optString("remaining");
                                seats.add(remaining);
                                dates.add(date);
                            }
                            Calendar c = Calendar.getInstance();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date next = null;
                            for (int i = 0;i<dates.size();i++){
                                next = sdf.parse(dates.get(i));
                                if (next.after(new Date())){
                                    available_dates.add(dates.get(i));
                                    ApiClient.availabledates.add(seats.get(i));
                                    ApiClient.dates.add(dates.get(i));
                                }
                            }
                            tabLayout.addTab(tabLayout.newTab().setText(available_dates.get(0)));
                            tabLayout.addTab(tabLayout.newTab().setText(available_dates.get(1)));
                            tabLayout.addTab(tabLayout.newTab().setText(available_dates.get(2)));
                            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                            final EventNewAdapter adapter = new EventNewAdapter(context,getSupportFragmentManager(), tabLayout.getTabCount());
                            viewPager.setAdapter(adapter);

                            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                @Override
                                public void onTabSelected(TabLayout.Tab tab) {
                                    viewPager.setCurrentItem(tab.getPosition());
                                }

                                @Override
                                public void onTabUnselected(TabLayout.Tab tab) {

                                }

                                @Override
                                public void onTabReselected(TabLayout.Tab tab) {

                                }
                            });


                        }catch (Exception e){

                            }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  Toast.makeText(EventNew.this,"//Error",Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();
                params.put("event_id", event_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        queue.add(stringRequest);

    }
}