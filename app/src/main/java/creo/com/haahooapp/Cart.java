package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.CombinedCartActivity;
import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.CartAdapter;
import creo.com.haahooapp.Implementation.CartPresenterImpl;
import creo.com.haahooapp.Modal.CartPojo;
import creo.com.haahooapp.Modal.CombinedCart;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.CartCallBack;
import creo.com.haahooapp.interfaces.CartInterface;
import creo.com.haahooapp.interfaces.CartPresenter;
import creo.com.haahooapp.interfaces.CartView;
import creo.com.haahooapp.interfaces.ItemClick;

public class Cart extends AppCompatActivity implements CartCallBack, CartView, CartInterface {

    RecyclerView recyclerView;
    public List<CartPojo> pjo = new ArrayList<>();
    ProgressDialog progressDialog;
    CartPresenter cartPresenter;
    ImageView back;
    LinearLayout linearLayout;
    RelativeLayout relativeLayout;
    TextView textView;
    JSONArray arr = new JSONArray();
    JSONObject products = new JSONObject();
    Context context = this;
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> prices = new ArrayList<>();
    TextView price1, total_price1;
    ArrayList<String> image = new ArrayList<>();
    ArrayList<String> variant = new ArrayList<>();
    String token = null;
    String shop_id;
    Activity activity = this;
    ArrayList<String> ids = new ArrayList<>();
    ArrayList<String> quantity = new ArrayList<>();
    ArrayList<String> delivery = new ArrayList<>();
    ArrayList<String> total = new ArrayList<>();
    ArrayList<String> price = new ArrayList<>();
    TextView continuea;
    String intentvalue = "null";
    CartAdapter cartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        showProgressDialogWithTitle("Loading.. Please Wait..");
        price1 = findViewById(R.id.price);
        total_price1 = findViewById(R.id.total_price);
        Intent intent = getIntent();
        cartAdapter = new CartAdapter(pjo, context, this);
        if (intent.hasExtra("from")) {
            Bundle bundle = getIntent().getExtras();
            intentvalue = bundle.getString("from");

        }
        continuea = findViewById(R.id.continue1);
        linearLayout = findViewById(R.id.linear);
        relativeLayout = findViewById(R.id.relative);
        textView = findViewById(R.id.start);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Cart.this, NewHome.class));
            }
        });
        continuea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Cart.this, ChooseAddress.class));
            }
        });

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        shop_id = (pref.getString("shop_id", ""));
        getData();
        back = findViewById(R.id.back);

        cartPresenter = new CartPresenterImpl(this);

        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Cart.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Cart.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Cart.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(Cart.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
        recyclerView = findViewById(R.id.recyclerView);
        cartPresenter = new CartPresenterImpl(this);
//        cartPresenter.viewCart(token);

    }


    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.cart_actions, popup.getMenu());
        popup.show();
    }

    public void getData() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "cart_view/cart_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            Log.d("response", "resp" + response);
                            linearLayout.setVisibility(View.VISIBLE);
                            relativeLayout.setVisibility(View.GONE);
                            ApiClient.quantity.clear();
                            ApiClient.name.clear();
                            pjo.clear();
                            ApiClient.images.clear();
                            ApiClient.prices.clear();
                            ApiClient.cart_id.clear();
                            ApiClient.variant_ids.clear();
                            ApiClient.virtual.clear();
                            ApiClient.ids.clear();
                            total.clear();
                            JSONObject jsonObject1 = new JSONObject(response);
                            String total_count = jsonObject1.optString("Total count");
//                            if (total_count.equals("")) {
//                                ApiClient.count = 0;
//                            }
//                            if (!(total_count.equals(""))) {
//                                ApiClient.count = Integer.valueOf(total_count);
//                            }
                            String message = jsonObject1.getString("message");
                            if (message.equals("Failed")) {
                                hideProgressDialogWithTitle();
                                relativeLayout.setVisibility(View.VISIBLE);
                                linearLayout.setVisibility(View.GONE);
                            }
                            if (!(message.equals("Failed"))) {
                                String jsonArray = jsonObject1.getString("data");
                                JSONArray jsonObject11 = new JSONArray(jsonArray);
                                String total_price = null;
                                for (int i = 0; i < jsonObject11.length(); i++) {
                                    CartPojo cartPojo = new CartPojo();
                                    cartPojo.setProductid(jsonObject11.getJSONObject(i).getString("id"));
                                    cartPojo.setName(jsonObject11.getJSONObject(i).getString("name"));
                                    cartPojo.setFrom(intentvalue);
                                    cartPojo.setShop_id(shop_id);
                                    cartPojo.setStock(jsonObject11.optJSONObject(i).optString("stock"));
                                    String images1 = jsonObject11.getJSONObject(i).getString("image");
                                    String[] seperated = images1.split(",");
                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    if (split.contains("media")) {
                                        image.add(ApiClient.BASE_URL + split);
                                    }
                                    if (!(split.contains("media"))){
                                        image.add(ApiClient.BASE_URL+"media/"+split);
                                    }
//                                    cartPojo.setPrice(String.valueOf(vv));
                                    cartPojo.setPrice(jsonObject11.getJSONObject(i).getString("price"));
                                    cartPojo.setImage(image.get(i));
                                    cartPojo.setQuantity(jsonObject11.getJSONObject(i).getString("count"));
                                    cartPojo.setVariant(jsonObject11.optJSONObject(i).optString("variant"));
                                    pjo.add(cartPojo);

                                    ApiClient.name.add(jsonObject11.getJSONObject(i).getString("name"));
                                    ApiClient.virtual.add(jsonObject11.optJSONObject(i).optString("virtual"));
                                    ApiClient.cart_id.add(jsonObject11.optJSONObject(i).optString("cart_id"));
                                    ApiClient.variant_ids.add(jsonObject11.optJSONObject(i).optString("variant"));
//                                    // image.add("http://haahoo.creopedia.com/media/"+jsonObject11.getJSONObject(i).getString("image"));
                                    ApiClient.ids.add(jsonObject11.getJSONObject(i).getString("id"));
////                                    String images1 = jsonObject11.getJSONObject(i).getString("image");
////                                    String[] seperated = images1.split(",");
////                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    ApiClient.images.add(ApiClient.BASE_URL + split);
//                                    variant.add(jsonObject11.optJSONObject(i).optString("variant"));
//                                    //Log.d("imagggg","url"+ApiClient.BASE_URL+"media/"+split);
                                    ApiClient.quantity.add(jsonObject11.getJSONObject(i).getString("count"));
//                                    quantity.add(jsonObject11.getJSONObject(i).getString("count"));
//                                    price.add("₹ " + jsonObject11.getJSONObject(i).getString("price"));
                                    // ApiClient.prices.add(jsonObject11.getJSONObject(i).getString("price"));
                                    ApiClient.prices.add("₹ " + jsonObject11.getJSONObject(i).getString("price"));

                                    int unit_price = Integer.parseInt(jsonObject11.getJSONObject(i).getString("price").trim());
                                    int unit_quantity = Integer.parseInt(jsonObject11.getJSONObject(i).getString("count"));
                                    if (total_price == null) {
                                        int final_price = unit_price * unit_quantity;
                                        total.add(String.valueOf(final_price));
                                    }
                                }
                                int value = 0;
                                for (int i = 0; i < total.size(); i++) {
                                    value += Integer.parseInt(total.get(i));
                                }
                                price1.setText("₹ " + String.valueOf(value));
                                total_price1.setText("₹ " + String.valueOf(value));
                                ApiClient.price = String.valueOf(value);
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                                recyclerView.setAdapter(cartAdapter);
                                recyclerView.setNestedScrollingEnabled(false);
                                cartAdapter.notifyDataSetChanged();
                                hideProgressDialogWithTitle();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shop_id", shop_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public void remove(final String id1, final String cart_id) {
        showProgressDialogWithTitle("Removing..");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "cart_view/cart_remove/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            pjo.remove(ApiClient.ids.indexOf(id1));
                            recyclerView.removeViewAt(ApiClient.ids.indexOf(id1));
                            cartAdapter.notifyDataSetChanged();
                            showProgressDialogWithTitle("Refreshing..");
                            getData();
                            hideProgressDialogWithTitle();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Cart.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("productid", id1);
                params.put("cart_id", cart_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }


    @Override
    public void successcart(String data) {

    }

    @Override
    public void failedcart(String msg) {

    }

    @Override
    public void viewsuccess(JSONArray data) {

    }

    @Override
    public void viewfail(String msg) {

    }

    @Override
    public void onSuccesscart(String data) {
        startActivity(new Intent(Cart.this, Cart.class));
    }

    @Override
    public void onFailedcart(String data) {

    }

    @Override
    public void onsuccess(JSONArray data) {


    }

    @Override
    public void onfail(String data) {

    }
//    CartAdapter cartAdapter = new CartAdapter()


    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    public void updateCount(String productid, String count, String isVirtual, String shop_id, String variant) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "cart_view/cart_add/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("addtocart", "cartadd" + obj);
                            String message = obj.optString("message");
                            if (message.equals("Success")) {
                                //  Toast.makeText(context1,"Product added to your cart",Toast.LENGTH_SHORT).show();
//                                            context1.startActivity(new Intent(context1,Cart.class));
//                                Intent intent = new Intent(context1, Cart.class);
//                                intent.putExtra("from", productPojo.get(position).getFrom());
//                                context1.startActivity(intent);
                                // buynow.setText("GO TO CART");
                                getData();
                            }
                            if (message.equals("Failed")) {
                                getData();
                                //Toast.makeText(context1, "Failed adding product to cart", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("productid", productid);
                params.put("pro_count", count);
                params.put("virtual", isVirtual);
                params.put("shop_id", shop_id);
                params.put("variant", variant);
                //Log.d("ghcfgxgfx", "tdrdtrd" + ApiClient.virtual.get(position) + productPojo.get(position).getVariant() + ApiClient.ids.get(position) + holder.qty.getText().toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
      /*  if (intentvalue.equals("null")) {
            startActivity(new Intent(Cart.this, NewHome.class));
        }
        if (intentvalue.equals("productsinshop")){
            super.onBackPressed();
        }
        if (intentvalue.equals("details")){
            super.onBackPressed();
        }*/
        if (intentvalue.equals("cart")) {
            Intent gotoScreenVar = new Intent(context, CombinedCartActivity.class);

            gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

            startActivity(gotoScreenVar);
        } else {
            super.onBackPressed();
        }
        finish();
    }


    @Override
    public void sendData(String productid, String count, String isVirtual, String shop_id, String variant) {
        Log.d("DATSGCGXC", "jkgv" + productid + "\n" + count + "\n" + isVirtual + "\n" + shop_id + "\n" + variant);
        updateCount(productid, count, isVirtual, shop_id, variant);
    }
}
