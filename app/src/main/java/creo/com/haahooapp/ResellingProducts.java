package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.List;
import creo.com.haahooapp.Adapters.ResellingCategoryAdapter;
import creo.com.haahooapp.Adapters.ResellingProductsAdapter;
import creo.com.haahooapp.Adapters.ResellingTabAdapter;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.Modal.HorizontalPojo;
import creo.com.haahooapp.Modal.ResellingPojo;
import creo.com.haahooapp.config.ItemClickSupport;
import creo.com.haahooapp.interfaces.MyViewModel;

public class ResellingProducts extends AppCompatActivity {

    ResellingProductsAdapter resellingAdapter;
    RecyclerView recyclerView,horizontal;
    Activity activity = this;
    ImageView back,cart;
    String categoryidd = "null";
    Context context = this;
    ArrayList<ResellingPojo> pojos = new ArrayList<>();
    List<HorizontalPojo> productFeaturepojos = new ArrayList<>();
    ResellingCategoryAdapter resellingCategoryAdapter;
    RecyclerView gridview1;
    ArrayList<Category> categories1 = new ArrayList<>();
    TabLayout tablayout;
    ViewPager viewpager;
    OnDataPassedListener onDataPassedListener = null;
    MyViewModel myViewModel;
    ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reselling_products);
        recyclerView = findViewById(R.id.recyclerview);
        back = findViewById(R.id.back);
        horizontal = findViewById(R.id.horizontal);
        tablayout = findViewById(R.id.tablayout);
        viewpager = findViewById(R.id.viewpager);
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.setTitle("Loading..");
        progressDialog.setMessage("Please wait while the data is loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(true);
        //progressDialog.show();
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        tablayout.addTab(tablayout.newTab().setText("BAZAAR"));
        //tablayout.addTab(tablayout.newTab().setText("RESALE MART"));
        myViewModel= ViewModelProviders.of(this).get(MyViewModel.class);
        myViewModel.init();
        //setOnDataListener(onDataPassedListener);
        tablayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ResellingTabAdapter tabAdapter = new ResellingTabAdapter(this,getSupportFragmentManager(),tablayout.getTabCount());
        viewpager.setAdapter(tabAdapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));
        tablayout.setSelectedTabIndicatorColor(Color.parseColor("#0078BC"));
        gridview1 = findViewById(R.id.gridview1);
        gridview1.setNestedScrollingEnabled(false);
        gridview1.setLayoutManager(new GridLayoutManager(context,2));
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // Toast.makeText(Earnings.this,"pos"+tab.getPosition(),Toast.LENGTH_SHORT).show();
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        ItemClickSupport.addTo(horizontal).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                categoryidd = productFeaturepojos.get(position).getId();
                myViewModel.sendData(categoryidd);
            }
        });
        cart= findViewById(R.id.cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResellingProducts.this, Cart.class);
                startActivity(intent);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResellingProducts.super.onBackPressed();
            }
        });
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;
                            case R.id.shop:

                                startActivity(new Intent(context,MyShopNew.class));
                                break;
                            case R.id.stories:

                                startActivity(new Intent(context,AllStories.class));
                                break;

                        }
                        return true;
                    }
                });

      //  getResellingProducts();

    }

   /* public void getResellingProducts(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
       String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"api_shop_app/reselling_pdts/";

        // Request a string response from the provided URL.


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("response", "resp" + response);
                            JSONArray jsonArray = obj.optJSONArray("results");

                            for (int i =0 ; i <jsonArray.length();i++){

                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                ResellingPojo resellingPojo = new ResellingPojo();
                                resellingPojo.setId(jsonObject.optString("id"));
                                resellingPojo.setShop_id(jsonObject.optString("shop_id"));
                                String images = jsonObject.optString("image") ;
                                String[] seperated = images.split(",");
                                resellingPojo.setImage(seperated[0].replace("[","").replace("]",""));
                                resellingPojo.setProduct_name(jsonObject.optString("name"));
                                resellingPojo.setActual_price(jsonObject.optString("price").trim());
                                resellingPojo.setResell_price(jsonObject.optString("selling"));
                                pojos.add(resellingPojo);

                            }

                            resellingAdapter = new ResellingProductsAdapter(context,pojos);
                            gridview1.setAdapter(resellingAdapter);
                            gridview1.setNestedScrollingEnabled(false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // progressDialog.dismiss();
            //
            }
        }) {

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

/*    public void loadProducts(String categoryid){


        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"api_shop_app/main_cat_resel_pdt/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            pojos.clear();
                            Log.d("response", "resp" + response);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            if (jsonArray.length() == 0){
                                Toast.makeText(context,"No products under this category",Toast.LENGTH_SHORT).show();
                            }
                            if (jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    ResellingPojo resellingPojo = new ResellingPojo();
                                    resellingPojo.setId(jsonObject.optString("id"));
                                    resellingPojo.setShop_id(jsonObject.optString("shop_id"));
                                    String images = jsonObject.optString("image");
                                    String[] seperated = images.split(",");
                                    resellingPojo.setImage(seperated[0].replace("[", "").replace("]", ""));
                                    resellingPojo.setProduct_name(jsonObject.optString("name"));
                                    resellingPojo.setActual_price(jsonObject.optString("price").trim());
                                    resellingPojo.setResell_price(jsonObject.optString("selling"));
                                    pojos.add(resellingPojo);
                                }
                            }
                                resellingAdapter = new ResellingProductsAdapter(context, pojos);
                                gridview1.setAdapter(resellingAdapter);
                                gridview1.setNestedScrollingEnabled(false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            //



            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cat_id", categoryid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };


        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }*/




    public interface OnDataPassedListener {
      public  void onDataPassed(String text);
    }
    private void setOnDataListener(OnDataPassedListener interface1){
        onDataPassedListener=interface1;
    }
}