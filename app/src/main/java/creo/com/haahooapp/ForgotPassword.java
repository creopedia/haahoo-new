package creo.com.haahooapp;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.textfield.TextInputEditText;
import creo.com.haahooapp.Implementation.ForgotOtpPresenterImpl;
import creo.com.haahooapp.interfaces.ForgotOtpCallback;
import creo.com.haahooapp.interfaces.ForgotOtpPresenter;
import creo.com.haahooapp.interfaces.ForgotOtpView;

public class ForgotPassword extends AppCompatActivity implements ForgotOtpView,ForgotOtpCallback {

  TextInputEditText new_password;
  TextInputEditText confirm_password;
  TextView reset;
  ForgotOtpPresenter forgotOtpPresenter;
  String phone_no = null;
  Activity activity = this;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
    getSupportActionBar().hide();
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_forgot_password);
    Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
    window.setStatusBarColor(activity.getResources().getColor(R.color.black));
    new_password = findViewById(R.id.new_password);
    confirm_password = findViewById(R.id.confirm_password);
    reset = findViewById(R.id.reset);
    phone_no = getIntent().getExtras().getString("phone_no").toString();
    forgotOtpPresenter = new ForgotOtpPresenterImpl(this);

    reset.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (new_password.getText().toString().length() < 4 ) {
          new_password.setError("Password should be minimum 4 characters ");
        }
        if (new_password.getText().toString().length() >= 4 ) {
          if(new_password.getText().toString().equals(confirm_password.getText().toString())){
            forgotOtpPresenter.resetPassword(phone_no,new_password.getText().toString());
//                        Toast.makeText(ForgotPassword.this," click",Toast.LENGTH_LONG).show();
          }
          if(!(new_password.getText().toString().equals(confirm_password.getText().toString()))){
            confirm_password.setError("Both passwords should be same");
          }
        }
      }
    });

  }


  @Override
  public void onVerifySuccessOtp(String data) {

  }

  @Override
  public void onVerifyFailedOtp(String msg) {

  }



  @Override
  public void onSuccessOtp(String response) {
    Toast.makeText(ForgotPassword.this,"Changed successfully",Toast.LENGTH_LONG).show();
    startActivity(new Intent(ForgotPassword.this,MainUI.class));
  }

  @Override
  public void onFailedOtp(String response) {
    Toast.makeText(ForgotPassword.this,"Failed to change",Toast.LENGTH_LONG).show();
  }

  @Override
  public void noInternetConnection() {

  }
}
