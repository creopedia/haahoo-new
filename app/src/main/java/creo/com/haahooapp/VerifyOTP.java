package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import creo.com.haahooapp.Implementation.NumberVerifyPresenterImpl;
import creo.com.haahooapp.Implementation.VerifyOTPPresenterImpl;
import creo.com.haahooapp.config.SMSReceiver;
import creo.com.haahooapp.interfaces.NumberVerifyPresenter;
import creo.com.haahooapp.interfaces.VerifyNumberCallBack;
import creo.com.haahooapp.interfaces.VerifyNumberView;
import creo.com.haahooapp.interfaces.VerifyOTPCallback;
import creo.com.haahooapp.interfaces.VerifyOTPPresenter;
import creo.com.haahooapp.interfaces.VerifyOTPView;

public class VerifyOTP extends AppCompatActivity implements
        SMSReceiver.OTPReceiveListener, VerifyOTPView, VerifyOTPCallback, VerifyNumberView, VerifyNumberCallBack {
    private SMSReceiver smsReceiver;
    EditText editText;
    TextView textView, phone, resentotp;
    ImageView edit;
    NumberVerifyPresenter numberVerify;
    Activity activity = this;
    String phone_no = null;
    VerifyOTPPresenter verifyOTPPresenter;
    Context context = this;
    CardView resend;
    TextView text;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        resentotp = findViewById(R.id.resentotp);
        numberVerify = new NumberVerifyPresenterImpl(this);
// finally change the color
        text = findViewById(R.id.text);
        progressDialog = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        Bundle bundle = getIntent().getExtras();
        phone_no = bundle.getString("phone_no");
        resend = findViewById(R.id.resend);
        resentotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialogWithTitle("Please wait while we are sending the OTP");
                numberVerify.verifyNumber(phone_no);
            }
        });
        edit = findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VerifyOTP.this, MainUI.class));
            }
        });
        phone = findViewById(R.id.phone);
        phone.setText(phone_no);
        editText = findViewById(R.id.otp);
        textView = findViewById(R.id.verifyotp);
        verifyOTPPresenter = new VerifyOTPPresenterImpl(this);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().length() < 4) {
                    editText.setError("Please enter the otp to proceed");
                }
                if (editText.getText().toString().length() == 4) {

                    verifyOTPPresenter.verifyOTP(editText.getText().toString(), phone_no);
                }
            }
        });
        startSMSListener();
    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    private void startSMSListener() {
        try {
            smsReceiver = new SMSReceiver();
            smsReceiver.setOTPListener(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            this.registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(this);

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Fail to start API
//                    showToast("failed");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        if (smsReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(smsReceiver);
        }
        super.onBackPressed();

    }

    @Override
    public void onOTPReceived(String otp) {
//        showToast("OTP Received: " + otp);
        editText.setText(otp);
        if (smsReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(smsReceiver);
        }
    }

    @Override
    public void onOTPTimeOut() {
        // showToast("OTP Time out");
    }

    @Override
    public void onOTPReceivedError(String error) {
        showToast(error);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (smsReceiver != null) {
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(smsReceiver);
//        }
    }


    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String response) {
        if (response.equals("failed")) {
            editText.setError("Invalid OTP");
        }
        if (!(response.equals("failed"))) {

            startActivity(new Intent(VerifyOTP.this, Verified.class));
        }
    }

    @Override
    public void onSuccess123(String response) {
        Toast.makeText(context, "Otp Resend Successfully", Toast.LENGTH_SHORT).show();
        hideProgressDialogWithTitle();
        resend.setVisibility(View.GONE);
        new CountDownTimer(60000, 1) {

            public void onTick(long millisUntilFinished) {
                text.setVisibility(View.VISIBLE);
                long remainedSecs = millisUntilFinished / 1000;
                text.setText("You can resend otp after " +(remainedSecs)+" seconds");
            }

            public void onFinish() {
                text.setVisibility(View.GONE);
            }
        }.start();
      resend.postDelayed(new Runnable() {
        public void run() {
          resend.setVisibility(View.VISIBLE);
        }
      }, 60000);
    }

    @Override
    public void onFailed(String response) {
        Toast.makeText(VerifyOTP.this, "Failed to verify", Toast.LENGTH_LONG).show();
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onVerifySuccess(String data) {
        Toast.makeText(VerifyOTP.this, "Successfully verified", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onVerifyFailed(String msg) {
        Toast.makeText(VerifyOTP.this, "Failed to verify", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onfailure(String errormessage) {

    }
}
