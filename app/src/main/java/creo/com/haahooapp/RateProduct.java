package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.config.ApiClient;

public class RateProduct extends AppCompatActivity {
    ImageView back,img;
    Context context = this;
    String image = "null";
    Activity activity = this;
    TextView name,price,submit;
    RatingBar productrating,shoprating;
    String pdt_rating = "";
    String shop_rating = "";
    EditText review;
    String productid = "null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_product);
        back = findViewById(R.id.back);
        img = findViewById(R.id.img);
        name = findViewById(R.id.name);
        price = findViewById(R.id.price);
        submit = findViewById(R.id.submit);
        review = findViewById(R.id.review);
        productrating = findViewById(R.id.ratingBar);
        shoprating = findViewById(R.id.rateshop);
        productrating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Float ratingval = (Float) rating;
                Float ratingvalue = ratingBar.getRating();
                pdt_rating = String.valueOf(ratingvalue);
            }
        });
        shoprating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Float ratingvalue = (Float) rating;
                Float ratingval = ratingBar.getRating();
                shop_rating = String.valueOf(ratingval);
            }
        });
        Bundle bundle = getIntent().getExtras();
        image = bundle.getString("image");
        name.setText(bundle.getString("name"));
        price.setText(bundle.getString("price"));
        productid = bundle.getString("productid");
        Glide.with(context).load(image).into(img);
        Window window = activity.getWindow();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if (pdt_rating.length()>0 && shop_rating.length()>0) {
                        submitreview(pdt_rating, shop_rating, review.getText().toString(), productid);
                    }
                    if (pdt_rating.length() == 0 || shop_rating.length()==0){
                        Toast.makeText(context,"Please rate product and shop",Toast.LENGTH_SHORT).show();
                    }
                  // Toast.makeText(context,"Please rate shop",Toast.LENGTH_SHORT).show();


            }

        });
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RateProduct.super.onBackPressed();
            }
        });

        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context,MyShopNew.class));
                                break;

                        }
                        return true;
                    }
                });
    }

    public void submitreview(String productrating,String shoprating,String review,String product_id){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"api_shop_app/add_pdt_rating/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("nnnnn", "jsonarray" + jsonObject);
                            String message = jsonObject.optString("message");
                            if (message.equals("success")){
                                Toast.makeText(context,"Review has been added successfully",Toast.LENGTH_SHORT).show();
                                RateProduct.super.onBackPressed();
                            }
                            if (message.equals("Failed")){
                                Toast.makeText(context,"Failed to add review",Toast.LENGTH_SHORT).show();
                            }
                           // Toast.makeText(context,"successfull",Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            //

            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pdt_id", product_id);
                params.put("rating",productrating);
                params.put("shop_rate",shoprating);
                params.put("virtual","1");
                params.put("review",review);
                   // Log.d("paramsssssssss","param"+product_id+productrating+shoprating+review);
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);

                return params;
            }
        };

        queue.add(stringRequest);
    }
}