package creo.com.haahooapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.ViewCouponActivity;
import creo.com.haahooapp.Adapters.CouponListAdapter;
import creo.com.haahooapp.Adapters.NewRewardAdapter;
import creo.com.haahooapp.Adapters.RewardAdapter;
import creo.com.haahooapp.Modal.Coupon;
import creo.com.haahooapp.Modal.Reward;
import creo.com.haahooapp.Modal.RewardPojo;
import creo.com.haahooapp.Modal.ViewCoupon;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.RetrofitClient;
import creo.com.haahooapp.interfaces.FragmentToActivity;
import creo.com.haahooapp.interfaces.ItemClick;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class EarnedFragment extends Fragment implements ItemClick {

    RecyclerView earned;
    private RewardAdapter orderAdapter;
    ArrayList<String> dataModelArrayList = new ArrayList<>();
    ArrayList<RewardPojo> rewardPojos = new ArrayList<>();
    RelativeLayout relativeLayout;
    PopupWindow popupWindow;
    ArrayList<Coupon> rewards = new ArrayList<>();
    String token;
    KonfettiView viewKonfetti;
    FragmentToActivity fragmentToActivity;

    public EarnedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentToActivity = (FragmentToActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_earned, container, false);
        earned = view.findViewById(R.id.offer);

        //viewKonfetti = view.findViewById(R.id.viewKonfetti);
        earned.setLayoutManager(new GridLayoutManager(getContext(), 2));
        relativeLayout = view.findViewById(R.id.relative);


        NewRewardAdapter newRewardAdapter = new NewRewardAdapter(getContext(), rewards, this);
        earned.setAdapter(newRewardAdapter);


        //getRewards();

//        earned.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                konfettiView.build()
//                        .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
//                        .setDirection(0.0, 359.0)
//                        .setSpeed(1f, 5f)
//                        .setFadeOutEnabled(true)
//                        .setTimeToLive(2000L)
//                        .addShapes(Shape.CIRCLE, Shape.CIRCLE)
//                        .addSizes(new Size(12, 5f))
//                        .setPosition(-50f, konfettiView.getWidth() + 50f, -50f, -50f)
//                        .streamFor(300, 5000L);
//            }
//        });
        SharedPreferences pref = this.getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        //  getCoupons();
        return view;

    }

    public void getCoupons() {
        Call<ViewCoupon> call = RetrofitClient
                .getInstance()
                .getApi()
                .getCoupons("Token " + token);
        call.enqueue(new Callback<ViewCoupon>() {
            @Override
            public void onResponse(Call<ViewCoupon> call, Response<ViewCoupon> response) {
                if (response.isSuccessful()) {
                    ViewCoupon viewCoupon = response.body();
                    if (response.body() != null) {
                        String success = response.body().getMessage();
                        if (success != null && success.equals("success")) {
                            List<Coupon> couponList = viewCoupon.getCoupon();
                            rewards = (ArrayList<Coupon>) couponList;
//                            NewRewardAdapter newRewardAdapter = new NewRewardAdapter(getContext(),rewards);
//                            earned.setAdapter(newRewardAdapter);

                            Toast.makeText(getContext(), "Successful", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                Toast.makeText(getContext(), "Successful", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<ViewCoupon> call, Throwable t) {
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void addProduct(String productid, String price1, String edited_price) {

    }

    @Override
    public void sendData(String id) {

        fragmentToActivity.send();

    }
//    public void getRewards(){
//
//        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//        String token = (pref.getString("token", ""));
//        RequestQueue queue = Volley.newRequestQueue(getContext());
//        //this is the url where you want to send the request
//        String url = ApiClient.BASE_URL+"api_shop_app/list_reward_pur_based/";
//        // Request a string response from the provided URL.
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        // Display the response string.
//
//                        try {
//                            JSONObject obj = new JSONObject(response);
//                            Log.d("rewqards","bncgbc"+obj);
//                            JSONArray data = obj.optJSONArray("data");
//                            for (int i = 0;i<data.length();i++){
//                                JSONObject jsonObject = data.optJSONObject(i);
//                                RewardPojo rewardPojo = new RewardPojo();
//                                rewardPojo.setId(jsonObject.optString("id"));
//                                rewardPojo.setReward_id(jsonObject.optString("reward_id"));
//                                rewardPojo.setValue(jsonObject.optString("value"));
//                                rewardPojo.setClaim_status(jsonObject.optString("claim_status"));
//                                rewardPojos.add(rewardPojo);
//                            }
//                            orderAdapter = new RewardAdapter(getContext(),rewardPojos);
//                            earned.setAdapter(orderAdapter);
//                            earned.setNestedScrollingEnabled(false);
//                            earned.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                                @Override
//                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                                    //Toast.makeText(getContext(),"value"+rewardPojos.get(i).getValue(),Toast.LENGTH_SHORT).show();
//                                    LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                                    View customView = layoutInflater.inflate(R.layout.reward_popup,null);
//                                    Button closePopupBtn = (Button) customView.findViewById(R.id.claim);
//                                    //instantiate popup window
//                                    ImageView imageView = customView.findViewById(R.id.image);
//                                    //Glide.with(getContext()).load("https://img.freepik.com/free-vector/realistic-golden-trophy-with-text-space_48799-1062.jpg?size=626&ext=jpg").into(imageView);
//                                    popupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.MATCH_PARENT, 650);
//                                    popupWindow.setBackgroundDrawable(new ColorDrawable(
//                                            android.graphics.Color.TRANSPARENT));
//                                    relativeLayout.setAlpha(0.2F);
//                                    //display the popup window
//                                    popupWindow.showAtLocation(relativeLayout, Gravity.CENTER, 0, 0);
//                                    //close the popup window on button click
//                                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            claimReward(rewardPojos.get(i).getId());
//                                            popupWindow.dismiss();
//                                            relativeLayout.setAlpha(1.0F);
//                                        }
//                                    });
//                                }
//                            });
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//               //
//            }
//        }) {
//
//            @Override
//            public java.util.Map<String, String> getHeaders()  {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Authorization", "Token " + token);
//                return params;
//            }
//        };
//        // Add the request to the RequestQueue.
//        queue.add(stringRequest);
//    }
//    public void claimReward(String id){
//
//        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//        String token = (pref.getString("token", ""));
//        RequestQueue queue = Volley.newRequestQueue(getContext());
//        //this is the url where you want to send the request
//
//        String url = ApiClient.BASE_URL+"api_shop_app/reward_claim/";
//
//        // Request a string response from the provided URL.
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        // Display the response string.
//
//                        try {
//
//                            JSONObject obj = new JSONObject(response);
//                            Log.d("claim","bncgbc"+obj);
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//               //
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("id", id);
//                return params;
//            }
//
//            @Override
//            public java.util.Map<String, String> getHeaders()  {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Authorization", "Token " + token);
//                return params;
//            }
//        };
//        // Add the request to the RequestQueue.
//        queue.add(stringRequest);
//
//    }

}