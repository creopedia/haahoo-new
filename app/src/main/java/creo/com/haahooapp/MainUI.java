package creo.com.haahooapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.provider.Settings;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahooapp.Implementation.ForgotOtpPresenterImpl;
import creo.com.haahooapp.Implementation.LoginPresenterImpl;
import creo.com.haahooapp.Implementation.NumberVerifyPresenterImpl;
import creo.com.haahooapp.Implementation.PinVerifyPresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.ForgotOtpCallback;
import creo.com.haahooapp.interfaces.ForgotOtpPresenter;
import creo.com.haahooapp.interfaces.ForgotOtpView;
import creo.com.haahooapp.interfaces.LoginCallBack;
import creo.com.haahooapp.interfaces.LoginPresenter;
import creo.com.haahooapp.interfaces.LoginView;
import creo.com.haahooapp.interfaces.NumberVerifyPresenter;
import creo.com.haahooapp.interfaces.PinVerifyCallback;
import creo.com.haahooapp.interfaces.PinVerifyPresenter;
import creo.com.haahooapp.interfaces.PinVerifyView;
import creo.com.haahooapp.interfaces.VerifyNumberCallBack;
import creo.com.haahooapp.interfaces.VerifyNumberView;

public class MainUI extends AppCompatActivity  implements VerifyNumberCallBack,VerifyNumberView,LoginCallBack,LoginView,ForgotOtpCallback,ForgotOtpView,PinVerifyCallback,PinVerifyView {
    boolean doubleBackToExitPressedOnce = false;
    ImageView cardView;
    TextInputEditText editText;
    Context context = this;
    TextInputEditText password;
    String total_count = "0";
    TextView forgot;
    ImageView logo;
    TextInputLayout textInputLayout;
    ForgotOtpPresenter forgotOtpPresenter;
    NumberVerifyPresenter numberVerify;
    Activity activity = this;
    private FirebaseAnalytics analytics;
    String refresh = null;
    JSONArray jsonObject = new JSONArray();
    ProgressBar progressBar;
    PinVerifyPresenter pinVerifyPresenter;
    LoginPresenter loginPresenter;
    TextView loginwithPin,login;
    String pin = null;
    private String android_id = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_ui);
        Window window = activity.getWindow();
        Log.d("jhgvhjvbjhv","jhghjg"+ApiClient.referal_id);
        Log.d("mainhgv","mainur"+ApiClient.product_referral_id);

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        analytics = FirebaseAnalytics.getInstance(MainUI.this);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        logo = findViewById(R.id.logo);
        Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/haahoo_logo1.png").into(logo);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        refresh = refreshedToken;
       // Log.d("tbhiuh", "Refreshed token: " + refreshedToken);
// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        login = findViewById(R.id.login);
        loginwithPin = findViewById(R.id.loginwithpin);
        pinVerifyPresenter = new PinVerifyPresenterImpl(this);
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        progressBar = findViewById(R.id.progressBar_cyclic);
        password = findViewById(R.id.pwd);
        textInputLayout = findViewById(R.id.pwdlayout);
        password.setVisibility(View.GONE);
        forgot = findViewById(R.id.forgot);
        loginPresenter = new LoginPresenterImpl(this);
        forgotOtpPresenter = new ForgotOtpPresenterImpl(this);
         android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        // AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(this);
        // Log.d("APP", "Apps Hash Key: " + appSignatureHashHelper.getAppSignatures().get(0));
        editText = findViewById(R.id.uname);
        numberVerify = new NumberVerifyPresenterImpl(this);
        cardView = findViewById(R.id.continuetologin);
        loginwithPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainUI.this,LoginWithPin.class));

            }
        });
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password.setVisibility(View.GONE);
                textInputLayout.setVisibility(View.GONE);
                forgot.setVisibility(View.GONE);
                login.setVisibility(View.GONE);
                cardView.setVisibility(View.VISIBLE);
            }
        });

        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                password.setVisibility(View.GONE);
                textInputLayout.setVisibility(View.GONE);
                forgot.setVisibility(View.GONE);
                login.setVisibility(View.GONE);
                cardView.setVisibility(View.VISIBLE);

                return false;
            }
        });

//        editText.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                password.setVisibility(View.GONE);
//                forgot.setVisibility(View.GONE);
//                login.setVisibility(View.GONE);
//                cardView.setVisibility(View.VISIBLE);
//
//                return true;
//            }
//        });

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (editText.getText().toString().length() != 10) {
                        editText.setError("Number must be 10 digits");
                    }
                    if (editText.getText().toString().length() == 10) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("phone_no", editText.getText().toString());
                        editor.commit();
                        numberVerify.verifyNumber(editText.getText().toString());

                        progressBar.setVisibility(View.VISIBLE);

                    }
                }

            });
        }


    @Override
    public void onVerifySuccess(String data, String data1) {

    }

    @Override
    public void onVerifySuccess(String data) {

    }

    @Override
    public void onVerifyFailed(String message) {
        Toast.makeText(MainUI.this,"Failed",Toast.LENGTH_LONG).show();
    }

    @Override
    public void jsonObjectSuccess(JSONArray jsonObject,String next) {

    }

    @Override
    public void onfailure(String errormessage) {

    }

    @Override
    public void onSuccess123(String response) {
       if(!(response.equals("verify"))) {
           progressBar.setVisibility(View.GONE);
           getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
           Intent intent = new Intent(MainUI.this, VerifyOTP.class);
           intent.putExtra("phone_no", editText.getText().toString());
           startActivity(intent);
       }
       if(response.equals("verify")){

           closeKeyboard();
           progressBar.setVisibility(View.GONE);
           getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
           password.setVisibility(View.VISIBLE);
           textInputLayout.setVisibility(View.VISIBLE);
           cardView.setVisibility(View.GONE);
           login.setVisibility(View.VISIBLE);
           if(password.getVisibility() == View.VISIBLE) {
               forgot.setVisibility(View.VISIBLE);
               forgot.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                        forgotOtpPresenter.forgotOtp(editText.getText().toString());
                        Intent intent = new Intent(MainUI.this,ForgotOTP.class);
                        intent.putExtra("phone_no",editText.getText().toString());
                        startActivity(intent);

                   }
               });
               login.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       cartCount(response);
                       if(password.getText().toString().length() == 0){
                           password.setError("Password cannot be empty");
                       }
                       if(!(password.getText().toString().length() == 0)) {

                           loginPresenter.login(editText.getText().toString(), password.getText().toString(), android_id,refresh);
                         //  pinVerifyPresenter.getProducts();
                           progressBar.setVisibility(View.VISIBLE);
                       }
                   }
               });
           }

       }

    }


//    @Override
//    public void onSuccess(String response,String data) {
//
//    }

    @Override
    public void onSuccess(String response, String mem) {

    }

    @Override
    public void onFailed(String response) {

        Toast.makeText(MainUI.this, "Failed to get response",Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.GONE);
        cardView.setClickable(true);
        loginwithPin.setClickable(true);
        loginwithPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainUI.this,LoginWithPin.class));
            }
        });

    }

    @Override
    public void jsonSUccess(JSONArray jsonObject1,String next) {
        jsonObject = jsonObject1;
        Intent intent = new Intent(MainUI.this,NewHome.class);
        intent.putExtra("products",jsonObject.toString());
        startActivity(intent);

    }

    @Override
    public void noInternetConnection() {

        Toast.makeText(MainUI.this,"No Internet Connection" ,Toast.LENGTH_LONG).show();

    }

    public void closeKeyboard(){

        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

    }

    @Override
    public void onBackPressed() {
        progressBar.setVisibility(View.GONE);
        if (doubleBackToExitPressedOnce) {
          //  finish();
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
           // System.exit(1);
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public void onSuccess1(String response,String data) {
        progressBar.setVisibility(View.GONE);
        if(response.equals("failed")) {

            Toast.makeText(MainUI.this,"Invalid credentials",Toast.LENGTH_LONG).show();
            password.setText("");

        }
        if(!(response.equals("failed"))) {
            Log.d("bvnckl","xcsfgvbhn"+response);
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("token", response);
            editor.putString("membership", data);
            editor.commit();

            ApiClient.user_token = response;
            //Log.d("bbbjhjh","tokkkk"+ApiClient.user_token);
            wishcount(response);
            loginToSkillq(editText.getText().toString(),response);
            if (ApiClient.product_referral_id.equals("0") && ApiClient.shopidrefer.equals("0")) {
                Log.d("GYVGYVYV","TYCRC"+ApiClient.product_referral_id+ApiClient.shopidrefer);
                cartCount(response);
            }
            if (!(ApiClient.productinshopid.equals("0"))){
                Log.d("GYVGYVYV","THREE");
                Intent intent = new Intent(context,ProductDetailsShop.class);
                intent.putExtra("shopid","1");
                intent.putExtra("productid",ApiClient.productinshopid);
                startActivity(intent);
            }
            if (!(ApiClient.shopidrefer.equals("0")) && (ApiClient.product_referral_id.equals("0"))){
                Log.d("GYVGYVYV","FOUR");
                Intent intent = new Intent(MainUI.this,ProductsInShop.class);
                intent.putExtra("categoryid",ApiClient.shopidrefer);
                intent.putExtra("from","refer");
                startActivity(intent);
            }

        }

    }

    public void wishcount(final String token){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"wishlist/wishlist_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            String message = jsonObject1.getString("message");
                            //Log.d("imagewish", "wish" +jsonObject1.optString("Total count"));
                            ApiClient.wish_count = jsonObject1.optString("Total count");

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       // Toast.makeText(MainUI.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public void loginToSkillq(String number,String token1){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.haahoo.in/api_login/api_haahoo_user_update/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);

                            Log.d("ffgh", "wish" +jsonObject1);
                           // ApiClient.wish_count = jsonObject1.optString("Total count");

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(MainUI.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("phone_number",number);
                params.put("token",token1);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


    public void cartCount(final String token){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"cart_view/cart_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            total_count = jsonObject1.optString("total all count");
                            if(total_count.equals("")){
                                total_count = "0";
                                ApiClient.count = Integer.valueOf(total_count);
                                if (!(ApiClient.study_referal_id.equals("0"))){
                                    Intent intent = new Intent(MainUI.this, CouponDetailsActivity.class);
                                    intent.putExtra("from","referal");
                                    startActivity(intent);
                                }

                                else {
                                    startActivity(new Intent(MainUI.this, NewHome.class));
                                }
                            }
                            if(!(total_count.equals(""))) {
                                //  mNotificationCounter = Integer.parseInt(total_count);
                                ApiClient.count = Integer.valueOf(total_count);
                                if(ApiClient.study_referal_id!="0"){
                                    Intent intent = new Intent(MainUI.this, CouponDetailsActivity.class);
                                    intent.putExtra("from","referal");
                                    startActivity(intent);
                                }else {
                                    startActivity(new Intent(MainUI.this, NewHome.class));
                                }
                            }
                            Log.d("rrdddfgh","resss"+ApiClient.count);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                     //   Toast.makeText(MainUI.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onFailed1(String response) {

    }

    @Override
    public void onSuccessOtp(String response) {
       // Toast.makeText(MainUI.this,"OTP Received",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailedOtp(String response) {
        Toast.makeText(MainUI.this,"Failed",Toast.LENGTH_LONG).show();

    }

    @Override
    public void onVerifySuccessOtp(String data) {

    }

    @Override
    public void onVerifyFailedOtp(String msg) {

    }


}
