package creo.com.haahooapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import creo.com.haahooapp.config.ApiClient;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * create an instance of this fragment.
 */
public class EventFirstFragment extends Fragment {
    RelativeLayout relativeLayout,checkout;
    TextView time,location;
    // TODO: Rename parameter arguments, choose names that match
    public EventFirstFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_event_first,container,false);
        relativeLayout = view.findViewById(R.id.rel);
        time = view.findViewById(R.id.time);
        checkout = view.findViewById(R.id.checkout);
        location = view.findViewById(R.id.location);
        location.setText(ApiClient.event_location);
        time.setText(ApiClient.event_time);
        Resources res = getResources();
        Calendar c = Calendar.getInstance();
        int date = Integer.parseInt(ApiClient.availabledates.get(0));
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerDetails();
            }
        });
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ghghfghf","uytrd"+date);
                if (date > 0) {
                Drawable drawable = res.getDrawable(R.drawable.border_relative_available);
                relativeLayout.setBackground(drawable);

                    checkout.setVisibility(View.VISIBLE);
                }
                if (date == 0){
                    Toast.makeText(getContext(),"Seats are full..",Toast.LENGTH_SHORT).show();
                }
               // relativeLayout.setBackgroundColor(Color.parseColor("#079f16"));
            }
        });
        if (date == 0) {
            Drawable drawable = res.getDrawable(R.drawable.border_relative_complete);
            relativeLayout.setBackground(drawable);
            time.setTextColor(Color.RED);
            checkout.setVisibility(View.GONE);
        }
        if (date > 0){
            Drawable drawable = res.getDrawable(R.drawable.border_relative_complete);
            relativeLayout.setBackground(drawable);
            time.setTextColor(Color.parseColor("#079f16"));
        }

        return view;

    }

    public void registerDetails(){

        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        Log.d("toke","token"+token);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"events/event_reg/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.optString("message");
                            if(message.equals("Success")){

                                String data = jsonObject.optString("data");

                                Intent intent = new Intent(getContext(),MakePaymentEvents.class);
                                intent.putExtra("number",ApiClient.event_number);
                                intent.putExtra("data",data);
                                intent.putExtra("email",ApiClient.event_email);
                                intent.putExtra("event_id",ApiClient.event_id);
                                startActivity(intent);

                            }
                            if(!(message.equals("Success"))){
//                                progressDialog.dismiss();
                                Toast.makeText(getContext(),"Some error occured",Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        progressDialog.dismiss();
                        //Toast.makeText(getContext(), "//error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id",ApiClient.event_id);
                params.put("proffession",ApiClient.proffession_name);
                params.put("proffession_type",ApiClient.proffession_type);
                params.put("name", ApiClient.event_user_name);
                params.put("date",ApiClient.dates.get(0));
                params.put("phone_no", ApiClient.event_number);
                params.put("email",ApiClient.event_email);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

}
