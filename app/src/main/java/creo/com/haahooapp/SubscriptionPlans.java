package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.SubscribeAdapter;
import creo.com.haahooapp.Modal.SubscriptionPlanPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ItemClickSupport;
import creo.com.haahooapp.interfaces.Recycler;

public class SubscriptionPlans extends AppCompatActivity implements Recycler {

   RecyclerView recyclerView;
   SubscribeAdapter subscribeAdapter;
   ImageView image;
   String shop_id = "null";
   Activity activity = this;
   String productid = "null";
   public  TextView proceed;
   Context context = this;
   ArrayList<SubscriptionPlanPojo> pojos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_plans);
        recyclerView = findViewById(R.id.recycle);
        proceed = findViewById(R.id.proceed);
        image = findViewById(R.id.back);
        Bundle bundle = getIntent().getExtras();
        productid = bundle.getString("productid");
        shop_id = bundle.getString("shopid");
        getSubscriptionDetails(productid);
        subscribeAdapter = new SubscribeAdapter(context,pojos,this);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        image.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SubscriptionPlans.super.onBackPressed();
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });

    }


    public void getSubscriptionDetails(String pdt_id){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"api_shop_app/subscription_mode/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {

                            JSONObject obj = new JSONObject(response);

                            JSONObject jsonObject = obj.optJSONObject("data");

                            for (int i = 0 ; i <jsonObject.length();i++){
                                JSONObject jsonObject1 = jsonObject.optJSONObject("sub_mode"+i);
                                SubscriptionPlanPojo subscriptionPlanPojo = new SubscriptionPlanPojo();
                                subscriptionPlanPojo.setName(jsonObject1.optString("name"));
                                subscriptionPlanPojo.setAmount(jsonObject1.optString("payment"));
                                subscriptionPlanPojo.setBenefits(jsonObject1.optString("benefits"));
                                pojos.add(subscriptionPlanPojo);
                            }

                            recyclerView.setAdapter(subscribeAdapter);
                            recyclerView.setNestedScrollingEnabled(false);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Toast.makeText(context,"//Error",Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", pdt_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public static void view_visible(String value){
        //proceed.setVisibility(View.VISIBLE);

    }

    @Override
    public void onClick(String value) {
        proceed.setVisibility(View.VISIBLE);

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Toast.makeText(context,"Selected value"+ApiClient.subscribe_amount,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context,SubscriptionPayment.class);
                intent.putExtra("amount",ApiClient.subscribe_amount+"00");
                intent.putExtra("type",value);
                intent.putExtra("shopid",shop_id);
                intent.putExtra("productid",productid);
                startActivity(intent);

            }
        });

    }

    @Override
    public void variationClick(String value) {

    }
}
