package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.config.ApiClient;

public class ForgotPinNumber extends AppCompatActivity {

    TextView next;
    EditText number;
    ImageView logo ;
    Context context = this;
    Activity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pin_number);
        logo = findViewById(R.id.logo);
        Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/phone_hand.jpg").into(logo);

        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        next = findViewById(R.id.next);
        number = findViewById(R.id.number);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(number.getText().toString().length() < 10){
                    number.setError("Please enter the correct number");
                }

              if (number.getText().toString().length() == 10){
                  submit();
              }
            }
        });
    }

    void submit(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/forget_password/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
//                            Log.d("rrrr","dddd"+jsonObject1.toString());
                            String message = jsonObject1.optString("message");
                            if(message.equals("Success")){
                               Intent intent = new Intent(ForgotPinNumber.this,ForgotPinOtp.class);
                               intent.putExtra("phone_number",number.getText().toString());
                               startActivity(intent);
                            }



                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                     //   Toast.makeText(ForgotPinNumber.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("phone_no",number.getText().toString());
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }
}
