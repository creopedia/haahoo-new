package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.config.ApiClient;

public class Track extends AppCompatActivity {

    Activity activity = this;
    TextView order_id,name,price;
    Context context = this;
    String token = null;
    String order = null;
    String virtual = "null";
    CardView cancel;
    PopupWindow popupWindow;
    RelativeLayout relativeLayout;
    TextView confirmed,ready;
    ImageView second,second_empty,third,third_empty,img,back;
    View secondline_empty,thirdline_empty,secondline,thirdline;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        Window window = activity.getWindow();
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Track.super.onBackPressed();
            }
        });
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        second = findViewById(R.id.second);
        cancel = findViewById(R.id.cancel);

        second_empty = findViewById(R.id.second_empty);
        third = findViewById(R.id.third);
        name = findViewById(R.id.name);
        price = findViewById(R.id.price);
        third_empty = findViewById(R.id.third_empty);
        relativeLayout = findViewById(R.id.relative);
        img = findViewById(R.id.img);
        secondline = findViewById(R.id.secondline);
        secondline_empty = findViewById(R.id.secondline_empty);
        thirdline = findViewById(R.id.thirdline);
        thirdline_empty = findViewById(R.id.thirdline_empty);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        Bundle bundle = getIntent().getExtras();
        order = bundle.getString("orderid");
        virtual = bundle.getString("virtual");
        order_id = findViewById(R.id.order_id);
        order_id.setText(ApiClient.sh_order_id);
        confirmed = findViewById(R.id.confirmed);
        ready = findViewById(R.id.ready);
        loadDetails(order);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater) Track.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.order_cancel, null);
                Button closePopupBtn = (Button) customView.findViewById(R.id.closePopupBtn);
                TextView apply = customView.findViewById(R.id.apply);

                apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showProgressDialogWithTitle("Cancelling Your Order....");
                        RequestQueue queue = Volley.newRequestQueue(Track.this);
                        //this is the url where you want to send the request

                        String url = ApiClient.BASE_URL+
                        "order_details/order_cancel/";

                        // Request a string response from the provided URL.


                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // Display the response string.

                                        try {

                                            JSONObject obj = new JSONObject(response);
                                            String message = obj.optString("message");
                                            if(message.equals("Success")){
                                                Toast.makeText(Track.this,"Successfully cancelled the order",Toast.LENGTH_SHORT).show();
                                                hideProgressDialogWithTitle();
                                                startActivity(new Intent(Track.this,Orders.class));
                                            }
                                            if(message.equals("Failed")){
                                                Toast.makeText(Track.this,"Failed to cancel the order",Toast.LENGTH_SHORT).show();
                                            }
                                            Log.d("ordrrrr","resp"+response);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
//order_details/order_cancel order_id
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Log.d("responseerror", "resp" + error.networkResponse.statusCode);
                                Toast.makeText(Track.this,"//Error",Toast.LENGTH_LONG).show();

                            }
                        }) {
                            //adding parameters to the request
                            @Override
                            protected Map<String, String> getParams()  {
                                Map<String, String> params = new HashMap<>();
                                params.put("order_id", order);

                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders()  {
                                Map<String, String> params = new HashMap<String, String>();
                                //params.put("Content-Type", "application/json; charset=utf-8");
                                params.put("Authorization", "Token " + token);
                                return params;
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(context);
                        requestQueue.add(stringRequest);
                        // Add the request to the RequestQueue.
                        queue.add(stringRequest);
                    }
                });


                popupWindow = new PopupWindow(customView, ViewPager.LayoutParams.MATCH_PARENT, 1000);
                popupWindow.setBackgroundDrawable(new ColorDrawable(
                        android.graphics.Color.TRANSPARENT));
                relativeLayout.setAlpha(0.2F);
                //display the popup window
                popupWindow.showAtLocation(relativeLayout, Gravity.CENTER, 0, 0);
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        relativeLayout.setAlpha(1.0F);
                    }
                });
            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Track.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Track.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Track.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(Track.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
    }


    public void loadDetails(String orderid){
        RequestQueue queue = Volley.newRequestQueue(Track.this);
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL+"order_details/view_order_id/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("ddsaa","reds"+obj);
                            JSONArray jsonArray = obj.getJSONArray("data");
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String name1 = jsonObject.getString("name");
                            name.setText(name1);
                            price.setText("₹ "+jsonObject.optString("price"));
                            String images1 = jsonObject.getString("image");
                            String status = jsonObject.optString("status");
                            if( status.equals("Processing")){
                                second_empty.setVisibility(View.VISIBLE);
                                secondline_empty.setVisibility(View.VISIBLE);
                                third_empty.setVisibility(View.VISIBLE);
                                thirdline_empty.setVisibility(View.VISIBLE);
                            }
                            if (status.equals("dispatched")){
                                second.setVisibility(View.VISIBLE);
                                secondline.setVisibility(View.VISIBLE);
                                third_empty.setVisibility(View.VISIBLE);
                                thirdline_empty.setVisibility(View.VISIBLE);
                                confirmed.setTypeface(null, Typeface.BOLD);
                                cancel.setVisibility(View.GONE);
                            }
                            if (status.equals("delivered")){
                                second.setVisibility(View.VISIBLE);
                                secondline.setVisibility(View.VISIBLE);
                                third.setVisibility(View.VISIBLE);
                                thirdline.setVisibility(View.VISIBLE);
                                confirmed.setTypeface(null,Typeface.BOLD);
                                ready.setTypeface(null,Typeface.BOLD);
                                cancel.setVisibility(View.GONE);
                            }

                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]","");
                            Glide.with(context).load(ApiClient.BASE_URL+"media/" + split).into(img);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Track.this,"//Error",Toast.LENGTH_LONG).show();

            }
        }) {
            //adding parameters to the request
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();
                params.put("order_id", order);
                params.put("sh_order_id",ApiClient.sh_order_id);
                params.put("virtual",virtual);
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
              //  params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        queue.add(stringRequest);
    }


    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }
}
