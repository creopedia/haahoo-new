package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahooapp.Adapters.AccountAdapter;

public class MyAccount extends AppCompatActivity {

    TabLayout tabLayout;
    Activity activity = this;
    ViewPager viewPager;
    ImageView back;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        Window window = activity.getWindow();
        viewPager = findViewById(R.id.viewPager);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = getIntent().getExtras();
                String value = bundle.getString("fromactivity");
                if(value.equals("update")){
                    startActivity(new Intent(MyAccount.this,NewHome.class));
                }
                if ((value.equals("home"))) {
                    MyAccount.super.onBackPressed();
                }
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(MyAccount.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(MyAccount.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(MyAccount.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(MyAccount.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, CouponDetailsActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Profile"));
        tabLayout.addTab(tabLayout.newTab().setText("Accounts"));
        tabLayout.addTab(tabLayout.newTab().setText("Withdrawal"));
        tabLayout.addTab(tabLayout.newTab().setText("KYC Details"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final AccountAdapter accountAdapter = new AccountAdapter(this,getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(accountAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        Bundle bundle = getIntent().getExtras();
        String value = bundle.getString("fromactivity");
        if(value.equals("update")){
            startActivity(new Intent(MyAccount.this,NewHome.class));
        }
        if ((value.equals("home"))) {
            MyAccount.super.onBackPressed();
        }
    }
}
