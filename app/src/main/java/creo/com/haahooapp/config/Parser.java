package creo.com.haahooapp.config;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Parser {
    public int getCode() {
        return code;
    }

    public JsonElement getData() {
        return data;
    }

    public String getResults() {
        return results;
    }

    public String getResponse() {
        return response;
    }

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }

    public String getToken1() {
        return token1;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getMembership() {
        return membership;
    }


    public void setMembership(String membership) {
        this.membership = membership;
    }

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("data")
    @Expose
    private JsonElement data;
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("Token")
    @Expose
    private String token1;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("results")
    @Expose
    private String results;

    @SerializedName("next")
    @Expose
    private String next;
    @SerializedName("membership")
    @Expose
    private String membership;

}
