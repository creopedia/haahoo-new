package creo.com.haahooapp.config;

import creo.com.haahooapp.Modal.AddaddressPojo;
import creo.com.haahooapp.Modal.CartId;
import creo.com.haahooapp.Modal.ChangePassword;
import creo.com.haahooapp.Modal.CombinedCart;
import creo.com.haahooapp.Modal.DeliveryModes;
import creo.com.haahooapp.Modal.DevicePojo;
import creo.com.haahooapp.Modal.EarningAddShop;
import creo.com.haahooapp.Modal.ForgotOTPPojo;
import creo.com.haahooapp.Modal.LoginPojo;
import creo.com.haahooapp.Modal.Membership;
import creo.com.haahooapp.Modal.PasswordReset;
import creo.com.haahooapp.Modal.PinVerifyPojo;
import creo.com.haahooapp.Modal.ProductId;
import creo.com.haahooapp.Modal.RegisterPojo;
import creo.com.haahooapp.Modal.SetPinPojo;
import creo.com.haahooapp.Modal.VerifyModal;
import creo.com.haahooapp.Modal.VerifyOtpPojo;
import creo.com.haahooapp.Modal.ViewCoupon;
import creo.com.haahooapp.Modal.WishListId;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/otp_generation/")
    Call<Parser> verifyNumber(@Body VerifyModal verifyModal);

    @FormUrlEncoded
    @POST("api_shop_app/list_products/")
    Call<ResponseBody> getProducts(@Field("id") String verifyModal, @Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api_shop_app/shops_pdt_cat_based/")
    Call<ResponseBody> getProductsundersubcat(@Field("shop_id") String verifyModal, @Field("cat_id") String category_id, @Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api_shop_app/shops_pdt_cat_based/")
    Call<ResponseBody> getProductsundersubcatPaginate(@Field("shop_id") String verifyModal, @Field("cat_id") String category_id, @Header("Authorization") String token, @Query("page") int page);

    @FormUrlEncoded
    @POST("api_shop_app/sorting_pdt/")
    Call<ResponseBody> getProductsSorted(@Field("shop_id") String verifyModal, @Field("sort_patrn") String sort, @Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api_shop_app/sorting_pdt/")
    Call<ResponseBody> getProductsSortedpaginate(@Field("shop_id") String verifyModal, @Field("sort_patrn") String sort, @Header("Authorization") String token, @Query("page") int page);

    @FormUrlEncoded
    @POST("api_shop_app/search_products_by_category/")
    Call<ResponseBody> getProductsFish(@Field("sub_category_name") String verifyModal, @Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api_shop_app/latest_products/")
    Call<ResponseBody> getProductslatest(@Header("Authorization") String token,@Field("latitude")String latitude,@Field("longitude")String longitude);

    @FormUrlEncoded
    @POST("api_shop_app/find_variant/")
    Call<ResponseBody> getProductVariants(@Field("product_id") String verifyModal, @Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api_shop_app/list_products/")
    Call<ResponseBody> getProductspaginate(@Field("id") String verifyModal, @Header("Authorization") String token, @Query("page") int page);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/phone_verify/")
    Call<Parser> verifyOTP(@Body VerifyOtpPojo verifyOtpPojo);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/registeration/")
    Call<Parser> register(@Body RegisterPojo verifyOtpPojo);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/model_id_verification/")
    Call<Parser> verify_device(@Body DevicePojo deviceId);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/pin_generation/")
    Call<Parser> set_pin(@Body SetPinPojo setPinPojo, @Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/pin_verify/")
    Call<Parser> pin_verify(@Body PinVerifyPojo pinVerifyPojo);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/login/")
    Call<Parser> login(@Body LoginPojo loginPojo);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/forget_password/")
    Call<Parser> forget_password(@Body ForgotOTPPojo forgotOTPPojo);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/change_password/")
    Call<Parser> change_password(@Body ChangePassword changePassword, @Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/password_reset/")
    Call<Parser> password_reset(@Body PasswordReset passwordReset);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET("product_details/product_view/")
    Call<Parser> products();

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET("events/events_view/")
    Call<Parser> events();

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("login/user_details/")
    Call<Parser> getProfile(@Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("product_details/product_view_id/")
    Call<Parser> getProductDetail(@Header("Authorization") String token, @Body ProductId productId);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("events/events_view_id/")
    Call<Parser> getEventDetail(@Body ProductId productId);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("wishlist/wishlist_add/")
    Call<Parser> addToWishList(@Body WishListId productId, @Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("wishlist/wishlist_remove/")
    Call<Parser> removeFromWishList(@Body WishListId productId, @Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("cart_view/cart_add/")
    Call<Parser> addToCart(@Body CartId cartId, @Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("cart_view/cart_show/")
    Call<Parser> viewCart(@Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("wishlist/wishlist_show/")
    Call<Parser> viewWishlist(@Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("booking/address_add/")
    Call<Parser> add_address(@Body AddaddressPojo addaddressPojo, @Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("booking/address_list/")
    Call<Parser> view_address(@Header("Authorization") String token);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET("product_details/product_view/")
    Call<Parser> callProducts();

    @FormUrlEncoded
    @POST("api_shop_app/resell_stock_balance/")
    Call<ResponseBody> getResellCount(
            @Header("Authorization") String token,
            @Field("pdt_id") String pdt_id,
            @Field("add_to_myshop_count") String count
    );

    @FormUrlEncoded
    @POST("api_shop_app/generate_coupon/")
    Call<Membership> getCouponCode(
            @Header("Authorization") String token,
            @Field("payment_id") String payment_id,
            @Field("payment_amount") String payment_amount
    );

    @FormUrlEncoded
    @POST("login/add_membership/")
    Call<Membership> getMembership(
            @Header("Authorization") String token,
            @Field("payment_id") String payment_id,
            @Field("amount") String amount
    );

    @POST("api_shop_app/view_user_all_coupons/")
    Call<ViewCoupon> getCoupons(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("api_shop_app/list_shop_delivery_modes/")
    Call<DeliveryModes> getDeliveryModes(
            @Header("Authorization") String token,
            @Field("shop_id") String shop_id
    );

    @POST("cart_view/club_user_cart/")
    Call<CombinedCart> getCartList(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("login/check_referal_id/")
    Call<Membership> getReferal(
            @Header("Authorization") String token,
            @Field("referal_id") String referal_id
    );

    @POST("api_shop_app/view_add_shop_wallet/")
    Call<EarningAddShop> getAddShopEarnings(
            @Header("Authorization") String token
    );

    @POST("login/view_coupon_reward/")
    Call<EarningAddShop> getReferalEarnings(
            @Header("Authorization") String token
    );
}