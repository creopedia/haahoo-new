package creo.com.haahooapp.config;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import creo.com.haahooapp.Modal.ProductB2BPojo;
import creo.com.haahooapp.Modal.Spec_header_values;
import creo.com.haahooapp.Modal.StoryPojo;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static String price = null;
    public static String bazaar = "null";
    public static String shop_refer = "0";
    public static Integer count=0;
    public static String proof="Choose document";
    public static String evt_img="null";
    public static String evt_location="null";
    public static String evt_date="null";
    public static String productid = "null";
    public static String shopid = "null";
    public static String directid = null;
    public static String is_virtual = "null";
    public static String own_id = "0";
    public static String wish_count = "0";
    public static String activity_status =null;
    public static String sh_order_id = null;
    public static String mobile = null;
    public static String subscribe_amount = "null";
    public static String email = null;
    public static String event_id = "null";
    public static String story_id = "null";
    public static String story_id_name = "null";
    public static String proffession_name = "null";
    public static String proffession_type = "null";
    public static ArrayList<StoryPojo>dataModelArrayList = new ArrayList<>();
    public static ArrayList<StoryPojo>dataModelArrayList1 = new ArrayList<>();
    public static String event_user_name = "null";
    public static String event_number = "null";
    public static String event_email = "null";
    public static String sell_phone = null;
    public static String qty = null;
    public static String id = null;
    public static String product_name = null;
    public static String product_price = null;
    public static String product_description = null;
    public static ArrayList<String> name = new ArrayList<>();
    public static ArrayList<String> product_features = new ArrayList<>();
    public static ArrayList<String> search_name = new ArrayList<>();
    public static ArrayList<String> prices = new ArrayList<>();
    public static ArrayList<String> virtual = new ArrayList<>();
    public static ArrayList<String> quantity = new ArrayList<>();
    public static ArrayList<String> delivery = new ArrayList<>();
    public static ArrayList<String> quantity1 = new ArrayList<>();
    public static ArrayList<String> ids = new ArrayList<>();
    public static ArrayList<String> variant_ids = new ArrayList<>();
    public static ArrayList<String> searchids = new ArrayList<>();
    public static ArrayList<String> featured = new ArrayList<>();
    public static ArrayList<String> cart_status = new ArrayList<>();
    public static String shop_id_del = "null";
    public static ArrayList<String> shop_id = new ArrayList<>();
    public static ArrayList<String> isvirtual = new ArrayList<>();
    public static String cate_id = "0";
    public static ArrayList<String> wishlist_ids = new ArrayList<>();
    public static ArrayList<String> productids = new ArrayList<>();
    public static ArrayList<String> shids = new ArrayList<>();
    public static ArrayList<String> virtual_order = new ArrayList<>();
    public static ArrayList<String> addressids = new ArrayList<>();
    public static ArrayList<Spec_header_values> values = new ArrayList<>();
    public static List<String> availabledates = new ArrayList<>();
    public static List<String> dates = new ArrayList<>();
    public static String eventprice = null;
    public static String address = null;
    public static Map<String,String> hashMap = new HashMap<String,String>();
    public static ArrayList<String> listdata  = new ArrayList<>();
    public static ArrayList<String> listdata_head  = new ArrayList<>();
    public static Map<String, List<String>> map = new HashMap<String, List<String>>();
    public static String referal_id = "0";
    public static String study_referal_id = "0";
    public static String productinshopid = "0";
    public static String shopidrefer = "0";
    public static String product_referral_id = "0";
    public static String token = null;
    public static String event_price = null;
    public static String event_name = "null";
    public static String event_time = "null";
    public static String event_location = "null";
    public static ArrayList<String> images = new ArrayList<>();
    public static ArrayList<String> cart_id = new ArrayList<>();
    //public static  final String BASE_URL = "https://haahoo.in/";
    public static final String BASE_URL = "https://testapi.creopedia.com/";
    public static String username = null;
    public static String user_contact_no = null;
    private static Retrofit retrofit = null;
    public static List<ProductB2BPojo> productB2BPojo = new ArrayList<>();
    public static String user_token = "null";
    public static String user_earnings = null;
    public static String directsell_cartcount = "null";
    public static String user_id = "null";
    public static String list_url = "null";

    public static Retrofit getClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getClientnew() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    public static  final String BASE_URL_1= ApiClient.BASE_URL+"product_details/product_view/";

    public static Retrofit retrofit1 = null;

    public static Retrofit getClient123() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if (retrofit1==null) {
            retrofit1 = new Retrofit.Builder()
                    .baseUrl(BASE_URL_1)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build();
        }
        return retrofit1;
    }

    public static Retrofit getRetrofitClient(Context context) {
        if (retrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}