package creo.com.haahooapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.Adapters.DirectEarningAdapter;
import creo.com.haahooapp.Modal.DirectOrderPojo;
import creo.com.haahooapp.config.ApiClient;
import static android.content.Context.MODE_PRIVATE;

public class DirectSellEarning extends Fragment {

    RecyclerView recyclerView;
    String token = null;
    ArrayList<String>name = new ArrayList<>();
    ArrayList<String>earnings = new ArrayList<>();
    ArrayList<String>date = new ArrayList<>();
    ArrayList<DirectOrderPojo> dataModelArrayList;
    private DirectEarningAdapter orderAdapter;

    public DirectSellEarning() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_direct_sell_earning,container,false);
        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        //Toast.makeText(getContext(),"not loading",Toast.LENGTH_SHORT).show();
        token = (pref.getString("token", ""));
        fetchingJSON();
        //Toast.makeText(getContext(),"direct",Toast.LENGTH_SHORT).show();
        recyclerView = view.findViewById(R.id.listview);

//         names = view.findViewById(R.id.name);
//         price = view.findViewById(R.id.price);


        // Inflate the layout for this fragment
        return view;
    }

    private void fetchingJSON() {

        RequestQueue queue = Volley.newRequestQueue(getContext());

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"direct_selling/direct_order_view/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("ssssd", "resp" + obj);
                           // amount.setText(obj.optString("total"));
                            dataModelArrayList = new ArrayList<>();
                            JSONArray dataArray  = obj.getJSONArray("data");

                            if(dataArray.length() == 0){
                                Toast.makeText(getContext(),"Nothing to display",Toast.LENGTH_SHORT).show();
                            }

                            for (int i = 0; i < dataArray.length(); i++) {

                                DirectOrderPojo playerModel = new DirectOrderPojo();
                                JSONObject dataobj = dataArray.getJSONObject(i);
                                String images1 = dataobj.getString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "");
                                playerModel.setProductname(dataobj.optString("name"));
                                ApiClient.productids.add(dataobj.optString("id"));
                                playerModel.setName(dataobj.optString("customer_name"));
                                playerModel.setPrice(dataobj.optString("earnings"));
                                playerModel.setStatus("");
                                playerModel.setImg(ApiClient.BASE_URL+"media/" + split);

                                dataModelArrayList.add(playerModel);

                            }

                            setupRecycler();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               //


            }
        }) {

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    private void setupRecycler(){

        orderAdapter = new DirectEarningAdapter(getContext(),dataModelArrayList);
        recyclerView.setAdapter(orderAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

    }

}
