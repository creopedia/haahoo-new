package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahoo.CombinedCartActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.ShopsAdapter;
import creo.com.haahooapp.Adapters.SliderAdapterExample;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ItemClickSupport;
import creo.com.haahooapp.config.PermissionUtils;

public class CategoryShop extends AppCompatActivity implements LocationListener {

    Activity activity = this;
    Context context = this;
    ImageView back;
    LocationManager locationManager;
    Location loc;
    public LocationManager mLocManager;
    Geocoder geocoder;
    int TAG_CODE_PERMISSION_LOCATION;
    final String TAG = "GPS";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    boolean isGPS = false;
    boolean isNetwork = false;
    String categoryid = "null";
    String latitude = "null";
    String longitude = "null";
    boolean canGetLocation = true;
    public String token = "null";
    String membership;
    RelativeLayout shownearest;
    TextView changebtn, selectedcat,count;
    RecyclerView gridView;
    private FusedLocationProviderClient mFusedLocationClient;
    TextView loadmore;
    ArrayList<Category> categories = new ArrayList<>();
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    String nextUrl = ApiClient.BASE_URL + "api_shop_app/shop_nearby/";
    String shopurl = ApiClient.BASE_URL + "api_shop_app/list_shop/";
    TextView loadmoreshop;
    ImageView cart;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    /**
     * An object representing the current location
     */
    private Location mCurrentLocation;

    private String provider;
    ProgressDialog progressDialog;
    ProgressDialog progressDialog1;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_product);
        loadmore = findViewById(R.id.loadmore);
        loadmoreshop = findViewById(R.id.loadmoreshop);
        provider = LocationManager.NETWORK_PROVIDER;
        progressDialog1 = new ProgressDialog(this);
        progressDialog1.setTitle("Please wait..");
        progressDialog1.setIcon(R.drawable.haahoologo);
        progressDialog1.setMessage("Fetching your location");
        progressDialog = new ProgressDialog(this);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmer();
        progressDialog.setTitle("Please wait..");
        count = findViewById(R.id.count);
        count.setText(String.valueOf(ApiClient.count));
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Fetching nearby shops");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            checkMyPermissionLocation();
        } else {
            initGoogleMapLocation();
        }
        shownearest = findViewById(R.id.shownearest);
        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        SliderView sliderView = findViewById(R.id.imageSlider);
        SliderAdapterExample adapter = new SliderAdapterExample(this);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.BLACK);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(10); //set scroll delay in seconds :
        sliderView.startAutoCycle();
        shownearest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                permissionsToRequest = findUnAskedPermissions(permissions);
//                if (!isGPS && !isNetwork) {
//                    Log.d(TAG, "Connection off");
//                    showSettingsAlert();
//                    getLastLocation();
//                } else {
//                    Log.d(TAG, "Connection on");
//                    // check permissions
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        if (permissionsToRequest.size() > 0) {
//                            requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
//                                    ALL_PERMISSIONS_RESULT);
//                            Log.d(TAG, "Permission requests");
//                            canGetLocation = false;
//                        }
//                    }
//                    // get location
//                    getLocation();
//                }
            }
        });
        cart = findViewById(R.id.cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CategoryShop.this, CombinedCartActivity.class);
                startActivity(intent);
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(context, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
        Window window = activity.getWindow();
        selectedcat = findViewById(R.id.selectedcat);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        changebtn = findViewById(R.id.changebtn);
        Bundle bundle = getIntent().getExtras();
        categoryid = bundle.getString("categoryid");
        ApiClient.cate_id = categoryid;
        String categoryname = bundle.getString("categoryname");
        selectedcat.setText(categoryname);
        getShops(categoryid, shopurl);
        changebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CategoryShop.super.onBackPressed();
                Intent gotoScreenVar = new Intent(context, AllCategories.class);

                gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                startActivity(gotoScreenVar);
            }
        });
        gridView = findViewById(R.id.gridview);
        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent gotoScreenVar = new Intent(context, AllCategories.class);
//
//                gotoScreenVar.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//
//                startActivity(gotoScreenVar);
                CategoryShop.super.onBackPressed();
            }
        });
    }

    private void checkMyPermissionLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //Permission Check
            PermissionUtils.requestPermission(this);
            initGoogleMapLocation();
        } else {
            //If you're authorized, start setting your location
            initGoogleMapLocation();
        }
    }

    private void initGoogleMapLocation() {
        progressDialog1.show();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        /**
         * Location Setting API to
         */
        SettingsClient mSettingsClient = LocationServices.getSettingsClient(this);
        /*
         * Callback returning location result
         */
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult result) {
                super.onLocationResult(result);
                //mCurrentLocation = locationResult.getLastLocation();
                mCurrentLocation = result.getLocations().get(0);


                if (mCurrentLocation != null) {
                    progressDialog1.dismiss();
                    Log.e("Location(Lat)==", "" + mCurrentLocation.getLatitude());
                    Log.e("Location(Long)==", "" + mCurrentLocation.getLongitude());
                    latitude = String.valueOf(mCurrentLocation.getLatitude());
                    longitude = String.valueOf(mCurrentLocation.getLongitude());
//                    lat = String.valueOf(mCurrentLocation.getLatitude());
//                    log = String.valueOf(mCurrentLocation.getLongitude());
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);

                }


                /**
                 * To get location information consistently
                 * mLocationRequest.setNumUpdates(1) Commented out
                 * Uncomment the code below
                 */
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            }

            //Locatio nMeaning that all relevant information is available
            @Override
            public void onLocationAvailability(LocationAvailability availability) {
                //boolean isLocation = availability.isLocationAvailable();
            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        //To get location information only once here
        mLocationRequest.setNumUpdates(3);
        if (provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER)) {
            //Accuracy is a top priority regardless of battery consumption
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        } else {
            //Acquired location information based on balance of battery and accuracy (somewhat higher accuracy)
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        /**
         * Stores the type of location service the client wants to use. Also used for positioning.
         */
        LocationSettingsRequest mLocationSettingsRequest = builder.build();

        Task<LocationSettingsResponse> locationResponse = mSettingsClient.checkLocationSettings(mLocationSettingsRequest);
        locationResponse.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                Log.e("Response", "Successful acquisition of location information!!");
                //
                if (ActivityCompat.checkSelfPermission(CategoryShop.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            }
        });
        //When the location information is not set and acquired, callback
        locationResponse.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.e("onFailure", "Location environment check");
                        createLocationRequest();
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        String errorMessage = "Check location setting";
                        Log.e("onFailure", errorMessage);
                }
            }
        });
    }

    private void initGoogleMapLocation1() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        /**
         * Location Setting API to
         */
        SettingsClient mSettingsClient = LocationServices.getSettingsClient(this);
        /*
         * Callback returning location result
         */
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult result) {
                super.onLocationResult(result);
                //mCurrentLocation = locationResult.getLastLocation();
                mCurrentLocation = result.getLocations().get(0);


                if (mCurrentLocation != null) {
                    Log.e("Location(Lat)==", "" + mCurrentLocation.getLatitude());
                    Log.e("Location(Long)==", "" + mCurrentLocation.getLongitude());
                    latitude = String.valueOf(mCurrentLocation.getLatitude());
                    longitude = String.valueOf(mCurrentLocation.getLongitude());
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                    categories.clear();
                    progressDialog.show();
                    progressDialog1.dismiss();

//                    lat = String.valueOf(mCurrentLocation.getLatitude());
//                    log = String.valueOf(mCurrentLocation.getLongitude());
                }

                mFusedLocationClient.removeLocationUpdates(mLocationCallback);

                //mLocationRequest.setNumUpdates(0);
                /**
                 * To get location information consistently
                 * mLocationRequest.setNumUpdates(1) Commented out
                 * Uncomment the code below
                 */

            }

            //Locatio nMeaning that all relevant information is available
            @Override
            public void onLocationAvailability(LocationAvailability availability) {
                //boolean isLocation = availability.isLocationAvailable();
            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        //To get location information only once here
        mLocationRequest.setNumUpdates(3);
        if (provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER)) {
            //Accuracy is a top priority regardless of battery consumption
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        } else {
            //Acquired location information based on balance of battery and accuracy (somewhat higher accuracy)
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        /**
         * Stores the type of location service the client wants to use. Also used for positioning.
         */
        LocationSettingsRequest mLocationSettingsRequest = builder.build();

        Task<LocationSettingsResponse> locationResponse = mSettingsClient.checkLocationSettings(mLocationSettingsRequest);
        locationResponse.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                Log.e("Response", "Successful acquisition of location information!!");
                //
                if (ActivityCompat.checkSelfPermission(CategoryShop.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            }
        });
        //When the location information is not set and acquired, callback
        locationResponse.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.e("onFailure", "Location environment check");
                        createLocationRequest();
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        String errorMessage = "Check location setting";
                        Log.e("onFailure", errorMessage);
                }
            }
        });
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    protected void createLocationRequest() {
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // Toast.makeText(SignUp.this, "addOnSuccessListener", Toast.LENGTH_SHORT).show();
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
                checkMyPermissionLocation();
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Toast.makeText(SignUp.this, "addOnFailureListener", Toast.LENGTH_SHORT).show();
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(CategoryShop.this,
                                0);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("RESSSCODE", "CJGHJGD" + resultCode);
        if (resultCode == -1) {
            checkMyPermissionLocation();
        }
        if (resultCode == 0) {
            createLocationRequest();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access."
                                    ,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(CategoryShop.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged" + location.getLatitude());
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String s) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            if (provider != null) {
                Location location = locationManager.getLastKnownLocation(provider);
                Log.d(TAG, provider);
                Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else {
                    loc.setLatitude(0);
                    loc.setLongitude(0);
                    updateUI(loc);
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void updateUI(Location loc) {

        latitude = Double.toString(loc.getLatitude());
        longitude = Double.toString(loc.getLongitude());
        categories.clear();
        //getNearbyShops(latitude,longitude,nextUrl);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void getNearbyShops(String latitude, String longitude, String url1) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            progressDialog.dismiss();
                            JSONObject obj = new JSONObject(response);
                            Log.d("nearbyapi", "near" + response);
                            nextUrl = obj.optString("next");
                            loadmore.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //  Toast.makeText(context,"nexturl"+nextUrl,Toast.LENGTH_SHORT).show();
                                    if (nextUrl.equals("null")) {
                                        Toast.makeText(context, "That's all for now", Toast.LENGTH_SHORT).show();
                                    }

                                    if (!(nextUrl.equals("null"))) {

                                        getNearbyShops(latitude, longitude, nextUrl);

                                    }

                                }
                            });
                            JSONArray jsonArray = obj.optJSONArray("results");
                            if (jsonArray.length() == 0) {
                                loadmore.setVisibility(View.GONE);
                                Toast.makeText(context, "Sorry no shops available", Toast.LENGTH_SHORT).show();
                            }
                            Category cat = new Category();
                            //cat.setImage("https://cdn0.iconfinder.com/data/icons/map-3/1024/location-512.png");
                            //cat.setName("Nearby Shops");
                            //  categories.add(cat);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                Category category = new Category();
                                category.setId(jsonObject.optString("id"));
                                category.setName(jsonObject.optString("name"));
                                String images1 = jsonObject.optString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "").replace("]", "");
                                category.setImage(ApiClient.BASE_URL + split);
                                categories.add(category);
                                // Log.d("response2wa", "resp" + ApiClient.BASE_URL+split);
                                //Log.d("imageandname","jhgfd"+jsonObject.optString("image"));
                            }
                            //location icon url
                            //https://cdn0.iconfinder.com/data/icons/map-3/1024/location-512.png

                            gridView.setLayoutManager(new GridLayoutManager(context, 3));
                            gridView.setNestedScrollingEnabled(false);
                            //gridView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                            gridView.setAdapter(new ShopsAdapter(context, categories));
                            loadmore.setVisibility(View.VISIBLE);
                            loadmoreshop.setVisibility(View.GONE);

                            ItemClickSupport.addTo(gridView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                                @Override
                                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                    Intent intent = new Intent(context, ProductsInShop.class);
                                    intent.putExtra("categoryid", categories.get(position).getId());
                                    intent.putExtra("categoryname", categories.get(position).getName());
                                    startActivity(intent);
                                }
                            });
//                            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                                public void onItemClick(AdapterView<?> parent, View v,
//                                                        int position, long id) {
//
//                                        Intent intent = new Intent(context, ProductsInShop.class);
//                                        intent.putExtra("categoryid", categories.get(position).getId());
//                                        intent.putExtra("categoryname", categories.get(position).getName());
//                                        startActivity(intent);
//
//                                }
//                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Toast.makeText(context,"//Error",Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cat_id", categoryid);
                params.put("lat", latitude);
                params.put("log", longitude);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                0,  // maxNumRetries = 0 means no retry
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void getShops(String category, String shopurl1) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        membership = (pref.getString("membership", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request
//        String url = ApiClient.BASE_URL+"api_shop_app/list_shop/";
//
//        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, shopurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            shopurl = obj.optString("next");
                            Log.d("tytctctc","sdfgh"+shopurl);
                            if (shopurl.equals("null")){
                                loadmoreshop.setVisibility(View.GONE);
                            }
                            if (!(shopurl.equals("null"))){
                                loadmoreshop.setVisibility(View.VISIBLE);
                            }
                            loadmoreshop.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //  Toast.makeText(context,"nexturl"+nextUrl,Toast.LENGTH_SHORT).show();
                                    if (shopurl.equals("null")) {
                                        Toast.makeText(context, "That's all for now", Toast.LENGTH_SHORT).show();
                                    }

                                    if (!(shopurl.equals("null"))) {

                                        //getNearbyShops(latitude,longitude,nextUrl);
                                        getShops(category, shopurl);
                                    }
                                }
                            });
                            JSONArray jsonArray = obj.optJSONArray("results");
                            Log.d("hjgdsfh", "hfgjh" + obj);
//                            Category cat = new Category();
//                            cat.setImage("https://cdn0.iconfinder.com/data/icons/map-3/1024/location-512.png");
//                            cat.setName("Nearby Shops");
//                            categories.add(0,cat);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                Category category = new Category();
                                category.setId(jsonObject.optString("id"));
                                category.setName(jsonObject.optString("name"));
                                String images1 = jsonObject.optString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "").replace("]", "");
                                category.setImage(ApiClient.BASE_URL + split);
                                categories.add(category);
                                Log.d("responsewa", "resp" + categories.get(i).getName());
                                //Log.d("imageandname","jhgfd"+jsonObject.optString("image"));
                            }

                            //location icon url
                            //https://cdn0.iconfinder.com/data/icons/map-3/1024/location-512.png
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            gridView.setVisibility(View.VISIBLE);
                            gridView.setLayoutManager(new GridLayoutManager(context, 3));
                            gridView.setNestedScrollingEnabled(false);
                            //gridView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                            gridView.setAdapter(new ShopsAdapter(context, categories));

                            ItemClickSupport.addTo(gridView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                                @Override
                                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                    if (position == 0) {

                                        categories.clear();
                                        checkMyPermissionLocation();
                                        initGoogleMapLocation();
                                        if ((latitude.equals("null")) || (longitude.equals("null"))) {
                                            Toast.makeText(context, "Unable to detect location", Toast.LENGTH_SHORT).show();
                                        }
                                        if (!((latitude.equals("null") && (longitude.equals("null"))))) {
                                            getNearbyShops(latitude, longitude, nextUrl);
                                        }
                                        //
                                    }

                                    if (position == 1) {
                                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                        String membershipval = (pref.getString("membership", "null"));
                                        if (membershipval.equals("1")) {
                                            Intent intent = new Intent(context, RegisterShop.class);
                                            intent.putExtra("fromactivity", "shop");
                                            startActivity(intent);
                                        }
                                        if (!(membershipval.equals("1"))) {
                                            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                                    .setTitle("Become Our Premium Member")
                                                    .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                                    .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                        startActivity(new Intent(context, MemberShipActivity.class));
                                                    }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                        dialog.dismiss();
                                                    });
// Show the alert
                                            builder.show();
                                        }
                                    }
//
                                    if (position != 0 && position != 1) {
                                        Intent intent = new Intent(context, ProductsInShop.class);
                                        intent.putExtra("categoryid", categories.get(position).getId());
                                        intent.putExtra("categoryname", categories.get(position).getName());
                                        startActivity(intent);
                                    }

                                }

                            });


//                            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                                public void onItemClick(AdapterView<?> parent, View v,
//                                                        int position, long id) {
//                                    if (position == 0){
//                                        permissionsToRequest = findUnAskedPermissions(permissions);
//                                        if (!isGPS && !isNetwork) {
//                                            Log.d(TAG, "Connection off");
//                                            showSettingsAlert();
//                                            getLastLocation();
//                                        } else {
//                                            Log.d(TAG, "Connection on");
//                                            // check permissions
//                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                                if (permissionsToRequest.size() > 0) {
//                                                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
//                                                            ALL_PERMISSIONS_RESULT);
//                                                    Log.d(TAG, "Permission requests");
//                                                    canGetLocation = false;
//                                                }
//                                            }
//                                            // get location
//                                            getLocation();
//                                        }
//
//                                        if (!(latitude.equals("null"))){
//                                            Log.d("latlong","near"+latitude+longitude);
//                                            categories.clear();
//                                            getNearbyShops(latitude,longitude,nextUrl);
//                                            //Toast.makeText(context,"hvgh"+latitude,Toast.LENGTH_SHORT).show();
//                                        }
//                                    }
//
//                                    if (position != 0) {
//                                        Intent intent = new Intent(context, ProductsInShop.class);
//                                        intent.putExtra("categoryid", categories.get(position).getId());
//                                        intent.putExtra("categoryname", categories.get(position).getName());
//                                        startActivity(intent);
//                                    }
//
//                                }
//                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context,"//Error",Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", category);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

}
