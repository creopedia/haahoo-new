package creo.com.haahooapp.Implementation;

import creo.com.haahooapp.interfaces.VerifyOTPCallback;
import creo.com.haahooapp.interfaces.VerifyOTPModel;
import creo.com.haahooapp.interfaces.VerifyOTPPresenter;
import creo.com.haahooapp.interfaces.VerifyOTPView;

public class VerifyOTPPresenterImpl implements VerifyOTPPresenter,VerifyOTPCallback{

    VerifyOTPModel verifyOTPModel;
    VerifyOTPView verifyOTPView;

    public VerifyOTPPresenterImpl(VerifyOTPView verifyOTPView1) {
        this.verifyOTPView = verifyOTPView1;
        verifyOTPModel = new VerifyOTPModelImpl();

    }

    @Override
    public void onVerifySuccess(String data) {
        verifyOTPView.onSuccess(data);
    }

    @Override
    public void onVerifyFailed(String msg) {
        verifyOTPView.onFailed(msg);
    }

    @Override
    public void verifyOTP(String otp , String number) {

        verifyOTPModel.submitOTP(otp,number,this);

    }
}
