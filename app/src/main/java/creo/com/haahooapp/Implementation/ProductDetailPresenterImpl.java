package creo.com.haahooapp.Implementation;

import org.json.JSONArray;
import org.json.JSONException;
import creo.com.haahooapp.interfaces.ProductDetailCallback;
import creo.com.haahooapp.interfaces.ProductDetailModel;
import creo.com.haahooapp.interfaces.ProductDetailPresenter;
import creo.com.haahooapp.interfaces.ProductDetailView;

public class ProductDetailPresenterImpl implements ProductDetailPresenter,ProductDetailCallback {

  ProductDetailModel productDetailModel;
  ProductDetailView productDetailView;

  public ProductDetailPresenterImpl(ProductDetailView productDetailView) {
    this.productDetailView = productDetailView;
    productDetailModel = new ProductDetailModelImpl();

  }

  @Override
  public void getProduct(String token , String id) {
    productDetailModel.getProductDetail(token,id,this);
  }

  @Override
  public void success123(JSONArray jsonArray) throws JSONException {
    productDetailView.success(jsonArray);
  }

  @Override
  public void failed(String data) {
    productDetailView.failure(data);
  }
}
