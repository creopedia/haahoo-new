package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.Modal.WishListId;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.WishListCallBack;
import creo.com.haahooapp.interfaces.WishListModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishListModelImpl implements WishListModel {

  ApiInterface apiInterface;
  Call<Parser> callregister;

  @Override
  public void addToWishlist(String id, String token, final WishListCallBack wishListCallBack) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    WishListId productId = new WishListId(id);
    productId.setVirtual("0");
    callregister = apiInterface.addToWishList(productId,"Token "+token);
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser parser = response.body();
        if(parser == null){
          wishListCallBack.onVerifySuccessWish("Server Error");
        }
        else{
          wishListCallBack.onVerifySuccessWish("Added to Wishlist");
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        wishListCallBack.onVerifyFailedWish("failed");
      }
    });

  }

  @Override
  public void removeFromWishList(String id, String token, final WishListCallBack wishListCallBack) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    WishListId productId = new WishListId(id);
    callregister = apiInterface.removeFromWishList(productId,"Token "+token);
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser parser = response.body();
        if(parser == null){
          wishListCallBack.onVerifySuccessWish("Server error");
        }
        else{
          wishListCallBack.onVerifySuccessWishRemove("Removed From WishList");
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {

      }
    });
  }

  @Override
  public void view_wishlist(String token, final WishListCallBack wishListCallBack) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    callregister = apiInterface.viewWishlist("Token "+token);

    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {

        Parser parser = response.body();
        if(response.body() == null){
          Log.d("wishfailed","wishlist");
          wishListCallBack.wishlistviewfailed("failed");
        }
        if(parser == null){

          wishListCallBack.wishlistviewsuccess("Server error");

        }
        else{

          wishListCallBack.wishlistviewsuccess(response.body().toString());
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        wishListCallBack.wishlistviewfailed("Failed");

      }
    });
  }
}
