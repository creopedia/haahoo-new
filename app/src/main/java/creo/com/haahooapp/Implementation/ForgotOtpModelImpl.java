package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.Modal.ForgotOTPPojo;
import creo.com.haahooapp.Modal.PasswordReset;
import creo.com.haahooapp.Modal.VerifyOtpPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.ForgotOtpCallback;
import creo.com.haahooapp.interfaces.ForgotOtpModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotOtpModelImpl implements ForgotOtpModel {

  ApiInterface apiInterface;
  Call<Parser>call;

  @Override
  public void forgotOtp(ForgotOTPPojo forgotOTPPojo, final ForgotOtpCallback forgotOtpCallback) {

    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    call = apiInterface.forget_password(forgotOTPPojo);
    call.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Log.d("otprewsp","otp"+response);
        forgotOtpCallback.onVerifySuccessOtp("success");
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        Log.d("otprewsp","failed otp");
        forgotOtpCallback.onVerifyFailedOtp("failed");
      }
    });

  }

  @Override
  public void verifyOtp(VerifyOtpPojo verifyOtpPojo,final ForgotOtpCallback forgotOtpCallback1) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    call = apiInterface.verifyOTP(verifyOtpPojo);
    call.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Log.d("otprewsp","otp"+response);
        Parser x = response.body();
        if(x == null){
          forgotOtpCallback1.onVerifyFailedOtp("");
        }
        String message = String.valueOf(x.getMessage());
        if(message.equals("failed")){
          forgotOtpCallback1.onVerifyFailedOtp("failed");
        }
        if(message.equals("verify")) {
          forgotOtpCallback1.onVerifySuccessOtp("verify");
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        Log.d("otprewsp","otp");
        forgotOtpCallback1.onVerifyFailedOtp("failed");
      }
    });
  }

  @Override
  public void resetPassword(String phone_no, String password, final ForgotOtpCallback forgotOtpCallback) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    PasswordReset passwordReset = new PasswordReset(phone_no, password);
    call = apiInterface.password_reset(passwordReset);
    call.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser parser = response.body();
        if(parser == null){
          forgotOtpCallback.onVerifyFailedOtp("Server error");
        }
        if(parser != null){
          forgotOtpCallback.onVerifySuccessOtp("success");
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        forgotOtpCallback.onVerifyFailedOtp("failed");
      }
    });
  }

  @Override
  public boolean isNetworkConnected() {
    return false;
  }
}
