package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.Modal.DevicePojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.VerifyDeviceCallback;
import creo.com.haahooapp.interfaces.VerifyDeviceModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyDeviceModelImpl extends BaseModelImpl implements VerifyDeviceModel {

    ApiInterface apiInterface;
    Call<Parser> callregister;
    @Override
    public void submitDevice(String deviceid, final VerifyDeviceCallback verifyDeviceCallback) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        DevicePojo devicePojo = new DevicePojo(deviceid);
        callregister= apiInterface.verify_device(devicePojo);
        callregister.enqueue(new Callback<Parser>() {
            @Override
            public void onResponse(Call<Parser> call, Response<Parser> response) {

//                if(response.body().getCode() == 500){
//                    verifyDeviceCallback.onVerifyFailed("Server error");
//                }
                Log.d("responsedevice", "device" + response.body());
                Parser x = response.body();

                if(x == null){
                    try {
                        verifyDeviceCallback.onVerifyFailed("Server error");
//                        if(response.errorBody().string().contains("TypeError")){
//                            verifyDeviceCallback.onVerifyFailed("Server error");
//                        }
//                        String errorMsg = response.errorBody().string();
//                        JSONObject jsonObjectError = new JSONObject(errorMsg);
//                        String code = jsonObjectError.getString("code");
//                        if(code.equals("500")) {
//
//                         Log.d("server error","err34");
//                            verifyDeviceCallback.onVerifyFailed("Server error");
//
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                else {
                    String code = String.valueOf(x.getCode());
                    Log.d("SUCCESS",code);
                    String data = String.valueOf(x.getMessage());
                    verifyDeviceCallback.onVerifySuccess(data);
                    Log.d("onresponse","verifynumbermodelimple"+data);


                }
            }

            @Override
            public void onFailure(Call<Parser> call, Throwable t) {
                verifyDeviceCallback.onVerifyFailed("Server error");
                Log.d("responsedevice","failed");
            }
        });

    }
}
