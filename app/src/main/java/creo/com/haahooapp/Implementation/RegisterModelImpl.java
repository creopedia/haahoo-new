package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.Modal.RegisterPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.RegisterCallBack;
import creo.com.haahooapp.interfaces.RegisterModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterModelImpl extends BaseModelImpl implements RegisterModel {

  ApiInterface apiInterface;
  Call<Parser> callregister;
  @Override
  public void submitDetails(RegisterPojo registerPojo, final RegisterCallBack registerCallBack) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    callregister = apiInterface.register(registerPojo);
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser x = response.body();
        Log.d("model response","modelres success"+response.body());
        if(x == null){
          registerCallBack.onVerifyFailed("failed");
        }
        if(x != null) {
          String message = String.valueOf(x.getToken());

          registerCallBack.onVerifySuccess(message);
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        Log.d("model response","modelres failed");
        registerCallBack.onVerifyFailed("failed");
      }
    });

  }
}
