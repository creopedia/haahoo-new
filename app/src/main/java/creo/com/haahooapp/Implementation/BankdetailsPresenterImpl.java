package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.Modal.BankdetailsPojo;
import creo.com.haahooapp.interfaces.BankdetailsCallback;
import creo.com.haahooapp.interfaces.BankdetailsModel;
import creo.com.haahooapp.interfaces.BankdetailsPresenter;
import creo.com.haahooapp.interfaces.BankdetailsView;

public class BankdetailsPresenterImpl implements BankdetailsPresenter, BankdetailsCallback{
  BankdetailsModel bankdetailsModel;
  BankdetailsView bankdetailsView;

  public BankdetailsPresenterImpl(BankdetailsView bankdetailsView1) {
    this.bankdetailsView = bankdetailsView1;
    bankdetailsModel= new BankdetailsModelImpl();
  }

  @Override
  public void onVerifySuccess(String data) {
    bankdetailsView.onSuccess(data);

    Log.d("success","successss");
  }

  @Override
  public void onVerifyFailed(String msg) {
    bankdetailsView.onFailed(msg);

  }


  @Override
  public void bankdetails(BankdetailsPojo bankdetailsPojo, String token) {
    bankdetailsModel.submitDetails(bankdetailsPojo,this,token);

  }
}