package creo.com.haahooapp.Implementation;

import org.json.JSONArray;
import creo.com.haahooapp.interfaces.CartCallBack;
import creo.com.haahooapp.interfaces.CartModel;
import creo.com.haahooapp.interfaces.CartPresenter;
import creo.com.haahooapp.interfaces.CartView;

public class CartPresenterImpl implements CartPresenter, CartCallBack {

  CartModel cartModel;
  CartView cartView;

  public CartPresenterImpl(CartView cartView) {
    this.cartView = cartView;
    cartModel = new CartModelImpl();
  }

  @Override
  public void addToCart(String id,String count, String token) {
    cartModel.addToCart(id,count,token,this);
  }

  @Override
  public void viewCart(String token) {

    cartModel.viewCart(token , this);

  }

  @Override
  public void successcart(String data) {
    cartView.onSuccesscart("success");

  }

  @Override
  public void failedcart(String msg) {
    cartView.onFailedcart("failed");
  }

  @Override
  public void viewsuccess(JSONArray data) {
    cartView.onsuccess(data);
  }

  @Override
  public void viewfail(String msg) {
    cartView.onFailedcart(msg);
  }
}
