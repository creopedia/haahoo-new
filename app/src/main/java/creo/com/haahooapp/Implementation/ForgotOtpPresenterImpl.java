package creo.com.haahooapp.Implementation;

import creo.com.haahooapp.Modal.ForgotOTPPojo;
import creo.com.haahooapp.Modal.VerifyOtpPojo;
import creo.com.haahooapp.interfaces.ForgotOtpCallback;
import creo.com.haahooapp.interfaces.ForgotOtpModel;
import creo.com.haahooapp.interfaces.ForgotOtpPresenter;
import creo.com.haahooapp.interfaces.ForgotOtpView;

public class ForgotOtpPresenterImpl implements ForgotOtpPresenter, ForgotOtpCallback {

  ForgotOtpModel forgotOtpModel;
  ForgotOtpView forgotOtpView;

  public ForgotOtpPresenterImpl(ForgotOtpView forgotOtpView) {
    this.forgotOtpView = forgotOtpView;
    forgotOtpModel = new ForgotOtpModelImpl();
  }

  @Override
  public void forgotOtp(String number) {
    ForgotOTPPojo forgotOTPPojo = new ForgotOTPPojo(number);
    forgotOtpModel.forgotOtp(forgotOTPPojo, this);
  }

  @Override
  public void verifyOtp(String number, String otp) {
    VerifyOtpPojo  verifyOtpPojo = new VerifyOtpPojo(otp,number);
    forgotOtpModel.verifyOtp(verifyOtpPojo,this);
  }

  @Override
  public void resetPassword(String number, String password) {
    forgotOtpModel.resetPassword(number,password,this);
  }

  @Override
  public void onVerifySuccessOtp(String data) {
    forgotOtpView.onSuccessOtp("success");
  }

  @Override
  public void onVerifyFailedOtp(String msg) {
    forgotOtpView.onFailedOtp("failed");
  }
}
