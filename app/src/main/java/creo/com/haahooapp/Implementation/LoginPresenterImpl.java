package creo.com.haahooapp.Implementation;

import creo.com.haahooapp.Modal.LoginPojo;
import creo.com.haahooapp.interfaces.LoginCallBack;
import creo.com.haahooapp.interfaces.LoginModel;
import creo.com.haahooapp.interfaces.LoginPresenter;
import creo.com.haahooapp.interfaces.LoginView;

public class LoginPresenterImpl implements LoginPresenter,LoginCallBack {

  LoginModel loginModel;
  LoginView loginView;

  public LoginPresenterImpl(LoginView loginView) {
    this.loginView = loginView;
    loginModel = new LoginModelImpl();
  }

  @Override
  public void login(String phone_no, String password, String device_id,String token) {
    LoginPojo loginPojo = new LoginPojo(phone_no,password,device_id,token);
    loginModel.login(loginPojo,this);
  }


  @Override
  public void onVerifySuccess(String data, String data1) {
    loginView.onSuccess1(data,data1);
  }

  @Override
  public void onVerifyFailed(String msg) {
    loginView.onFailed1(msg);
  }
}
