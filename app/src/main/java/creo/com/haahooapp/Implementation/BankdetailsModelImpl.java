package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.Modal.BankdetailsPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.BankdetailsCallback;
import creo.com.haahooapp.interfaces.BankdetailsModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankdetailsModelImpl implements BankdetailsModel {
    ApiInterface apiInterface;
    Call<Parser> callbankdetails;
    @Override
    public void submitDetails(BankdetailsPojo bankdetailsPojo, final BankdetailsCallback bankdetailsCallback, String token) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        callbankdetails = apiInterface.bankdetails(bankdetailsPojo,token);

        callbankdetails.enqueue(new Callback<Parser>() {
            @Override
            public void onResponse(Call<Parser> call, Response<Parser> response) {
                Parser x = response.body();
                String message = String.valueOf(x.getMessage());
                bankdetailsCallback.onVerifySuccess(message);
            }

            @Override
            public void onFailure(Call<Parser> call, Throwable t) {
                bankdetailsCallback.onVerifyFailed("failed");
            }
        });

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }
}