package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.interfaces.VerifyDeviceCallback;
import creo.com.haahooapp.interfaces.VerifyDeviceModel;
import creo.com.haahooapp.interfaces.VerifyDevicePresenter;
import creo.com.haahooapp.interfaces.VerifyDeviceView;

public class VerifyDevicePresenterImpl implements VerifyDevicePresenter,VerifyDeviceCallback{

  VerifyDeviceModel verifyDeviceModel;
  VerifyDeviceView verifyDeviceView;

  public VerifyDevicePresenterImpl(VerifyDeviceView verifyDeviceView) {
    this.verifyDeviceView = verifyDeviceView;
    verifyDeviceModel = new VerifyDeviceModelImpl();
  }

  @Override
  public void verifyDevice(String deviceId) {
    verifyDeviceModel.submitDevice(deviceId,this);
  }

  @Override
  public void onVerifySuccess(String data) {

    Log.d("success","success24"+data);
    verifyDeviceView.onSuccess(data);
  }

  @Override
  public void onVerifyFailed(String msg) {
    verifyDeviceView.onFailed(msg);
  }
}
