package creo.com.haahooapp.Implementation;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import creo.com.haahooapp.Modal.CartId;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.CartCallBack;
import creo.com.haahooapp.interfaces.CartModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartModelImpl implements CartModel {

  ApiInterface apiInterface;
  Call<Parser> call ;

  @Override
  public void addToCart(String id,String count, String token, final CartCallBack cartCallBack) {

    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    CartId cartId = new CartId(id,count,"0");
    call = apiInterface.addToCart(cartId,"Token "+token);
    call.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser parser = response.body();
        if(parser == null){
          cartCallBack.successcart("Server Error");
        }
        else{
          cartCallBack.successcart("success");
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        cartCallBack.failedcart("failed");
      }
    });

  }

  @Override
  public void viewCart(String token, final CartCallBack cartCallBack) {

    apiInterface = ApiClient.getClient().create(ApiInterface.class);

    call = apiInterface.viewCart("Token "+token);
    call.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser parser =response.body();

        try {
          JSONArray jsonObject = new JSONArray(parser.getData());
          Log.d("jsonarrayobjk","json"+jsonObject);
          cartCallBack.viewsuccess(jsonObject);
        }catch (JSONException e){
          e.printStackTrace();
        }

      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        Log.d("cartview","viewcarfailed");
        cartCallBack.viewfail("failed");
      }

    });

  }
}
