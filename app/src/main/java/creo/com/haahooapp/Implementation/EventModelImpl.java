package creo.com.haahooapp.Implementation;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import creo.com.haahooapp.Modal.ProductId;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.EventCallBack;
import creo.com.haahooapp.interfaces.EventModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventModelImpl implements EventModel {


  ApiInterface apiInterface;
  Call<Parser> call;
  @Override
  public void getEvents(final EventCallBack eventCallBack) {

    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    call = apiInterface.events();
    call.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser parser = response.body();
        if(parser == null){
          eventCallBack.failed("Server Error , Please try again after sometime");
          Log.d("server failed","failed");
        }
        else{

          try {
            Log.d("responsebbb","resp"+response.body().getResults().toString());



            String data = response.body().getResults().toString();

            JSONArray jsonArray = new JSONArray(data);

            eventCallBack.success(jsonArray);
//                        JSONArray jsonObject = new JSONArray(response.body().getData().getAsString());
//                        eventCallBack.success(jsonObject);
          }catch (Exception e){
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {

      }
    });


  }

  @Override
  public void getEventDetails(String id,final EventCallBack eventCallBack) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    ProductId productId = new ProductId(id);
    call = apiInterface.getEventDetail(productId);
    call.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser parser = response.body();
        if(parser == null){
          eventCallBack.failed("Server Error , Please try again after sometime");

        }
        else{
          try{
            String data = response.body().getData().toString();
            JSONArray jsonArray = new JSONArray(data);
            eventCallBack.success(jsonArray);
          }catch (JSONException e){
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        eventCallBack.failed("Server error");
        Log.d("server failed","failed");
      }
    });
  }

  @Override
  public boolean isNetworkConnected() {
    return false;
  }
}
