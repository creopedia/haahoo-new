package creo.com.haahooapp.Implementation;

import android.util.Log;
import org.json.JSONArray;
import creo.com.haahooapp.Modal.AddaddressPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.AddaddressCallback;
import creo.com.haahooapp.interfaces.AddaddressModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddaddressModelImpl  implements AddaddressModel {

    ApiInterface apiInterface;
    Call<Parser> calladdaddress;
    @Override
    public void submitDetails(AddaddressPojo addaddressPojo, final AddaddressCallback addaddressCallback, String token) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        calladdaddress = apiInterface.add_address(addaddressPojo,token);

        calladdaddress.enqueue(new Callback<Parser>() {
            @Override
            public void onResponse(Call<Parser> call, Response<Parser> response) {
                Parser x = response.body();

                String message = String.valueOf(x.getMessage());
                addaddressCallback.onVerifySuccess(message);
            }

            @Override
            public void onFailure(Call<Parser> call, Throwable t) {
                addaddressCallback.onVerifyFailed("failed");
            }
        });

    }

    @Override
    public void view_address(final AddaddressCallback addaddressCallback, String token) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        calladdaddress = apiInterface.view_address("Token "+token);
        calladdaddress.enqueue(new Callback<Parser>() {
            @Override
            public void onResponse(Call<Parser> call, Response<Parser> response) {

                try {
                    JSONArray jsonArray = new JSONArray(response.body().getData());
                    addaddressCallback.viewAddress(jsonArray);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Parser> call, Throwable t) {
                Log.d("addressview","failed");
            }
        });
    }


    @Override
    public boolean isNetworkConnected() {
        return false;
    }


}