package creo.com.haahooapp.Implementation;

import creo.com.haahooapp.interfaces.PasswordCallBack;
import creo.com.haahooapp.interfaces.PasswordModel;
import creo.com.haahooapp.interfaces.PasswordPresenter;
import creo.com.haahooapp.interfaces.PasswordView;

public class PasswordPresenterImpl implements PasswordPresenter,PasswordCallBack {

  PasswordView passwordView;
  PasswordModel passwordModel;

  public PasswordPresenterImpl(PasswordView passwordView) {
    this.passwordView = passwordView;
    passwordModel = new PasswordModelImpl();
  }

  @Override
  public void change_password(String current_password, String new_password, String token) {
    passwordModel.change_password(current_password,new_password ,token,this);
  }

  @Override
  public void onVerifySuccesschangepass(String data) {
    passwordView.onSuccess(data);
  }

  @Override
  public void onVerifyFailedchangepass(String msg) {
    passwordView.onFailed(msg);
  }
}
