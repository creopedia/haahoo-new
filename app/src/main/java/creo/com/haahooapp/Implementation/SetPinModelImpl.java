package creo.com.haahooapp.Implementation;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import creo.com.haahooapp.Modal.SetPinPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.SetPinCallBack;
import creo.com.haahooapp.interfaces.SetPinModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetPinModelImpl implements SetPinModel {

  ApiInterface apiInterface;
  Call<Parser> callregister;

  @Override
  public void setPin(String device_id, String pin, String token, final SetPinCallBack setPinCallBack) {

    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    SetPinPojo setPinPojo = new SetPinPojo(device_id,pin);
    callregister = apiInterface.set_pin(setPinPojo,"Token "+""+token);
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser x = response.body();
        if(x == null){
          try {
            String errorMsg = response.errorBody().string();
            JSONObject jsonObjectError = new JSONObject(errorMsg);
            String code = jsonObjectError.getString("code");
            if(code.equals("500"))
              setPinCallBack.onVerifyFailed("Server error");
          } catch (IOException e) {
            e.printStackTrace();
          } catch (JSONException e) {
            e.printStackTrace();
          }
        }

        else {
          String code = String.valueOf(x.getCode());
          Log.d("SUCCESS",code);


          String data = String.valueOf(x.getMessage());
          setPinCallBack.onVerifySuccess(data);
          Log.d("onresponse","verifynumbermodelimple"+data);


        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        setPinCallBack.onVerifyFailed("failed");
        Log.d("pin setting success","failed");
      }
    });

  }
}
