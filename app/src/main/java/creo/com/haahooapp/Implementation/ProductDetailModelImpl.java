package creo.com.haahooapp.Implementation;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import creo.com.haahooapp.Modal.ProductId;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.ProductDetailCallback;
import creo.com.haahooapp.interfaces.ProductDetailModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailModelImpl implements ProductDetailModel {

  ApiInterface apiInterface;
  Call<Parser> call;

  @Override
  public void getProductDetail(String token , String id, final ProductDetailCallback productDetailCallback) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    ProductId productId = new ProductId(id);
    call = apiInterface.getProductDetail("Token "+token,productId);
    call.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {

        Parser parser = response.body();
        if(parser == null){
          productDetailCallback.failed("server error");
        }
        else{
          try {
            Log.d("json111","array"+response.body().getData().toString());
            JSONArray jsonArray = new JSONArray(response.body().getData().toString());
            productDetailCallback.success123(jsonArray);
          }catch (JSONException e){
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        productDetailCallback.failed("Server error");
        Log.d("faile","fail");
      }
    });

  }
}
