package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.Modal.LoginPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.LoginCallBack;
import creo.com.haahooapp.interfaces.LoginModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginModelImpl implements LoginModel {
  ApiInterface apiInterface;
  Call<Parser> callregister;

  @Override
  public void login(LoginPojo loginPojo, final LoginCallBack loginCallBack) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    callregister = apiInterface.login(loginPojo);
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser x = response.body();
        if(x == null){
          try {
            loginCallBack.onVerifyFailed("");
//                        if(response.errorBody().string().contains("TypeError")){
//                            verifyDeviceCallback.onVerifyFailed("Server error");
//                        }
//                        String errorMsg = response.errorBody().string();
//                        JSONObject jsonObjectError = new JSONObject(errorMsg);
//                        String code = jsonObjectError.getString("code");
//                        if(code.equals("500")) {
//
//                         Log.d("server error","err34");
//                            verifyDeviceCallback.onVerifyFailed("Server error");
//
//                        }
          } catch (Exception e) {
            e.printStackTrace();
          }
        }

        else {
          String code = String.valueOf(x.getCode());
          Log.d("SUCCESS",code);

          String message = String.valueOf(x.getMessage());
          if(message.equals("success")) {
            String token = String.valueOf(x.getToken1());
            String membership = String.valueOf(x.getMembership());
            loginCallBack.onVerifySuccess(token,membership);
//                    Log.d("onresponse","verifynumbermodelimple"+data);
          }
          if(message.equals("Failed")){
            loginCallBack.onVerifySuccess("failed","failed");
          }
          if(code.equals("203")){
            loginCallBack.onVerifySuccess("failed","failed");
          }

        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        loginCallBack.onVerifyFailed("failed");
      }
    });
  }

  @Override
  public boolean isNetworkConnected() {
    return false;
  }
}
