package creo.com.haahooapp.Implementation;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import creo.com.haahooapp.config.AppController;
import creo.com.haahooapp.interfaces.BaseModel;

public class BaseModelImpl implements BaseModel {


    @Override
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) AppController.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        return netInfo != null
                && netInfo.isConnectedOrConnecting();
    }
}
