package creo.com.haahooapp.Implementation;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import creo.com.haahooapp.Modal.VerifyModal;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.VerifyNumberCallBack;
import creo.com.haahooapp.interfaces.VerifyNumberModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyNumberModelImpl extends  BaseModelImpl implements VerifyNumberModel {

  ApiInterface apiInterface;
  Call<Parser> callregister;
  @Override
  public void submitNumber(String number, final VerifyNumberCallBack verifyNumberCallBack) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    Log.d("onresponse",number);
    VerifyModal verifyModal = new VerifyModal(number);
    callregister = apiInterface.verifyNumber(verifyModal);
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser x = response.body();
        if(x == null){
          try {
            String errorMsg = response.errorBody().string();
            JSONObject jsonObjectError = new JSONObject(errorMsg);
            String code = jsonObjectError.getString("code");
            if(code.equals("500"))
              verifyNumberCallBack.onVerifyFailed("Server error");
          } catch (IOException e) {
            e.printStackTrace();
          } catch (JSONException e) {
            e.printStackTrace();
          }
        }

        else {
          String code = String.valueOf(x.getCode());
          Log.d("SUCCESS",code);

          if (code.equals("200")) {
            String data = String.valueOf(x.getMessage());
            verifyNumberCallBack.onVerifySuccess(data);
            Log.d("onresponse","verifynumbermodelimple"+data);
          }

        }

      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        verifyNumberCallBack.onVerifyFailed("failed");
        Log.d("onfailure","verifynumbermodelimple");
      }
    });
  }
}
