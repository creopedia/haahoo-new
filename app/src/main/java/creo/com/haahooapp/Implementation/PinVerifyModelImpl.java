package creo.com.haahooapp.Implementation;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import creo.com.haahooapp.Modal.PinVerifyPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.PinVerifyCallback;
import creo.com.haahooapp.interfaces.PinVerifyModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PinVerifyModelImpl implements PinVerifyModel {

  ApiInterface apiInterface;
  Call<Parser> callregister;


  @Override
  public void verifyPin(PinVerifyPojo pinVerifyPojo, final PinVerifyCallback pinVerifyCallback) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    callregister = apiInterface.pin_verify(pinVerifyPojo);
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser x = response.body();
        if(x == null){
          try {
            pinVerifyCallback.onVerifyFailed("Server error");
//                        if(response.errorBody().string().contains("TypeError")){
//                            verifyDeviceCallback.onVerifyFailed("Server error");
//                        }
//                        String errorMsg = response.errorBody().string();
//                        JSONObject jsonObjectError = new JSONObject(errorMsg);
//                        String code = jsonObjectError.getString("code");
//                        if(code.equals("500")) {
//
//                         Log.d("server error","err34");
//                            verifyDeviceCallback.onVerifyFailed("Server error");
//
//                        }
          } catch (Exception e) {
            e.printStackTrace();
          }
        }

        else {
          String code = String.valueOf(x.getCode());
          Log.d("SUCCESS",code);
          String message = String.valueOf(x.getMessage());
          if(message.equals("Success")) {
            String token = String.valueOf(x.getToken());
            String mem = String.valueOf(x.getMembership());

            pinVerifyCallback.onVerifySuccess(token,mem);
//                    Log.d("onresponse","verifynumbermodelimple"+data);
          }
          if(message.equals("Failed")){
            pinVerifyCallback.onVerifySuccess("failed","0");
          }

        }

      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        Log.d("verificationsuccess", "failure");
        pinVerifyCallback.onVerifyFailed("failed");
      }
    });
  }

  @Override
  public void getProducts(String url,final PinVerifyCallback pinVerifyCallback) {
    apiInterface = ApiClient.getClient123().create(ApiInterface.class);
    callregister = apiInterface.callProducts();
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser x = response.body();
        if(x == null){
          pinVerifyCallback.onVerifyFailed("Server error");
        }
        else{

          try {
//            add below line after adding more products
//            String next = response.body().getNext().toString();
//            Log.d("nextvals","next"+next);
            JSONArray jsonObject = new JSONArray(response.body().getResults().toString());
            JSONObject jsonObject1 =  jsonObject.getJSONObject(0);
            JSONObject jsonObject2 = jsonObject1.getJSONObject("fields");
            pinVerifyCallback.jsonObjectSuccess(jsonObject,"");
          }catch (Exception e){
            e.printStackTrace();
          }

        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        pinVerifyCallback.onVerifyFailed("Server error");
      }
    });
  }

  @Override
  public boolean isNetworkConnected() {
    return false;
  }
}
