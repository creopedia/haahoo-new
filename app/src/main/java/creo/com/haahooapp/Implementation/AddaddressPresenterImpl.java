package creo.com.haahooapp.Implementation;

import android.util.Log;
import org.json.JSONArray;
import creo.com.haahooapp.Modal.AddaddressPojo;
import creo.com.haahooapp.interfaces.AddaddressCallback;
import creo.com.haahooapp.interfaces.AddaddressModel;
import creo.com.haahooapp.interfaces.AddaddressPresenter;
import creo.com.haahooapp.interfaces.AddaddressView;

public class AddaddressPresenterImpl implements AddaddressPresenter, AddaddressCallback{
  AddaddressModel addaddressModel;
  AddaddressView addaddressView;

  public AddaddressPresenterImpl(AddaddressView addaddressView1) {
    this.addaddressView = addaddressView1;
    addaddressModel= new AddaddressModelImpl();
  }

  @Override
  public void onVerifySuccess(String data) {
    addaddressView.onSuccess(data);

    Log.d("success","successss");
  }

  @Override
  public void onVerifyFailed(String msg) {
    addaddressView.onFailed(msg);

  }

  @Override
  public void viewAddress(JSONArray data) {
    addaddressView.addressview(data);
  }

  @Override
  public void viewFailed(String data) {
    addaddressView.addressfail(data);
  }


  @Override
  public void addaddress(AddaddressPojo addaddressPojo ,String token) {
    addaddressModel.submitDetails(addaddressPojo,this,token);

  }

  @Override
  public void viewAddress1(String token) {
    addaddressModel.view_address(this,token);
  }
}