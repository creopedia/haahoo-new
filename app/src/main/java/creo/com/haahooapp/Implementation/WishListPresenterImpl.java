package creo.com.haahooapp.Implementation;

import creo.com.haahooapp.interfaces.WishListCallBack;
import creo.com.haahooapp.interfaces.WishListModel;
import creo.com.haahooapp.interfaces.WishListPresenter;
import creo.com.haahooapp.interfaces.WishListView;

public class WishListPresenterImpl implements WishListPresenter,WishListCallBack {
  WishListModel wishListModel;
  WishListView wishListView;

  public WishListPresenterImpl(WishListView wishListView) {
    this.wishListView = wishListView;
    wishListModel = new WishListModelImpl();
  }

  @Override
  public void addToWishList(String id,String token) {
    wishListModel.addToWishlist(id,token,this);
  }

  @Override
  public void removeFromWishList(String id, String token) {
    wishListModel.removeFromWishList(id,token,this);
  }

  @Override
  public void viewWishList(String token) {
    wishListModel.view_wishlist(token,this);
  }

  @Override
  public void onVerifySuccessWish(String data) {
    wishListView.onSuccessWish(data);
  }

  @Override
  public void onVerifySuccessWishRemove(String data) {
    wishListView.onRemove(data);
  }

  @Override
  public void onVerifyFailedWish(String msg) {
    wishListView.onFailedWish(msg);
  }

  @Override
  public void wishlistviewsuccess(String msg) {
    wishListView.wishView(msg);
  }

  @Override
  public void wishlistviewfailed(String data) {
    wishListView.wishFailed(data);
  }
}
