package creo.com.haahooapp.Implementation;

import creo.com.haahooapp.interfaces.SetPinCallBack;
import creo.com.haahooapp.interfaces.SetPinModel;
import creo.com.haahooapp.interfaces.SetPinPresenter;
import creo.com.haahooapp.interfaces.SetPinView;

public class SetPinPresenterImpl implements SetPinPresenter,SetPinCallBack {

  SetPinModel setPinModel;
  SetPinView setPinView;

  public SetPinPresenterImpl(SetPinView setPinView) {
    this.setPinView = setPinView;
    setPinModel = new SetPinModelImpl();
  }

  @Override
  public void setPin(String device_id, String pin, String token) {
    setPinModel.setPin(device_id,pin,token,this);
  }

  @Override
  public void onVerifySuccess(String data) {
    setPinView.onSuccess(data);

  }

  @Override
  public void onVerifyFailed(String msg) {
    setPinView.onFailed(msg);
  }
}
