package creo.com.haahooapp.Implementation;

import android.util.Log;
import org.json.JSONArray;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.ProfileCallback;
import creo.com.haahooapp.interfaces.ProfileModel;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileModelImpl implements ProfileModel {

  ApiInterface apiInterface;
  retrofit2.Call<Parser> callregister;

  @Override
  public void getProfile(String token, final ProfileCallback profileCallback) {
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    callregister = apiInterface.getProfile("Token "+token);
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(retrofit2.Call<Parser> call, Response<Parser> response) {

        Parser parser = response.body();
        if(parser == null){
          profileCallback.profileFailure("Server error");
        }
        else{
          String data = String.valueOf(parser.getData());
          try {
            JSONArray jsonObject = new JSONArray(response.body().getData().getAsString());
            Log.d("jsonarray","array"+jsonObject);
            profileCallback.profileSuccess(jsonObject);
          }catch (Exception e){
            e.printStackTrace();
          }
        }

      }

      @Override
      public void onFailure(retrofit2.Call<Parser> call, Throwable t) {
        profileCallback.profileFailure("Server error");
      }
    });
  }
}
