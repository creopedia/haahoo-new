package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.interfaces.NumberVerifyPresenter;
import creo.com.haahooapp.interfaces.VerifyNumberCallBack;
import creo.com.haahooapp.interfaces.VerifyNumberModel;
import creo.com.haahooapp.interfaces.VerifyNumberView;

public class NumberVerifyPresenterImpl implements NumberVerifyPresenter , VerifyNumberCallBack{

    VerifyNumberModel verifyNumberModel;
    VerifyNumberView verifyNumberView1;
    public NumberVerifyPresenterImpl( VerifyNumberView verifyNumberView ) {
        this.verifyNumberView1 = verifyNumberView;
        verifyNumberModel = new VerifyNumberModelImpl();

    }

    @Override
    public void verifyNumber(String number) {
        Log.d("onresponse",number);
        verifyNumberModel.submitNumber(number,this);
    }

    @Override
    public void onVerifySuccess(String data) {
        verifyNumberView1.onSuccess123(data);
    }

    @Override
    public void onVerifyFailed(String msg) {
        verifyNumberView1.onFailed(msg);
    }

    @Override
    public void onfailure(String errormessage) {

    }
}
