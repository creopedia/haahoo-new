package creo.com.haahooapp.Implementation;

import creo.com.haahooapp.Modal.ChangePassword;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.PasswordCallBack;
import creo.com.haahooapp.interfaces.PasswordModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordModelImpl implements PasswordModel {

  ApiInterface apiInterface;
  Call<Parser> call;

  @Override
  public void change_password(String currentpass, String newpass, String token, final PasswordCallBack passwordCallBack) {

    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    ChangePassword changePassword = new ChangePassword(currentpass,newpass);
    call = apiInterface.change_password(changePassword,"Token "+token);
    call.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {

        Parser parser = response.body();
        if(parser == null){
          passwordCallBack.onVerifySuccesschangepass("Server error");
        }
        if(parser != null){
          if(parser.getMessage().equals("Failed")){
            passwordCallBack.onVerifySuccesschangepass("Please enter the correct password");
          }
          if(parser.getMessage().equals("Success")) {
            passwordCallBack.onVerifySuccesschangepass("Password Changed");
          }
        }

      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        passwordCallBack.onVerifyFailedchangepass("Failed");
      }
    });

  }
}
