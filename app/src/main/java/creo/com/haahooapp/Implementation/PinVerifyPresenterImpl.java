package creo.com.haahooapp.Implementation;

import org.json.JSONArray;
import creo.com.haahooapp.Modal.PinVerifyPojo;
import creo.com.haahooapp.interfaces.PinVerifyCallback;
import creo.com.haahooapp.interfaces.PinVerifyModel;
import creo.com.haahooapp.interfaces.PinVerifyPresenter;
import creo.com.haahooapp.interfaces.PinVerifyView;

public class PinVerifyPresenterImpl implements PinVerifyPresenter ,PinVerifyCallback{

  PinVerifyModel pinVerifyModel;
  PinVerifyView pinVerifyView;

  public PinVerifyPresenterImpl(PinVerifyView pinVerifyView) {
    this.pinVerifyView = pinVerifyView;
    pinVerifyModel = new PinVerifyModelImpl();
  }

  @Override
  public void verifyPin(String device_id, String pin,String fire_token) {
    PinVerifyPojo pinVerifyPojo = new PinVerifyPojo(device_id,pin,fire_token);

    pinVerifyModel.verifyPin(pinVerifyPojo,this);
  }

  @Override
  public void getProducts(String url) {
    pinVerifyModel.getProducts(url,this);
  }

  @Override
  public void onVerifySuccess(String data,String mem) {
    pinVerifyView.onSuccess(data,mem);
  }

  @Override
  public void onVerifyFailed(String msg) {
    pinVerifyView.onFailed(msg);
  }

  @Override
  public void jsonObjectSuccess(JSONArray jsonObject,String next) {
    pinVerifyView.jsonSUccess(jsonObject,next);
  }
}
