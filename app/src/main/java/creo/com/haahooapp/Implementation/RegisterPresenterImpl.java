package creo.com.haahooapp.Implementation;

import android.util.Log;
import creo.com.haahooapp.Modal.RegisterPojo;
import creo.com.haahooapp.interfaces.RegisterCallBack;
import creo.com.haahooapp.interfaces.RegisterModel;
import creo.com.haahooapp.interfaces.RegisterPresenter;
import creo.com.haahooapp.interfaces.RegisterView;

public class RegisterPresenterImpl implements RegisterPresenter , RegisterCallBack{
  RegisterModel registerModel;
  RegisterView registerView;

  public RegisterPresenterImpl(RegisterView registerView1) {
    this.registerView = registerView1;
    registerModel = new RegisterModelImpl();
  }

  @Override
  public void onVerifySuccess(String data) {
    registerView.onSuccess(data);

    Log.d("success","successss");
  }

  @Override
  public void onVerifyFailed(String msg) {
    registerView.onFailed(msg);

  }

  @Override
  public void register(RegisterPojo registerPojo) {

    registerModel.submitDetails(registerPojo,this);

  }
}
