package creo.com.haahooapp.Implementation;

import org.json.JSONArray;
import creo.com.haahooapp.interfaces.ProfileCallback;
import creo.com.haahooapp.interfaces.ProfileModel;
import creo.com.haahooapp.interfaces.ProfilePresenter;
import creo.com.haahooapp.interfaces.ProfileView;

public class ProfilePresenterImpl implements ProfilePresenter ,ProfileCallback {

  ProfileModel profileModel;
  ProfileView profileView;

  public ProfilePresenterImpl(ProfileView profileView) {
    this.profileView = profileView;
    profileModel = new ProfileModelImpl();
  }

  @Override
  public void getProfile(String token) {
    profileModel.getProfile(token,this);
  }



  @Override
  public void profileSuccess(JSONArray jsonArray) {
    profileView.onSuccess(jsonArray);
  }

  @Override
  public void profileFailure(String msg) {
    profileView.onFailed(msg);
  }
}
