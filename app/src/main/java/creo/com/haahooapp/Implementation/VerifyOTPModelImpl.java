package creo.com.haahooapp.Implementation;

import creo.com.haahooapp.Modal.VerifyOtpPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.Parser;
import creo.com.haahooapp.interfaces.VerifyOTPCallback;
import creo.com.haahooapp.interfaces.VerifyOTPModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOTPModelImpl extends BaseModelImpl implements VerifyOTPModel {


  ApiInterface apiInterface;
  Call<Parser> callregister;
  @Override
  public void submitOTP(String otp, String number, final VerifyOTPCallback verifyOTPCallback) {

    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    VerifyOtpPojo verifyOtpPojo = new VerifyOtpPojo(otp , number);
    callregister = apiInterface.verifyOTP(verifyOtpPojo);
    callregister.enqueue(new Callback<Parser>() {
      @Override
      public void onResponse(Call<Parser> call, Response<Parser> response) {
        Parser parser = response.body();
        String message = String.valueOf(parser.getMessage());
        if (message.equals("failed")){
          verifyOTPCallback.onVerifySuccess("failed");
        }
        if(!(message.equals("failed"))) {

          verifyOTPCallback.onVerifySuccess("success");
        }
      }

      @Override
      public void onFailure(Call<Parser> call, Throwable t) {
        verifyOTPCallback.onVerifyFailed("failed");
      }
    });

  }
}
