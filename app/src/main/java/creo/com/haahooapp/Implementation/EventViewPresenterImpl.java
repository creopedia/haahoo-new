package creo.com.haahooapp.Implementation;

import org.json.JSONArray;
import creo.com.haahooapp.interfaces.EventCallBack;
import creo.com.haahooapp.interfaces.EventModel;
import creo.com.haahooapp.interfaces.EventView;
import creo.com.haahooapp.interfaces.EventViewPresenter;

public class EventViewPresenterImpl implements EventViewPresenter,EventCallBack {

  EventModel eventModel;
  EventView eventView;

  public EventViewPresenterImpl(EventView eventView) {
    this.eventView = eventView;
    eventModel = new EventModelImpl();
  }

  @Override
  public void getEvents() {
    eventModel.getEvents(this);
  }

  @Override
  public void getEventDetail(String id) {
    eventModel.getEventDetails(id,this);
  }

  @Override
  public void success(JSONArray data) {
    eventView.onSuccess(data);
  }

  @Override
  public void failed(String msg) {
    eventView.onFailed(msg);
  }
}
