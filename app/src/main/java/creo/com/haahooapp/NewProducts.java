package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.NewHomeCat;
import creo.com.haahooapp.Modal.TrendingPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class NewProducts extends AppCompatActivity {

    Activity activity = this;
    Context context = this;
    RecyclerView list;
    ProgressDialog progressDialog;
    String lat = "null";
    String log = "null";
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_products);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        list = findViewById(R.id.list);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmer();
        progressDialog = new ProgressDialog(this);
        Bundle bundle = getIntent().getExtras();
        lat = bundle.getString("lat");
        log = bundle.getString("log");
        Log.d("hgcgh","gyfdt"+lat+log);
        progressDialog.setTitle("Loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Fetching Products...");
        progressDialog.show();
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.action_home:
                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:
                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:
                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:
                                startActivity(new Intent(context, AllStories.class));
                                break;

                            case R.id.shop:

//                                startActivity(new Intent(context, MyShopNew.class));
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
        newAPI();
    }

    public void newAPI() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        //NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        Call<ResponseBody> callregister = apiInterface.getProductslatest("Token " + token,lat,log);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    List<TrendingPojo> trendingPojos = new ArrayList<>();
                    JSONArray jsonArray = obj.optJSONArray("data");
                    if (jsonArray.length() == 0) {
                        Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            TrendingPojo category = new TrendingPojo();
                            category.setId(jsonObject.optString("id"));
                            category.setName(jsonObject.optString("name"));
                            category.setShop_id(jsonObject.optString("shop_id"));
                            String images1 = jsonObject.optString("image");
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            trendingPojos.add(category);
                        }
                    }
                    mShimmerViewContainer.stopShimmer();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                    list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                    list.setAdapter(new NewHomeCat(getApplicationContext(), trendingPojos));
                    list.setNestedScrollingEnabled(false);
                    progressDialog.dismiss();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

}