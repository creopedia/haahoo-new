package creo.com.haahooapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.common.util.NumberUtils;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.Modal.StoryPojo;
import creo.com.haahooapp.camera.DetailedImageVideo;
import creo.com.haahooapp.config.ApiClient;
import jp.shts.android.storiesprogressview.StoriesProgressView;

public class StoryActivityOthers extends AppCompatActivity implements StoriesProgressView.StoriesListener {

    private static final int PROGRESS_COUNT = 0;
    MediaController mediaController;
    View reverse;
    public static ArrayList<StoryPojo>dataModelArrayList = new ArrayList<>();
    private StoriesProgressView storiesProgressView;
    private ImageView image,viewers;
    String currentImage = "null";
    String currentImageId = "null";
    public int status=0;
    String currentimagecount = "null";
    private MediaSource mVideoSource;
    long timeInMillisec;
    DefaultDataSourceFactory dataSourceFactory;
    SimpleExoPlayer player;
    private PlayerView mExoPlayerView;
    DefaultHttpDataSourceFactory httpDataSourceFactory;
    DefaultExtractorsFactory extractorsFactory;
    TextView user_story;
    Uri uri;
    VideoView videoView;
    ProgressDialog progressDialog;
    private int counter = 0;
    Context context = this;
    long pressTime = 0L;
    long limit = 500L;

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);
        user_story = findViewById(R.id.user_story);
        user_story.setText(ApiClient.story_id_name);
        viewers = findViewById(R.id.viewers);
        progressDialog = new ProgressDialog(this);
        mExoPlayerView = (PlayerView) findViewById(R.id.exoplayer);
        String userAgent = Util.getUserAgent(context, getApplicationContext().getApplicationInfo().packageName);
        httpDataSourceFactory = new DefaultHttpDataSourceFactory(userAgent, null, DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
        dataSourceFactory = new DefaultDataSourceFactory(context, null, httpDataSourceFactory);
        //   Uri daUri = Uri.parse(streamUrl);

        // mVideoSource = new HlsMediaSource(daUri, dataSourceFactory, 1, null, null);

        getStoryDetails();

        initExoPlayer();
        //SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(context);
        viewers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(StoryActivityOthers.this,StoryViewers.class));
                Intent intent = new Intent(context,StoryViewers.class);
                intent.putExtra("image",currentImage);
                intent.putExtra("imageid",currentImageId);
                intent.putExtra("count",currentimagecount);
                startActivity(intent);
            }
        });
        if (ApiClient.story_id_name.equals("My Story")){
            viewers.setVisibility(View.VISIBLE);
        }
        if (!(ApiClient.story_id_name.equals("My Story"))){
            viewers.setVisibility(View.GONE);

        }
        ApiClient.dataModelArrayList1.clear();
      // getStoryDetails();




       // user_story.setText((ApiClient.dataModelArrayList1.get(counter).getName()));
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );
        storiesProgressView = (StoriesProgressView) findViewById(R.id.stories);
        storiesProgressView.setStoriesListener(this);
        videoView = findViewById(R.id.video);
        mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        //  videoView.setMediaController(mediaController);
//        videoView.setVideoURI();
        image = (ImageView) findViewById(R.id.image);
        // bind reverse view
        reverse = findViewById(R.id.reverse);
        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.reverse();
            }
        });
        reverse.setOnTouchListener(onTouchListener);
        // bind skip view
        View skip = findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.skip();
            }
        });
        skip.setOnTouchListener(onTouchListener);
    }
    @Override
    public void onNext() {
//        mVideoSource.releaseSource();
        progressDialog.setMessage("Loading image....");
        progressDialog.show();
        progressDialog.setCancelable(false);
        status = 0;
        if (player!=null) {
            player.stop();
        }
        int count = ++counter;
        if (count>=ApiClient.dataModelArrayList1.size()){
            return;
        }
//        Log.d("jhgfds","iuytrdtygu"+ApiClient.dataModelArrayList1.get(count).getImage()+count);
        if (count<ApiClient.dataModelArrayList1.size()){
        String value = ApiClient.dataModelArrayList1.get(count).getImage();
        currentImage = value;

        currentImageId = ApiClient.dataModelArrayList1.get(count).getStory_id();
        currentimagecount  = ApiClient.dataModelArrayList1.get(count).getView_count();
        if (!(ApiClient.story_id_name.equals("My Story"))) {
            sendViewStatus();
        }
        if (value.contains(".jpeg")|| value.contains(".jpg")|| value.contains(".png")) {
            mExoPlayerView.setVisibility(View.GONE);
            progressDialog.setMessage("Loading image....");
            progressDialog.setCancelable(false);
            progressDialog.show();
            Glide.with(context).load(value).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    progressDialog.dismiss();
                    return false;
                }


                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    progressDialog.dismiss();
                    return false;
                }
            }).into(image);

          // Glide.with(context).load(value).into(image);
            videoView.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
        }
        if (value.contains(".mp4")) {
            image.setVisibility(View.GONE);
            mExoPlayerView.setVisibility(View.VISIBLE);
            long time3 = Long.parseLong(ApiClient.dataModelArrayList1.get(count).getDuration());
            extractorsFactory = new DefaultExtractorsFactory();
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            LoadControl loadControl = new DefaultLoadControl();
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), trackSelector, loadControl);
            mExoPlayerView.setPlayer(player);
            mExoPlayerView.setUseController(false);
            storiesProgressView.setStoryDuration(time3);
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(value, new HashMap<String, String>());
//            Log.d("hgfdsfgh","uygtfdryutre"+timeInMillisec);

            mVideoSource = new ExtractorMediaSource(Uri.parse(ApiClient.dataModelArrayList1.get(count).getImage()),
                    dataSourceFactory, extractorsFactory, null, null);
            player.prepare(mVideoSource);
            player.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                }

                @Override
                public void onLoadingChanged(boolean isLoading) {
                    if (isLoading){
                        progressDialog.setMessage("Loading... Please Wait...");
                        progressDialog.show();
                        storiesProgressView.pause();
                    }
                    if (!isLoading){
                        progressDialog.dismiss();
                        storiesProgressView.resume();
                    }
                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (playbackState == 2){
                        storiesProgressView.pause();
                    }
                    if (playbackState == 1){
                        storiesProgressView.pause();
                    }
                    if (playbackState == Player.STATE_READY){

                        status = 1;
                        storiesProgressView.resume();
                        mExoPlayerView.getPlayer().setPlayWhenReady(true);

                    }
                    if (playbackState == Player.STATE_ENDED){
                       // Toast.makeText(context,"completed",Toast.LENGTH_SHORT).show();
                        //storiesProgressView.skip();
                    }
                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {

                }
            });





            //old



//            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//            retriever.setDataSource(value, new HashMap<String, String>());
//            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//            long timeInMillisec = Long.parseLong(time);
//            retriever.release();
//            String duration = convertMillieToHMmSs(timeInMillisec); //use this duration
//            storiesProgressView.setStoryDuration(timeInMillisec);
//            image.setVisibility(View.GONE);
//            mVideoSource = new ExtractorMediaSource(Uri.parse(value),
//                    dataSourceFactory, extractorsFactory, null, null);
//            Log.d("hgfdsfgh", "hgfdsfgh");
//            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
//            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
//            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
//            LoadControl loadControl = new DefaultLoadControl();
//            SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);
//            mExoPlayerView.setPlayer(player);
//            mExoPlayerView.setUseController(false);
//
////        boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;
////
////        if (haveResumePosition) {
////            mExoPlayerView.getPlayer().seekTo(mResumeWindow, mResumePosition);
////        }
//
//            player.prepare(mVideoSource);
//            mExoPlayerView.getPlayer().setPlayWhenReady(true);


            uri = Uri.parse(value);
            videoView.setVisibility(View.GONE);
//            videoView.setVideoURI(uri);
//            videoView.start();
        }
        }
        // image.setImageResource(resources[++counter]);
    }

    @Override
    public void onPrev() {
        if (player!=null) {
            player.stop();
        }
        progressDialog.setMessage("Loading image....");
        progressDialog.show();
        progressDialog.setCancelable(false);
        if ((counter - 1) < 0) return;
        // image.setImageResource(resources[--counter]);
        int count = --counter;
        String value = ApiClient.dataModelArrayList1.get(count).getImage();
        currentImage = value;
        currentImageId = ApiClient.dataModelArrayList1.get(count).getStory_id();
        currentimagecount  = ApiClient.dataModelArrayList1.get(count).getView_count();

        if (!(ApiClient.story_id_name.equals("My Story"))) {
            sendViewStatus();
        }
        //Log.d("hgfdsghj","valueeee"+value);
        if (value.contains(".jpeg")|| value.contains(".jpg")) {
            storiesProgressView.setStoryDuration(10000L);
            progressDialog.setMessage("Loading image....");
            progressDialog.show();
            progressDialog.setCancelable(false);
            Glide.with(context).load(value).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    progressDialog.dismiss();
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                   // Toast.makeText(context,"Loaded",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    return false;
                }
            }).into(image);


           // Glide.with(context).load(value).into(image);
            videoView.setVisibility(View.GONE);
            mExoPlayerView.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
        }
        if (value.contains(".mp4")){

            //player.stop();
            //old
            long time3 = Long.parseLong(ApiClient.dataModelArrayList1.get(count).getDuration());
            mExoPlayerView.setVisibility(View.VISIBLE);
            image.setVisibility(View.GONE);
            extractorsFactory = new DefaultExtractorsFactory();
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            LoadControl loadControl = new DefaultLoadControl();
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), trackSelector, loadControl);
            mExoPlayerView.setPlayer(player);
            mExoPlayerView.setUseController(false);
            mVideoSource = new ExtractorMediaSource(Uri.parse(ApiClient.dataModelArrayList1.get(count).getImage()),
                    dataSourceFactory, extractorsFactory, null, null);
            player.prepare(mVideoSource);
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(value, new HashMap<String, String>());
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            //long timeInMillisec = Long.parseLong(time);

            player.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                }

                @Override
                public void onLoadingChanged(boolean isLoading) {
                    if (isLoading){
                        progressDialog.setMessage("Loading... Please Wait...");
                        progressDialog.show();
                        storiesProgressView.pause();
                    }
                    if (!isLoading){
                        progressDialog.dismiss();
                        storiesProgressView.resume();
                    }
                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (playbackState == 2){
                        storiesProgressView.pause();
                    }
                    if (playbackState == 1){
                        storiesProgressView.pause();
                    }
                    if (playbackState == Player.STATE_READY){

                        status = 1;
                        storiesProgressView.resume();
                        storiesProgressView.setStoryDuration(time3);
                        mExoPlayerView.getPlayer().setPlayWhenReady(true);

                    }
                    if (playbackState == Player.STATE_ENDED){
                        // Toast.makeText(context,"completed",Toast.LENGTH_SHORT).show();
                        //storiesProgressView.skip();
                    }
                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {

                }
            });
//
//            mExoPlayerView.getPlayer().setPlayWhenReady(true);



//            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//            retriever.setDataSource(value, new HashMap<String, String>());
//            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//            long timeInMillisec = Long.parseLong(time);
//            retriever.release();
//            String duration=convertMillieToHMmSs(timeInMillisec); //use this duration
////            Log.d("vidduration","duration"+timeInMillisec);
//            mVideoSource = new ExtractorMediaSource(Uri.parse(value),
//                    dataSourceFactory, extractorsFactory, null, null);
//            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
//            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
//            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
//            LoadControl loadControl = new DefaultLoadControl();
//            SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), trackSelector, loadControl);
//            mExoPlayerView.setPlayer(player);
//            mExoPlayerView.setUseController(false);
//
////        boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;
////
////        if (haveResumePosition) {
////            mExoPlayerView.getPlayer().seekTo(mResumeWindow, mResumePosition);
////        }
//
//            player.prepare(mVideoSource);
//            mExoPlayerView.getPlayer().setPlayWhenReady(true);




            image.setVisibility(View.GONE);
            uri = Uri.parse(value);
            videoView.setVisibility(View.GONE);
//            videoView.setVideoURI(uri);
//            videoView.start();
        }
    }
    @Override
    public void onComplete() {
        //send to home activity coz in whatsapp also after the last video goes back to listview
        if (player!=null) {
            player.stop();
        }
        super.onBackPressed();

    }

    @Override
    public void onBackPressed() {
        if (player!=null) {
            player.stop();
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        // Very important !
        storiesProgressView.destroy();
        super.onDestroy();
    }

    public static String convertMillieToHMmSs(long millie) {
        long seconds = (millie / 1000);
        long second = seconds % 60;
        long minute = (seconds / 60) % 60;
        long hour = (seconds / (60 * 60)) % 24;
        String result = "";
        if (hour > 0) {
            return String.format("%02d:%02d:%02d", hour, minute, second);
        }
        else {
            return String.format("%02d:%02d" , minute, second);
        }

    }
    public void getStoryDetails(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));


        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"stories/story_list_individual/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);

                            JSONArray jsonArray = jsonObject1.optJSONArray("data");
                            JSONObject jsonObject = jsonArray.optJSONObject(0);
                           // Log.d("nbhgfds","iuytr"+jsonObject1);
                            JSONArray jsonArray1 = jsonObject.optJSONArray("story");
                            for (int i = 0 ;i <jsonArray1.length();i++){
                                JSONObject jsonObject2 = jsonArray1.optJSONObject(i);
                                StoryPojo storyPojo = new StoryPojo();
                                storyPojo.setImage(ApiClient.BASE_URL+"media/"+jsonObject2.optString("story"));
                                storyPojo.setStory_id(jsonObject2.optString("id"));
                                storyPojo.setView_count(jsonObject2.optString("viewed_count"));
                                storyPojo.setDuration(jsonObject2.optString("duration"));
                                dataModelArrayList.add(storyPojo);
                               // Log.d("obaaaj","json"+jsonObject2.optString("id"));
                            }
                            ApiClient.dataModelArrayList1 = dataModelArrayList;
                            storiesProgressView.setStoriesCount(ApiClient.dataModelArrayList1.size());


                            if (ApiClient.dataModelArrayList1.get(0).getImage().contains(".jpeg")|| ApiClient.dataModelArrayList1.get(counter).getImage().contains(".jpg")) {
                                storiesProgressView.setStoryDuration(10000L);
                                storiesProgressView.startStories();
                                videoView.setVisibility(View.GONE);
                                mExoPlayerView.setVisibility(View.GONE);
                                image.setVisibility(View.VISIBLE);
                                currentImage = ApiClient.dataModelArrayList1.get(0).getImage();
                                currentImageId = ApiClient.dataModelArrayList1.get(0).getStory_id();
                                currentimagecount  = ApiClient.dataModelArrayList1.get(0).getView_count();
                                //Toast.makeText(context,"image id"+currentImageId,Toast.LENGTH_SHORT).show();
                                progressDialog.setMessage("Loading image....");
                                progressDialog.show();
                                progressDialog.setCancelable(false);
                                Glide.with(context).load(ApiClient.dataModelArrayList1.get(0).getImage()).listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                       progressDialog.dismiss();
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                        Toast.makeText(context,"Loaded",Toast.LENGTH_SHORT).show();
                                        progressDialog.dismiss();
                                        return false;
                                    }
                                }).into(image);
                                //Glide.with(context).load(ApiClient.dataModelArrayList1.get(0).getImage()).into(image);
                            }
                            if (ApiClient.dataModelArrayList1.get(0).getImage().contains(".mp4")){
                                mExoPlayerView.setVisibility(View.VISIBLE);
                                image.setVisibility(View.GONE);
                                extractorsFactory = new DefaultExtractorsFactory();
                                mExoPlayerView.setUseController(false);
                                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                                TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
                                TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
                                LoadControl loadControl = new DefaultLoadControl();
                                player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), trackSelector, loadControl);
                                mExoPlayerView.setPlayer(player);
                                currentImage = ApiClient.dataModelArrayList1.get(0).getImage();
                                currentImageId = ApiClient.dataModelArrayList1.get(0).getStory_id();
                                currentimagecount  = ApiClient.dataModelArrayList1.get(0).getView_count();
                                mExoPlayerView.setUseController(false);
                                String duration = ApiClient.dataModelArrayList1.get(0).getDuration();
                                long dura = Long.parseLong(duration);
                                storiesProgressView.setStoryDuration(dura);
                                mVideoSource = new ExtractorMediaSource(Uri.parse(ApiClient.dataModelArrayList1.get(0).getImage()),
                                        dataSourceFactory, extractorsFactory, null, null);
                                player.prepare(mVideoSource);
                                storiesProgressView.startStories();

                                player.addListener(new Player.EventListener() {
                                    @Override
                                    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                                        Log.d("CHANGESSS","CHCKBDHJVD");

                                    }

                                    @Override
                                    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                                        Log.d("CHANGESSS","KGKGJHGJH");

                                    }

                                    @Override
                                    public void onLoadingChanged(boolean isLoading) {
                                        if (isLoading){
                                            progressDialog.setMessage("Loading... Please Wait...");
                                            progressDialog.show();
                                            storiesProgressView.pause();
                                        }
                                        if (!isLoading){
                                            progressDialog.dismiss();
                                            storiesProgressView.resume();
                                        }
                                    }

                                    @Override
                                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                                        if (playbackState == 2){
                                            storiesProgressView.pause();
                                        }
                                        if (playbackState == 1){
                                            storiesProgressView.pause();
                                        }
                                        if (playbackState == Player.STATE_READY){

                                            status = 1;
                                            storiesProgressView.resume();
                                            storiesProgressView.setStoryDuration(10000L);
                                            mExoPlayerView.getPlayer().setPlayWhenReady(true);

                                        }
                                        if (playbackState == Player.STATE_ENDED){
                                            // Toast.makeText(context,"completed",Toast.LENGTH_SHORT).show();
                                            //storiesProgressView.skip();
                                            player.stop();
                                        }
                                    }

                                    @Override
                                    public void onRepeatModeChanged(int repeatMode) {

                                    }

                                    @Override
                                    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                                    }

                                    @Override
                                    public void onPlayerError(ExoPlaybackException error) {

                                    }

                                    @Override
                                    public void onPositionDiscontinuity(int reason) {

                                    }

                                    @Override
                                    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                                    }

                                    @Override
                                    public void onSeekProcessed() {

                                    }
                                });





//        boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;
//
//        if (haveResumePosition) {
//            mExoPlayerView.getPlayer().seekTo(mResumeWindow, mResumePosition);
//        }

                                //old
//                                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//                                String userAgent = Util.getUserAgent(context, getApplicationContext().getApplicationInfo().packageName);
//                                httpDataSourceFactory = new DefaultHttpDataSourceFactory(userAgent, null, DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
//                                dataSourceFactory = new DefaultDataSourceFactory(context, null, httpDataSourceFactory);
//
//
//                                mVideoSource = new ExtractorMediaSource(Uri.parse(ApiClient.dataModelArrayList1.get(0).getImage()),
//                                        dataSourceFactory, extractorsFactory, null, null);
//                                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
//                                TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
//                                TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
//                                LoadControl loadControl = new DefaultLoadControl();
//                                SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), trackSelector, loadControl);
//                                mExoPlayerView.setPlayer(player);
//                                mExoPlayerView.setUseController(false);
//                                retriever.setDataSource(ApiClient.dataModelArrayList1.get(0).getImage(), new HashMap<String, String>());
//                                String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                                long timeInMillisec = Long.parseLong(time);
//                                retriever.release();
//                                String duration=convertMillieToHMmSs(timeInMillisec); //use this duration
//                                storiesProgressView.setStoryDuration(timeInMillisec);
//                                storiesProgressView.startStories();
//                                image.setVisibility(View.GONE);
////        boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;
////
////        if (haveResumePosition) {
////            mExoPlayerView.getPlayer().seekTo(mResumeWindow, mResumePosition);
////        }
//
//                                player.prepare(mVideoSource);
//                                mExoPlayerView.getPlayer().setPlayWhenReady(true);
//
//
//
//
//                                currentImage = ApiClient.dataModelArrayList1.get(0).getImage();
//                                currentImageId = ApiClient.dataModelArrayList1.get(0).getStory_id();
//                                currentimagecount  = ApiClient.dataModelArrayList1.get(0).getView_count();

//                                uri = Uri.parse(ApiClient.dataModelArrayList1.get(0).getImage());
//                                videoView.setVisibility(View.GONE);
//                                videoView.setVideoURI(uri);
//                                videoView.start();
                            }
                            if (!(ApiClient.story_id_name.equals("My Story"))) {
                                sendViewStatus();
                            }
                            if (ApiClient.story_id_name.equals("My Story")){

                            }

//                            JSONObject jsonObject2 = jsonArray1.optJSONObject(0);
//                            for (int i = 0;i<jsonArray1.length();i++){
//                                StoryPojo storyPojo = new StoryPojo();
//                                storyPojo.setImage(ApiClient.BASE_URL+"media/"+jsonArray1.optString(i));
//                                dataModelArrayList.add(storyPojo);
//                            }
//                            ApiClient.dataModelArrayList1 = dataModelArrayList;
//                            storiesProgressView.setStoriesCount(ApiClient.dataModelArrayList1.size());
//                            storiesProgressView.setStoryDuration(10000L);
//                            storiesProgressView.startStories();
//                            // or
//                            // storiesProgressView.setStoriesCountWithDurations(durations);
//                            if (ApiClient.dataModelArrayList1.get(counter).getImage().contains(".jpeg")|| ApiClient.dataModelArrayList1.get(counter).getImage().contains(".jpg")) {
//                                storiesProgressView.setStoryDuration(10000L);
//                                videoView.setVisibility(View.GONE);
//                                image.setVisibility(View.VISIBLE);
//                                Glide.with(context).load(ApiClient.dataModelArrayList1.get(counter).getImage()).into(image);
//                            }
//                            if (ApiClient.dataModelArrayList1.get(counter).getImage().contains(".mp4")){
//                                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//                                retriever.setDataSource(ApiClient.dataModelArrayList1.get(counter).getImage(), new HashMap<String, String>());
//                                String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                                long timeInMillisec = Long.parseLong(time);
//                                retriever.release();
//                                String duration=convertMillieToHMmSs(timeInMillisec); //use this duration
//                                Log.d("vidduration","duration"+timeInMillisec);
//                                storiesProgressView.setStoryDuration(timeInMillisec);
//                                image.setVisibility(View.GONE);
//                                uri = Uri.parse(ApiClient.dataModelArrayList1.get(counter).getImage());
//                                videoView.setVisibility(View.VISIBLE);
//                                videoView.setVideoURI(uri);
//                                videoView.start();
//                            }




                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id",ApiClient.story_id);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


    public void sendViewStatus(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"stories/view_story_history/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                           // Log.d("reshh","response"+jsonObject1+currentImageId);

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id",currentImageId);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }




    private void initExoPlayer() {

    }

}
