package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.ResellingCategoryAdapter;
import creo.com.haahooapp.Adapters.ResellingProductsAdapter;
import creo.com.haahooapp.Modal.HorizontalPojo;
import creo.com.haahooapp.Modal.ResellingPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.ItemClick;

public class ResellProducts extends AppCompatActivity implements ItemClick {

    TextView loadmore;
    RecyclerView recyclerview, subcategories;
    String nexturl = ApiClient.BASE_URL + "api_shop_app/view_bazar/";
    CardView card;
    Activity activity = this;
    ArrayList<ResellingPojo> pojos = new ArrayList<>();
    ResellingProductsAdapter resellingAdapter;
    String val = "1";
    ProgressDialog progressDialog;
    List<HorizontalPojo> productFeaturepojos = new ArrayList<>();
    ResellingCategoryAdapter resellingCategoryAdapter;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resell_products);
        loadmore = findViewById(R.id.laodmore);
        recyclerview = findViewById(R.id.recyclerview);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        subcategories = findViewById(R.id.subcategories);
        card = findViewById(R.id.card);
        resellingAdapter = new ResellingProductsAdapter(context, pojos, this);
        resellingCategoryAdapter = new ResellingCategoryAdapter(productFeaturepojos, context, this);
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait while the data is loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(true);
        progressDialog.show();
        loadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(nexturl.equals("null"))) {
                    progressDialog.setTitle("Loading");
                    progressDialog.setMessage("Please wait while the data is loading");
                    progressDialog.setIcon(R.drawable.haahoologo);
                    progressDialog.setCancelable(true);
                    progressDialog.show();
                    Runnable runnable2 = new Runnable() {
                        @Override
                        public void run() {
                            synchronized (this) {
                                try {

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                getResellingProducts();
                            }
                        }
                    };
                    Thread threadresell = new Thread(runnable2);
                    threadresell.start();
                }
                if (nexturl.equals("null")) {
                    Toast.makeText(context, "No more data", Toast.LENGTH_SHORT).show();
                }

            }
        });
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getcategories();
                }
            }
        };
        Thread threadcat = new Thread(runnable);
        threadcat.start();
        //getcategories();
        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getResellingProducts();
                }
            }
        };
        Thread thread1 = new Thread(runnable1);
        thread1.start();
        //getResellingProducts();
        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
    }

    public void getcategories() {
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/list_shop_cat/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("categories", "bncgbc" + obj);
                            progressDialog.dismiss();
                            JSONArray data = obj.optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = data.optJSONObject(i);
                                HorizontalPojo horizontalPojo = new HorizontalPojo();
                                horizontalPojo.setId(jsonObject.optString("id"));
                                horizontalPojo.setName(jsonObject.optString("name"));
                                horizontalPojo.setImage(ApiClient.BASE_URL + jsonObject.optString("image").replace("[", "").replace("]", "").trim());
                                productFeaturepojos.add(horizontalPojo);
                            }

                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                            subcategories.setLayoutManager(horizontalLayoutManagaer);
                            subcategories.setAdapter(resellingCategoryAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                //

            }
        }) {

            @Override
            public java.util.Map<String, String> getHeaders() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void getResellingProducts() {

        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, nexturl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            progressDialog.dismiss();
                            nexturl = obj.optString("next");
                            if (!(nexturl.equals("null"))) {
                                card.setVisibility(View.VISIBLE);
                            }
                            Log.d("cccCCVVVVV", "resp" + response);
                            JSONArray jsonArray = obj.optJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                ResellingPojo resellingPojo = new ResellingPojo();
                                resellingPojo.setId(jsonObject.optString("product_id"));
                                resellingPojo.setShop_id(jsonObject.optString("shop_id"));
                                String images = jsonObject.optString("pdt_image");
                                String[] seperated = images.split(",");
                                resellingPojo.setImage(seperated[0].replace("[", "").replace("]", ""));
                                resellingPojo.setProduct_name(jsonObject.optString("pdt_name"));
                                resellingPojo.setWholesale_price(jsonObject.optString("discount").trim());
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setResell(jsonObject.optString("resell"));
                                resellingPojo.setResell_count(jsonObject.optString("resell_count"));
                                resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                resellingPojo.setMin_wholesale_qty(jsonObject.optString("minimum_wholesale_quantity"));
                                resellingPojo.setDelivery_expense(jsonObject.optString("myshop_delivery_expense"));
                                resellingPojo.setStatus(jsonObject.optString("virtual_shop_status"));
                                pojos.add(resellingPojo);
                            }
                            recyclerview.setAdapter(resellingAdapter);
                            recyclerview.setNestedScrollingEnabled(false);
                            recyclerview.setLayoutManager(new GridLayoutManager(context, 2));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() throws AuthFailureError {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("resell_value", val);
                return params;
            }

            @Override
            public java.util.Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void addProduct(String productid, String price1, String edited_price) {
        progressDialog.setTitle("Adding...");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Adding product to MyShop..");
        progressDialog.show();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    try{

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    addProductToMyShop(productid, price1, edited_price);
                }
            }
        };
        Thread threadload = new Thread(runnable);
        threadload.start();

    }

    @Override
    public void sendData(String id) {
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait while the data is loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(true);
        progressDialog.show();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    try{
                        loadProducts(id);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread threadsend = new Thread(runnable);
        threadsend.start();

    }

    public void addProductToMyShop(String productid, String price2, String edited_price) {
        // Log.d("eddd_price","hvjgcfdx"+edited_price);
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "virtual_shop/add_virtual_pdt/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            //Log.d("nnnnn", "jsonarray" + jsonObject);
                            String message = jsonObject.optString("message");
                            progressDialog.dismiss();
                            if (message.equals("Success")) {
                                Toast.makeText(context, "Product added to shop successfully", Toast.LENGTH_SHORT).show();
                                new FancyAlertDialog.Builder(activity)
                                        .setTitle("Go to My Shop")
                                        .setBackgroundColor(Color.parseColor("#0078BC"))  //Don't pass R.color.colorvalue
                                        .setMessage("Product has been added to your shop. Want to have a look on them ?")
                                        .setNegativeBtnText("NO")
                                        .setPositiveBtnBackground(Color.parseColor("#0078BC"))  //Don't pass R.color.colorvalue
                                        .setPositiveBtnText("YES")
                                        .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                                        .setAnimation(Animation.POP)
                                        .isCancellable(false)
                                        .setIcon(R.drawable.white_check, Icon.Visible)
                                        .OnPositiveClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {
                                                startActivity(new Intent(context, MyShopNew.class));
                                            }
                                        })
                                        .OnNegativeClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {

                                            }
                                        })
                                        .build();
                            }
                            if (message.equals("Failed")) {
                                progressDialog.dismiss();
                                Toast.makeText(context, "Failed to add product ", Toast.LENGTH_SHORT).show();
                            }
                            if (!(message.equals("Success")) || (message.equals("Failed"))) {
                                progressDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
                progressDialog.dismiss();
            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", productid);
                params.put("changed_price", edited_price);
                params.put("orginal_price", price2);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);

                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void loadProducts(String categoryid) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/view_reselling_products_under_category/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            pojos.clear();
                            Log.d("response8889", "resp" + response);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            if (jsonArray.length() == 0) {
                                progressDialog.dismiss();
                                Toast.makeText(context, "No products under this category", Toast.LENGTH_SHORT).show();
                            }
                            if (jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    ResellingPojo resellingPojo = new ResellingPojo();
                                    resellingPojo.setId(jsonObject.optString("product_id"));
                                    resellingPojo.setShop_id(jsonObject.optString("shop_id"));
                                    String images = jsonObject.optString("pdt_image");
                                    String[] seperated = images.split(",");
                                    resellingPojo.setImage(seperated[0].replace("[", "").replace("]", ""));
                                    resellingPojo.setProduct_name(jsonObject.optString("pdt_name"));
                                    resellingPojo.setWholesale_price(jsonObject.optString("discount").trim());
                                    resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                    resellingPojo.setResell(jsonObject.optString("resell"));
                                    resellingPojo.setResell_count(jsonObject.optString("resell_count"));
                                    resellingPojo.setPdt_price(jsonObject.optString("pdt_price"));
                                    resellingPojo.setMin_wholesale_qty(jsonObject.optString("minimum_wholesale_quantity"));
                                    resellingPojo.setDelivery_expense(jsonObject.optString("delivery_expense"));
                                    resellingPojo.setStatus(jsonObject.optString("virtual_shop_status"));
                                    pojos.add(resellingPojo);
                                }
                            }
                            recyclerview.setAdapter(resellingAdapter);
                            recyclerview.setNestedScrollingEnabled(false);
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("category_id", categoryid);
                params.put("resell_value", "1");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

}