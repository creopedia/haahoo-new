package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.DownloadAdapter;
import creo.com.haahooapp.Adapters.DownloadPager;
import creo.com.haahooapp.Modal.DownloadPojo;
import creo.com.haahooapp.Modal.Downloadd;
import creo.com.haahooapp.config.ApiClient;

public class Downloads extends AppCompatActivity {

    Activity activity = this;
    RecyclerView recyclerView;
    Context context = this;
    ImageView back, phone;
    TextView total;
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> date = new ArrayList<>();
    ArrayList<String> number = new ArrayList<>();
    DownloadAdapter downloadAdapter;
    private List<DownloadPojo> pojo = new ArrayList<>();
    TabLayout tabLayout;
    ViewPager pager;
    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads);
        Window window = activity.getWindow();
        tabLayout = findViewById(R.id.tabs);
        total = findViewById(R.id.total);
        pager = findViewById(R.id.pager);
        phone = findViewById(R.id.phone);
        requestContactPermission();
        tabLayout.addTab(tabLayout.newTab().setText("COINS"));
        tabLayout.addTab(tabLayout.newTab().setText("WHATSAPP"));
        tabLayout.addTab(tabLayout.newTab().setText("CALL"));
        DownloadPager downloadPager = new DownloadPager(context, getSupportFragmentManager(), tabLayout.getTabCount());
        pager.setAdapter(downloadPager);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        recyclerView = findViewById(R.id.recyclerView);
        getDownloads();
        back = findViewById(R.id.back);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Downloads.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Downloads.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Downloads.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                               // startActivity(new Intent(Downloads.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Downloads.super.onBackPressed();
            }
        });


    }

    public void getDownloads() {

        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/login_ref_count/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("refcount", "refff" + jsonObject);
                            name.clear();
                            date.clear();
                            String message = jsonObject.optString("message");
                            if (message.equals("Success")) {
                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                total.setText("Total Customers : " + jsonArray.length());
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                    name.add(jsonObject1.optString("name"));
                                    date.add(jsonObject1.optString("join_date"));
                                    number.add(jsonObject1.optString("phone_no"));
                                }
                                final List<Downloadd> downloadPojos = new ArrayList<>();
                                for (int k = 0; k < name.size(); k++) {
                                    Downloadd downloadPojo = new Downloadd(name.get(k), date.get(k), number.get(k));
                                    downloadPojos.add(downloadPojo);
                                }
                                downloadAdapter = new DownloadAdapter(downloadPojos, context);
                                recyclerView.setNestedScrollingEnabled(false);
                                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                                recyclerView.setAdapter(downloadAdapter);
                            }
                            if (!(message.equals("Success"))) {
                                Toast.makeText(context, "No data found", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Downloads.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void requestContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Read Contacts permission");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("Please enable access to contacts.");
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {android.Manifest.permission.READ_CONTACTS}
                                    , PERMISSIONS_REQUEST_READ_CONTACTS);
                        }
                    });
                    builder.show();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.READ_CONTACTS},
                            PERMISSIONS_REQUEST_READ_CONTACTS);
                }
            } else {
                //getContacts();
            }
        } else {
            //getContacts();
        }
    }

}
