package creo.com.haahooapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.config.ApiClient;
import static android.content.Context.MODE_PRIVATE;


public class ProfileAccountFragment extends Fragment {

    TextView bankaccount, editaccount;
    String id = "null";

    public ProfileAccountFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_account, container, false);
        bankaccount = view.findViewById(R.id.bankaccount);
        editaccount = view.findViewById(R.id.editaccount);
        editaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),EditBankAccount.class));
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getBankdetails();
    }


    public void getBankdetails(){

        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"bank_details/show_bank_details/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("bank","hgfccgv"+response);
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optJSONArray("data").length() == 0){
                                bankaccount.setText("Add Bank Account");
                                //Log.d("bank","hgfccgv"+bankaccount.getText().toString());
                                // editaccount.setVisibility(View.GONE);
                            }
                            if (jsonObject.optJSONArray("data").length() != 0) {
                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                JSONObject jsonObject1 = jsonArray.optJSONObject(0);
                                JSONObject jsonObject2 = jsonObject1.optJSONObject("fields");
                                String account_number = jsonObject2.optString("acc_no");
                                bankaccount.setText(account_number);
                                editaccount.setVisibility(View.VISIBLE);
                            }
                            if (bankaccount.getText().toString().equals("Add Bank Account")) {
                                bankaccount.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startActivity(new Intent(getContext(), BankAccount.class));
                                    }
                                });
                                if (!(bankaccount.getText().toString().equals("Add Bank Account"))) {
                                    startActivity(new Intent(getContext(), EditBankAccount.class));
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),"Some error occured",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }

}