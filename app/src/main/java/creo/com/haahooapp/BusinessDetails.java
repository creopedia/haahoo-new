package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.Adapters.FeatureAdapter;
import creo.com.haahooapp.Modal.ProductFeaturepojo;
import creo.com.haahooapp.config.ApiClient;

public class BusinessDetails extends AppCompatActivity {

    Activity activity = this;
    PopupWindow popupWindow;
    Context context = this;
    RelativeLayout relativeLayout;
    ImageView img,back;
    TextView title,description,scratch,price;
    ArrayList<ProductFeaturepojo> productFeaturepojos = new ArrayList<>();
    RecyclerView recycle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_details);
        Window window = activity.getWindow();
        img = findViewById(R.id.img);
        relativeLayout = findViewById(R.id.rela);
        title = findViewById(R.id.title);
        recycle = findViewById(R.id.recycle);
        description = findViewById(R.id.description);
        price = findViewById(R.id.price);
        back = findViewById(R.id.back);
        Bundle bundle = getIntent().getExtras();
        scratch = findViewById(R.id.scratch);
        String id = bundle.getString("id");
        getBusinessDetails(id);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusinessDetails.super.onBackPressed();
            }
        });

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context, MyShopNew.class));
                                break;

                        }
                        return true;
                    }
                });


//        closePopupBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popupWindow.dismiss();
//                relativeLayout.setAlpha(1.0F);
//            }
//        });


    }


    public void getBusinessDetails(String business_id){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"bussiness/bussiness_id/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("busineessss","shopprods"+response);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0 ;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                title.setText(jsonObject.optString("name"));
                                description.setText(jsonObject.optString("description"));
                                Glide.with(context).load(ApiClient.BASE_URL+"media/"+jsonObject.optString("image")).into(img);
                                price.setText("FEE Rs."+jsonObject.optString("price"));
                                scratch.setText("SHARE NOW AND GET Rs. "+jsonObject.optString("cus_earn"));
                                    Log.d("productsinshosssp","shopprods"+jsonArray.optJSONObject(i));
                                    JSONObject jsonObject1 = jsonObject.optJSONObject("features");
                                ArrayList<String>features = new ArrayList<>();
                                    for (int j =0 ;j<jsonObject1.length();j++){
//                                        features.add(jsonObject1.optString("feature"+j));
//                                        Log.d("featurghghgsd","feats"+jsonObject1.optString("feature"+j));
                                        ProductFeaturepojo productFeaturepojo = new ProductFeaturepojo(jsonObject1.optString("feature"+j));
                                        productFeaturepojos.add(productFeaturepojo);
                                    }

                            }
                            FeatureAdapter featureAdapter = new FeatureAdapter(productFeaturepojos,activity);
                            recycle.setHasFixedSize(true);
                            recycle.setLayoutManager(new LinearLayoutManager(activity));
                            recycle.setAdapter(featureAdapter);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id",  business_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
