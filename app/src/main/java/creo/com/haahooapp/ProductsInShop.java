package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.michaelbel.bottomsheet.BottomSheet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.BottomAdapter;
import creo.com.haahooapp.Adapters.FilterHeadersAdapter;
import creo.com.haahooapp.Adapters.FilterValueAdapter;
import creo.com.haahooapp.Adapters.Horizontal_item_adapter;
import creo.com.haahooapp.Adapters.InfiniteScrolling;
import creo.com.haahooapp.Adapters.ProductsnewAdapter;
import creo.com.haahooapp.Adapters.SlidingContentAdapter;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.Modal.FeaturePojoHeader;
import creo.com.haahooapp.Modal.FeaturePojoValues;
import creo.com.haahooapp.Modal.HorizontalPojo;
import creo.com.haahooapp.Modal.NewProduct;
import creo.com.haahooapp.Modal.ProductVariant;
import creo.com.haahooapp.Modal.StoryPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import creo.com.haahooapp.config.EndlessRecyclerViewScrollListener;
import creo.com.haahooapp.interfaces.FilterInterface;
import creo.com.haahooapp.interfaces.ItemClick;
import creo.com.haahooapp.interfaces.SendData;
import creo.com.haahooapp.interfaces.SubCategoryInterface;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import static creo.com.haahooapp.config.ApiClient.shop_refer;

public class ProductsInShop extends AppCompatActivity implements SearchView.OnQueryTextListener, OnMapReadyCallback, FilterInterface, SubCategoryInterface, ItemClick, SendData {
    RecyclerView gridView, horizontal;
    ArrayList<Category> categories = new ArrayList<>();
    ArrayList<Category> subcatcategories = new ArrayList<>();
    ArrayList<Category> filtercategories = new ArrayList<>();
    Activity activity = this;
    BottomAdapter adapter1;
    List<ProductVariant> lists1 = new ArrayList<>();
    RecyclerView list;
    TextView textView, loadmore, loadmoresort, shop, connect, pdtcount, connected, invited, invite;
    Context context = this;
    String phone_no = "null";
    String lat = "9.9312";
    int page = 0;
    String link = "null";
    String log = "76.2673";
    String token = "null";
    private InfiniteScrolling infiniteScrolling;
    ImageView cover, tele, chat1;
    RelativeLayout call;
    CircleImageView call3, one1;
    private SlidingUpPanelLayout slidingLayout;
    RelativeLayout connectrel;
    String filterbaseurl = ApiClient.BASE_URL + "api_shop_app/specification_values_filter/";
    RecyclerView gridView1;
    CircleImageView profile;
    ImageView close;
    TextView count;
    ProgressDialog progressDialog;
    RecyclerView sliding;
    SearchView searchView;
    ArrayList<StoryPojo> storyPojos = new ArrayList<>();
    SlidingContentAdapter slidingContentAdapter;
    ImageView back, location;
    Horizontal_item_adapter horizontal_item_adapter;
    String url = "api_shop_app/list_products/";
    String sorturl = ApiClient.BASE_URL + "api_shop_app/sorting_pdt/";
    private EndlessRecyclerViewScrollListener scrollListener;
    List<HorizontalPojo> productFeaturepojos = new ArrayList<>();
    private GoogleMap gmap;
    MapView mMapView;
    TextView closebtn;
    String shop_id = "null";
    RelativeLayout sortrel, rel, filter;
    ArrayList<String> valss = new ArrayList<>();
    private static final String MAP_VIEW_BUNDLE_KEY = "AIzaSyChtKJtbRgY9vcAr3BRpTHArH16Tnws-TI";
    String firsturl = ApiClient.BASE_URL + "api_shop_app/shops_pdt_cat_based/";
    ArrayList<FeaturePojoValues> filters = new ArrayList<>();
    ArrayList<FeaturePojoHeader> featurePojoHeaders = new ArrayList<>();
    FilterHeadersAdapter orderAdapter;
    FilterValueAdapter adapter;
    RecyclerView second;
    RelativeLayout slidingrela;
    ArrayList<ArrayList<String>> test = new ArrayList<>();
    TextView subcatloadmore, filterloadmore, shopstatus;
    SearchView search1;
    ProductsnewAdapter productsnewAdapter;
    ScrollView scroll;
    ImageView cart;
    CardView variantcard;
    boolean succeed = false;
    ImageView mailimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        //getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_in_shop);
        close = findViewById(R.id.close);
        scroll = findViewById(R.id.scroll);
        search1 = findViewById(R.id.search1);
        adapter1 = new BottomAdapter(lists1, context, this);
        horizontal = findViewById(R.id.horizontal);
        search1.setIconified(true);
        mailimg = findViewById(R.id.mailimg);
        mailimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent email = new Intent(Intent.ACTION_SEND);
                    email.putExtra(Intent.EXTRA_EMAIL, new String[]{"shijin@creopedia.com"});
                    email.putExtra(Intent.EXTRA_SUBJECT, "");
                    email.putExtra(Intent.EXTRA_TEXT, "");
                    //need this to prompts email client only.
                    email.setType("message/rfc822");
                    startActivity(Intent.createChooser(email, "Send email..."));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        ApiClient.list_url = "api_shop_app/list_products/";
        search1.setQueryHint("Search Products");
        count = findViewById(R.id.count);
        count.setText(String.valueOf(ApiClient.count));
       // cartCount();
        search1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search1.onActionViewExpanded();
            }
        });
        productsnewAdapter = new ProductsnewAdapter(context, categories, this,this);
        search1.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                horizontal.setVisibility(View.VISIBLE);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                horizontal.setVisibility(View.GONE);
                View targetView = findViewById(R.id.gridview1);
                targetView.getParent().requestChildFocus(targetView, targetView);
                filter(newText);
                return false;
            }
        });
        subcatloadmore = findViewById(R.id.subcatloadmore);
        shopstatus = findViewById(R.id.shopstatus);
        filterloadmore = findViewById(R.id.filterloadmore);
        slidingrela = findViewById(R.id.slidingrela);
        orderAdapter = new FilterHeadersAdapter(context, featurePojoHeaders, this);
        gridView1 = findViewById(R.id.gridview1);
        gridView1.setAdapter(new ProductsnewAdapter(context, filtercategories, this,this));
        productsnewAdapter = new ProductsnewAdapter(context, categories, this,this);
        //   gridView1.smoothScrollToPosition(5);
        rel = findViewById(R.id.rel);
        filter = findViewById(R.id.filter);
        loadmoresort = findViewById(R.id.loadmoresort);
        filterloadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterbaseurl.equals("null")) {
                    Toast.makeText(context, "No more products ", Toast.LENGTH_SHORT).show();
                }
                if (!(filterbaseurl.equals("null"))) {
                    filterData(filterbaseurl);
                }
            }
        });
        cart = findViewById(R.id.cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductsInShop.this, Cart.class);
                intent.putExtra("from", "shop");
                startActivity(intent);
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.filter_layout, null);
                TextView close = popupView.findViewById(R.id.close);
                TextView apply = popupView.findViewById(R.id.apply);
                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                boolean focusable = false; // lets taps outside the popup also dismiss it
                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
                // show the popup window
                // which view you pass in doesn't matter, it is only used for the window tolken
                popupWindow.showAtLocation(rel, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(
                        android.graphics.Color.TRANSPARENT));
                rel.setAlpha(0.2F);
                TextView clear = popupView.findViewById(R.id.clear);
                clear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ApiClient.map.clear();
                        ApiClient.hashMap.clear();
                    }
                });
                apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ApiClient.map.size() > 0) {
                            popupWindow.dismiss();
                            rel.setAlpha(1.0F);
                            filterData(filterbaseurl);
                        }
                        if (ApiClient.map.size() == 0) {
                            Toast.makeText(context, "Please choose atleast one filter", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                RecyclerView first = popupView.findViewById(R.id.first);
                second = popupView.findViewById(R.id.second);
                adapter = new FilterValueAdapter(context, filters);
                first.setAdapter(orderAdapter);
                first.setNestedScrollingEnabled(false);
                first.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                // create the popup window

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        rel.setAlpha(1.0F);
                    }
                });
            }
        });
        sortrel = findViewById(R.id.sortrel);
        sortrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.products_onpopup, null);
                ImageView imageView = popupView.findViewById(R.id.image);
                TextView hightolow = popupView.findViewById(R.id.hightolow);
                TextView lowtohigh = popupView.findViewById(R.id.lowtohigh);
                TextView popularity = popupView.findViewById(R.id.popularity);
                TextView newarrival = popupView.findViewById(R.id.newarrival);
                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                boolean focusable = false; // lets taps outside the popup also dismiss it
                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
                popupWindow.showAtLocation(rel, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(
                        android.graphics.Color.TRANSPARENT));
                rel.setAlpha(0.2F);
                hightolow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        rel.setAlpha(1.0F);
                        categories.clear();
                        progressDialog.setTitle("Loading..");
                        progressDialog.setMessage("Fetching Products..");
                        progressDialog.setIcon(R.drawable.haahoologo);
                        progressDialog.show();
                        //getSortedData("high_to_low");
                        sortProducts("high_to_low");
                    }
                });
                lowtohigh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        rel.setAlpha(1.0F);
                        categories.clear();
                        progressDialog.setTitle("Loading..");
                        progressDialog.setMessage("Fetching Products..");
                        progressDialog.setIcon(R.drawable.haahoologo);
                        progressDialog.show();
                        //getSortedData("low_to_high");
                        sortProducts("low_to_high");
                    }
                });
                popularity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        rel.setAlpha(1.0F);
                        categories.clear();
                        progressDialog.setTitle("Loading..");
                        progressDialog.setMessage("Fetching Products..");
                        progressDialog.setIcon(R.drawable.haahoologo);
                        progressDialog.show();
                        //getSortedData("popularity");
                        sortProducts("popularity");
                    }
                });
                // create the popup window

                // show the popup window
                // which view you pass in doesn't matter, it is only used for the window tolken

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        rel.setAlpha(1.0F);
                    }
                });
                // dismiss the popup window when touched
//                popupView.setOnTouchListener(new View.OnTouchListener() {
//                    @Override
//                    public boolean onTouch(View v, MotionEvent event) {
//                        rel.setAlpha(1.0F);
//                        popupWindow.dismiss();
//
//                        return true;
//                    }
//                });
            }
        });
        pdtcount = findViewById(R.id.pdtcount);
        invite = findViewById(R.id.invite);
        tele = findViewById(R.id.tele);
        connectrel = findViewById(R.id.connectrel);
        connect = findViewById(R.id.connect);
        sliding = findViewById(R.id.sliding);
        location = findViewById(R.id.location);
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.map);
        closebtn = dialog.findViewById(R.id.close);
        closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        mMapView = (MapView) dialog.findViewById(R.id.mapView);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    try{

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    MapsInitializer.initialize(context);

                }
            }
        };
        Thread threadmap = new Thread(runnable);
        threadmap.start();
        mMapView.onCreate(dialog.onSaveInstanceState());
        mMapView.onResume();

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        // mapView = findViewById(R.id.mapView);
        mMapView.onCreate(mapViewBundle);
        mMapView.getMapAsync(this);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.show();

            }
        });
        searchView = findViewById(R.id.search);
        searchView.setOnQueryTextListener(this);
        searchView.setIconified(true);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.onActionViewExpanded();
            }
        });
        invited = findViewById(R.id.invited);
        connected = findViewById(R.id.connected);
        chat1 = findViewById(R.id.chat1);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Fetching Products...");
        progressDialog.setTitle("Loading");
        progressDialog.show();
        one1 = findViewById(R.id.one1);
        gridView = findViewById(R.id.gridview);
        loadmore = findViewById(R.id.loadmore);
        textView = findViewById(R.id.shopf);
        Runnable runnable22 = new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    try{

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    getDownloads();
                }
            }
        };
        Thread threaddownload = new Thread(runnable22);
        threaddownload.start();
        slidingLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        slidingLayout.setPanelSlideListener(onSlideListener());
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        });
        cover = findViewById(R.id.imageview);
        profile = findViewById(R.id.circle);
        //call = findViewById(R.id.call);
        call3 = findViewById(R.id.two);
        // onHideListener();

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                String membershipval = (pref.getString("membership", "null"));
                if (membershipval.equals("1")) {
                    if (connect.getText().toString().equals("Connect")) {
                        Toast.makeText(context, "Connect the shop first", Toast.LENGTH_SHORT).show();
                    }
                    if (connect.getText().toString().equals("Connected")) {
                        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    }
                }
                if (!(membershipval.equals("1"))) {
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                            .setTitle("Become Our Premium Member")
                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                startActivity(new Intent(context, MemberShipActivity.class));
                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                dialog.dismiss();
                            });
// Show the alert
                    builder.show();
                }
                //onShowListener();
            }
        });
        chat1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=" + "91" + phone_no + "&text=" + "");

                Intent sendIntent = new Intent(Intent.ACTION_VIEW, uri);

                context.startActivity(sendIntent);

            }
        });
        tele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phone_no));
                if (ContextCompat.checkSelfPermission(activity,
                        Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(activity,
                            new String[]{Manifest.permission.CALL_PHONE},
                            1);

                    // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                } else {
                    //You already have permission
                    try {
                        startActivity(callIntent);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        shop = findViewById(R.id.shopf);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        Bundle bundle = getIntent().getExtras();
        shop_id = bundle.getString("categoryid");
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("shop_id", shop_id);
        ApiClient.shop_id_del = shop_id;
        editor.commit();
        //getFilterHeads(shop_id);
        slidingrela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareLongDynamicLink(shop_id);
            }
        });

        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    try{

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    getSubCategories();
                }
            }
        };
       Thread threadsubcat = new Thread(runnable1);
       threadsubcat.start();

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (connect.getText().toString().equals("Connect")) {
                    connecttoshop(shop_id);
                }
                if (connect.getText().toString().equals("Connected")) {
                    disconnectshop(shop_id);
                }
            }
        });
        String shop_name = bundle.getString("categoryname");
        Log.d("shoppp_id", "shoppp" + shop_id + shop_name);
        //getproductsNew(shop_id,url);
        textView.setText(shop_name);
        shop.setText(shop_name);
        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    try{

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    getShopDetails(shop_id);
                }
            }
        };
        Thread threadshop = new Thread(runnable2);
        threadshop.start();

        //newAPI();
        //getproducts(shop_id,url);

        Runnable runnable3 = new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    try{

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    newAPI();
                }
            }
        };
        Thread threadprod = new Thread(runnable3);
        threadprod.start();
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiClient.shop_refer = "0";
                Intent intent = getIntent();
                if (intent.hasExtra("from")) {
                    startActivity(new Intent(context, NewHome.class));
                }
                if (!(intent.hasExtra("from"))) {
                    ProductsInShop.super.onBackPressed();
                }
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }

                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });

        Runnable runnable4 = new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    try{

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    getShopimages(shop_id);
                }
            }
        };
        Thread threadimage = new Thread(runnable4);
        threadimage.start();

    }

    public void showAlert(){

        View child = getLayoutInflater().inflate(R.layout.shop_closed_ui, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(child);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();
        alertDialog.setCancelable(false);
        CardView card = child.findViewById(R.id.card);
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductsInShop.super.onBackPressed();
            }
        });

    }


    public void shareLongDynamicLink(String shop_id) {
//        String query = "";
//        try {
//            query = URLEncoder.encode(String.format("&%1s=%2s", "id", "1234"), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        final String path = "https://haahooapp.page.link/?" +
                "link=" + /*link*/
                "https://www.google.com/?" +
                "shopid=" +
                shop_id + "+" + ApiClient.own_id +
                "&apn=" + /*getPackageName()*/
                "creo.com.haahooapp" +
                "&st=" + /*titleSocial*/
                "Download+Haahoo" +
                "&sd=" + /*description*/
                "" +
                "&utm_source=" + /*source*/
                "AndroidApp";
        Log.d("referpath", "ohh" + path);
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(path))
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created

                            Uri shortLink = task.getResult().getShortLink();
                            link = shortLink.toString();
                            Intent intent = new Intent();
//                            String path = buildDynamicLink();
                            Log.d("actual link", "linkkk" + link);
                            String msg = "visit my awesome website: " + link;
                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT, link);

                            Intent shareintent = Intent.createChooser(intent, null);
                            startActivity(shareintent);

                        } else {
                            // Error
                            // ...
                        }
                    }
                });

    }

    //to be done
    public void filterData(String url) {
        JSONObject mapval = new JSONObject(ApiClient.map);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, filterbaseurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("jhgfd", "ohgc" + obj);

                            filterbaseurl = obj.optString("next");
                            JSONArray jsonArray = obj.optJSONArray("results");

                            if (jsonArray.length() == 0) {
                                filtercategories.clear();
                                categories.clear();
                                subcatcategories.clear();
                                //loadmore.setVisibility(View.GONE);
                                subcatloadmore.setVisibility(View.GONE);
                                filterloadmore.setVisibility(View.GONE);
                                progressDialog.dismiss();
                                Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                            }
                            if (jsonArray.length() != 0) {
                                categories.clear();
                                subcatcategories.clear();
                                // filterloadmore.setVisibility(View.VISIBLE);
                                // loadmore.setVisibility(View.GONE);
                                subcatloadmore.setVisibility(View.GONE);
                                progressDialog.dismiss();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    Category category = new Category();
                                    category.setId(jsonObject.optString("id"));
                                    category.setName(jsonObject.optString("name"));
                                    category.setPrice(jsonObject.optString("discount"));
                                    category.setOriginal_price(jsonObject.optString("price"));
                                    category.setCart_status(jsonObject.optString("cart_status"));
                                    category.setRating(jsonObject.optString("pdt_rating"));
                                    String images1 = jsonObject.optString("image");
                                    category.setShopid(shop_id);
                                    String[] seperated = images1.split(",");
                                    String split = seperated[0].replace("[", "").replace("]", "");
                                    category.setImage(ApiClient.BASE_URL + split);
                                    filtercategories.add(category);
                                }
                            }
                            gridView1.setNestedScrollingEnabled(false);
                            gridView1.setLayoutManager(new GridLayoutManager(context, 2));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shop_id", shop_id);
                params.put("spec", mapval.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getShopDetails(String shop_id) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "virtual_shop/shop_pcp_det/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            String connect_count = obj.optString("connect_count");
                            String pdt_count = obj.optString("pdt_count");
                            String invite_count = obj.optString("rating_count") + "*";
                            pdtcount.setText(pdt_count);
                            connected.setText(connect_count);
                            invited.setText(invite_count);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shop_id", shop_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getDownloads() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/login_ref_count/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("refcount", "refff" + jsonObject);
                            String message = jsonObject.optString("message");
                            if (message.equals("Success")) {
                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                    StoryPojo storyPojo = new StoryPojo();
                                    storyPojo.setName(jsonObject1.optString("name"));
                                    storyPojo.setId(jsonObject1.optString("id"));
                                    storyPojo.setShop_id(getIntent().getExtras().getString("categoryid"));
                                    storyPojo.setToken(token);
                                    storyPojos.add(storyPojo);

                                }

                                slidingContentAdapter = new SlidingContentAdapter(context, storyPojos);
                                sliding.setAdapter(slidingContentAdapter);
                                sliding.setNestedScrollingEnabled(false);
                                sliding.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Downloads.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public void disconnectshop(String shop_id) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "virtual_shop/shop_unconnecting/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("connectingshop", "shopprods" + obj);
                            String message = obj.optString("message");
                            if (message.equals("success")) {
//                                Resources res = getResources();
//                                Drawable drawable = res.getDrawable(R.drawable.border_relative_available);
                                connect.setText("Connect");
                                int count = Integer.parseInt(connected.getText().toString()) - 1;
                                connected.setText(String.valueOf(count));

                            } else {
                                Toast.makeText(context, "Failed to connect", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shop_id", shop_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void connecttoshop(String shop_id) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));

        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "virtual_shop/shop_connecting/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("connectingshop", "shopprods" + obj);
                            String message = obj.optString("message");

                            if (message.equals("success")) {
                                Resources res = getResources();
                                Drawable drawable = res.getDrawable(R.drawable.border_relative_available);
                                connect.setText("Connected");
                                int count = Integer.parseInt(connected.getText().toString()) + 1;
                                connected.setText(String.valueOf(count));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shop_id", shop_id);
                params.put("referrar_user_id", ApiClient.shop_refer);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    private View.OnClickListener onHideListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide sliding layout

                //  btnShow.setVisibility(View.VISIBLE);
            }
        };
    }

    private View.OnClickListener onShowListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show sliding layout in bottom of screen (not expand it)

                //               // btnShow.setVisibility(View.GONE);
            }
        };
    }

    private SlidingUpPanelLayout.PanelSlideListener onSlideListener() {
        return new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
                //textView.setText("panel is sliding");
            }

            @Override
            public void onPanelCollapsed(View view) {
                // textView.setText("panel Collapse");
            }

            @Override
            public void onPanelExpanded(View view) {
                //textView.setText("panel expand");
            }

            @Override
            public void onPanelAnchored(View view) {
                //textView.setText("panel anchored");
            }

            @Override
            public void onPanelHidden(View view) {
                // textView.setText("panel is Hidden");
            }
        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;
        gmap.setMinZoomPreference(12);

    }

    public void getShopimages(String shop_id) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "api_shop_app/shop_images_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("productsinshop", "shopprods" + response);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            JSONObject object = jsonArray.optJSONObject(0);
                            String cover_image = object.optString("cover_image");
                            String prof = object.optString("profile");
                            phone_no = object.optString("phone");
                            lat = object.optString("lat");
                            log = object.optString("log");
                            LatLng ny = new LatLng(Double.parseDouble(lat), Double.parseDouble(log));
                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(ny);
                            gmap.addMarker(markerOptions);
                            gmap.moveCamera(CameraUpdateFactory.newLatLng(ny));
                            String connection_status = object.optString("connection_status");
                            String shop_status = object.optString("shop_status");
                            shopstatus.setText(shop_status);
                            if (shop_status.equals("Closed")) {
                                shopstatus.setTextColor(Color.parseColor("#E00000"));
                                showAlert();
                            }
                            if (!(shop_status.equals("Closed"))) {
                                shopstatus.setTextColor(Color.parseColor("#00b300"));
                            }
                            if (connection_status.equals("1")) {
                                connect.setText("Connected");
                            }
                            if (connection_status.equals("0")) {
                                connect.setText("Connect");
                            }
                            Glide.with(context).load(ApiClient.BASE_URL + prof.replace("[", "").replace("]", "")).into(profile);
                            Glide.with(context).load(ApiClient.BASE_URL + cover_image.replace("[", "").replace("]", "")).diskCacheStrategy(DiskCacheStrategy.ALL).into(cover);
                            cover.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, ImageZoom.class);
                                    intent.putExtra("image", ApiClient.BASE_URL + cover_image.replace("[", "").replace("]", ""));
                                    context.startActivity(intent);
                                }
                            });
                            profile.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, ImageZoom.class);
                                    intent.putExtra("image", ApiClient.BASE_URL + prof.replace("[", "").replace("]", ""));
                                    context.startActivity(intent);
                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", shop_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    public void getFilterHeads(String shopid) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "api_shop_app/shop_category_filter/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("filterheads", "shopprods" + response);
                            JSONArray jsonArray = obj.optJSONArray("spec_header");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                FeaturePojoHeader featurePojoHeader = new FeaturePojoHeader();
                                featurePojoHeader.setId(jsonObject.optString("id"));
                                featurePojoHeader.setName(jsonObject.optString("head" + i));
                                featurePojoHeaders.add(featurePojoHeader);
                                //valss.add(jsonObject.optString("head"+i));
                            }
                            JSONArray spec_value = obj.optJSONArray("spec_value");
                            for (int j = 0; j < spec_value.length(); j++) {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shop_id", shopid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void getSubCategories() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "api_shop_app/sub_under_shop/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("caterrr", "shopprods" + response);
                            JSONArray data = obj.optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject = data.optJSONObject(i);
                                HorizontalPojo horizontalPojo = new HorizontalPojo();
                                horizontalPojo.setId(jsonObject.optString("id"));
                                horizontalPojo.setName(jsonObject.optString("name"));
                                horizontalPojo.setImage(ApiClient.BASE_URL + jsonObject.optString("image").replace("[", "").replace("]", ""));
                                productFeaturepojos.add(horizontalPojo);
                            }
                            horizontal_item_adapter = new Horizontal_item_adapter(productFeaturepojos, context, ProductsInShop.this);
                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                            horizontal.setLayoutManager(horizontalLayoutManagaer);
                            horizontal.setAdapter(horizontal_item_adapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.dismiss();
            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shop_id", shop_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void newAPI() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), shop_id);
        Call<ResponseBody> callregister = apiInterface.getProducts(shop_id, "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    url = obj.optString("next");
                    //Log.d("PAGINATINGVAL", "APIII" + url.replace(ApiClient.BASE_URL + "api_shop_app/list_products/?page=", ""));
                    ApiClient.list_url = obj.optString("next");
                    JSONArray jsonArray = obj.optJSONArray("results");
                    if (jsonArray.length() == 0) {
                        categories.clear();
                        subcatcategories.clear();
                        filtercategories.clear();
                        // loadmore.setVisibility(View.GONE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {
                        subcatcategories.clear();
                        filtercategories.clear();
                        loadmore.setVisibility(View.VISIBLE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Category category = new Category();
                            category.setId(jsonObject.optString("id"));
                            category.setVariant_count(jsonObject.optString("variant_count"));
                            category.setName(jsonObject.optString("name"));
                            category.setPrice(jsonObject.optString("discount"));
                            category.setStock(jsonObject.optString("stock").trim());
                            category.setOriginal_price(jsonObject.optString("price"));
                            category.setCart_status(jsonObject.optString("cart_status"));
                            category.setRating(jsonObject.optString("pdt_rating"));
                            category.setShopid(shop_id);
                            String images1 = jsonObject.optString("image");
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            categories.add(category);
                        }
                    }
                    gridView1.setNestedScrollingEnabled(false);
                    GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
                    gridView1.setLayoutManager(layoutManager);
                    gridView1.setAdapter(productsnewAdapter);
                    progressDialog.dismiss();

                    loadmore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (url.equals("null")) {
                                Toast.makeText(context, "That's all for now", Toast.LENGTH_SHORT).show();
                            }
                            if (!(url.equals("null"))) {
                                page = Integer.parseInt(url.replace(ApiClient.BASE_URL + "api_shop_app/list_products/?page=", "").trim());
                                progressDialog.setTitle("Loading..");
                                progressDialog.setMessage("Fetching Products..");
                                progressDialog.setIcon(R.drawable.haahoologo);
                                progressDialog.show();
                                newAPIPaginate(page);
                                //getproducts(shop_id,url);
                            }
                        }
                    });
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    public void productsUnderSubcategory(String subcategory) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), shop_id);
        Call<ResponseBody> callregister = apiInterface.getProductsundersubcat(shop_id, subcategory, "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());


                    url = obj.optString("next");
                    //Log.d("PAGINATINGVAL", "APIII" + url.replace(ApiClient.BASE_URL + "api_shop_app/list_products/?page=", ""));
                    ApiClient.list_url = obj.optString("next");
                    JSONArray jsonArray = obj.optJSONArray("data");
                    if (jsonArray.length() == 0) {
                        categories.clear();
                        subcatcategories.clear();
                        filtercategories.clear();
                        // loadmore.setVisibility(View.GONE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {
                        categories.clear();
                        subcatcategories.clear();
                        filtercategories.clear();
                        loadmore.setVisibility(View.VISIBLE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Category category = new Category();
                            category.setId(jsonObject.optString("id"));
                            category.setVariant_count(jsonObject.optString("variant_count"));
                            category.setName(jsonObject.optString("name"));
                            category.setPrice(jsonObject.optString("discount"));
                            category.setStock(jsonObject.optString("stock").trim());
                            category.setOriginal_price(jsonObject.optString("price"));
                            category.setCart_status(jsonObject.optString("cart_status"));
                            category.setRating(jsonObject.optString("pdt_rating"));
                            category.setShopid(shop_id);
                            String images1 = jsonObject.optString("image");
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            categories.add(category);
                        }
                    }
                    gridView1.setNestedScrollingEnabled(false);
                    GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
                    gridView1.setLayoutManager(layoutManager);
                    gridView1.setAdapter(productsnewAdapter);
                    progressDialog.dismiss();

                    loadmore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (url.equals("null")) {
                                Toast.makeText(context, "That's all for now", Toast.LENGTH_SHORT).show();
                            }
                            if ((url.contains("api_shop_app/shops_pdt_cat_based/"))) {
                                page = Integer.parseInt(url.replace(ApiClient.BASE_URL + "api_shop_app/shops_pdt_cat_based/?page=", "").trim());
                                progressDialog.setTitle("Loading..");
                                progressDialog.setMessage("Fetching Products..");
                                progressDialog.setIcon(R.drawable.haahoologo);
                                progressDialog.show();
                                productsUnderSubcategoryPaginate(subcategory);
                                //getproducts(shop_id,url);
                            }
                        }
                    });
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    public void productsUnderSubcategoryPaginate(String subcategory) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), shop_id);
        Call<ResponseBody> callregister = apiInterface.getProductsundersubcatPaginate(shop_id, subcategory, "Token " + token, page);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());


                    url = obj.optString("next");
                    //Log.d("PAGINATINGVAL", "APIII" + url.replace(ApiClient.BASE_URL + "api_shop_app/list_products/?page=", ""));
                    ApiClient.list_url = obj.optString("next");
                    JSONArray jsonArray = obj.optJSONArray("data");
                    if (jsonArray.length() == 0) {
                        categories.clear();
                        subcatcategories.clear();
                        filtercategories.clear();
                        // loadmore.setVisibility(View.GONE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {
                        subcatcategories.clear();
                        filtercategories.clear();
                        loadmore.setVisibility(View.VISIBLE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Category category = new Category();
                            category.setId(jsonObject.optString("id"));
                            category.setVariant_count(jsonObject.optString("variant_count"));
                            category.setName(jsonObject.optString("name"));
                            category.setPrice(jsonObject.optString("discount"));
                            category.setStock(jsonObject.optString("stock").trim());
                            category.setOriginal_price(jsonObject.optString("price"));
                            category.setCart_status(jsonObject.optString("cart_status"));
                            category.setRating(jsonObject.optString("pdt_rating"));
                            category.setShopid(shop_id);
                            String images1 = jsonObject.optString("image");
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            categories.add(category);
                        }
                    }
                    gridView1.setNestedScrollingEnabled(false);
                    GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
                    gridView1.setLayoutManager(layoutManager);
                    gridView1.setAdapter(productsnewAdapter);
                    progressDialog.dismiss();

                    loadmore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (url.equals("null")) {
                                Toast.makeText(context, "That's all for now", Toast.LENGTH_SHORT).show();
                            }
                            if ((url.contains("api_shop_app/shops_pdt_cat_based/"))) {
                                page = Integer.parseInt(url.replace(ApiClient.BASE_URL + "api_shop_app/shops_pdt_cat_based/?page=", "").trim());
                                progressDialog.setTitle("Loading..");
                                progressDialog.setMessage("Fetching Products..");
                                progressDialog.setIcon(R.drawable.haahoologo);
                                progressDialog.show();
                                productsUnderSubcategoryPaginate(subcategory);
                            }
                        }
                    });
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    public void newAPIPaginate(int page1) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestBody body =
                RequestBody.create(MediaType.parse("text/plain"), shop_id);
        Call<ResponseBody> callregister = apiInterface.getProductspaginate(shop_id, "Token " + token, page);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                Log.d("NEWWAPIII", "APIII" + url);

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());


                    url = obj.optString("next");
                    ApiClient.list_url = obj.optString("next");
                    JSONArray jsonArray = obj.optJSONArray("results");
                    if (jsonArray.length() == 0) {
                        categories.clear();
                        subcatcategories.clear();
                        filtercategories.clear();
                        //  loadmore.setVisibility(View.GONE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {
                        subcatcategories.clear();
                        filtercategories.clear();
                        loadmore.setVisibility(View.VISIBLE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);
                        progressDialog.dismiss();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Category category = new Category();
                            category.setId(jsonObject.optString("id"));

                            category.setName(jsonObject.optString("name"));
                            category.setPrice(jsonObject.optString("discount"));
                            category.setStock(jsonObject.optString("stock").trim());
                            category.setOriginal_price(jsonObject.optString("price"));
                            category.setCart_status(jsonObject.optString("cart_status"));
                            category.setRating(jsonObject.optString("pdt_rating"));
                            category.setVariant_count(jsonObject.optString("variant_count"));
                            category.setShopid(shop_id);
                            String images1 = jsonObject.optString("image");
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            categories.add(category);
                        }
                    }
                    gridView1.setNestedScrollingEnabled(false);
                    GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
                    gridView1.setLayoutManager(layoutManager);
                    gridView1.setAdapter(productsnewAdapter);
                    progressDialog.dismiss();

                    loadmore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (url.equals("null")) {
                                Toast.makeText(context, "That's all for now", Toast.LENGTH_SHORT).show();
                            }
                            if ((url.contains("api_shop_app/list_products"))) {

                                page = Integer.parseInt(url.replace(ApiClient.BASE_URL + "api_shop_app/list_products/?page=", "").trim());
                                progressDialog.setTitle("Loading..");
                                progressDialog.setMessage("Fetching Products..");
                                progressDialog.setIcon(R.drawable.haahoologo);
                                progressDialog.show();
                                newAPIPaginate(page);
                            }
                        }
                    });
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        String userinput = newText.toLowerCase();

        //Log.d("inputt","user"+userinput);
        ArrayList<StoryPojo> storyPojos1 = new ArrayList<>();
        for (StoryPojo storyPojo : storyPojos) {
            if (storyPojo.getName().toLowerCase().contains(userinput)) {
                storyPojos1.add(storyPojo);
            }
            slidingContentAdapter.updateList(storyPojos1);
        }
        return true;
    }

    @Override
    public void getFilterId(String filter_id, String filtername) {

        filters.clear();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Filters...");
        progressDialog.setTitle("Loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.show();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/listing_specification_values/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String values = jsonObject.optString("spec_val");
                            String[] sepaerated = values.split(",");
                            Log.d("ghghghghg", "jsonarray" + sepaerated.length);
                            for (int i = 0; i < sepaerated.length; i++) {
                                FeaturePojoValues featurePojoValues = new FeaturePojoValues();
                                featurePojoValues.setHeader(filtername);
                                featurePojoValues.setName((sepaerated[i].replace("[", "").replace("]", "").replace("'", "")));
                                filters.add(featurePojoValues);
                            }
                            adapter = new FilterValueAdapter(context, filters);
//                            notify();
                            progressDialog.dismiss();
                            second.setAdapter(adapter);
                            second.setNestedScrollingEnabled(false);
                            second.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                //

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                if (filtername.equals("price")) {
                    params.put("id", "price");
                    params.put("cat_id", "0");
                    params.put("spec_name", "price");
                }
                if (filtername.equals("rating")) {
                    params.put("id", "rating");
                    params.put("cat_id", "0");
                    params.put("spec_name", "rating");
                }
                if (!(filtername.equals("price"))) {
                    if (!(filtername.equals("rating"))) {
                        Log.d("hjbhjb", "true");
                        params.put("id", filter_id);
                        params.put("cat_id", ApiClient.cate_id);
                        params.put("spec_name", filtername);
                    }
                }


                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    @Override
    public void setsubcategoryId(String subcategory) {
        progressDialog.setTitle("Loading..");
        progressDialog.setMessage("Fetching Products..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.show();
        productsUnderSubcategory(subcategory);

    }

    @Override
    public void variant(String variant) {

        BottomSheet.Builder builder = new BottomSheet.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.bottom_sheet_ui, rel, false);
        builder.setCustomView(view);
        list = view.findViewById(R.id.list);
        progressDialog.setTitle("Loading..");
        progressDialog.setMessage("Finding Product Variants");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.show();
        builder.show();

        showVariants(variant);


    }

    public void showVariants(String variant) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestBody body =
                RequestBody.create(MediaType.parse("text/plain"), variant);
        Call<ResponseBody> callregister = apiInterface.getProductVariants(variant, "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    if (obj.optJSONArray("data") != null) {
                        JSONArray data = obj.optJSONArray("data");
                        lists1.clear();
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jsonObject = data.optJSONObject(i);
                            ProductVariant productVariant = new ProductVariant();
                            productVariant.setId(jsonObject.optString("product_id"));
                            productVariant.setName(jsonObject.optString("variant_name"));
                            productVariant.setImage(ApiClient.BASE_URL + "media/" + jsonObject.optString("variant_img"));
                            productVariant.setVariant_value(jsonObject.optString("variant_value"));
                            productVariant.setPrice("Rs." + jsonObject.optString("fixed_price"));
                            lists1.add(productVariant);
                        }
                        list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        list.setAdapter(adapter1);
                    }

                    progressDialog.dismiss();


                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void addProduct(String productid, String price1, String edited_price) {

    }

    @Override
    public void sendData(String id) {
        try {
            Intent intent = new Intent(context, ProductDetailsShop.class);
            intent.putExtra("productid", id);
            intent.putExtra("shopid", shop_id);
            startActivity(intent);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendData(int count1) {
        count.setText(String.valueOf(count1));
    }

    static class EndlessScrollListener extends RecyclerView.OnScrollListener {
        private boolean isLoading;
        private boolean hasMorePages;
        private int pageNumber = 0;
        private RefreshList refreshList;
        private boolean isRefreshing;
        private int pastVisibleItems;

        EndlessScrollListener(RefreshList refreshList) {
            this.isLoading = false;
            this.hasMorePages = true;
            this.refreshList = refreshList;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            StaggeredGridLayoutManager manager =
                    (StaggeredGridLayoutManager) recyclerView.getLayoutManager();

            int visibleItemCount = manager.getChildCount();
            int totalItemCount = manager.getItemCount();
            int[] firstVisibleItems = manager.findFirstVisibleItemPositions(null);
            if (firstVisibleItems != null && firstVisibleItems.length > 0) {
                pastVisibleItems = firstVisibleItems[0];
            }

            if (visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                isLoading = true;
                if (hasMorePages && !isRefreshing) {
                    isRefreshing = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refreshList.onRefresh(pageNumber);
                        }
                    }, 200);
                }
            } else {
                isLoading = false;
            }
        }

        public void noMorePages() {
            this.hasMorePages = false;
        }

        void notifyMorePages() {
            isRefreshing = false;
            pageNumber = pageNumber + 1;
        }

        interface RefreshList {
            void onRefresh(int pageNumber);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mMapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public void sortProductsPaginate(String query) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), shop_id);
        Call<ResponseBody> callregister = apiInterface.getProductsSortedpaginate(shop_id, query, "Token " + token, page);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());


                    url = obj.optString("next");
                    Log.d("hgtyxtrxFFF", "hjdty" + url);
                    //Log.d("PAGINATINGVAL", "APIII" + url.replace(ApiClient.BASE_URL + "api_shop_app/list_products/?page=", ""));
                    ApiClient.list_url = obj.optString("next");
                    JSONArray jsonArray = obj.optJSONArray("results");
                    if (jsonArray.length() == 0) {
                        categories.clear();
                        subcatcategories.clear();
                        filtercategories.clear();
                        // loadmore.setVisibility(View.GONE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {
                        subcatcategories.clear();
                        filtercategories.clear();
                        loadmore.setVisibility(View.VISIBLE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Category category = new Category();
                            category.setId(jsonObject.optString("id"));
                            category.setVariant_count(jsonObject.optString("variant_count"));
                            category.setName(jsonObject.optString("name"));
                            category.setPrice(jsonObject.optString("discount"));
                            category.setStock(jsonObject.optString("stock").trim());
                            category.setOriginal_price(jsonObject.optString("price"));
                            category.setCart_status(jsonObject.optString("cart_status"));
                            category.setRating(jsonObject.optString("pdt_rating"));
                            category.setShopid(shop_id);
                            String images1 = jsonObject.optString("image");
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            categories.add(category);
                        }
                    }
                    gridView1.setNestedScrollingEnabled(false);
                    GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
                    gridView1.setLayoutManager(layoutManager);

                    gridView1.setAdapter(productsnewAdapter);
                    progressDialog.dismiss();

                    loadmore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (url.equals("null")) {
                                Toast.makeText(context, "That's all for now", Toast.LENGTH_SHORT).show();
                            }
                            if ((url.contains("api_shop_app/sorting_pdt"))) {
                                page = Integer.parseInt(url.replace(ApiClient.BASE_URL + "api_shop_app/sorting_pdt/?page=", "").trim());
                                progressDialog.setTitle("Loading..");
                                progressDialog.setMessage("Fetching Products..");
                                progressDialog.setIcon(R.drawable.haahoologo);
                                progressDialog.show();
                                sortProductsPaginate(query);
                                //getproducts(shop_id,url);
                            }
                        }
                    });
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    public void sortProducts(String query) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), shop_id);
        Call<ResponseBody> callregister = apiInterface.getProductsSorted(shop_id, query, "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());


                    url = obj.optString("next");
                    Log.d("hgtyxtrx", "hjdty" + url);
                    //Log.d("PAGINATINGVAL", "APIII" + url.replace(ApiClient.BASE_URL + "api_shop_app/list_products/?page=", ""));
                    ApiClient.list_url = obj.optString("next");
                    JSONArray jsonArray = obj.optJSONArray("results");
                    if (jsonArray.length() == 0) {
                        categories.clear();
                        subcatcategories.clear();
                        filtercategories.clear();
                        // loadmore.setVisibility(View.GONE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        Toast.makeText(context, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {
                        subcatcategories.clear();
                        filtercategories.clear();
                        loadmore.setVisibility(View.VISIBLE);
                        filterloadmore.setVisibility(View.GONE);
                        subcatloadmore.setVisibility(View.GONE);

//                                JSONObject object = jsonArray.optJSONObject(0);
//                                String cover_image = object.optString("cover_image");
//                                String prof = object.optString("profile");
//                                Glide.with(context).load(ApiClient.BASE_URL+prof.replace("[","").replace("]","")).into(profile);
//                                if (cover_image.length() != 0){
//                                    //Log.d("ciohihiuh","ckjhjhgj"+cover_image);
//                                    Glide.with(context).load(ApiClient.BASE_URL+cover_image.replace("[","").replace("]","")).into(cover);
//                                }
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Category category = new Category();
                            category.setId(jsonObject.optString("id"));
                            category.setVariant_count(jsonObject.optString("variant_count"));
                            category.setName(jsonObject.optString("name"));
                            category.setPrice(jsonObject.optString("discount"));
                            category.setStock(jsonObject.optString("stock").trim());
                            category.setOriginal_price(jsonObject.optString("price"));
                            category.setCart_status(jsonObject.optString("cart_status"));
                            category.setRating(jsonObject.optString("pdt_rating"));
                            category.setShopid(shop_id);
                            String images1 = jsonObject.optString("image");
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            categories.add(category);
                        }
                    }
                    gridView1.setNestedScrollingEnabled(false);
                    GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
                    gridView1.setLayoutManager(layoutManager);

                    gridView1.setAdapter(productsnewAdapter);
                    progressDialog.dismiss();


                    loadmore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (url.equals("null")) {
                                Toast.makeText(context, "That's all for now", Toast.LENGTH_SHORT).show();
                            }
                            if ((url.contains("api_shop_app/sorting_pdt"))) {
                                page = Integer.parseInt(url.replace(ApiClient.BASE_URL + "api_shop_app/sorting_pdt/?page=", "").trim());
                                progressDialog.setTitle("Loading..");
                                progressDialog.setMessage("Fetching Products..");
                                progressDialog.setIcon(R.drawable.haahoologo);
                                progressDialog.show();
                                sortProductsPaginate(query);
                            }
                        }
                    });
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private void filter(String s) {
        ArrayList<Category> pojos = new ArrayList<Category>();
        for (Category hospitalsPojo : categories) {
            if (hospitalsPojo.getName().toLowerCase().contains(s.toLowerCase())) {
                // Log.d("ffdsddfb","xgfdgd"+s+hospitalsPojo.getName());
                pojos.add(hospitalsPojo);
            }
        }
//        gridView1.getLayoutManager().scrollToPosition(0);
        //  gridView1.smoothScrollToPosition(productsnewAdapter.getItemCount() -1);

        productsnewAdapter.filteredlist(pojos);
        productsnewAdapter.notifyDataSetChanged();


//        scroll.post(new Runnable() {
//            public void run() {
//                scroll.setSmoothScrollingEnabled(true);
//                scroll.smoothScrollBy(0,500);
//            }
//        });
    }


    private void scrollToBottom(final RecyclerView recyclerView) {
        // scroll to last item to get the view of last item
        final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        final RecyclerView.Adapter adapter = recyclerView.getAdapter();
        final int lastItemPosition = adapter.getItemCount() - 1;

        layoutManager.scrollToPositionWithOffset(lastItemPosition, 0);
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                // then scroll to specific offset
                View target = layoutManager.findViewByPosition(lastItemPosition);
                if (target != null) {
                    int offset = recyclerView.getMeasuredHeight() - target.getMeasuredHeight();
                    layoutManager.scrollToPositionWithOffset(lastItemPosition, offset);
                }
            }
        });
    }

    public void cartCount() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "cart_view/cart_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            String total_count = jsonObject1.optString("total all count");
                            if (total_count.equals("")) {
                                total_count = "0";
                                //ApiClient.count = Integer.valueOf(total_count);

                            }
                            if (!(total_count.equals(""))) {
                                //ApiClient.count = Integer.valueOf(total_count);
                                //count.setText(total_count);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {

        ApiClient.shop_refer = "0";
        Intent intent = getIntent();
        if (intent.hasExtra("from")) {
            startActivity(new Intent(context, NewHome.class));
        }
        if (!(intent.hasExtra("from"))) {
            super.onBackPressed();
        }
    }
}