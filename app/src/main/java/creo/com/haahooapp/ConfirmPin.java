package creo.com.haahooapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import creo.com.haahooapp.Implementation.SetPinPresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.SetPinCallBack;
import creo.com.haahooapp.interfaces.SetPinPresenter;
import creo.com.haahooapp.interfaces.SetPinView;

public class ConfirmPin extends AppCompatActivity implements SetPinCallBack,SetPinView{
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    String code = null;
    String token = null;
    SetPinPresenter setPinPresenter;
    String device_id = null;
    Activity activity = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_pin);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        setPinPresenter = new SetPinPresenterImpl(this);
        Bundle bundle = getIntent().getExtras();
        code = bundle.getString("code");
        device_id =  Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        //attach lock view with dot indicator
        mPinLockView.attachIndicatorDots(mIndicatorDots);

        //set lock code length
        mPinLockView.setPinLength(4);

        //set listener for lock code change
        mPinLockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {

                //User input true code
                if (pin.equals(code)) {
//                    Intent intent = new Intent(ConfirmPin.this, MainActivity.class);
//                    intent.putExtra("code", pin);
//                    startActivity(intent);
//                    finish();

                    setPinPresenter.setPin(device_id,pin,token);
                } else {
                    Toast.makeText(ConfirmPin.this, "Failed code, try again!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ConfirmPin.this,SetPin.class));
                }
            }

            @Override
            public void onEmpty() {

            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {

            }
        });
    }

    @Override
    public void onVerifySuccess(String data) {

    }

    @Override
    public void onVerifyFailed(String msg) {

    }

    @Override
    public void onSuccess(String response) {

        if(response.equals("success")) {
            if(ApiClient.product_referral_id.equals("0")) {
                startActivity(new Intent(ConfirmPin.this, NewHome.class));
            }
            if (!(ApiClient.product_referral_id.equals("0"))){
                Intent intent = new Intent(ConfirmPin.this,ShareProductDetails.class);
                intent.putExtra("id",ApiClient.product_referral_id);
                startActivity(intent);
            }

            if (!(ApiClient.productinshopid.equals("0"))){
                Intent intent = new Intent(ConfirmPin.this,ProductDetailsShop.class);
                intent.putExtra("shopid","1");
                intent.putExtra("productid",ApiClient.productinshopid);
                startActivity(intent);
            }
        }

    }

    @Override
    public void onFailed(String response) {

    }

    @Override
    public void noInternetConnection() {

    }
}
