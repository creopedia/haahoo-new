package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import creo.com.haahooapp.Adapters.EventMoreAdapter;
import creo.com.haahooapp.Adapters.InfiniteScrolling;
import creo.com.haahooapp.Modal.ProductPojo;
import creo.com.haahooapp.config.ApiClient;

public class MoreEvents extends AppCompatActivity {
    GridView gridView;
    ImageView back;
    ProgressDialog progressDialog;
    Activity activity = this;
    EventMoreAdapter imageAdapter = null;
    Context context = this;
    private InfiniteScrolling infiniteScrolling;

    ArrayList<String> ids = new ArrayList<>();
    ArrayList<String> date = new ArrayList<>();
    ArrayList<String> place = new ArrayList<>();
    String next_api= ApiClient.BASE_URL+"events/events_view/";
    ArrayList<String> names = new ArrayList<>();
    ArrayList<String> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_events);
        Window window = activity.getWindow();
        infiniteScrolling = new InfiniteScrolling();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoreEvents.super.onBackPressed();
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(MoreEvents.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(MoreEvents.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(MoreEvents.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context, MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                        }

                        return true;

                    }
                });
        infiniteScrolling.setOnReachEndListener(new InfiniteScrolling.OnReachEndListener() {
            @Override
            public void onEndReached() {
                getProducts123(next_api);
            }
        });
        gridView = findViewById(R.id.simpleGridView);
        gridView.setOnScrollListener(infiniteScrolling);
        progressDialog = new ProgressDialog(this,R.style.MyAlertDialogStyle);

        getProducts123(next_api);



    }

    public void getProducts123(String url){

        if(url.equals("null")){
            Toast.makeText(MoreEvents.this,"That's all for now" ,Toast.LENGTH_SHORT).show();
        }
        if(!(url.equals("null"))) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                next_api = jsonObject.optString("next");
                                String results = jsonObject.optString("results");
                                JSONArray jsonArray = new JSONArray(results);
                                for (int i = 0 ; i<jsonArray.length();i++){
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                    ids.add(jsonObject1.optString("pk"));
                                    JSONObject jsonObject2 = jsonObject1.optJSONObject("fields");
                                    names.add(jsonObject2.optString("name"));
                                    date.add(jsonObject2.optString("date"));
                                    place.add(jsonObject2.optString("place"));
                                    String images1 = jsonObject2.optString("image");
                                    String[] seperated = images1.split(",");
                                    String split = seperated[0].replace("[","");
                                    images.add(split);
                                }

                                final List<ProductPojo> productPojos = new ArrayList<>();
                                for (int i = 0; i < names.size(); i++) {
                                    ProductPojo productPojo = new ProductPojo(names.get(i), date.get(i), images.get(i),images.get(i),images.get(i),images.get(i));
                                    productPojos.add(productPojo);
                                }
                                infiniteScrolling.notifyItemsCountChanged();
                                if (imageAdapter == null) {
                                    imageAdapter = new EventMoreAdapter(context, productPojos);
                                    gridView.setAdapter(imageAdapter);
                                } else {
                                    imageAdapter.updateData(productPojos);
                                    imageAdapter.notifyDataSetChanged();
                                }
                                //Log.d("eventsres","jhvjhv"+response);
//                                next_api = jsonObject.getString("next");
//
//                                JSONArray jsonArray = new JSONArray(results);
//                                Log.d("response222", "res" + jsonArray.length());
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                                    ids.add(jsonObject1.getString("pk"));
//                                    JSONObject jsonObject2 = jsonObject1.getJSONObject("fields");
//                                    names.add(jsonObject2.getString("name"));
//
//                                    description.add("₹ " + jsonObject2.getString("price"));
//                                    String images1 = jsonObject2.getString("image");
//                                    String[] seperated = images1.split(",");
//                                    String split = seperated[0].replace("[", "");
//                                    images.add(split);
//
//                                }
//                                final List<ProductPojo> productPojos = new ArrayList<>();
//                                for (int i = 0; i < names.size(); i++) {
//                                    ProductPojo productPojo = new ProductPojo(names.get(i), description.get(i), images.get(i));
//                                    productPojos.add(productPojo);
//                                }
//
//                                infiniteScrolling.notifyItemsCountChanged();
//
//                                if (imageAdapter == null) {
//                                    imageAdapter = new ImageAdapter(context, productPojos);
//                                    gridView.setAdapter(imageAdapter);
//                                } else {
//                                    imageAdapter.updateData(productPojos);
//                                    imageAdapter.notifyDataSetChanged();
//                                }
//
//
                                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    public void onItemClick(AdapterView<?> parent, View v,
                                                            int position, long id) {
                                        Intent intent = new Intent(MoreEvents.this, Details.class);
                                        intent.putExtra("id", ids.get(position));
                                        startActivity(intent);
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MoreEvents.this, error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

}
