package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.B2BListAdapter;
import creo.com.haahooapp.Implementation.PinVerifyPresenterImpl;
import creo.com.haahooapp.Modal.ProductB2BPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.PinVerifyCallback;
import creo.com.haahooapp.interfaces.PinVerifyPresenter;
import creo.com.haahooapp.interfaces.PinVerifyView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class B2B extends AppCompatActivity implements PinVerifyCallback,PinVerifyView {
    RecyclerView recyclerView;
    TextView customername,proceed;
    private B2BListAdapter b2BListAdapter;
    ArrayList<ProductB2BPojo> dataModelArrayList;
    PinVerifyPresenter pinVerifyPresenter;
    ImageView back;
    ArrayList<String>ids = new ArrayList<>();
    Activity activity = this;
    ArrayList<String>names = new ArrayList<>();
    ArrayList<String>images = new ArrayList<>();
    ArrayList<String> shop_names = new ArrayList<>();
    ProgressDialog progressDialog;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b2_b);
        Window window = activity.getWindow();
        customername = findViewById(R.id.customername);
        proceed = findViewById(R.id.proceed);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                B2B.super.onBackPressed();
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(B2B.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(B2B.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(B2B.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
        ApiClient.ids.clear();
        ApiClient.prices.clear();
        ApiClient.quantity.clear();
        ApiClient.images.clear();
        ApiClient.name.clear();
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApiClient.directsell_cartcount.equals("0")){
                    Toast.makeText(B2B.this,"Please select atleast one product",Toast.LENGTH_SHORT).show();
                }
                if (!(ApiClient.directsell_cartcount.equals("0"))) {
                    startActivity(new Intent(B2B.this, OrderSummarySell.class));
                }
            }
        });
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        recyclerView = findViewById(R.id.recyclerView);
        progressDialog = new ProgressDialog(this);
        Bundle bundle = getIntent().getExtras();
        String customer_name = bundle.getString("customer_name");
        customername.setText(customer_name);
        progressDialog.setCancelable(false);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Loading , Please Wait...");
        progressDialog.show();
        dataModelArrayList = new ArrayList<>();
        shop_names.add("Choose Shop ");
        pinVerifyPresenter = new PinVerifyPresenterImpl(this);
        pinVerifyPresenter.getProducts(ApiClient.BASE_URL+"product_details/product_view/");

    }

    private void setupRecycler(){

        b2BListAdapter = new B2BListAdapter(dataModelArrayList,this);
        recyclerView.setAdapter(b2BListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

    }



    @Override
    public void onVerifySuccess(String data, String mem) {

    }

    @Override
    public void onVerifyFailed(String msg) {

    }

    @Override
    public void jsonObjectSuccess(JSONArray jsonObject, String next) {

    }


    @Override
    public void onSuccess(String response, String mem) {

    }

    @Override
    public void onFailed(String response) {

    }

    @Override
    public void jsonSUccess(JSONArray jsonObject, String next) {
        try {
            progressDialog.dismiss();
            jsonObject = jsonObject;
            ApiClient.search_name.clear();
            for (int i = 0; i < jsonObject.length(); i++) {
                ProductB2BPojo productB2BPojo = new ProductB2BPojo();
                JSONObject jsonObject2 = jsonObject.getJSONObject(i);
                ids.add(jsonObject2.getString("pk"));
                productB2BPojo.setId(jsonObject2.optString("pk"));
                JSONObject jsonObject3 = jsonObject2.getJSONObject("fields");
                names.add(jsonObject3.getString("name"));
                productB2BPojo.setProduct_name(jsonObject3.optString("name"));
                productB2BPojo.setPrice("₹ "+jsonObject3.optString("price"));
                ApiClient.search_name.add(jsonObject3.optString("name"));
                String images1 = jsonObject3.getString("image");
                String[] seperated = images1.split(",");
                String split = seperated[0].replace("[", "");
                images.add(split);
                productB2BPojo.setQuantity("0");
                productB2BPojo.setImage(ApiClient.BASE_URL+"media/"+split);
                Log.d("jhgvgughg","images"+split);
                dataModelArrayList.add(productB2BPojo);
            }
            setupRecycler();
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }
}
