package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.devs.readmoreoption.ReadMoreOption;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.viewpagerindicator.CirclePageIndicator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import androidx.viewpager.widget.ViewPager;
import creo.com.haahooapp.Adapters.SlidingImage_Adapter;
import creo.com.haahooapp.Implementation.CartPresenterImpl;
import creo.com.haahooapp.Implementation.ProductDetailPresenterImpl;
import creo.com.haahooapp.Implementation.WishListPresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.CartCallBack;
import creo.com.haahooapp.interfaces.CartPresenter;
import creo.com.haahooapp.interfaces.CartView;
import creo.com.haahooapp.interfaces.ProductDetailCallback;
import creo.com.haahooapp.interfaces.ProductDetailPresenter;
import creo.com.haahooapp.interfaces.ProductDetailView;
import creo.com.haahooapp.interfaces.WishListCallBack;
import creo.com.haahooapp.interfaces.WishListPresenter;
import creo.com.haahooapp.interfaces.WishListView;

public class ShareProductDetails extends AppCompatActivity implements ProductDetailView,ProductDetailCallback,WishListView,WishListCallBack,CartView,CartCallBack {

    private static ViewPager mPager;
    private static int currentPage = 0;
    ProgressDialog progressDialog;
    ArrayList<String> featur = new ArrayList<>();
    String link = null;
    ImageView fav,fav_fill;
    private static int NUM_PAGES = 0;
    TextView check, avail,features,pin;
    TextInputEditText pincode;
    TextView addTocart,gotocart,key,key2,morefeat,change,delvry,sold_by;
    RelativeLayout relativeLayout,more;
    JSONArray jsonArray1 = new JSONArray();
    ArrayList<String> imgs = new ArrayList<>();
    Activity activity = this;
    PopupWindow popupWindow;
    String id = null;
    CartPresenter cartPresenter;
    WishListPresenter wishListPresenter;
    Context context = this;
    ScrollView scrollView;
    RelativeLayout defaulte;
    ImageView whatsapp,back;
    String descrip = "null";
    RelativeLayout sharemajor;
    TextView buynow,nametext,pricetxt,whatsapptext,ret;
    TextView desctxt;
    ProductDetailPresenter productDetailPresenter;
    private static final Integer[] IMAGES = {};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private ArrayList<String>images = new ArrayList<>();
    RelativeLayout relativeLayout1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_product_details);
        Window window = activity.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        delvry = findViewById(R.id.delvry);
        sold_by = findViewById(R.id.sold_by);
        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        more = findViewById(R.id.more);
        sharemajor = findViewById(R.id.sharemajor);
        scrollView = findViewById(R.id.scroll);
        features = findViewById(R.id.features);
        morefeat = findViewById(R.id.morefeat);
        defaulte = findViewById(R.id.defaulte);
        ret = findViewById(R.id.ret);
        relativeLayout1 = findViewById(R.id.del);
        pin = findViewById(R.id.pin);
        key2 = findViewById(R.id.key2);
        change = findViewById(R.id.change);

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(0, scrollView.getBottom());
                relativeLayout1.setVisibility(View.VISIBLE);
            }
        });

        back = findViewById(R.id.back);
        avail = findViewById(R.id.avail);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ShareProductDetails.this,NewHome.class));
            }
        });
        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        relativeLayout = findViewById(R.id.relative);
        check = findViewById(R.id.check);
        pincode = findViewById(R.id.pincode);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(ShareProductDetails.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(ShareProductDetails.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(ShareProductDetails.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context,MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context,AllStories.class));
                                break;

                        }
                        return true;
                    }
                });
        wishListPresenter = new WishListPresenterImpl(this);
        cartPresenter = new CartPresenterImpl(this);
        progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        fav = findViewById(R.id.fav);
        addTocart = findViewById(R.id.addTocart);
        whatsapptext = findViewById(R.id.textwhatsapp);
        whatsapp = findViewById(R.id.whatsapp);
        key = findViewById(R.id.key);
        gotocart = findViewById(R.id.gotocart);
        fav_fill = findViewById(R.id.fav_fill);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wishListPresenter.addToWishList(getIntent().getExtras().getString("id"), token);
                fav.setVisibility(View.GONE);
                fav_fill.setVisibility(View.VISIBLE);
            }
        });
        fav_fill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wishListPresenter.removeFromWishList(getIntent().getExtras().getString("id"), token);
                fav_fill.setVisibility(View.GONE);
                fav.setVisibility(View.VISIBLE);

            }
        });
        addTocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater layoutInflater = (LayoutInflater) ShareProductDetails.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.popup, null);
                Button closePopupBtn = (Button) customView.findViewById(R.id.closePopupBtn);
                TextView decrease = customView.findViewById(R.id.decrease);
                TextView increase = customView.findViewById(R.id.increase);
                final TextView quantity = customView.findViewById(R.id.qty);
                TextView apply = customView.findViewById(R.id.apply);
                apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cartPresenter.addToCart(getIntent().getExtras().getString("id"), quantity.getText().toString(), token);
                    }
                });
                increase.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Integer.parseInt(quantity.getText().toString()) >= 1) {

                            String value = quantity.getText().toString();
                            int value1 = Integer.parseInt(value) + 1;
                            String new_value = String.valueOf(value1);
                            quantity.setText(new_value);

                        }
                    }
                });

                decrease.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Integer.parseInt(quantity.getText().toString()) >= 1) {

                            String value = quantity.getText().toString();
                            int value1 = Integer.parseInt(value) - 1;
                            String new_value = String.valueOf(value1);
                            quantity.setText(new_value);
                        }

                        if (quantity.getText().toString().equals("0")) {
                            quantity.setText("1");

                        }
                    }
                });

//                relativeLayout.setBackgroundResource(R.drawable.dim);
//                mPager.setBackgroundResource(R.drawable.dim);
                //instantiate popup window
                popupWindow = new PopupWindow(customView, ViewPager.LayoutParams.MATCH_PARENT, 800);
                popupWindow.setBackgroundDrawable(new ColorDrawable(
                        android.graphics.Color.TRANSPARENT));
                relativeLayout.setAlpha(0.2F);


                //display the popup window
                popupWindow.showAtLocation(relativeLayout, Gravity.CENTER, 0, 0);
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        relativeLayout.setAlpha(1.0F);
                    }
                });
            }
        });
        nametext = findViewById(R.id.name);
        pricetxt = findViewById(R.id.price);
        desctxt = findViewById(R.id.desc);
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        getDefaultAddress();
        getProductFeatures();
        productDetailPresenter = new ProductDetailPresenterImpl(this);
        showProgressDialogWithTitle("Loading.. Please Wait");
        //productDetailPresenter.getProduct(token,id);
        getProductDetails();
        morefeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShareProductDetails.this,ProductMoreDetails.class);
                intent.putExtra("product",nametext.getText().toString());
                intent.putExtra("price",pricetxt.getText().toString());
                intent.putExtra("description",descrip);
                startActivity(intent);
            }
        });
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pincode.getText().length() == 0) {
                    pincode.setError("Enter valid pincode");
                }
                if (pincode.getText().length() < 6) {
                    pincode.setError("Enter valid pincode");
                } else if (pincode.getText().length() == 6) {
                    checkavailability();
                }
            }
        });


        buynow = findViewById(R.id.buynow);
        buynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ShareProductDetails.this, ChooseAddress.class));
            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareLongDynamicLink();
            }
        });
        sharemajor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareLongDynamicLink();
            }
        });
        whatsapptext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                shareLongDynamicLink();

//        String query = "";
//        try {
//            query = URLEncoder.encode(String.format("&%1s=%2s", "id", "1234"), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }

            }




        });

        gotocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ShareProductDetails.this, Cart.class));

            }
        });

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    @Override
    public void onBackPressed() {
      startActivity(new Intent(ShareProductDetails.this,NewHome.class));
    }

    public void shareLongDynamicLink() {
//        String query = "";
//        try {
//            query = URLEncoder.encode(String.format("&%1s=%2s", "id", "1234"), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        final String path = "https://haahooapp.page.link/?" +
                "link=" + /*link*/
                "https://www.google.com/?" +
                "id="+
                ApiClient.own_id+"+"+id+
                "&apn=" + /*getPackageName()*/
                "creo.com.haahooapp" +
                "&st=" + /*titleSocial*/
                "Download+Haahoo" +
                "&sd=" + /*description*/
                descrip +
                "&utm_source=" + /*source*/
                "AndroidApp";
        Log.d("referpath","ohh"+path);
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(path))
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created

                            Uri shortLink = task.getResult().getShortLink();
                            link = shortLink.toString();
                            Intent intent = new Intent();
//                            String path = buildDynamicLink();
                            Log.d("actual link","linkkk"+link);
                            String msg = "visit my awesome website: " +link;

                            intent.setAction(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, link);
                            intent.setType("text/html");
                            Intent shareintent = Intent.createChooser(intent,null);
                            startActivity(shareintent);

                        } else {
                            // Error
                            // ...
                        }
                    }
                });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        // Check which request we're responding to
        if (requestCode == 0) {

            // Toast.makeText(ProductDetails.this,"success",Toast.LENGTH_LONG).show();
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
//send successfully
                // Toast.makeText(ProductDetails.this,"success",Toast.LENGTH_LONG).show();
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                // Do something with the contact here (bigger example below)
            }
            else{
//message not send
                // Toast.makeText(ProductDetails.this,"fail;ed",Toast.LENGTH_LONG).show();

            }
        }
    }

    public static void dimBehind(PopupWindow popupWindow) {
        View container = popupWindow.getContentView().getRootView();
        Context context = popupWindow.getContentView().getContext();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
        p.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.3f;
        wm.updateViewLayout(container, p);
    }


    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    private void init() {
        for(int i=0;i<IMAGES.length;i++)
            ImagesArray.add(IMAGES[i]);

        mPager =  findViewById(R.id.pager);

        mPager.setAdapter(new SlidingImage_Adapter(ShareProductDetails.this,images));

        CirclePageIndicator indicator =
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle1 indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES =images.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
//
        //Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }

        });

    }

    public void getDefaultAddress(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"booking/fetch_default_address/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("jsonobjecttdsdfgh","pbjj"+jsonObject);
                            String message = jsonObject.optString("message");
                            if(message.equals("Failed")){
                                defaulte.setVisibility(View.GONE);
                                relativeLayout1.setVisibility(View.VISIBLE);
                            }
                            if(message.equals("Success")) {
                                defaulte.setVisibility(View.VISIBLE);
                                relativeLayout1.setVisibility(View.GONE);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                String data = jsonArray.optString(0);
                                JSONArray jsonArray2 = new JSONArray(data);
                                JSONObject jsonObject1 = jsonArray2.optJSONObject(0).optJSONObject("fields");
                                String pincode1 = jsonObject1.optString("pincode");
                                pin.setText(pincode1);

                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ShareProductDetails.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>params = new HashMap<String,String>();
                params.put("Authorization", "Token "+token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void getProductFeatures(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"product_details/product_features/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String count = jsonObject.optString("data");
                            JSONArray jsonObject1 = new JSONArray(count);
                            String counter = jsonObject1.optString(0);
                            String vv = jsonObject1.optString(1);
                            JSONObject jsonObject2 = new JSONObject(counter);
                            JSONObject valuess = new JSONObject(vv);
                            // Log.d("featdfgh","fttt"+jsonObject);
                            ArrayList<String> features = new ArrayList<>();

                            for(int i = 1;i<=Integer.parseInt(jsonObject2.optString("count"));i++) {
                                // Log.d("jsonbbb","ghj"+jsonObject1.optString(i));
                                features.add(jsonObject1.optString(i));

                            }
                            ApiClient.product_features.clear();
                            ApiClient.product_features = features;
                            String numb = jsonObject1.optString(2);
                            JSONObject jsonObject3 = new JSONObject(numb);
                            JSONObject jsonObject4 = new JSONObject(features.get(0));
                            String jj = jsonObject4.getString("1");
                            key.setText(jj);
                            JSONObject jsonObject5 = new JSONObject(features.get(0));

                            //String secondfeature = jsonObject1.optString(3);
                            JSONObject featuretwo = new JSONObject(features.get(1));
                            key2.setText(featuretwo.optString("2"));
                            Log.d("dfghjk","pbjj"+jsonObject);
//                            String value = jsonObject1.optString(1);
//                            JSONObject jsonObject2 = new JSONObject(value);



                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ShareProductDetails.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("id",id);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>params = new HashMap<String,String>();
                params.put("Authorization", "Token "+token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


    void checkavailability(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"order_details/check_pin/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if(message.equals("Failed")){
                                avail.setVisibility(View.VISIBLE);
                                avail.setText("Delivery not available");
                                pincode.clearFocus();
                            }
                            if(message.equals("Success")){
                                avail.setVisibility(View.VISIBLE);
                                avail.setText("Delivery available");
                                pincode.clearFocus();
                            }
                            Log.d("jsonobjecttd","pbjj"+message);

                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ShareProductDetails.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("pin",pincode.getText().toString());
                params.put("product_id",id);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>params = new HashMap<String,String>();
                params.put("Authorization", "Token "+token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    void getProductDetails(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"product_details/product_view_id/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ApiClient.quantity.clear();
                            ApiClient.name.clear();
                            ApiClient.images.clear();
                            ApiClient.prices.clear();
                            JSONObject jsonObject1 = new JSONObject(response);
                            String return_period = jsonObject1.optString("return_period");
                            ret.setText(return_period);
                            sold_by.setText(jsonObject1.optString("sold_by"));
                            delvry.setText("Delivery on or before " +jsonObject1.optString("est_dlvry"));
                            Log.d("devler","hjgjg"+jsonObject1);
                            JSONArray jsonArray = jsonObject1.getJSONArray("data");
                            String s = jsonArray.getString(jsonArray.length()-1);
                            JSONArray jsonObject5 = new JSONArray(s);
                            JSONObject jsonObject6 =jsonObject5.getJSONObject(0);
                            String id = jsonObject6.getString("pk");
                            ApiClient.ids.clear();
                            ApiClient.ids.add(id);
                            String fields = jsonObject6.getString("fields");
                            JSONObject jsonObject7 = new JSONObject(fields);
                            String name = jsonObject7.getString("name");
                            nametext.setText(name);
                            String features1 = jsonObject7.optString("features");
                            //JSONObject featu = new JSONObject(features);
                            String[] seperated = features1.split(",");
                            //features.setLines(seperated.length);
                            StringBuilder stringBuilder = new StringBuilder();
                            for(int i = 0;i<seperated.length;i++){
                                stringBuilder.append(seperated[i]+"\n");
                                // featur.add(seperated[i]);
                            }
                            String description = jsonObject7.getString("description");
                            descrip = description;
                            ReadMoreOption readMoreOption = new ReadMoreOption.Builder(context)
                                    .textLength(3, ReadMoreOption.TYPE_LINE) // OR
                                    //.textLength(300, ReadMoreOption.TYPE_CHARACTER)
                                    .moreLabel("More")
                                    .lessLabel(" Less")
                                    .moreLabelColor(Color.parseColor("#0078BC"))
                                    .lessLabelColor(Color.parseColor("#0078BC"))
                                    .labelUnderLine(false)
                                    .expandAnimation(false)
                                    .build();
                            readMoreOption.addReadMoreTo(desctxt, description);
                            //desctxt.setText(description);
                            String price = jsonObject7.getString("price");
                            pricetxt.setText("₹ "+price);
                            ApiClient.name.add(name);
                            ApiClient.price = "₹ "+price;
                            ApiClient.prices.add("₹ "+price);
                            ApiClient.quantity.add("1");
                            hideProgressDialogWithTitle();
                            String wishlisted = jsonArray.getJSONObject(0).getString("wishlisted");
                            if(wishlisted.equals("0")){
                                fav.setVisibility(View.VISIBLE);
                                fav_fill.setVisibility(View.GONE);
                            }
                            if(wishlisted.equals("1")){
                                fav_fill.setVisibility(View.VISIBLE);
                                fav.setVisibility(View.GONE);
                            }
                            JSONObject jsonObject = new JSONObject();
                            int k =0;
                            for(int i = 1 ; i<jsonArray.length()-1; i++) {
//                                 jsonObject = jsonArray.getJSONObject(i);
//                                String jsonArray2 = jsonObject.optString(String.valueOf(i));
                                String image = jsonArray.getString(i);
                                JSONObject jsonObject2 = new JSONObject(image);
                                imgs.add(jsonObject2.toString());
                            }

                            for (int i=0 ; i <imgs.size();i++){
                                JSONObject jsonObject3 = new JSONObject(imgs.get(i));
                                String jsonObject2 = imgs.get(i);
                                images.add(ApiClient.BASE_URL+"media/"+jsonObject3.getString(String.valueOf(i)));

                            }
                            ApiClient.images.add(images.get(0));
                            init();

                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ShareProductDetails.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("id",id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void success(JSONArray jsonArray) throws JSONException {
        ApiClient.quantity.clear();
        ApiClient.name.clear();
        ApiClient.images.clear();
        ApiClient.prices.clear();
        jsonArray1 = jsonArray;

        for(int i=0 ; i<jsonArray1.length()-1 ; i++){

            String image = jsonArray1.getString(i);
            JSONObject jsonObject = new JSONObject(image);
            images.add(ApiClient.BASE_URL+"media/"+jsonObject.optString(String.valueOf(i)));
        }
        ApiClient.images.add(images.get(0));
//        init();
        try {
            String name = null;
            String price = null;
            String description = null;
            for(int i = 0 ; i<=jsonArray1.length() ; i++) {
                //   JSONObject jsonObject = jsonArray1.getString(i);
//                JSONObject jsonObject = jsonArray1.getJSONObject(i);
                String jsonArray2 = jsonArray1.getString(jsonArray1.length()-1);
                JSONArray jsonObject1 = new JSONArray(jsonArray2);
                JSONObject jsonObject2 = jsonObject1.getJSONObject(0);
                JSONObject jsonObject3 = jsonObject2.getJSONObject("fields");
                name = jsonObject3.getString("name");
                price ="₹ "+jsonObject3.getString("price");
                description = jsonObject3.getString("description");


                //
                //String jsonObject1 = jsonObject.getString("fields");
            }
            ApiClient.name.add(name);
            ApiClient.price = price;
            ApiClient.prices.add(price);
            ApiClient.quantity.add("Quantity : 1");
            nametext.setText(name);
            pricetxt.setText(price);
            descrip = description;
            desctxt.setText(description);
            // hideProgressDialogWithTitle();
//            String name = jsonObject1.getString("name");
//            String description = jsonObject1.getString("description");
//            String price = jsonObject1.getString("price");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void failure(String data) {

    }

    @Override
    public void success123(JSONArray jsonArray) {

    }

    @Override
    public void failed(String data) {

    }


    @Override
    public void onVerifySuccessWish(String data) {
        Toast.makeText(ShareProductDetails.this,data,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVerifySuccessWishRemove(String data) {

    }

    @Override
    public void onVerifyFailedWish(String msg) {

    }

    @Override
    public void wishlistviewsuccess(String msg) {

    }

    @Override
    public void wishlistviewfailed(String data) {

    }

    @Override
    public void onSuccessWish(String response) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"wishlist/wishlist_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            String message = jsonObject1.getString("message");
                            Log.d("imagewish", "wish" +jsonObject1.optString("Total count"));
                            ApiClient.wish_count = jsonObject1.optString("Total count");

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ShareProductDetails.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        Toast.makeText(ShareProductDetails.this,"Added to Wishlist",Toast.LENGTH_SHORT).show();
    }



    @Override
    public void onRemove(String response) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"wishlist/wishlist_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
//                            String message = jsonObject1.getString("message");
//                            Log.d("imagewish", "wish" +jsonObject1.optString("Total count"));
                            ApiClient.wish_count = jsonObject1.optString("Total count");

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ShareProductDetails.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        Toast.makeText(ShareProductDetails.this,"Removed from wishlist",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailedWish(String response) {

    }

    @Override
    public void wishView(String response) {

    }

    @Override
    public void wishFailed(String response) {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void successcart(String data) {

    }

    @Override
    public void failedcart(String msg) {

    }

    @Override
    public void viewsuccess(JSONArray data) {

    }

    @Override
    public void viewfail(String msg) {

    }

    @Override
    public void onSuccesscart(String data) {
        Toast.makeText(ShareProductDetails.this,"Added to cart", Toast.LENGTH_SHORT).show();
        addTocart.setVisibility(View.GONE);
        gotocart.setVisibility(View.VISIBLE);
        startActivity(new Intent(ShareProductDetails.this,Cart.class));
        popupWindow.dismiss();
    }

    @Override
    public void onFailedcart(String data) {

    }

    @Override
    public void onsuccess(JSONArray data) {

    }

    @Override
    public void onfail(String data) {

    }


}
