package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PasswordReset {

  @SerializedName("phone_no")
  @Expose
  private String phone_no;

  @SerializedName("password")
  @Expose
  private String password;

  public PasswordReset(String phone_no, String password) {
    this.phone_no = phone_no;
    this.password = password;
  }


  public String getPhone_no() {
    return phone_no;
  }

  public void setPhone_no(String phone_no) {
    this.phone_no = phone_no;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
