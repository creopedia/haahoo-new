package creo.com.haahooapp.Modal;

public class SearchPojo {

    public String shop_id;
    public String shop_name;
    public String product_id;
    public String suggestion;
    public String product_name;
    public String pdt_shop_id;

    public String getPdt_shop_id() {
        return pdt_shop_id;
    }

    public void setPdt_shop_id(String pdt_shop_id) {
        this.pdt_shop_id = pdt_shop_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }
}
