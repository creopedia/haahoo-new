package creo.com.haahooapp.Modal;

public class DownloadPojo {

    public String name;
    public String date;

    public DownloadPojo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
