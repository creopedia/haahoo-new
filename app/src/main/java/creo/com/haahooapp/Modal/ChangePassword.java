package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePassword {

  @SerializedName("oldpassword")
  @Expose
  public String currentpassword;

  @SerializedName("newpassword")
  @Expose
  public String newpassword;

  public ChangePassword(String currentpassword, String newpassword) {
    this.currentpassword = currentpassword;
    this.newpassword = newpassword;
  }

  public String getCurrentpassword() {
    return currentpassword;
  }

  public void setCurrentpassword(String currentpassword) {
    this.currentpassword = currentpassword;
  }

  public String getNewpassword() {
    return newpassword;
  }

  public void setNewpassword(String newpassword) {
    this.newpassword = newpassword;
  }
}
