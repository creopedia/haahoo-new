package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DevicePojo {


  @SerializedName("device_id")
  @Expose
  private String device_id;

  public DevicePojo(String device_id) {
    this.device_id = device_id;
  }

  public String getDevice_id() {
    return device_id;
  }

  public void setDevice_id(String device_id) {
    this.device_id = device_id;
  }
}
