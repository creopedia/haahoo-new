package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeliveryModes {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("delivery_mode")
    @Expose
    private List<String> delivery_mode;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<String> getDelivery_mode() {
        return delivery_mode;
    }

    public void setDelivery_mode(List<String> delivery_mode) {
        this.delivery_mode = delivery_mode;
    }
}
