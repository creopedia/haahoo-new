package creo.com.haahooapp.Modal;

public class ViewersPojo {

    public String name;
    public String time;

    public ViewersPojo(String name, String time) {
        this.name = name;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
