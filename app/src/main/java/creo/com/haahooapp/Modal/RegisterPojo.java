package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterPojo {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone_no")
    @Expose
    private String phone_no;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("device_id")
    @Expose
    private String device_id;
    @SerializedName("referal_id")
    @Expose
    private String referal_id;
    @SerializedName("proof_id")
    @Expose
    private String proof_id;
    @SerializedName("proof_id_no")
    @Expose
    private String proof_id_no;
    @SerializedName("date_of_birth")
    @Expose
    private String date_of_birth;

    @SerializedName("fire")
    @Expose
    private String fire_token;

    public RegisterPojo(String name, String email, String phone_no, String password, String device_id, String referal_id, String proof_id, String proof_id_no, String date_of_birth,String firebase) {
        this.name = name;
        this.email = email;
        this.phone_no = phone_no;
        this.password = password;
        this.device_id = device_id;
        this.referal_id = referal_id;
        this.proof_id = proof_id;
        this.proof_id_no = proof_id_no;
        this.date_of_birth = date_of_birth;
        this.fire_token = firebase;
    }

    public String getFire_token() {
        return fire_token;
    }

    public void setFire_token(String fire_token) {
        this.fire_token = fire_token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getReferal_id() {
        return referal_id;
    }

    public void setReferal_id(String referal_id) {
        this.referal_id = referal_id;
    }

    public String getProof_id() {
        return proof_id;
    }

    public void setProof_id(String proof_id) {
        this.proof_id = proof_id;
    }

    public String getProof_id_no() {
        return proof_id_no;
    }

    public void setProof_id_no(String proof_id_no) {
        this.proof_id_no = proof_id_no;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }
}
