package creo.com.haahooapp.Modal;

public class Subscribedpojo {

    public String pdt_id;
    public String order_id;
    public String mode;
    public String pdt_name;
    public String pdt_image;
    public String shop_name;
    public String amount;
    public String from;
    public String until;
    public String days;
    public String benefits;

    public String getBenefits() {
        return benefits;
    }

    public void setBenefits(String benefits) {
        this.benefits = benefits;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getPdt_id() {
        return pdt_id;
    }

    public void setPdt_id(String pdt_id) {
        this.pdt_id = pdt_id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getUntil() {
        return until;
    }

    public void setUntil(String until) {
        this.until = until;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getPdt_name() {
        return pdt_name;
    }

    public void setPdt_name(String pdt_name) {
        this.pdt_name = pdt_name;
    }

    public String getPdt_image() {
        return pdt_image;
    }

    public void setPdt_image(String pdt_image) {
        this.pdt_image = pdt_image;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
