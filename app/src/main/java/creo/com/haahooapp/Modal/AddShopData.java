package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddShopData {
    @SerializedName("wallet_amount")
    @Expose
    private String wallet_amount;
    @SerializedName("amount_status")
    @Expose
    private String amount_status;
    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("user_name")
    @Expose
    private String user_name;
    @SerializedName("earning_status")
    @Expose
    private String earning_status;
    @SerializedName("refferal_id")
    @Expose
    private String refferal_id;
    @SerializedName("coupon_reward")
    @Expose
    private String coupon_reward;
    @SerializedName("name")
    @Expose
    private String name;
    public AddShopData(String wallet_amount, String amount_status, String user_id, String earning_status, String refferal_id) {
        this.wallet_amount = wallet_amount;
        this.amount_status = amount_status;
        this.user_id = user_id;
        this.earning_status = earning_status;
        this.refferal_id = refferal_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoupon_reward() {
        return coupon_reward;
    }

    public void setCoupon_reward(String coupon_reward) {
        this.coupon_reward = coupon_reward;
    }

    public String getWallet_amount() {
        return wallet_amount;
    }

    public void setWallet_amount(String wallet_amount) {
        this.wallet_amount = wallet_amount;
    }

    public String getAmount_status() {
        return amount_status;
    }

    public void setAmount_status(String amount_status) {
        this.amount_status = amount_status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEarning_status() {
        return earning_status;
    }

    public void setEarning_status(String earning_status) {
        this.earning_status = earning_status;
    }

    public String getRefferal_id() {
        return refferal_id;
    }

    public void setRefferal_id(String refferal_id) {
        this.refferal_id = refferal_id;
    }
}
