package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginPojo {
  @SerializedName("phone_no")
  @Expose
  private String phone_no;

  @SerializedName("password")
  @Expose
  private String password;

  @SerializedName("device_id")
  @Expose
  private String device_id;

  @SerializedName("fire_token")
  @Expose
  private String token;

  public LoginPojo(String phone_no, String password, String device_id, String token) {
    this.phone_no = phone_no;
    this.password = password;
    this.device_id = device_id;
    this.token = token;
  }


  public String getPhone_no() {
    return phone_no;
  }

  public void setPhone_no(String phone_no) {
    this.phone_no = phone_no;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getDevice_id() {
    return device_id;
  }

  public void setDevice_id(String device_id) {
    this.device_id = device_id;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
