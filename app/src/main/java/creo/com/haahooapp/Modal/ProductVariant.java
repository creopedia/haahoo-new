package creo.com.haahooapp.Modal;

public class ProductVariant {

    public String id;
    public String name;
    public String image;
    public String price;
    public String variant_value;

    public String getVariant_value() {
        return variant_value;
    }

    public void setVariant_value(String variant_value) {
        this.variant_value = variant_value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
