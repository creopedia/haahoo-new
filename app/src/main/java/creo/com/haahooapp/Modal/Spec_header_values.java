package creo.com.haahooapp.Modal;

public class Spec_header_values {

    public String spec_header;
    public String isclicked;
    public String spec_value;


    public String getSpec_header() {
        return spec_header;
    }

    public void setSpec_header(String spec_header) {
        this.spec_header = spec_header;
    }

    public String getIsclicked() {
        return isclicked;
    }

    public void setIsclicked(String isclicked) {
        this.isclicked = isclicked;
    }

    public String getSpec_value() {
        return spec_value;
    }

    public void setSpec_value(String spec_value) {
        this.spec_value = spec_value;
    }
}
