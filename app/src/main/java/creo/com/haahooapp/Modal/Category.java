package creo.com.haahooapp.Modal;

public class Category {

    public String image;
    public String name;
    public String id;
    public String price;
    public String edited_price;
    public String product_id;
    public String original_price;
    public String reselling_max;
    public String cart_status;
    public String rating;
    public String description;
    public String stock;
    public String shopid;
    public String variant_count;

    public String getVariant_count() {
        return variant_count;
    }

    public void setVariant_count(String variant_count) {
        this.variant_count = variant_count;
    }

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCart_status() {
        return cart_status;
    }

    public void setCart_status(String cart_status) {
        this.cart_status = cart_status;
    }

    public String getReselling_max() {
        return reselling_max;
    }

    public void setReselling_max(String reselling_max) {
        this.reselling_max = reselling_max;
    }

    public String getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(String original_price) {
        this.original_price = original_price;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getEdited_price() {
        return edited_price;
    }

    public void setEdited_price(String edited_price) {
        this.edited_price = edited_price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
