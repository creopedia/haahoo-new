package creo.com.haahooapp.Modal;

public class RatingPojo {

    public String name,rating,reviews;

    public RatingPojo(String name,String reviews){
        this.name=name;
        this.reviews=reviews;

    }

    public String getName() {
        return name;
    }

    public String getReviews() {
        return reviews;
    }

    public String getRating() {
        return rating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }
}