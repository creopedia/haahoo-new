package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyOtpPojo {

    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("phone_no")
    @Expose
    private String phone_no;



    public VerifyOtpPojo(String otp, String phone_no) {
        this.phone_no = phone_no;
        this.otp = otp;
    }


    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
