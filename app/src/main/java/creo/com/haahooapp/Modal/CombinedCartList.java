package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CombinedCartList {

    @SerializedName("cart_count")
    @Expose
    private String cart_count;
    @SerializedName("shop_name")
    @Expose
    private String shop_name;
    @SerializedName("shop_id")
    @Expose
    private String shop_id;

    @SerializedName("shop_image")
    @Expose
    private String shop_img;

    public CombinedCartList(String cart_count, String shop_name, String shop_id,String shop_img) {
        this.cart_count = cart_count;
        this.shop_name = shop_name;
        this.shop_id = shop_id;
        this.shop_img = shop_img;
    }

    public String getShop_img() {
        return shop_img;
    }

    public void setShop_img(String shop_img) {
        this.shop_img = shop_img;
    }

    public String getCart_count() {
        return cart_count;
    }

    public void setCart_count(String cart_count) {
        this.cart_count = cart_count;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }
}
