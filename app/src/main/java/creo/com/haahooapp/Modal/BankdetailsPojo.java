package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankdetailsPojo {
  @SerializedName("name")
  @Expose
  private String accountnumber;
  @SerializedName("address")
  @Expose
  private String ifsc;
  @SerializedName("phone_no")
  @Expose
  private String holdername;


  public BankdetailsPojo( String accountnumber, String ifsc, String holdername,String device_id) {
    this.accountnumber=accountnumber;
    this.ifsc=ifsc;
    this.holdername=holdername;
  }

  public String getAccountnumber() {
    return accountnumber;
  }

  public String getIfsc() {
    return ifsc;
  }

  public String getHoldername() {
    return holdername;
  }

  public void setAccountnumber(String accountnumber) {
    this.accountnumber = accountnumber;
  }

  public void setIfsc(String ifsc) {
    this.ifsc = ifsc;
  }

  public void setHoldername(String holdername) {
    this.holdername = holdername;
  }
}