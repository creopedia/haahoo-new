package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishListId {

  @SerializedName("productid")
  @Expose
  private String product_id;
  @SerializedName("virtual")
  @Expose
  private String virtual;

  public String getVirtual() {
    return virtual;
  }

  public void setVirtual(String virtual) {
    this.virtual = virtual;
  }

  public WishListId(String product_id) {
    this.product_id = product_id;
  }

  public String getProduct_id() {
    return product_id;
  }

  public void setProduct_id(String product_id) {
    this.product_id = product_id;
  }
}
