package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyModal {


    @SerializedName("phone_no")
    @Expose
    private String phone_no;


    public VerifyModal(String phone_no) {
        this.phone_no = phone_no;
    }


    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }
}
