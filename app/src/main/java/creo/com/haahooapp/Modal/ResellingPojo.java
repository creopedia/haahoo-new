package creo.com.haahooapp.Modal;

public class ResellingPojo {

    public String id;
    public String product_name;
    public String pdt_price;
    public String wholesale_price;
    public String delivery_expense;
    public String min_wholesale_qty;
    public String resell_count;
    public String resell;
    public String image;
    public String shop_id;
    public String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPdt_price() {
        return pdt_price;
    }

    public void setPdt_price(String pdt_price) {
        this.pdt_price = pdt_price;
    }

    public String getWholesale_price() {
        return wholesale_price;
    }

    public void setWholesale_price(String wholesale_price) {
        this.wholesale_price = wholesale_price;
    }

    public String getDelivery_expense() {
        return delivery_expense;
    }

    public void setDelivery_expense(String delivery_expense) {
        this.delivery_expense = delivery_expense;
    }

    public String getMin_wholesale_qty() {
        return min_wholesale_qty;
    }

    public void setMin_wholesale_qty(String min_wholesale_qty) {
        this.min_wholesale_qty = min_wholesale_qty;
    }

    public String getResell_count() {
        return resell_count;
    }

    public void setResell_count(String resell_count) {
        this.resell_count = resell_count;
    }

    public String getResell() {
        return resell;
    }

    public void setResell(String resell) {
        this.resell = resell;
    }
}
