package creo.com.haahooapp.Modal;

public class ProductPojo {

    private String name , description , image,discount,percent,rating;

    public ProductPojo(String name, String description, String image, String discount, String percent, String rating) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.discount = discount;
        this.percent = percent;
        this.rating = rating;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
