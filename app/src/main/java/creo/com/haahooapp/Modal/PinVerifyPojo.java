package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PinVerifyPojo {

  @SerializedName("device_id")
  @Expose
  private String device_id;

  @SerializedName("pin")
  @Expose
  private String pin;

  @SerializedName("fire")
  @Expose
  private  String fire;


  public PinVerifyPojo(String device_id, String pin,String fire_token) {
    this.device_id = device_id;
    this.pin = pin;
    this.fire = fire_token;
  }


  public String getFire() {
    return fire;
  }

  public void setFire(String fire) {
    this.fire = fire;
  }

  public String getDevice_id() {
    return device_id;
  }

  public void setDevice_id(String device_id) {
    this.device_id = device_id;
  }

  public String getPin() {
    return pin;
  }

  public void setPin(String pin) {
    this.pin = pin;
  }
}
