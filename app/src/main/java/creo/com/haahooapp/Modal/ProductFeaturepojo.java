package creo.com.haahooapp.Modal;

public class ProductFeaturepojo {

    public String feature_name;

    public ProductFeaturepojo(String feature_name) {
        this.feature_name = feature_name;
    }

    public String getFeature_name() {
        return feature_name;
    }

    public void setFeature_name(String feature_name) {
        this.feature_name = feature_name;
    }

}
