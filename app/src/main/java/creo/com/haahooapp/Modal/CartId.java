package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartId {

    @SerializedName("productid")
    @Expose
    private String product_id;

    @SerializedName("pro_count")
    @Expose
    private String pro_count;

    @SerializedName("virtual")
    @Expose
    private String virtual;

    public CartId(String product_id, String pro_count,String virtual) {
        this.product_id = product_id;
        this.pro_count = pro_count;
        this.virtual = virtual;
    }

    public String getVirtual() {
        return virtual;
    }

    public void setVirtual(String virtual) {
        this.virtual = virtual;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getPro_count() {
        return pro_count;
    }

    public void setPro_count(String pro_count) {
        this.pro_count = pro_count;
    }
}
