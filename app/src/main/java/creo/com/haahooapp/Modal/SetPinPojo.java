package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetPinPojo {


  @SerializedName("device_id")
  @Expose
  private String device_id;

  @SerializedName("pin")
  @Expose
  private String pin;


  public SetPinPojo(String device_id, String pin) {
    this.device_id = device_id;
    this.pin = pin;
  }

  public String getDevice_id() {
    return device_id;
  }

  public void setDevice_id(String device_id) {
    this.device_id = device_id;
  }

  public String getPin() {
    return pin;
  }

  public void setPin(String pin) {
    this.pin = pin;
  }
}
