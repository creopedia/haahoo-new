package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddaddressPojo {

  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("house_no")
  @Expose
  private String address;
  @SerializedName("phone_no")
  @Expose
  private String phone_no;
  @SerializedName("pincode")
  @Expose
  private String pincode;
  @SerializedName("area")
  @Expose
  private String area;
  @SerializedName("landmark")
  @Expose
  private String landmark;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("state")
  @Expose
  private String state;
  @SerializedName("device_id")
  @Expose
  private String device_id;
  @SerializedName("use_current_location_status")
  @Expose
  private String location_status;
  @SerializedName("lat")
  @Expose
  private String lat;
  @SerializedName("log")
  @Expose
  private String log;

  public AddaddressPojo(String name, String address, String phone_no, String pincode, String area, String landmark, String city, String state, String device_id,String location_status,String lat,String log) {
    this.name = name;
    this.address = address;
    this.phone_no = phone_no;
    this.pincode = pincode;
    this.area = area;
    this.landmark = landmark;
    this.city = city;
    this.state = state;
    this.device_id = device_id;
    this.location_status = location_status;
    this.lat = lat;
    this.log = log;
  }


  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLog() {
    return log;
  }

  public void setLog(String log) {
    this.log = log;
  }

  public String getLocation_status() {
    return location_status;
  }

  public void setLocation_status(String location_status) {
    this.location_status = location_status;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhone_no() {
    return phone_no;
  }

  public void setPhone_no(String phone_no) {
    this.phone_no = phone_no;
  }

  public String getPincode() {
    return pincode;
  }

  public void setPincode(String pincode) {
    this.pincode = pincode;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getLandmark() {
    return landmark;
  }

  public void setLandmark(String landmark) {
    this.landmark = landmark;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getDevice_id() {
    return device_id;
  }

  public void setDevice_id(String device_id) {
    this.device_id = device_id;
  }
}