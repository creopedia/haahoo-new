package creo.com.haahooapp.Modal;

import java.util.List;

public class ProductVariationPojo {

    public String id;
    public String name;
    public String image;
    public String price;
    public List<String> del_mode;

    public List<String> getDel_mode() {
        return del_mode;
    }

    public void setDel_mode(List<String> del_mode) {
        this.del_mode = del_mode;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
