package creo.com.haahooapp.Modal;

public class AddressPojo {

  private String name,address,phone_no,pincode,def;

  public AddressPojo(String name, String address, String phone_no, String pincode, String def) {
    this.name = name;
    this.address = address;
    this.phone_no = phone_no;
    this.pincode = pincode;
    this.def = def;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhone_no() {
    return phone_no;
  }

  public void setPhone_no(String phone_no) {
    this.phone_no = phone_no;
  }

  public String getPincode() {
    return pincode;
  }

  public void setPincode(String pincode) {
    this.pincode = pincode;
  }

  public String getDef() {
    return def;
  }

  public void setDef(String def) {
    this.def = def;
  }
}
