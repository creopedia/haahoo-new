package creo.com.haahooapp.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EarningAddShop {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<AddShopData> addShopData;
    @SerializedName("Response")
    @Expose
    private String response;

    public EarningAddShop(String code, String message, List<AddShopData> addShopData, String response) {
        this.code = code;
        this.message = message;
        this.addShopData = addShopData;
        this.response = response;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AddShopData> getAddShopData() {
        return addShopData;
    }

    public void setAddShopData(List<AddShopData> addShopData) {
        this.addShopData = addShopData;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
