package creo.com.haahooapp.Modal;

public class ChatPojo {

    public String message;
    public boolean ismine;


    public ChatPojo(String message, boolean ismine) {
        this.message = message;
        this.ismine = ismine;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIsmine() {
        return ismine;
    }

    public void setIsmine(boolean ismine) {
        this.ismine = ismine;
    }
}
