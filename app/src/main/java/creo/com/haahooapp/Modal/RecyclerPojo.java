package creo.com.haahooapp.Modal;

public class RecyclerPojo {

    public String name,address,phone;

    public RecyclerPojo(String name, String address, String phone) {
        this.name=name;
        this.address=address;
        this.phone=phone;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}