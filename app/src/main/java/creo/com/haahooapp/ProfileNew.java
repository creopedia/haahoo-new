package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Implementation.ProfilePresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.ProfileCallback;
import creo.com.haahooapp.interfaces.ProfilePresenter;
import creo.com.haahooapp.interfaces.ProfileView;

public class ProfileNew extends AppCompatActivity implements ProfileCallback, ProfileView {
    Activity activity = this;
    String token = null;
    JSONArray jsonArray = new JSONArray();
    ProfilePresenter profilePresenter;
    Context context = this;
    TextView emailtext,mobiletext,nametext,login,referrals,orders,earnings;
    RelativeLayout relativeLayout;
    ImageView back,image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_new);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        image = findViewById(R.id.image);
        Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/haahoo_logo1.png").into(image);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        referrals = findViewById(R.id.referals);
        orders = findViewById(R.id.orders);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        back = findViewById(R.id.back);
        profilePresenter = new ProfilePresenterImpl(this);
        earnings = findViewById(R.id.earnings);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        getEarnings();
        token = (pref.getString("token", ""));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileNew.super.onBackPressed();
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(ProfileNew.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(ProfileNew.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(ProfileNew.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(ProfileNew.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();

                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });

        emailtext = findViewById(R.id.email);
        mobiletext = findViewById(R.id.mobile);
        nametext = findViewById(R.id.name);
        profilePresenter.getProfile(token);
        getRefrralDetails();
    }

    @Override
    public void profileSuccess(JSONArray jsonArray) {

    }

    @Override
    public void profileFailure(String msg) {

    }

    @Override
    public void onSuccess(JSONArray jsonArray1) {
        jsonArray = jsonArray1;
        try {

            JSONObject jsonObject = jsonArray.getJSONObject(0);
            JSONObject jsonObject1 = jsonObject.getJSONObject("fields");
            String phone_no = jsonObject1.getString("phone_no");
            String email = jsonObject1.getString("email");
            String name = jsonObject1.getString("name");
            emailtext.setText(email);
            mobiletext.setText(phone_no);
            nametext.setText(name);

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void onFailed(String response) {

    }

    public void getRefrralDetails(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/login_count/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("jsonobjj","hhhh"+jsonObject);
                            String data = jsonObject.optJSONArray("data").optString(0);
                            JSONObject jsonObject1 = new JSONObject(data);
                            referrals.setText(jsonObject1.optString("referal_count"));
                            orders.setText(jsonObject1.optString("order_count"));



                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProfileNew.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public void getEarnings(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/login_ref_earnings/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String total = jsonObject.optString("total");

                            earnings.setText(total);

                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProfileNew.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>params = new HashMap<String,String>();
                params.put("Authorization", "Token "+token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}
