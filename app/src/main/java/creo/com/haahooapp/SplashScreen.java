package creo.com.haahooapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.HashMap;

import creo.com.haahooapp.Implementation.VerifyDevicePresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.VerifyDeviceCallback;
import creo.com.haahooapp.interfaces.VerifyDevicePresenter;
import creo.com.haahooapp.interfaces.VerifyDeviceView;

public class SplashScreen extends AppCompatActivity implements VerifyDeviceCallback, VerifyDeviceView, ForceUpdateChecker.OnUpdateNeededListener {
    private static int TIME_OUT = 1000;
    VerifyDevicePresenter verifyDevicePresenter;
    boolean pin = false;
    Context mainContext = this;
    String link = null;
    public static final String VERSION_CODE_KEY = "latest_app_version";
    Activity activity = this;
    FirebaseRemoteConfig firebaseRemoteConfig;
    private FirebaseAnalytics analytics;
    final String versionCode = BuildConfig.VERSION_NAME;
    private final String TAG = getClass().getName();
    private FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String, Object> firebaseDefaultMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        verifyDevicePresenter = new VerifyDevicePresenterImpl(this);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        firebaseDefaultMap = new HashMap<>();
        firebaseDefaultMap.put(VERSION_CODE_KEY, versionCode);
        mFirebaseRemoteConfig.setDefaults(firebaseDefaultMap);
        mFirebaseRemoteConfig.setConfigSettings(
                new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(BuildConfig.DEBUG)
                        .build());


        mFirebaseRemoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mFirebaseRemoteConfig.activateFetched();
                    Log.d(TAG, "Fetched value: " + mFirebaseRemoteConfig.getString(VERSION_CODE_KEY));
                    //calling function to check if new version is available or not
                    checkForUpdate();
                } else
                    Toast.makeText(SplashScreen.this, "Someting went wrong please try again", Toast.LENGTH_SHORT).show();
            }
        });

        Log.d(TAG, "Default value: " + mFirebaseRemoteConfig.getString(VERSION_CODE_KEY));
    }

    private void checkForUpdate() {
        ApiClient.referal_id = "0";
        ApiClient.shopidrefer = "0";
        ApiClient.shop_refer = "0";
        analytics = FirebaseAnalytics.getInstance(SplashScreen.this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("tbhiuh", "Refreshed token: " + refreshedToken);
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        Log.d("FAILLLL","jgcfhx"+pendingDynamicLinkData);
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData == null) {
                            ApiClient.study_referal_id = "0";
                            return;
                        }
                        if (pendingDynamicLinkData.getLink() != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            Log.d("ghvhgvghv","uyyvycy"+deepLink);
                            String cs = String.valueOf(deepLink.getQueryParameter("utm_source"));
                            String cn = String.valueOf(deepLink.getQueryParameter("utm_campaign"));
                            String cm = String.valueOf(deepLink.getQueryParameter("utm_medium"));
                            analytics.logEvent("dynamic_link_app_open", null);
                            analytics.logEvent("dynamic_link_app_update", null);
                            analytics.logEvent("click", null);
                            if (cs != null && cn != null) {
                                Bundle params = new Bundle();
                                params.putString(FirebaseAnalytics.Param.SOURCE, cs);
                                params.putString(FirebaseAnalytics.Param.MEDIUM, cm);
                                params.putString(FirebaseAnalytics.Param.CAMPAIGN, cn);
                                analytics.logEvent(FirebaseAnalytics.Event.CAMPAIGN_DETAILS, params);
                                analytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, params);
                            }
                        }
                        if (deepLink != null) {
//                            String product_id = pendingDynamicLinkData.getLink().getQueryParameter("productId");
                            Log.d("jhvghc", "jhgfdgf" + pendingDynamicLinkData.getLink().getQueryParameterNames().contains("productid"));
                            if (pendingDynamicLinkData.getLink().getQueryParameterNames().contains("productid")) {
                                String product_id = pendingDynamicLinkData.getLink().getQueryParameter("productid");
//                                Intent intent = new Intent(mainContext,ProductDetailsShop.class);
//                                intent.putExtra("shopid","1");
//                                intent.putExtra("productid",product_id);
//                                startActivity(intent);
                                ApiClient.productinshopid = product_id;
                                String ids[] = product_id.split(" ");
                                if (ids.length == 2) {
                                    Log.d("PRODUCTREFERE", "reffg" + ids[0]);
                                    Log.d("prefer", "reffg" + ids[1]);
                                    ApiClient.referal_id = ids[0];
                                    ApiClient.product_referral_id = ids[1];
                                    ApiClient.productinshopid = ids[0];
                                }
                                if (ids.length == 1) {
                                    ApiClient.referal_id = product_id;
                                    ApiClient.product_referral_id = "0";
                                }
                            }
                            if (pendingDynamicLinkData.getLink().getQueryParameterNames().contains("shopid")) {
                                String shop_id = pendingDynamicLinkData.getLink().getQueryParameter("shopid");
                                Log.d("bcgfcgfx","iuiuyuiyuy"+shop_id);
                                String[] dd = shop_id.split(" ");
                                ApiClient.shopidrefer = dd[0];
                                ApiClient.shop_refer = dd[1];
                            }
                            if (pendingDynamicLinkData.getLink().getQueryParameterNames().contains("id")) {

                                String referal_id = pendingDynamicLinkData.getLink().getQueryParameter("id");

                                String ids[] = referal_id.split(" ");
                                if (ids.length == 2) {
                                    ApiClient.referal_id = ids[0];
                                    ApiClient.product_referral_id = ids[1];
                                }
                                if (ids.length == 1) {
                                    ApiClient.referal_id = referal_id;
                                    ApiClient.product_referral_id = "0";
                                }
                            }
                            if (pendingDynamicLinkData.getLink().getQueryParameterNames().contains("studyid")) {
                                String study_referal_id = pendingDynamicLinkData.getLink().getQueryParameter("studyid");
                                ApiClient.study_referal_id = study_referal_id;
                            }
                            if (!(pendingDynamicLinkData.getLink().getQueryParameterNames().contains("studyid"))) {
                            }

                        }
                        if (deepLink == null) {
                            ApiClient.referal_id = "0";
                            ApiClient.product_referral_id = "0";
                        }
                        // [END_EXCLUDE]
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.w(TAG, "getDynamicLink:onFailure", e);
                    }
                });



        String latestAppVersion = mFirebaseRemoteConfig.getString("version_code");
        if (!(latestAppVersion.equals(versionCode))) {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("New version available")
                    .setMessage("Please, update app to new version for a better experience.")
                    .setPositiveButton("Update",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    redirectStore("https://play.google.com/store/apps/details?id=creo.com.haahooapp");
                                }
                            }).setNegativeButton("No, thanks",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

                                    verifyDevicePresenter.verifyDevice(device_id);

                                }
                            }).create();
            dialog.show();
        }
        else {
            String device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            verifyDevicePresenter.verifyDevice(device_id);
            // Toast.makeText(this,"This app is already upto date", Toast.LENGTH_SHORT).show();
        }


//        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
//        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
//                .setMinimumFetchIntervalInSeconds(3600)
//                .build();
//        firebaseRemoteConfig.setConfigSettingsAsync(configSettings);
//        ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
// clear FLAG_TRANSLUCENT_STATUS flag:


//          shareLongDynamicLink();
    }

    //    public void shareLongDynamicLink() {
////        String query = "";
////        try {
////            query = URLEncoder.encode(String.format("&%1s=%2s", "id", "1234"), "UTF-8");
////        } catch (UnsupportedEncodingException e) {
////            e.printStackTrace();
////        }
//        String path = "https://haahooapp.page.link/?" +
//                "link=" + /*link*/
//                "https://www.google.com/?" +
//                "id="+
//                "1111"+
//                "&apn=" + /*getPackageName()*/
//                "creo.com.haahooapp" +
//                "&st=" + /*titleSocial*/
//                "Share+this+App" +
//                "&sd=" + /*description*/
//                "looking+to+learn+how+to+use+Firebase+in+Android?+this+app+is+what+you+are+looking+for." +
//                "&utm_source=" + /*source*/
//                "AndroidApp";
//        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
//                .setLongLink(Uri.parse(path))
//                .buildShortDynamicLink()
//                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
//                    @Override
//                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
//                        if (task.isSuccessful()) {
//                            // Short link created
//
//                            Uri shortLink = task.getResult().getShortLink();
//                            link = shortLink.toString();
//                            Intent intent = new Intent();
//                            String path = buildDynamicLink();
//                            Log.d("actual link","linkkk"+path);
//                            String msg = "visit my awesome website: " +link;
//
//                            intent.setAction(Intent.ACTION_SEND);
//                            intent.putExtra(Intent.EXTRA_TEXT, link);
//                            intent.setType("text/html");
//                            startActivity(intent);
//
//
//
//                        } else {
//                            // Error
//                            // ...
//                        }
//                    }
//                });
//
//    }
    @Override
    public void onVerifySuccess(String data) {

    }

    @Override
    public void onVerifyFailed(String msg) {
        //Toast.makeText(this, "Server Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String response) {

        if (response.equals("Device already exist")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashScreen.this, LoginWithPin
                            .class);
                    i.putExtra("val", "0");
                    startActivity(i);
                    finish();
                }
            }, TIME_OUT);
        }
        if (!(response.equals("Device already exist"))) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashScreen.this, MainUI
                            .class);
                    i.putExtra("val", "1");

                    startActivity(i);
                    finish();
                }
            }, TIME_OUT);
        }

    }

    @Override
    public void onFailed(String response) {
        //Toast.makeText(this, "Server error , Please try again after some time", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void noInternetConnection() {

    }


    @Override
    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                // finish();
                            }
                        }).create();
        dialog.show();
    }

    public void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
