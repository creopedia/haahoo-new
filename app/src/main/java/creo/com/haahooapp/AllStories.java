package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.Adapters.AllStoriesAdapter;
import creo.com.haahooapp.Modal.StoryPojo;
import creo.com.haahooapp.config.ApiClient;

public class AllStories extends AppCompatActivity {

    Context context = this;
    ImageView back;
    Activity activity = this;
    RecyclerView recyclerView;
    AllStoriesAdapter storyAdapter;
    ArrayList<StoryPojo> dataModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_stories);
        recyclerView = findViewById(R.id.recyclerview);
        back = findViewById(R.id.back);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllStories.super.onBackPressed();
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;
                            case R.id.shop:

                                startActivity(new Intent(context, MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                        }
                        return true;
                    }
                });

        getStories();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void getStories(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"stories/story_list/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            Log.d("jhgfdghjk","allstories"+jsonObject1);
                            JSONArray jsonArray = jsonObject1.optJSONArray("data");

                            if (jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    if (jsonObject.optString("mine").equals("1")) {
                                        StoryPojo storyPojo = new StoryPojo();
                                        storyPojo.setStory_id(jsonObject.optString("user_id"));
                                        storyPojo.setName(jsonObject.optString("name"));
                                        storyPojo.setDate(jsonObject.optString("date"));
                                        storyPojo.setTime(jsonObject.optString("time"));
                                        storyPojo.setImage(ApiClient.BASE_URL + "media/" + jsonObject.optString("story"));
                                        dataModelArrayList.add(storyPojo);
                                    }
                                    if (jsonObject.optString("mine").equals("0")) {
                                        StoryPojo storyPojo = new StoryPojo();
                                        storyPojo.setName("My Story");
                                        storyPojo.setDate(jsonObject.optString("date"));
                                        storyPojo.setTime(jsonObject.optString("time"));
                                        storyPojo.setStory_id(jsonObject.optString("user_id"));
                                        storyPojo.setImage(ApiClient.BASE_URL + "media/" + jsonObject.optString("story"));
                                        dataModelArrayList.add(storyPojo);

                                    }
                                }
                            }
                            storyAdapter = new AllStoriesAdapter(context,dataModelArrayList);
                            recyclerView.setAdapter(storyAdapter);
                            recyclerView.setNestedScrollingEnabled(false);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

}