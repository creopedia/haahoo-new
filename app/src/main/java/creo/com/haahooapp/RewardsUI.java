package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.anupkumarpanwar.scratchview.ScratchView;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import creo.com.haahooapp.Adapters.OfferAdapter;
import creo.com.haahooapp.Adapters.RewardAdapter;
import creo.com.haahooapp.Adapters.RewardTabAdapter;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.FragmentToActivity;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

public class RewardsUI extends AppCompatActivity implements FragmentToActivity {

    Context context = this;
    Activity activity = this;
    private RewardAdapter orderAdapter;
    private OfferAdapter offerAdapter;
    ArrayList<String> offersList = new ArrayList<>();
    ArrayList<String> dataModelArrayList = new ArrayList<>();
    RecyclerView earned, offers;
    ViewPager viewPager;
    TabLayout tabLayout;
    ImageView back;
    RelativeLayout rela;
    protected int[] colors;
    protected int goldDark, goldMed, gold, goldLight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards_new_ui);
        Window window = activity.getWindow();
        back = findViewById(R.id.back);
        //main_content = findViewById(R.id.main_content);
        final Resources res = getResources();
        goldDark = res.getColor(R.color.red);
        goldMed = res.getColor(R.color.theme);
        gold = res.getColor(R.color.green);
        goldLight = res.getColor(R.color.gray);
        colors = new int[]{goldDark, goldMed, gold, goldLight};
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RewardsUI.super.onBackPressed();
            }
        });
//        earned = findViewById(R.id.earned1);
//        offers = findViewById(R.id.offers);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("COUPONS"));
        tabLayout.addTab(tabLayout.newTab().setText("OFFERS"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final RewardTabAdapter tabAdapter = new RewardTabAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#0078BC"));
        //tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        //tabLayout.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#ffffff"));
        tabLayout.getTabAt(0).setIcon(R.drawable.earned);
        tabLayout.getTabAt(1).setIcon(R.drawable.offer);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // Toast.makeText(Earnings.this,"pos"+tab.getPosition(),Toast.LENGTH_SHORT).show();
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
//
//        orderAdapter = new RewardAdapter(context,dataModelArrayList);
//        earned.setAdapter(orderAdapter);
//        earned.setNestedScrollingEnabled(false);
//        earned.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
//
//        offerAdapter = new OfferAdapter(context,offersList);
//        offers.setAdapter(offerAdapter);
//        offers.setNestedScrollingEnabled(false);
//        offers.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void send() {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.popupcustom, null);
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .create();
        RelativeLayout rela = view.findViewById(R.id.main);
        ImageView close = view.findViewById(R.id.close);
        ImageView banner = view.findViewById(R.id.banner);
        Glide.with(context).load(ApiClient.BASE_URL + "media/files/events_add/reward2.png").into(banner);
        final KonfettiView konfettiView = view.findViewById(R.id.viewKonfetti);
        dialog.show();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ScratchView scratchView = view.findViewById(R.id.scratch_view);
        scratchView.setRevealListener(new ScratchView.IRevealListener() {
            @Override
            public void onRevealed(ScratchView scratchView) {
                Toast.makeText(getApplicationContext(), "Congrats on unlocking the reward", Toast.LENGTH_LONG).show();
                ;
            }

            @Override
            public void onRevealPercentChangedListener(ScratchView scratchView, float percent) {
                if (percent >= 0.5) {
                    scratchView.reveal();
                }
            }
        });
// Alternatively, we provide some helper methods inside `Utils` to generate square, circle,
// and triangle bitmaps.
// Utils.generateConfettiBitmaps(new int[] { Color.BLACK }, 20 /* size */);
        konfettiView.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));

        konfettiView.build()
                .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
                .setDirection(80.0, 359.0)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(1000L)
                .addShapes(Shape.CIRCLE, Shape.RECT)
                //.addShapes(Shape.Square.INSTANCE, Shape.Circle.INSTANCE)
                .addSizes(new Size(12, 5f))
                // .setPosition(-50f, konfettiView.getWidth() + 50f, -50f, -50f)
                .streamFor(300, 1000L);
    }


}