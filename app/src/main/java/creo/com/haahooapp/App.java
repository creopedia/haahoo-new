package creo.com.haahooapp;

import android.app.Application;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;

public class App extends Application {
    final String versionCode = BuildConfig.VERSION_NAME;
    private static final String TAG = App.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        final FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        Log.d("veeee","version"+versionCode+firebaseRemoteConfig.getString(ForceUpdateChecker.KEY_CURRENT_VERSION));
        // set in-app defaults
        Map<String, Object> remoteConfigDefaults = new HashMap();
        //remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_REQUIRED, "true");
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_CURRENT_VERSION, "1.0");
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_URL,
                "https://play.google.com/store/apps/details?id=creo.com.haahooapp");

        firebaseRemoteConfig.setDefaults(remoteConfigDefaults);
        firebaseRemoteConfig.fetch(3600) // fetch every minutes
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "remoteconfigisfetched.");
                            firebaseRemoteConfig.activateFetched();
                        }
                    }
                });
    }

}
