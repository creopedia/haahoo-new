package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Empty_wishlist extends AppCompatActivity {

    ImageView back;
    TextView start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty_wishlist);
        back = findViewById(R.id.back);
        start = findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Empty_wishlist.this,NewHome.class));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(Empty_wishlist.this,NewHome.class));
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Empty_wishlist.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Empty_wishlist.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Empty_wishlist.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(Empty_wishlist.this, MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(Empty_wishlist.this, AllStories.class));
                                break;

                        }
                        return true;
                    }
                });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(Empty_wishlist.this,NewHome.class));
    }
}
