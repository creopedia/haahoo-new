package creo.com.haahooapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import creo.com.haahooapp.config.ApiClient;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class ContactUs extends AppCompatActivity {

  TextView primary,office;
  Context context = this;
  ImageView back,logo;
  Activity activity = this;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
    getSupportActionBar().hide();
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_contact_us);
    logo=findViewById(R.id.logo);
    Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/haahoo_logo1.png").into(logo);
    Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
    window.setStatusBarColor(activity.getResources().getColor(R.color.black));
    back = findViewById(R.id.back);

    back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ContactUs.super.onBackPressed();
      }
    });

    primary = findViewById(R.id.primary_no);
    office = findViewById(R.id.office);

    primary.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+primary.getText().toString()));
        startActivity(intent);
      }
    });

//    office.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        Intent intent = new Intent(Intent.ACTION_DIAL);
//        intent.setData(Uri.parse("tel:"+office.getText().toString()));
//        startActivity(intent);
//      }
//    });
  }
}
