package creo.com.haahooapp;

import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import com.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import creo.com.haahooapp.Adapters.ViewPagerAdapter;

public class ViewPager2 extends AppCompatActivity {

  private static ViewPager mPager;
  private static int currentPage = 0;
  private static int NUM_PAGES = 0;
  private static final Integer[] IMAGES= {};
  private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
    getSupportActionBar().hide();
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_view_pager);
    mPager =  findViewById(R.id.pager);
    init();
  }

  private void init() {
    for(int i=0;i<IMAGES.length;i++)
      ImagesArray.add(IMAGES[i]);




    mPager.setAdapter(new ViewPagerAdapter(ViewPager2.this,ImagesArray));


    CirclePageIndicator indicator = (CirclePageIndicator)
            findViewById(R.id.indicator);

    indicator.setViewPager(mPager);

    final float density = getResources().getDisplayMetrics().density;

//Set circle1 indicator radius
    indicator.setRadius(5 * density);

    NUM_PAGES =IMAGES.length;

    // Auto start of viewpager
    final Handler handler = new Handler();
    final Runnable Update = new Runnable() {
      public void run() {
        if (currentPage == NUM_PAGES) {
          currentPage = 0;
        }
        mPager.setCurrentItem(currentPage++, true);
      }
    };
    Timer swipeTimer = new Timer();
    swipeTimer.schedule(new TimerTask() {
      @Override
      public void run() {
        handler.post(Update);
      }
    }, 1000, 1000);

    // Pager listener over indicator
    indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

      @Override
      public void onPageSelected(int position) {
        currentPage = position;

      }

      @Override
      public void onPageScrolled(int pos, float arg1, int arg2) {

      }

      @Override
      public void onPageScrollStateChanged(int pos) {

      }
    });

  }
}
