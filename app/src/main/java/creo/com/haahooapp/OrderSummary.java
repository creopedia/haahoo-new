package creo.com.haahooapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import creo.com.haahooapp.Adapters.DeliveryModeAdapter;
import creo.com.haahooapp.Adapters.SummaryAdapter;
import creo.com.haahooapp.Modal.CartPojo;
import creo.com.haahooapp.Modal.DeliveryModes;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.RetrofitClient;
import creo.com.haahooapp.interfaces.OnClickMenu;
import retrofit2.Call;
import retrofit2.Callback;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.razorpay.Checkout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class OrderSummary extends AppCompatActivity implements OnClickMenu {

    TextView makepayment, total;
    EditText date;
    ScrollView scrollView;
    RecyclerView recyclerView;
    PopupWindow popupWindow;
    ImageView back;
    Activity activity = this;
    RelativeLayout relativeLayout;
    ProgressDialog progressDialog;
    String token = null;
    final Calendar myCalendar = Calendar.getInstance();
    ArrayList<String> name = new ArrayList<>();
    public List<CartPojo> pjo = new ArrayList<>();
    String payment_mode = "null";
    Context context = this;
    public int i = 0;
    String delivery = "null";
    Spinner spinner;
    JSONArray arr = new JSONArray();
    JSONObject products = new JSONObject();
    public String orderid = null;
    TextView deliverycharge;
    String val = "0", shop_id;
    ArrayList<String> newArray = new ArrayList<>();
    TextView show, hide;
    LinearLayout linearLayout;
    RecyclerView modes;
    List<String> stringList = new ArrayList<>();
    DeliveryModeAdapter deliveryModeAdapter;
    SummaryAdapter cartAdapter;
    String bazaar_status = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary);
        Window window = activity.getWindow();
        spinner = findViewById(R.id.spinner);
        date = findViewById(R.id.date);
        modes = findViewById(R.id.modes);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading..");
        progressDialog.setMessage("Fetching Address..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.show();
        linearLayout = findViewById(R.id.linearlay);
        deliverycharge = findViewById(R.id.deliverycharge);
        deliveryModeAdapter = new DeliveryModeAdapter(context, stringList, this::onClick);
        cartAdapter = new SummaryAdapter(pjo, context);
        //progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        relativeLayout = findViewById(R.id.relative);
        scrollView = findViewById(R.id.scrollview);
        recyclerView = findViewById(R.id.recyclerview);
        back = findViewById(R.id.back);
        show = findViewById(R.id.show);
        hide = findViewById(R.id.hide);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiClient.price = val;
                OrderSummary.super.onBackPressed();
            }
        });
        total = findViewById(R.id.total);
        total.setText("Rs. " + ApiClient.price);
        scrollView.fullScroll(ScrollView.FOCUS_UP);
        if (ApiClient.bazaar.equals("null")) {

            for (int i = 0; i < ApiClient.name.size(); i++) {
                String str = ApiClient.prices.get(i);
                str = str.replaceAll("[^\\d.]", "");
                int price = Integer.parseInt(str) * Integer.parseInt(ApiClient.quantity.get(i));
                Log.d("FALSEEEEE", "hhxztrgxxty" + ApiClient.quantity.size());
                bazaar_status = "0";
                CartPojo cartPojo = new CartPojo();
                cartPojo.setName(ApiClient.name.get(i));
                cartPojo.setPrice("Rs. " + String.valueOf(price));
                cartPojo.setImage(ApiClient.images.get(i));
                cartPojo.setQuantity("Quantity : " + ApiClient.quantity.get(i));
                pjo.add(cartPojo);
            }
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager manager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);
            cartAdapter = new SummaryAdapter(pjo, activity);
            recyclerView.setAdapter(cartAdapter);
            cartAdapter.notifyDataSetChanged();
        }
        if (ApiClient.bazaar.equals("true")) {
            deliverycharge.setText("FREE");
            total.setText("Rs. " + ApiClient.prices.get(0));
            for (int i = 0; i < ApiClient.name.size(); i++) {
                String str = ApiClient.prices.get(i);
                str = str.replaceAll("[^\\d.]", "");
                int price = Integer.parseInt(str);
                CartPojo cartPojo = new CartPojo();
                cartPojo.setName(ApiClient.name.get(i));
                bazaar_status = "1";
                cartPojo.setPrice("Rs. " + String.valueOf(price));
                cartPojo.setImage(ApiClient.images.get(i));
                cartPojo.setQuantity("Quantity : " + ApiClient.quantity.get(i));
                pjo.add(cartPojo);
            }
            cartAdapter = new SummaryAdapter(pjo, context);
            recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(cartAdapter);
        }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        shop_id = (pref.getString("shop_id", ""));
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        try {
            HashMap<String, JSONObject> map = new HashMap<String, JSONObject>();
            if (ApiClient.variant_ids.size() == 0) {
                ApiClient.variant_ids.add("0");
            }
            for (int i = 0; i < ApiClient.ids.size(); i++) {
                JSONObject json = new JSONObject();
                json.put("id", ApiClient.ids.get(i));
                json.put("price", ApiClient.prices.get(i));
                json.put("quantity", ApiClient.quantity.get(i));
                json.put("refer_token", "");
                json.put("variant", ApiClient.variant_ids.get(i));
                json.put("virtual", ApiClient.virtual.get(i));
                map.put("json" + i, json);
                arr.put(map.get("json" + i));
            }
            products.put("product", arr);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        getDeliveryCharges();

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(OrderSummary.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(OrderSummary.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(OrderSummary.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

//                                startActivity(new Intent(context, MyShopNew.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(context, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
        Checkout.preload(getApplicationContext());
        makepayment = findViewById(R.id.gotopayment);

//        apply.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cartPresenter.addToCart(getIntent().getExtras().getString("id"),quantity.getText().toString(),token);
//            }
//        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString().trim();
                if (value.equals("Custom Delivery")) {
                    date.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        DatePickerDialog.OnDateSetListener datepick = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePicker = new DatePickerDialog(OrderSummary.this, datepick, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePicker.getDatePicker().setMinDate(new Date().getTime());
                datePicker.show();
            }
        });

        makepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (delivery.equals("null")) {
                    Toast.makeText(context, "Please Choose Your Delivery Mode", Toast.LENGTH_SHORT).show();
                } else {
                    LayoutInflater layoutInflater = (LayoutInflater) OrderSummary.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View customView = layoutInflater.inflate(R.layout.payment_modes, null);
                    RadioButton cod = customView.findViewById(R.id.cod);
                    RadioButton online = customView.findViewById(R.id.online);
                    cod.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (ApiClient.bazaar.equals("null")) {
                                payment_mode = "COD";
                            }
                            if (ApiClient.bazaar.equals("true")) {
                                payment_mode = "COD";
                                Toast.makeText(context, "Cash On Delivery is not available right now..", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    online.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            payment_mode = "ONLINE";
                        }
                    });
                    Button closePopupBtn = (Button) customView.findViewById(R.id.closePopupBtn);
                    final TextView apply = customView.findViewById(R.id.apply);
                    apply.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (payment_mode.equals("null")) {
                                Toast.makeText(OrderSummary.this, "Please select the mode of payment", Toast.LENGTH_SHORT).show();
                            }
                            if (payment_mode.equals("COD")) {
                                if (ApiClient.bazaar.equals("null")) {
                                    showProgressDialogWithTitle("Confirming Your Order...");
                                    makepayment.setVisibility(View.GONE);
                                    apply.setVisibility(View.GONE);
                                    checkavail();
                                }
                                if (ApiClient.bazaar.equals("true")) {
                                    Toast.makeText(context, "Please choose payment mode", Toast.LENGTH_SHORT).show();
                                }
                                //create_order(payment_mode);
                            }
                            if (payment_mode.equals("ONLINE")) {
                                showProgressDialogWithTitle("Confirming Your Order...");
                                checkavail_online();
//                            Intent intent = new Intent(OrderSummary.this,MakePayment.class);
//                            intent.putExtra("products",products.toString());
//                            intent.putExtra("address",ApiClient.address);
//                            intent.putExtra("payment_method",payment_mode);
//                            ApiClient.token = token;
//                            intent.putExtra("token",token);
//                            startActivity(intent);
                            }
                        }
                    });

                    popupWindow = new PopupWindow(customView, ViewPager.LayoutParams.MATCH_PARENT, 1000);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(
                            android.graphics.Color.TRANSPARENT));
                    relativeLayout.setAlpha(0.2F);
                    popupWindow.showAtLocation(relativeLayout, Gravity.CENTER, 0, 0);
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                            relativeLayout.setAlpha(1.0F);
                        }
                    });

                }
            }

        });
        try {
            getDeliveryModes();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateLabel() {
        String myFormat = "yyyy/MM/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        date.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ApiClient.productinshopid = "0";
        ApiClient.product_referral_id = "0";
    }

    public void getDeliveryModes() {

        if (shop_id.length() == 0) {
            shop_id = ApiClient.shop_id_del;
        }
        Log.d("HGCHXGX", "OHIUFUD" + ApiClient.shop_id_del + "\n" + shop_id);
        Call<DeliveryModes> call = RetrofitClient
                .getInstance()
                .getApi()
                .getDeliveryModes("Token " + token, shop_id);
        call.enqueue(new Callback<DeliveryModes>() {
            @Override
            public void onResponse(Call<DeliveryModes> call, retrofit2.Response<DeliveryModes> response) {
                if (response.isSuccessful()) {
                    DeliveryModes deliveryModes = response.body();
                    if (response.body() != null) {
                        String success = response.body().getMessage();
                        if (success != null && success.equals("success")) {
                            ArrayList<String> type = new ArrayList<>();
                            type.add("Choose Your Delivery Mode");
                            newArray = (ArrayList<String>) deliveryModes.getDelivery_mode();
                            for (int j = 0; j < newArray.size(); j++) {
                                type.add(newArray.get(j));
                                stringList.add(newArray.get(j));
                            }
//                            ArrayAdapter<String> adapter =
//                                    new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, type);
//                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spinner.setAdapter(adapter);
                            modes.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            modes.setNestedScrollingEnabled(false);
                            modes.setAdapter(deliveryModeAdapter);
                            deliveryModeAdapter.notifyDataSetChanged();
                            progressDialog.dismiss();
                        }
                    }
                }
                try {
                    if (response.body().getCode().equals("203")) {
                        progressDialog.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DeliveryModes> call, Throwable t) {

            }
        });
    }

    public void getDeliveryCharges() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "order_details/delivery_charge_calculation/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String charge = jsonObject.optString("delivery_charge").trim();
                            val = ApiClient.price;
                            String value = ApiClient.price.replace("₹", "").trim();
                            if (charge.equals("0")) {
                                deliverycharge.setText("FREE");
                            }
                            if (!(charge.equals("0"))) {
                                deliverycharge.setText("₹ " + charge);
                                int valint = Integer.parseInt(value) + Integer.parseInt(charge);
                                total.setText("Rs. " + String.valueOf(valint));
                                ApiClient.price = total.getText().toString();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(OrderSummary.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product", products.toString());
                Log.d("GCGHCGHC","hjvghchgc"+products.toString());
                params.put("address_id", ApiClient.address);
                if (ApiClient.bazaar.equals("true")) {
                    params.put("bazar_key", "1");
                }
                if (ApiClient.bazaar.equals("null")) {
                    params.put("bazar_key", "0");
                }

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    void checkavail_online() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "api_shop_app/check_delivery_availablity/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("jsknkn", "online_availability" + jsonObject);
                            String message = jsonObject.getString("message");

                            if (message.equals("success")) {
//                                create_order111("ONLINE");
                                //create_order111("ONLINE");
                                Intent intent = new Intent(OrderSummary.this, MakePayment.class);
                                intent.putExtra("products", products.toString());
                                intent.putExtra("address", ApiClient.address);
                                intent.putExtra("payment_method", payment_mode);
                                intent.putExtra("bazaar_status", bazaar_status);
                                hideProgressDialogWithTitle();
                                ApiClient.token = token;
                                ApiClient.ids.clear();
                                intent.putExtra("token", token);
                                startActivity(intent);
                            }
                            if (message.equals("Delivery not available to this location")) {
                                Toast.makeText(OrderSummary.this, "Delivery not available to this location", Toast.LENGTH_SHORT).show();
                                hideProgressDialogWithTitle();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(OrderSummary.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", ApiClient.ids.toString());
                params.put("address_id", ApiClient.address);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    void checkavail() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "api_shop_app/check_delivery_availablity/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("jsknknjjj", "online_availability" + jsonObject);
                            String message = jsonObject.getString("message");
                            if (message.equals("success")) {
                                create_order(payment_mode);
                            }
                            if (message.equals("Delivery not available to this location")) {
                                hideProgressDialogWithTitle();
                                makepayment.setVisibility(View.VISIBLE);
                                popupWindow.dismiss();
                                relativeLayout.setAlpha(1.0F);
                                Toast.makeText(OrderSummary.this, "Delivery not available to this location", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialogWithTitle();
                        Toast.makeText(OrderSummary.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", ApiClient.ids.toString());
                params.put("address_id", ApiClient.address);
                Log.d("ghdtye", "ftdrtsy" + ApiClient.ids.toString() + ApiClient.address);

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void create_order111(final String payment_mode) {
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "order_details/place_order/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String code = jsonObject.optString("code");
                            if (code.equals("203")) {
                                hideProgressDialogWithTitle();
                                popupWindow.dismiss();
                                relativeLayout.setAlpha(1.0F);
                                makepayment.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product", products.toString());
                params.put("address_id", ApiClient.address);
                params.put("payment_method", payment_mode);
                params.put("payment_status", "0");
                params.put("payment_id", "");
                params.put("payment_amount", "");
                params.put("delivery_mode", delivery);
                params.put("pdt_refer", ApiClient.referal_id);
                params.put("pdt_refer_id", ApiClient.product_referral_id);

                return params;

            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;

            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public void create_order(final String payment_mode) {
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "order_details/place_order/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String code = jsonObject.optString("code");
                            if (code.equals("203")) {
                                hideProgressDialogWithTitle();
                                popupWindow.dismiss();
                                relativeLayout.setAlpha(1.0F);
                                makepayment.setVisibility(View.VISIBLE);
                            }
                            if (code.equals("200")) {
                                if (jsonObject.getString("message").toString().equals("Success")) {
                                    popupWindow.dismiss();
                                    relativeLayout.setAlpha(1.0F);
                                    ApiClient.ids.clear();
                                    hideProgressDialogWithTitle();
                                    startActivity(new Intent(OrderSummary.this, OrderSuccess.class));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product", products.toString());
                params.put("address_id", ApiClient.address);
                params.put("payment_method", payment_mode);
                params.put("payment_status", "0");
                params.put("payment_id", "");
                try {
                    params.put("payment_amount", total.getText().toString().split("Rs. ")[1]);
                }catch (Exception e){
                    e.printStackTrace();
                }
                params.put("delivery_mode", delivery);
                params.put("pdt_refer", ApiClient.product_referral_id);
                params.put("bazar_key", bazaar_status);
                //params.put("pdt_refer_id", ApiClient.product_referral_id);
                Log.d("GHGHHGGHGH", "TERETE" + bazaar_status + products.toString() + ApiClient.address + payment_mode + delivery + ApiClient.referal_id + ApiClient.product_referral_id);

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;

            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        ApiClient.price = val;
        super.onBackPressed();
    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void onClick(String id) {
        if (id.toLowerCase().contains("haahoo delivery")) {
            delivery = id;
        } else {
            delivery = "null";
            Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();
        }
    }
}