package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.os.Parcelable;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.ImageAdapter;
import creo.com.haahooapp.Adapters.InfiniteScrolling;
import creo.com.haahooapp.Implementation.PinVerifyPresenterImpl;
import creo.com.haahooapp.Modal.ProductPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.EndlessRecyclerViewScrollListener;
import creo.com.haahooapp.interfaces.PinVerifyCallback;
import creo.com.haahooapp.interfaces.PinVerifyPresenter;
import creo.com.haahooapp.interfaces.PinVerifyView;

public class MoreProducts extends AppCompatActivity implements PinVerifyCallback,PinVerifyView {

    GridView gridView;
    PinVerifyPresenter pinVerifyPresenter;
    JSONArray jsonObject = new JSONArray();
    ArrayList<String> ids = new ArrayList<>();
    private InfiniteScrolling infiniteScrolling;
    Activity activity = this;
    ImageView back;
    ImageAdapter imageAdapter = null;
    ProgressDialog progressDialog;
    Context context = this;
    private Parcelable recyclerViewState;
    String next_api=ApiClient.BASE_URL+"product_details/product_view/";
    ArrayList<String> names = new ArrayList<>();
    ArrayList<String> description = new ArrayList<>();
    private List<ProductPojo> pojo = new ArrayList<>();
    ArrayList<String> images = new ArrayList<>();
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_products);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        pojo.clear();
        infiniteScrolling = new InfiniteScrolling();
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoreProducts.super.onBackPressed();
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(MoreProducts.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(MoreProducts.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(MoreProducts.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(MoreProducts.this, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }

                        return true;

                    }
                });
        progressDialog = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        pinVerifyPresenter = new PinVerifyPresenterImpl(this);
        getProducts123(next_api);
        gridView = findViewById(R.id.simpleGridView);
        infiniteScrolling.setOnReachEndListener(new InfiniteScrolling.OnReachEndListener() {
            @Override
            public void onEndReached() {
                        getProducts123(next_api);
            }
        });

        gridView.setOnScrollListener(infiniteScrolling);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    }



    public void getProducts123(String url){

        if(url.equals("null")){
            Toast.makeText(MoreProducts.this,"That's all for now" ,Toast.LENGTH_SHORT).show();
        }
        if(!(url.equals("null"))) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String results = jsonObject.getString("results");
                                next_api = jsonObject.getString("next");
                                JSONArray jsonArray = new JSONArray(results);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    ids.add(jsonObject1.getString("pk"));
                                    JSONObject jsonObject2 = jsonObject1.getJSONObject("fields");
                                    names.add(jsonObject2.getString("name"));
                                    description.add("₹ " + jsonObject2.getString("price"));
                                    String images1 = jsonObject2.getString("image");
                                    String[] seperated = images1.split(",");
                                    String split = seperated[0].replace("[", "");
                                    images.add(split);

                                }
                                final List<ProductPojo> productPojos = new ArrayList<>();
                                for (int i = 0; i < names.size(); i++) {
                                    ProductPojo productPojo = new ProductPojo(names.get(i), description.get(i), images.get(i),images.get(i),images.get(i),images.get(i));
                                    productPojos.add(productPojo);
                                }

                                infiniteScrolling.notifyItemsCountChanged();

                                if (imageAdapter == null) {
                                    imageAdapter = new ImageAdapter(context, productPojos);
                                    gridView.setAdapter(imageAdapter);
                                } else {
                                    imageAdapter.updateData(productPojos);
                                    imageAdapter.notifyDataSetChanged();
                                }

                                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    public void onItemClick(AdapterView<?> parent, View v,
                                                            int position, long id) {
                                        Intent intent = new Intent(MoreProducts.this, ProductDetails.class);
                                        intent.putExtra("id", ids.get(position));
                                        startActivity(intent);
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MoreProducts.this, error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }
    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void onVerifySuccess(String data, String mem) {

    }

    @Override
    public void onVerifyFailed(String msg) {

    }

    @Override
    public void jsonObjectSuccess(JSONArray jsonObject,String next) {

    }



    @Override
    public void onSuccess(String response, String mem) {

    }

    @Override
    public void onFailed(String response) {

    }

    @Override
    public void jsonSUccess(JSONArray jsonObject1,String next) {
//        next_api = next;
//        jsonObject = jsonObject1;
//        try {
//            for (int i = 0; i < jsonObject.length(); i++) {
//
//                JSONObject jsonObject2 = jsonObject.getJSONObject(i);
//                ids.add(jsonObject2.getString("pk"));
//                JSONObject jsonObject3 = jsonObject2.getJSONObject("fields");
//                names.add(jsonObject3.getString("name"));
////                images.add(jsonObject3.getString("image"));
//                description.add("₹ "+jsonObject3.getString("price"));
//                String images1 = jsonObject3.getString("image");
//                String[] seperated = images1.split(",");
//                String split = seperated[0].replace("[","");
//                images.add(split);
//            }
//            for(int i=0;i<names.size();i++) {
//                ProductPojo productPojo = new ProductPojo(names.get(i),description.get(i),images.get(i));
//                pojo.add(productPojo);
//            }
//            MoreProductsAdapter eventAdapter = new MoreProductsAdapter(pojo,this);
//            gridView.setHasFixedSize(true);
//            gridView.setLayoutManager(new LinearLayoutManager(this));
//            gridView.setAdapter(eventAdapter);
//            hideProgressDialogWithTitle();
//            eventAdapter.setOnBottomReachedListener(new OnBottomReachedListener() {
//                @Override
//                public void onBottomReached(int position) {
//                    getProducts123(next_api);
//                }
//            });
//
//            ItemClickSupport.addTo(gridView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
//                @Override
//                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                    Intent intent = new Intent(MoreProducts.this,ProductDetails.class);
//                    intent.putExtra("id",ids.get(position));
//                    startActivity(intent);
//                }
//            });
//        }catch (JSONException e){
//            e.printStackTrace();
//        }
    }

    @Override
    public void noInternetConnection() {

    }
}
