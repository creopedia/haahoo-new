package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Modal.EarningPojo;
import creo.com.haahooapp.config.ApiClient;

public class Earnings extends AppCompatActivity {
    Activity activity = this;
    ImageView back;
    TextView total_earnings, totalbal;
    CardView salary;
    Context context = this;
    //    RecyclerView recyclerView;
    TabLayout tabLayout;
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> earnings = new ArrayList<>();
    ArrayList<String> date = new ArrayList<>();
    ViewPager viewPager;
    private List<EarningPojo> pojo = new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earnings);
        total_earnings = findViewById(R.id.total_earnings);
        salary = findViewById(R.id.salary);
        totalbal = findViewById(R.id.totalbal);
        progressDialog = new ProgressDialog(this);

        progressDialog.setTitle("Loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Getting Details...");
        progressDialog.show();
        checkRole();
        getTransactionDetails();

        //getEarnings();
        Window window = activity.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back = findViewById(R.id.back);
        tabLayout = findViewById(R.id.tabLayout);
        salary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Earnings.this, SalaryCriteria.class));
            }
        });
        viewPager = findViewById(R.id.viewPager);
        // recyclerView = findViewById(R.id.recyclerView);
        tabLayout.addTab(tabLayout.newTab().setText("Referral"));
        tabLayout.addTab(tabLayout.newTab().setText("Resell"));
        tabLayout.addTab(tabLayout.newTab().setText("Add Shop"));
        //tabLayout.addTab(tabLayout.newTab().setText("Joining Bonus"));
        //tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final EarningTabAdapter tabAdapter = new EarningTabAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // Toast.makeText(Earnings.this,"pos"+tab.getPosition(),Toast.LENGTH_SHORT).show();
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Earnings.super.onBackPressed();
            }
        });


        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Earnings.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Earnings.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Earnings.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(Earnings.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
    }

    public void getTransactionDetails(){
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "incentive_and_earnings/view_user_earnings/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {

                            JSONObject obj = new JSONObject(response);
                            total_earnings.setText("Rs. " + obj.optString("data"));
                            totalbal.setText("Rs. " + obj.optString("data"));
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
                progressDialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        queue.add(stringRequest);


    }

//    public void getTransactionDetails() {
//        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
//        String token = (pref.getString("token", ""));
//        RequestQueue queue = Volley.newRequestQueue(context);
//        //this is the url where you want to send the request
//
//        String url = ApiClient.BASE_URL + "api_shop_app/show_withdraw_history/";
//
//        // Request a string response from the provided URL.
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                new com.android.volley.Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        // Display the response string.
//                        try {
//
//                            JSONObject obj = new JSONObject(response);
//                            Log.d("withdrawfrag", "dszfxgh" + obj);
//                            total_earnings.setText("Rs. " + obj.optString("wallet_balance"));
//                            totalbal.setText("Rs. " + obj.optString("reward"));
//                            progressDialog.dismiss();
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new com.android.volley.Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //
//                progressDialog.dismiss();
//            }
//        }) {
//
//            @Override
//            public Map<String, String> getHeaders() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Authorization", "Token " + token);
//                return params;
//            }
//        };
//
//        queue.add(stringRequest);
//
//    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void checkRole() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "direct_selling/check_role/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            // Log.d("roless","cjeckrole"+response);
                            JSONObject jsonObject1 = new JSONObject(response);
                            String message = jsonObject1.optString("message");
                            String user_type = jsonObject1.optString("types");
                            if (user_type.equals("freelancer")) {
                                salary.setVisibility(View.GONE);
                            }
                            if (user_type.equals("employee")) {
                                salary.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
