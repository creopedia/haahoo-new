package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import cdflynn.android.library.checkview.CheckView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class OrderSuccess extends AppCompatActivity {

    CheckView checkView;
    TextView textView;
    Activity activity = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_success);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        checkView = findViewById(R.id.check);
        textView = findViewById(R.id.text);
        checkView.check();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms

              Intent intent = new Intent(OrderSuccess.this,Orders.class);
              intent.putExtra("fromactivity","payment");
              startActivity(intent);
            }
        }, 4000);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(OrderSuccess.this,NewHome.class));
    }
}
