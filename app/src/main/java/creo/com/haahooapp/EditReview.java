package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.config.ApiClient;

public class EditReview extends AppCompatActivity {

    String productimage="null";
    String productname = "null";
    String productprice = "null";
    String productrating = "null";
    String shoprating = "null";
    String description = "null";
    String productid = "null";
    String reviewid="null";
    Activity activity = this;
    Context context = this;
    ImageView back,img;
    TextView name,price,submit;
    String pdt_rating = "";
    String shp_rating = "";
    EditText review;
    RatingBar product_rating,shop_rating;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_review);
        back = findViewById(R.id.back);
        name = findViewById(R.id.name);
        price = findViewById(R.id.price);
        submit = findViewById(R.id.submit);
        img = findViewById(R.id.img);
        product_rating = findViewById(R.id.ratingBar);
        review = findViewById(R.id.review);
        shop_rating = findViewById(R.id.rateshop);

        product_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Float ratingval = (Float) rating;
                Float ratingvalue = ratingBar.getRating();
                pdt_rating = String.valueOf(ratingvalue);
            }
        });
        shop_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Float ratingvalue = (Float) rating;
                Float ratingval = ratingBar.getRating();
                shp_rating = String.valueOf(ratingval);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditReview.super.onBackPressed();
            }
        });
        Bundle bundle = getIntent().getExtras();
        productrating = bundle.getString("productrating");
        description = bundle.getString("description");
        shoprating = bundle.getString("shoprating");
        product_rating.setRating(Float.parseFloat(productrating));
        shop_rating.setRating(Float.parseFloat(shoprating));
        productname = bundle.getString("productname");
        productimage = bundle.getString("productimage");
        productid = bundle.getString("productid");
        productprice = bundle.getString("productprice");
        reviewid = bundle.getString("reviewid");
        name.setText(productname);
        price.setText(productprice);
        review.setText(description);
        Glide.with(context).load(productimage).into(img);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pdt_rating.length()>0 && shp_rating.length()>0) {
                    submitreview(pdt_rating, shp_rating, review.getText().toString(), productid);
                }
                if (pdt_rating.length() == 0 || shp_rating.length()==0){
                    Toast.makeText(context,"Please rate product and shop",Toast.LENGTH_SHORT).show();
                }
                // Toast.makeText(context,"Please rate shop",Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void submitreview(String productrating,String shoprating,String review,String product_id){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"api_shop_app/pdt_rating_edit/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("nnnnn", "jsonarray" + jsonObject);
                            String message = jsonObject.optString("message");
                            if (message.equals("success")){
                                Toast.makeText(context,"Review has been edited successfully",Toast.LENGTH_SHORT).show();
                            }
                            if (message.equals("Failed")){
                                Toast.makeText(context,"Failed to add review",Toast.LENGTH_SHORT).show();
                            }
                            // Toast.makeText(context,"successfull",Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {


            @Override
            protected java.util.Map<String, String> getParams() throws AuthFailureError {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("pdt_id", product_id);
                params.put("rating",productrating);
                params.put("shop_rate",shoprating);
                params.put("review",review);
                params.put("id",reviewid);
                // Log.d("paramsssssssss","param"+product_id+productrating+shoprating+review);
                return params;
            }

            @Override
            public java.util.Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);

                return params;
            }
        };

        queue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
