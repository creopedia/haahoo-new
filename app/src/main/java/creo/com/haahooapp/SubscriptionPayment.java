package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.config.ApiClient;

public class SubscriptionPayment extends AppCompatActivity implements PaymentResultListener {
Activity activity = this;
Context context = this;
String amount = "null";
String type = "null";
String shop_id = "null";
String product_id = "null";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_payment);
        Bundle bundle = getIntent().getExtras();
        amount = bundle.getString("amount");
        type = bundle.getString("type");
        shop_id = bundle.getString("shopid");
        product_id = bundle.getString("productid");
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Haahoo");
            options.put("description", "Subscription Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("currency", "INR");
            // String price[] = ApiClient.price.split("₹ ");
            options.put("amount", amount);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment", Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
//        Toast.makeText(activity, "Payment successfull", Toast.LENGTH_SHORT)
//                .show();
        //startActivity(new Intent(context,NewHome.class));
        submitDetails(s,amount,type,product_id,shop_id);

    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(activity, "Payment has failed", Toast.LENGTH_SHORT)
                .show();
        super.onBackPressed();
    }

    public void submitDetails(String payment_id,String pay_amount, String payment_type,String productid,String shopid){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"virtual_shop/sub_pay_accpt/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("paymmmments","payments"+jsonObject);
                            String message = jsonObject.optString("message");
                            if (message.equals("success")){
                                Toast.makeText(context,"Payment successfull",Toast.LENGTH_SHORT).show();
                                Intent intent3 = new Intent(context,SubscriptionList.class);
                                intent3.putExtra("fromactivity","payment");
                                startActivity(intent3);
                            }
                            if (message.equals("Failed")){
                                Toast.makeText(context,"Payment failed ",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            //

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("payment_id", payment_id);
                params.put("payment_amount",pay_amount);
                params.put("mode",type);
                params.put("address_id",ApiClient.address);
                params.put("shop_id",shop_id);
                params.put("pdt_id",product_id);
                //Log.d("orid_price","hjgh"+price2);
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

}
