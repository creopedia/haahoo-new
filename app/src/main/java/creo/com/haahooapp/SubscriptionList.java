package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.Adapters.ActiveSubscriptionAdapter;
import creo.com.haahooapp.Modal.Subscribedpojo;
import creo.com.haahooapp.Modal.SubscriptionPlanPojo;
import creo.com.haahooapp.config.ApiClient;

public class SubscriptionList extends AppCompatActivity {

    RecyclerView recycle;
    Context context = this;
    ImageView back;
    Activity activity = this;
    String fromactivity = "null";
    ArrayList<String>datas = new ArrayList<>();
    ActiveSubscriptionAdapter activeSubscriptionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_list);
        recycle = findViewById(R.id.recycle);
        Window window = activity.getWindow();
        Bundle bundle = getIntent().getExtras();
        fromactivity = bundle.getString("fromactivity");
        getSubscriptionDetails();
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fromactivity.equals("home")){
                    SubscriptionList.super.onBackPressed();
                }
                if (fromactivity.equals("payment")){
                    startActivity(new Intent(context,NewHome.class));
                }
            }
        });
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));


        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.action_home:
                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:
                                startActivity(new Intent(context,Search.class));
                                break;

                            case R.id.action_account:
                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context,MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context,AllStories.class));
                                break;

                        }
                        return true;
                    }
                });
    }

    @Override
    public void onBackPressed() {
        if (fromactivity.equals("home")){
            SubscriptionList.super.onBackPressed();
        }
        if (fromactivity.equals("payment")){
            startActivity(new Intent(context,NewHome.class));
        }
    }

    public void getSubscriptionDetails(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"virtual_shop/user_subscription_list/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("hjgfhjgfdghf","lll"+obj);
                            ArrayList<Subscribedpojo> subscribedpojos = new ArrayList<>();
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0 ;i < jsonArray.length() ; i++){
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                Subscribedpojo subscribedpojo = new Subscribedpojo();
                                subscribedpojo.setPdt_id(jsonObject.optString("pdt_id"));
                                subscribedpojo.setOrder_id(jsonObject.optString("order_id"));
                                subscribedpojo.setMode(jsonObject.optString("mode"));
                                subscribedpojo.setPdt_name(jsonObject.optString("pdt_name"));
                                subscribedpojo.setBenefits(jsonObject.optString("benefit"));
                                subscribedpojo.setDays(jsonObject.optString("days_left"));
                                String[] seperatedv = jsonObject.optString("pdt_image").split(",");
                                subscribedpojo.setPdt_image(seperatedv[0].replace("[","").replace("]",""));
                                subscribedpojo.setShop_name(jsonObject.optString("shop_name"));
                                subscribedpojo.setAmount(jsonObject.optString("amount"));
                                subscribedpojo.setFrom("Valid from : "+jsonObject.optString("ordered_at"));
                                subscribedpojo.setUntil("Valid until : "+jsonObject.optString("expired_at"));
                                subscribedpojos.add(subscribedpojo);
                            }
                            activeSubscriptionAdapter = new ActiveSubscriptionAdapter(subscribedpojos,context);
                            //recycle.setAdapter(activeSubscriptionAdapter);
                            recycle.setHasFixedSize(true);
                            recycle.setNestedScrollingEnabled(false);
                            recycle.setLayoutManager(new LinearLayoutManager(context));
                            recycle.setAdapter(activeSubscriptionAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            //

            }
        }) {


            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}