package creo.com.haahooapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import creo.com.haahooapp.Adapters.OrderAdapter;
import creo.com.haahooapp.Adapters.TransactionHistoryAdapter;
import creo.com.haahooapp.Modal.OrderPojo;
import creo.com.haahooapp.Modal.TransactionPojo;
import creo.com.haahooapp.config.ApiClient;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * <p>
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class ProfileWithdrawalFragment extends Fragment {

    ImageView imageView;
    RelativeLayout relativeLayout;
    TextView name, earnings, withdraw, balance;
    RecyclerView recyclerView;
    String token = null;
    private TransactionHistoryAdapter orderAdapter;
    ArrayList<TransactionPojo> transactionPojos;
    ProgressDialog progressDialog;

    public ProfileWithdrawalFragment() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileWithdrawalFragment.
     */
    // TODO: Rename and change types and number of parameters
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_withdrawal, container, false);
        imageView = view.findViewById(R.id.img);
        withdraw = view.findViewById(R.id.withdraw);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setTitle("Loading..");
        progressDialog.setMessage("Fetching Datas..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.show();
        balance = view.findViewById(R.id.availablebal);
        name = view.findViewById(R.id.name);
        getTransactionDetails();
        earnings = view.findViewById(R.id.earnings);
        recyclerView = view.findViewById(R.id.recycle);
        transactionPojos = new ArrayList<>();


        relativeLayout = view.findViewById(R.id.sendmoneyrel);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), WithdrawalAmount.class));
            }
        });
        earnings.setText("₹ " + ApiClient.user_earnings);
        balance.setText(ApiClient.user_earnings);
        name.setText(ApiClient.username);
        Glide.with(getContext()).load(ApiClient.BASE_URL + "media/files/events_add/extra_pic/haahoo_logo1.png").into(imageView);
        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int earnings = Integer.parseInt(ApiClient.user_earnings);
                if (earnings < 500) {
                    Toast.makeText(getContext(), "Minimum 500", Toast.LENGTH_SHORT).show();
                }
                if (earnings >= 500) {
                    Toast.makeText(getContext(), "You can withdraw in the next version", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;

    }

    public void getTransactionDetails() {
        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/show_withdraw_history/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("withdrawfrag", "dszfxgh" + obj);
                            balance.setText(obj.optString("wallet_balance"));
                            JSONArray history = obj.optJSONArray("withdraw history");
                            for (int i = 0; i < history.length(); i++) {
                                JSONObject jsonObject = history.optJSONObject(i);
                                TransactionPojo transactionPojo = new TransactionPojo();
                                transactionPojo.setName("Withdrawn to your Bank Account");
                                transactionPojo.setAmount(jsonObject.optString("amount"));
                                String[] arr = jsonObject.optString("withdrawal_date").split("T");
                                transactionPojo.setDate(arr[0] + " , " + arr[1].split("Z")[0]);
                                transactionPojos.add(transactionPojo);
                                orderAdapter = new TransactionHistoryAdapter(transactionPojos, getContext());
                                recyclerView.setAdapter(orderAdapter);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
                                recyclerView.setNestedScrollingEnabled(false);
                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
                progressDialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        queue.add(stringRequest);

    }


}
