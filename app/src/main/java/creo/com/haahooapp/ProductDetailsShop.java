package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.haozhang.lib.SlantedTextView;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.FeatureAdapter;
import creo.com.haahooapp.Adapters.ProductInShopAdapter;
import creo.com.haahooapp.Adapters.ProductSpecAdapter;
import creo.com.haahooapp.Adapters.ProductVariationAdapter;
import creo.com.haahooapp.Adapters.SlidingContentAdapterProduct;
import creo.com.haahooapp.Modal.FeaturePojoHeader;
import creo.com.haahooapp.Modal.ProductFeaturepojo;
import creo.com.haahooapp.Modal.ProductVariationPojo;
import creo.com.haahooapp.Modal.StoryPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.RetrofitClient;
import creo.com.haahooapp.interfaces.Recycler;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ProductDetailsShop extends AppCompatActivity implements SearchView.OnQueryTextListener, Recycler {
    private static ViewPager mPager;
    private static int currentPage = 0;
    Context context = this;
    int count = 0;
    Activity activity = this;
    String fileUri;
    TextView highlights;
    private static int NUM_PAGES = 0;
    TextView subscribe, resell_qty;
    String link = "null";
    String img = "null";
    RelativeLayout slidingrela;
    String amount = "0";
    ProgressDialog progressDialog;
    TextView title, price, description, buynow, addtomyshop, available, addtomyshop1;
    RelativeLayout quantity;
    EditText pincodecheck;
    String stockval = "";
    TextView checkbtn, shopname, shopaddress, shopnumber;
    String resel_max_price = "null";
    String token = "null";
    String membership;
    String shop_id = "null";
    String from = "null";
    CirclePageIndicator indicator;
    String productid = "null";
    private SlidingUpPanelLayout slidingLayout;
    ImageView close;
    ImageView back, cart;
    String price1 = "null";
    int i;
    LinearLayout addlinear, view, first;
    SearchView searchView;
    TextView addtocart, minus, qty, plus, deldate, minus1, qty1, plus1, addtocart1, plusbuynow, qtybuynow, minusbuynow, reward_amt;
    ArrayList<String> images = new ArrayList<>();
    RelativeLayout subrel, add;
    ArrayList<ProductFeaturepojo> productFeaturepojos = new ArrayList<>();
    LinearLayout linear, myshoprel;
    RecyclerView recycler;
    TextView noreturn;
    RelativeLayout sub, pick;
    ProductVariationAdapter productVariationAdapter;
    RecyclerView sliding;
    LinearLayout buyrel, buynowlayout;
    ArrayList<StoryPojo> storyPojos = new ArrayList<>();
    SlidingContentAdapterProduct slidingContentAdapter;
    TextView review, offer;
    RecyclerView variation, specs;
    String name_pdt = "null";
    ArrayList<ProductVariationPojo> varpojos = new ArrayList<>();
    ArrayList<FeaturePojoHeader> featurePojoHeaders = new ArrayList<>();
    String variant = "null";
    RelativeLayout rell;
    Spinner spinner;
    TextView shopnam, count1;
    SlantedTextView slant;
    ArrayList<JSONObject> typelist = new ArrayList<>();
    TextView plusqty, minusqty, qtypdt;
    String qty_resell;
    String[] separated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details_shop);
        title = findViewById(R.id.title);
        pick = findViewById(R.id.pick);
        shopnam = findViewById(R.id.shopnam);
        pick.setVisibility(View.GONE);
        count1 = findViewById(R.id.count);
        cartCount();
        specs = findViewById(R.id.specs);
        spinner = findViewById(R.id.spinner);
        slant = findViewById(R.id.slant);
        close = findViewById(R.id.close);
        plusbuynow = findViewById(R.id.plusbuynow);
        minusbuynow = findViewById(R.id.minusbuynow);
        qtybuynow = findViewById(R.id.qtybuynow);
        first = findViewById(R.id.first);
        variation = findViewById(R.id.variation);
        price = findViewById(R.id.price);
        reward_amt = findViewById(R.id.reward_amt);
        available = findViewById(R.id.available);
        slidingrela = findViewById(R.id.slidingrela);
        rell = findViewById(R.id.rell);
        review = findViewById(R.id.review);
        buynowlayout = findViewById(R.id.buynowlayout);
        buyrel = findViewById(R.id.buyrel);
        offer = findViewById(R.id.offer);
        sliding = findViewById(R.id.sliding);
        searchView = findViewById(R.id.search);
        resell_qty = findViewById(R.id.resell_qty);
        plusqty = findViewById(R.id.plusqty);
        minusqty = findViewById(R.id.minusqty);
        qtypdt = findViewById(R.id.qtyShop);
        searchView.setOnQueryTextListener(this);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.onActionViewExpanded();
            }
        });
        addlinear = findViewById(R.id.addlinear);
        view = findViewById(R.id.view);
        shopname = findViewById(R.id.shopname);
        addtocart = findViewById(R.id.addtocart);
        slidingLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        slidingLayout.setPanelSlideListener(onSlideListener());
        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });
        cart = findViewById(R.id.cart);
        cart.setOnClickListener(v -> {
            Intent intent = new Intent(ProductDetailsShop.this, Cart.class);
            startActivity(intent);
        });
        getDownloads();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        });
        addtomyshop1 = findViewById(R.id.addtomyshop1);
        myshoprel = findViewById(R.id.myshoprel);
        minus1 = findViewById(R.id.minus1);
        sub = findViewById(R.id.sub);
        plus1 = findViewById(R.id.plus1);
        qty1 = findViewById(R.id.qty1);
        addtocart1 = findViewById(R.id.addtocart1);
        plus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(qty1.getText().toString()) >= 1 && Integer.parseInt(qty1.getText().toString()) < Integer.parseInt(stockval.trim())) {
                    String quantity = qty1.getText().toString();
                    int added = Integer.parseInt(quantity) + 1;
                    qty1.setText(String.valueOf(added));
                }
                if (Integer.parseInt(qty1.getText().toString()) >= Integer.parseInt(stockval.trim())) {
                    Toast.makeText(context, "No more stock available", Toast.LENGTH_SHORT).show();
                }
            }
        });
        minus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(qty1.getText().toString()) > 1) {
                    String quantity = qty1.getText().toString();
                    int decrease = Integer.parseInt(quantity) - 1;
                    qty1.setText(String.valueOf(decrease));
                }
            }
        });
        minusqty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(qtypdt.getText().toString()) > 1) {
                    String qty = qtypdt.getText().toString();
                    int decrease = Integer.parseInt(qty) - 1;
                    qtypdt.setText(String.valueOf(decrease));
                }
            }
        });
        plusqty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(qtypdt.getText().toString()) >= 1) {
                    String quantity = qtypdt.getText().toString();
                    int added = Integer.parseInt(quantity) + 1;
                    qtypdt.setText(String.valueOf(added));
                }
            }
        });
        shopaddress = findViewById(R.id.shopaddress);
        shopnumber = findViewById(R.id.shopnumber);
        noreturn = findViewById(R.id.noreturn);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                if (intent.hasExtra("from")) {
                    if (intent.getStringExtra("from").equals("share")) {
                        startActivity(new Intent(context, NewHome.class));
                    }
                    if (intent.getStringExtra("from").equals("reselling")) {
                        ProductDetailsShop.super.onBackPressed();
                    }
                }
                if (!(intent.hasExtra("from"))) {
                    ProductDetailsShop.super.onBackPressed();
                }
            }
        });
        buynow = findViewById(R.id.buynow);
        add = findViewById(R.id.add);
        recycler = findViewById(R.id.recycler);
        linear = findViewById(R.id.linear);
        subrel = findViewById(R.id.subrel);
        pincodecheck = findViewById(R.id.pincodecheck);
        checkbtn = findViewById(R.id.checkbtn);
        description = findViewById(R.id.description);
        Bundle bundle = getIntent().getExtras();
        subscribe = findViewById(R.id.subscribe);
        highlights = findViewById(R.id.highlights);
        shop_id = bundle.getString("shopid");
        ApiClient.shop_id_del = shop_id;
        shopnam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductsInShop.class);
                intent.putExtra("categoryid", shop_id);
                intent.putExtra("categoryname", shopnam.getText().toString());
                startActivity(intent);
            }
        });
        if (bundle.containsKey("from")) {
            from = bundle.getString("from");
            if (from.equals("reselling")) {
                first.setVisibility(View.GONE);
                buynow.setText("Add To MyShop");
                addtocart.setVisibility(View.GONE);
                plusbuynow.setVisibility(View.GONE);
                minusbuynow.setVisibility(View.GONE);
                qtybuynow.setVisibility(View.GONE);
                buyrel.setVisibility(View.VISIBLE);
                pick.setVisibility(View.GONE);
                myshoprel.setVisibility(View.GONE);
            }
            if (from.equals("noresell")) {
                buyrel.setVisibility(View.VISIBLE);
                first.setVisibility(View.GONE);
                pick.setVisibility(View.GONE);
                myshoprel.setVisibility(View.GONE);
            }
        }
        plusbuynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = Integer.parseInt(qtybuynow.getText().toString()) + 1;
                if (i > Integer.parseInt(stockval.trim())) {
                    Toast.makeText(context, "No more stock available", Toast.LENGTH_SHORT).show();
                }
                if (i <= Integer.parseInt(stockval.trim())) {
                    qtybuynow.setText(String.valueOf(i));
                    ApiClient.quantity.add(qtybuynow.getText().toString());
                }
            }
        });
        minusbuynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(qtybuynow.getText().toString()) > 1) {
                    int i = Integer.parseInt(qtybuynow.getText().toString()) - 1;
                    qtybuynow.setText(String.valueOf(i));
                    ApiClient.quantity.add(qtybuynow.getText().toString());
                }
            }
        });
        if (!bundle.containsKey("from")) {
            buyrel.setVisibility(View.VISIBLE);
        }
        //Log.d("shoppppid","shop_id"+shop_id+bundle.getString("productid"));
        checkbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pincodecheck.getText().toString().length() < 6) {
                    pincodecheck.setError("Pincode must 6 six digits");
                }
                if (pincodecheck.getText().toString().length() == 6) {
                    checkAvailablility(pincodecheck.getText().toString(), shop_id);
                }
            }
        });
        productid = bundle.getString("productid");

        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, AllReviews.class);
                intent.putExtra("image", img);
                intent.putExtra("productid", productid);
                intent.putExtra("name", title.getText().toString());
                intent.putExtra("price", price.getText().toString());
                startActivity(intent);
            }
        });
        addtocart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addlinear.getVisibility() == View.VISIBLE) {
                    addproductTocart(productid, qty1.getText().toString());
                    /*ApiClient.count++;
                    finish();
                    startActivity(getIntent());*/
                }
            }
        });
        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (subscribe.getText().toString().equals("SUBSCRIBE")) {
                    ApiClient.productid = productid;
                    ApiClient.shopid = shop_id;
                    Intent intent = new Intent(context, SubscriptionAddress.class);
                    startActivity(intent);
                }
//                startActivity(new Intent(context,SubscriptionPlans.class));
            }
        });
        minus = findViewById(R.id.minus);
        qty = findViewById(R.id.qty);

        addtomyshop = findViewById(R.id.addtomyshop);

        plus = findViewById(R.id.plus);
        quantity = findViewById(R.id.quantity);
        deldate = findViewById(R.id.deldate);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        mPager = findViewById(R.id.pager);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading...");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Fetching Products..");
        progressDialog.show();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getproductDetails(productid);
                }
            }
        };
        Thread threaddetails = new Thread(runnable);
        threaddetails.start();

        productVariationAdapter = new ProductVariationAdapter(context, varpojos, this);
//        buynow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, ChooseAddressShop.class);
//                startActivity(intent);
//            }
//        });
        indicator = findViewById(R.id.indicator);
//        addtocart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                addtocart.setVisibility(View.GONE);
//                quantity.setVisibility(View.GONE);
//                buynow.setText("ADD TO CART");
//            }
//        });
        buynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (buynow.getText().toString().equals("ADD TO CART")) {
                    //Toast.makeText(context,"Add to cart",Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Add to cart");
                    builder.setMessage("Do you wish to add this product to cart ?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing but close the dialog

                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                if (buynow.getText().toString().equals("Buy Now")) {
                    ApiClient.delivery.clear();
                    int vv = Integer.parseInt(amount.trim()) * Integer.parseInt(qtybuynow.getText().toString());
                    ApiClient.price = String.valueOf(vv);
                    ApiClient.prices.add("₹ " + String.valueOf(vv));
                    ApiClient.quantity.clear();
                    ApiClient.quantity.add(qtybuynow.getText().toString());
                    Intent intent = new Intent(context, ChooseAddress.class);
                    intent.putExtra("from", "details");
                    startActivity(intent);
                }
                if (buynow.getText().toString().equals("GO TO CART")) {
                    startActivity(new Intent(context, Cart.class));
                }

                if (buynow.getText().toString().equals("Add To MyShop")) {
                    LayoutInflater layoutInflater = LayoutInflater.from(context);
                    View view = layoutInflater.inflate(R.layout.edit_price, null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setView(view)
                            .create();
                    final EditText userInput = (EditText) view
                            .findViewById(R.id.editTextDialogUserInput);
                    ImageView image = view.findViewById(R.id.image);
                    TextView range = view.findViewById(R.id.range);
                    range.setText("You can set a price between " + price1 + " and " + resel_max_price);
                    Glide.with(context).load(img).into(image);
                    userInput.setText(price1.trim());
                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {

                        @Override
                        public void onShow(DialogInterface dialogInterface) {
                            CardView okcard = view.findViewById(R.id.okcard);
                            CardView cancelcard = view.findViewById(R.id.cancelcard);
                            Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                            cancelcard.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            okcard.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View view) {
                                    // TODO Do something
                                    if (userInput.getText().toString().length() == 0) {
                                        userInput.setError("Please enter valid price");
                                    }
                                    if (userInput.getText().toString().length() != 0) {
                                        //Log.d("ghvghv","hjbhjbjhb"+productid+price1+userInput.getText().toString());
                                        if (Integer.parseInt(userInput.getText().toString().trim()) < Integer.parseInt(price1.trim())) {
                                            userInput.setError("Price cannot be lesser than original amount");
                                        }
                                        if (Integer.parseInt(userInput.getText().toString().trim()) <= Integer.parseInt(resel_max_price.trim())) {
                                            if (Integer.parseInt(userInput.getText().toString().trim()) < Integer.parseInt(price1.trim())) {
                                                userInput.setError("Price cannot be lesser than original amount");
                                            }

                                            if (Integer.parseInt(userInput.getText().toString().trim()) > Integer.parseInt(price1.trim())) {
                                                addProduct(productid, price1, userInput.getText().toString());
                                                dialog.dismiss();
                                            }
                                        }
                                        if (Integer.parseInt(userInput.getText().toString().trim()) > Integer.parseInt(resel_max_price.trim())) {
                                            userInput.setError("Maximum price is " + resel_max_price);
                                        }
                                    }
                                    //Dismiss once everything is OK.
                                    // dialog.dismiss();
                                }
                            });
                        }
                    });
                    dialog.show();
                }
            }
        });
        count = Integer.parseInt(qty.getText().toString());
        if (count == 0) {
            buynow.setText("BUY NOW");
        }
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (count >= 1) {
                    if (count > Integer.parseInt(stockval.trim())) {
                        Toast.makeText(context, "No more stock available", Toast.LENGTH_SHORT).show();
                    }
                    if (count <= Integer.parseInt(stockval.trim())) {
                        count = count + 1;
                        qty.setText(String.valueOf(count));
                        buynow.setText("ADD TO CART");
                    }
                }


                if (count == 0) {
                    count = count + 1;
                    qty.setText("1");
                }
                if (count >= 1) {
                    buynow.setText("ADD TO CART");
                }

            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count >= 1) {

                    count = count - 1;
                    qty.setText(String.valueOf(count));
                }
                if (count == 0) {
                    count = count + 1;
                    qty.setText("1");

                }
                if (count >= 1) {
                    buynow.setText("ADD TO CART");
                }
//                if (count == 1){
//                    buynow.setText("BUY NOW");
//                }
            }
        });


        addtomyshop1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();

                if (addtomyshop1.getText().toString().equals("View in MyShop")) {
                    Intent intent = new Intent(context, MyShopNew.class);
                    intent.putExtra("from", "details");
                    startActivity(intent);
                }

                if (addtomyshop1.getText().toString().equals("Add To MyShop")) {
                    LayoutInflater layoutInflater = LayoutInflater.from(context);
                    View view = layoutInflater.inflate(R.layout.edit_price, null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setView(view)
                            .create();
                    final EditText userInput = (EditText) view
                            .findViewById(R.id.editTextDialogUserInput);
                    userInput.setText(price1.trim());
                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {

                        @Override
                        public void onShow(DialogInterface dialogInterface) {

                            CardView okcard = view.findViewById(R.id.okcard);
                            CardView cancelcard = view.findViewById(R.id.cancelcard);
                            cancelcard.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            okcard.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View view) {
                                    // TODO Do something
                                    if (userInput.getText().toString().length() == 0) {
                                        userInput.setError("Please enter valid price");
                                    }
                                    if (userInput.getText().toString().length() != 0) {
                                        //Log.d("ghvghv","hjbhjbjhb"+productid+price1+userInput.getText().toString());
                                        if (Integer.parseInt(userInput.getText().toString().trim()) < Integer.parseInt(price1.trim())) {
                                            userInput.setError("Price cannot be lesser than original amount");
                                        }
                                        if (Integer.parseInt(userInput.getText().toString().trim()) <= Integer.parseInt(resel_max_price.trim())) {
                                            if (Integer.parseInt(userInput.getText().toString().trim()) < Integer.parseInt(price1.trim())) {
                                                userInput.setError("Price cannot be lesser than original amount");
                                            }

                                            if (Integer.parseInt(userInput.getText().toString().trim()) > Integer.parseInt(price1.trim())) {
                                                addProduct(productid, price1, userInput.getText().toString());
                                                dialog.dismiss();
                                            }
                                        }
                                        if (Integer.parseInt(userInput.getText().toString().trim()) > Integer.parseInt(resel_max_price.trim())) {
                                            userInput.setError("Maximum price is " + resel_max_price);
                                        }
                                    }
                                    //Dismiss once everything is OK.
                                    // dialog.dismiss();
                                }
                            });
                        }
                    });
                    dialog.show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        if (intent.hasExtra("from")) {
            if (intent.getStringExtra("from").equals("share")) {
                startActivity(new Intent(context, NewHome.class));
            }
            if (intent.getStringExtra("from").equals("reselling")) {
                super.onBackPressed();
            }
        }
        if (!(intent.hasExtra("from"))) {
            super.onBackPressed();
        }
    }

    public void getDownloads() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/login_ref_count/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            //   Log.d("refcount","refff"+jsonObject);
                            String message = jsonObject.optString("message");
                            if (message.equals("Success")) {
                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                    StoryPojo storyPojo = new StoryPojo();
                                    storyPojo.setName(jsonObject1.optString("name"));
                                    storyPojo.setId(jsonObject1.optString("id"));
                                    storyPojo.setShop_id(productid);
                                    storyPojo.setToken(token);
                                    storyPojos.add(storyPojo);

                                }

                                slidingContentAdapter = new SlidingContentAdapterProduct(context, storyPojos);
                                sliding.setAdapter(slidingContentAdapter);
                                sliding.setNestedScrollingEnabled(false);
                                sliding.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Downloads.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public void checkAvailablility(String pincode, String shop_id) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "virtual_shop/check_virtual_pin/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {

                            JSONObject obj = new JSONObject(response);
                            String message = obj.optString("message");
                            if (message.equals("Success")) {
                                Toast.makeText(context, "Delivery available", Toast.LENGTH_SHORT).show();

                            }
                            if (message.equals("Failed")) {
                                Toast.makeText(context, "Delivery not available", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //

            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shop_id", shop_id);
                params.put("pincode", pincode);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private SlidingUpPanelLayout.PanelSlideListener onSlideListener() {
        return new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
                //textView.setText("panel is sliding");
            }

            @Override
            public void onPanelCollapsed(View view) {
                // textView.setText("panel Collapse");
            }

            @Override
            public void onPanelExpanded(View view) {
                //textView.setText("panel expand");
            }

            @Override
            public void onPanelAnchored(View view) {
                //textView.setText("panel anchored");
            }

            @Override
            public void onPanelHidden(View view) {
                // textView.setText("panel is Hidden");
            }
        };
    }

    public void addproductTocart(String productid, String count) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Adding Product to Cart");
        progressDialog.show();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        // shop_id = (pref.getString("shop_id", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "cart_view/cart_add/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {

                            JSONObject obj = new JSONObject(response);
                            //  Log.d("addtocart","cartadd"+obj);
                            String message = obj.optString("message");
                            if (message.equals("Success")) {
                                //Toast.makeText(context,"Product added to your cart",Toast.LENGTH_SHORT).show();

                                addlinear.setVisibility(View.GONE);
                                view.setVisibility(View.VISIBLE);
                                progressDialog.hide();
                                ApiClient.count++;
                                finish();
                                startActivity(getIntent());
                                view.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(context, Cart.class);
                                        intent.putExtra("from", "details");
                                        startActivity(intent);
//                                        startActivity(new Intent(context,Cart.class));
                                    }
                                });

                                //buynow.setText("GO TO CART");
                            }
                            if (message.equals("Failed")) {
                                Toast.makeText(context, "Failed adding product to cart", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                //
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("productid", productid);
                if (count.equals("0")) {
                    params.put("pro_count", "1");
                }
                if (!(count.equals("0"))) {
                    params.put("pro_count", count);
                }
                params.put("virtual", "1");
                if (ApiClient.variant_ids.size() > 0) {
                    params.put("variant", ApiClient.variant_ids.get(0));
                }
                // Log.d("variant_idd","idddd"+ApiClient.variant_ids.get(0));
                if (ApiClient.variant_ids.size() == 0) {
                    params.put("variant", "0");
                }
                params.put("shop_id", shop_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getproductDetails(String productid) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        membership = (pref.getString("membership", ""));

        if (membership.equals("1")) {
            if (!(from.equals("reselling"))) {
                reward_amt.setVisibility(View.VISIBLE);
            }
            if (from.equals("reselling")) {
                reward_amt.setVisibility(View.GONE);
            }
        }

        if (!(membership.equals("1"))) {
            reward_amt.setVisibility(View.GONE);
        }

        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/product_id/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            ApiClient.quantity.clear();
                            ApiClient.name.clear();
                            ApiClient.images.clear();
                            ApiClient.prices.clear();
                            ApiClient.variant_ids.clear();
                            ApiClient.virtual.clear();
                            List<String> specsvals = new ArrayList<>();
                            List<String> specheader = new ArrayList<>();
                            JSONObject obj = new JSONObject(response);
                            Log.d("kjkhgfhjk", "iuytui" + obj);
                            varpojos.clear();
                            JSONArray variants = obj.optJSONArray("variant");
                            for (int i = 0; i < variants.length(); i++) {
                                JSONObject jsonObject = variants.optJSONObject(i);
                                ProductVariationPojo productFeaturepojo = new ProductVariationPojo();
                                productFeaturepojo.setId(jsonObject.optString("id"));
                                productFeaturepojo.setImage(ApiClient.BASE_URL + "media/" + jsonObject.optString("variant_image"));
                                productFeaturepojo.setName(jsonObject.optString("variant_name"));
                                productFeaturepojo.setPrice(jsonObject.optString("fixed_price"));
                                varpojos.add(productFeaturepojo);
                            }
                            variation.setHasFixedSize(true);
                            variation.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                            variation.setAdapter(productVariationAdapter);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                String name = jsonObject.optString("name");
                                ApiClient.ids.clear();
                                String id = jsonObject.optString("id");
                                ApiClient.ids.add(id);
                                ApiClient.name.add(name);
                                name_pdt = name;
                                String name1 = name.substring(0, 1).toUpperCase();
                                String title1 = name1 + name.substring(1);
                                if (jsonObject.optString("product_weight").length() >= 1) {
                                    title.setText(title1 + " , " + jsonObject.optString("product_weight"));
                                }
                                if (jsonObject.optString("product_weight").length() < 1) {
                                    title.setText(title1);
                                }
                                String desc = jsonObject.optString("description");
                                String return_period = jsonObject.optString("return_period");
                                String shop_name = jsonObject.optString("shop_name");
                                shopnam.setText(shop_name);
                                String resell_qtytxt = jsonObject.optString("resell_count");
                                if (!resell_qtytxt.equals("0")) {
                                    resell_qty.setVisibility(View.GONE);
                                    resell_qty.setText("No. of quantity available for resell: " + resell_qtytxt);
                                }
                                String stock = jsonObject.optString("stock");
                                stockval = jsonObject.optString("stock");
                                if (stock.equals("0")) {
                                    //red
                                    slant.setText("Out of stock");
                                    slant.setSlantedBackgroundColor(Color.parseColor("#fc0015"));
                                    buyrel.setVisibility(View.GONE);
                                }
                                if (!(stock.equals("0"))) {
                                    //green
                                    slant.setText("In stock");
                                    slant.setSlantedBackgroundColor(Color.parseColor("#38be55"));
                                    buyrel.setVisibility(View.VISIBLE);
                                }

                                String shop_phone = jsonObject.optString("shop_phone");
                                String shop_location = jsonObject.optString("shop_address");
                                String cart_status = jsonObject.optString("cart_sta");
                                slidingrela.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        progressDialog.setTitle("Loading..");
                                        progressDialog.setMessage("Loading Apps..");
                                        progressDialog.setIcon(R.drawable.haahoologo);
                                        progressDialog.show();
                                        shareLongDynamicLink(productid, desc);
                                        //newShare();
                                    }
                                });
                                if (cart_status.equals("1")) {
                                    addlinear.setVisibility(View.GONE);
                                    view.setVisibility(View.VISIBLE);
                                    view.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(context, Cart.class);
                                            intent.putExtra("from", "details");
                                            startActivity(intent);
                                        }
                                    });
                                }
                                if (cart_status.equals("0")) {
                                    addlinear.setVisibility(View.VISIBLE);
                                    view.setVisibility(View.GONE);
                                }
                                shopname.setText(shop_name);
                                shopnumber.setText(shop_phone);
                                shopaddress.setText(shop_location);
                                if (return_period.equals("0")) {
                                    noreturn.setText("You cannot return this product once delivered");
                                }
                                if (!(return_period.equals("0"))) {
                                    noreturn.setText("You can return this product within " + return_period + " days");
                                }
                                description.setText(desc);
                                String images1 = jsonObject.optString("image");
                                amount = jsonObject.optString("discount");
                                reward_amt.setText("SHARE AND EARN UPTO Rs. " + jsonObject.optString("incentive"));
                                int valsss = Integer.parseInt(jsonObject.optString("price").trim()) - Integer.parseInt(jsonObject.optString("discount").trim());
                                offer.setText("(Rs. " + String.valueOf(valsss) + " OFF)");
                                if (jsonObject.optString("myshop_status").equals("0")) {
                                    addtomyshop1.setText("Add to MyShop");
                                }
                                if (jsonObject.optString("myshop_status").equals("1")) {
                                    addtomyshop1.setText("View in MyShop");
                                    if (from.equals("reselling")) {
                                        buynow.setText("View in MyShop");
                                    }
                                }
                                if (jsonObject.optString("subscription_status").equals("1")) {
                                    subscribe.setText("Already subscribed");
                                }
                                if (jsonObject.optString("subscription_status").equals("0")) {
                                    subscribe.setText("SUBSCRIBE");
                                }
                                if (Integer.parseInt(jsonObject.optString("stock")) < 10) {
                                    if (Integer.parseInt(jsonObject.optString("stock")) == 0) {
                                        available.setVisibility(View.VISIBLE);
                                        available.setText("Stock not available");
                                        linear.setVisibility(View.GONE);
                                        available.setTextColor(ContextCompat.getColor(context, R.color.red));
                                    }
                                    if (Integer.parseInt(jsonObject.optString("stock")) != 0) {
                                        available.setVisibility(View.VISIBLE);
                                        available.setText("Hurry only a few available");
                                        available.setTextColor(ContextCompat.getColor(context, R.color.red));
                                    }
                                }
                                if (jsonObject.optString("resell_status").equals("0")) {
                                    add.setVisibility(View.GONE);
                                    myshoprel.setVisibility(View.GONE);
                                }
                                if (jsonObject.optString("resell_status").equals("1") && from.equals("null")) {
                                    addtocart.setVisibility(View.GONE);
                                    if (myshoprel.getVisibility() == View.VISIBLE) {
                                        addtomyshop1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (membership.equals("1")) {
                                                    if (addtomyshop1.getText().toString().equals("Add to MyShop")) {
                                                        qty_resell = qtypdt.getText().toString();
                                                        getresellcount();
                                                        LayoutInflater layoutInflater = LayoutInflater.from(context);
                                                        View view = layoutInflater.inflate(R.layout.edit_price, null);
                                                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                                                .setView(view)
                                                                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                                                                .setNegativeButton(android.R.string.cancel, null)
                                                                .create();
                                                        final EditText userInput = (EditText) view
                                                                .findViewById(R.id.editTextDialogUserInput);
                                                        userInput.setText(price1.trim());
                                                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

                                                            @Override
                                                            public void onShow(DialogInterface dialogInterface) {

                                                                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                                                                button.setOnClickListener(new View.OnClickListener() {

                                                                    @Override
                                                                    public void onClick(View view) {
                                                                        // TODO Do something
                                                                        if (userInput.getText().toString().length() == 0) {
                                                                            userInput.setError("Please enter valid price");
                                                                        }
                                                                        if (userInput.getText().toString().length() != 0) {
                                                                            if (Integer.parseInt(userInput.getText().toString().trim()) < Integer.parseInt(price1.trim())) {
                                                                                userInput.setError("Price cannot be lesser than original amount");
                                                                            }
                                                                            if (Integer.parseInt(userInput.getText().toString().trim()) <= Integer.parseInt(resel_max_price.trim())) {
                                                                                if (Integer.parseInt(userInput.getText().toString().trim()) < Integer.parseInt(price1.trim())) {
                                                                                    userInput.setError("Price cannot be lesser than original amount");
                                                                                }
                                                                                if (Integer.parseInt(userInput.getText().toString().trim()) > Integer.parseInt(price1.trim())) {
                                                                                    addProduct(productid, price1, userInput.getText().toString());
                                                                                    dialog.dismiss();
                                                                                }
                                                                            }
                                                                            if (Integer.parseInt(userInput.getText().toString().trim()) > Integer.parseInt(resel_max_price.trim())) {
                                                                                userInput.setError("Maximum price is " + resel_max_price);
                                                                            }
                                                                        }
                                                                        //Dismiss once everything is OK.
                                                                        // dialog.dismiss();
                                                                    }
                                                                });
                                                            }
                                                        });
                                                        dialog.show();
                                                    }

                                                    if (addtomyshop1.getText().toString().equals("View in MyShop")) {
                                                        Intent intent = new Intent(context, MyShopNew.class);
                                                        intent.putExtra("from", "details");
                                                        startActivity(intent);
                                                    }
                                                } else if (membership.equals("0")) {
                                                    Toast.makeText(context, "You need to be a member to add product to my shop", Toast.LENGTH_LONG).show();
                                                    Intent intent = new Intent(context, MemberShipActivity.class);
                                                    startActivity(intent);
                                                }
                                            }
                                        });
                                    }
                                }
                                if (Integer.parseInt(jsonObject.optString("stock")) > 10) {
//                                    available.setVisibility(View.VISIBLE);
//                                    available.setText("Stock Available");
//                                    available.setTextColor(ContextCompat.getColor(context,R.color.green));
                                }
                                if (jsonObject.optString("suubscription").equals("0")) {
                                    sub.setVisibility(View.GONE);
                                }
                                if (jsonObject.optString("subscription").equals("1")) {
                                    sub.setVisibility(View.VISIBLE);
                                }
                                JSONObject specification_header = jsonObject.optJSONObject("specification_header");
                                ArrayList<String> spec_headers = new ArrayList<>();
                                for (int j = 0; j < specification_header.length(); j++) {
                                    String value = specification_header.optString("spec" + j);
                                    spec_headers.add(value);
                                    //  Log.d("JHGFJHFHDHD","yfyyfytfyt"+value);
                                }
                                JSONObject specifications = jsonObject.optJSONObject("specification");
                                ArrayList<String> spec_values = new ArrayList<>();
                                for (int k = 0; k < spec_headers.size(); k++) {
                                    spec_values.add(specifications.optString(spec_headers.get(k)));
                                }
                                if (spec_headers.size() > 0) {
                                    for (int l = 0; l < spec_headers.size(); l++) {

                                        ProductFeaturepojo productFeaturepojo = new ProductFeaturepojo(spec_headers.get(l) + " : " + spec_values.get(l));
                                        productFeaturepojos.add(productFeaturepojo);
                                        //Log.d("FEATUREPOJOOSS", "jkbvjhv" + productFeaturepojo.getFeature_name());

                                    }
                                }
                                if (spec_headers.size() == 0) {
                                    ProductFeaturepojo productFeaturepojo = new ProductFeaturepojo("No specifications added for this product");
                                    productFeaturepojos.add(productFeaturepojo);
                                }

                                ProductSpecAdapter productSpecAdapter = new ProductSpecAdapter(productFeaturepojos, context);
                                specs.setAdapter(productSpecAdapter);
                                specs.setNestedScrollingEnabled(false);
                                specs.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                                price1 = amount;
                                int vv = Integer.parseInt(amount.trim()) * Integer.parseInt(qty1.getText().toString());
                                ApiClient.price = String.valueOf(vv);
                                ApiClient.prices.add("₹ " + String.valueOf(vv));
                                // ApiClient.quantity.add("1");
                                deldate.setText("Delivery : Within " + jsonObject.optString("delivery_duration") + " business days");
                                price.setText("Rs. " + amount.trim());
                                String[] seperated = images1.split(",");
                                resel_max_price = jsonObject.optString("price");
                                String split = seperated[0].replace("[", "").replace("]", "");
                                ArrayList<String> imagess = new ArrayList<>();
                                for (int j = 0; j < seperated.length; j++) {
                                    imagess.add(ApiClient.BASE_URL + seperated[j].replace("[", "").replace("]", "").trim());
                                    img = ApiClient.BASE_URL + seperated[0].replace("[", "").replace("]", "").trim();
                                }
                                NUM_PAGES = imagess.size();
                                ApiClient.images.add(imagess.get(0));
                                ApiClient.virtual.add("1");
                                mPager.setAdapter(new ProductInShopAdapter(ProductDetailsShop.this, imagess));
                                indicator.setViewPager(mPager);
                                final float density = getResources().getDisplayMetrics().density;
                                indicator.setRadius(5 * density);
                                final Runnable Update = new Runnable() {
                                    public void run() {
                                        if (currentPage == NUM_PAGES) {
                                            currentPage = 0;
                                        }
                                        mPager.setCurrentItem(currentPage++, true);
                                    }
                                };
                                //Pager listener over indicator
                                indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                                    @Override
                                    public void onPageSelected(int position) {
                                        currentPage = position;

                                    }

                                    @Override
                                    public void onPageScrolled(int pos, float arg1, int arg2) {

                                    }

                                    @Override
                                    public void onPageScrollStateChanged(int pos) {

                                    }

                                });
                            }

                            recycler.setVisibility(View.VISIBLE);
                            if (productFeaturepojos.size() == 0) {
                                recycler.setVisibility(View.GONE);
                                highlights.setVisibility(View.GONE);

                            }
                            if (productFeaturepojos.size() > 0) {
                                recycler.setVisibility(View.VISIBLE);
                                highlights.setVisibility(View.VISIBLE);
                                FeatureAdapter featureAdapter = new FeatureAdapter(productFeaturepojos, context);
                                recycler.setHasFixedSize(true);
                                recycler.setLayoutManager(new LinearLayoutManager(context));
                                recycler.setAdapter(featureAdapter);
                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //
                progressDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pdt_id", productid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getresellcount() {
        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .getResellCount(token, productid, qty_resell);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Toast.makeText(context, "Successful", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addProduct(String productid, String price2, String edited_price) {
        // Log.d("eddd_price","hvjgcfdx"+edited_price);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "virtual_shop/add_virtual_pdt/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            //Log.d("nnnnn", "jsonarray" + jsonObject);
                            String message = jsonObject.optString("message");
                            if (message.equals("Success")) {
                                Toast.makeText(context, "Product added to shop successfully", Toast.LENGTH_SHORT).show();
                                addtomyshop1.setText("View in MyShop");
                                new FancyAlertDialog.Builder(activity)
                                        .setTitle("Go to My Shop")
                                        .setBackgroundColor(Color.parseColor("#0078BC"))  //Don't pass R.color.colorvalue
                                        .setMessage("Product has been added to your shop. Want to have a look on them ?")
                                        .setNegativeBtnText("NO")
                                        .setPositiveBtnBackground(Color.parseColor("#0078BC"))  //Don't pass R.color.colorvalue
                                        .setPositiveBtnText("YES")
                                        .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                                        .setAnimation(Animation.POP)
                                        .isCancellable(false)
                                        .setIcon(R.drawable.white_check, Icon.Visible)
                                        .OnPositiveClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {
                                                startActivity(new Intent(context, MyShopNew.class));
                                            }
                                        })
                                        .OnNegativeClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {

                                            }
                                        })
                                        .build();
                            }
                            if (message.equals("Failed")) {
                                Toast.makeText(context, "Failed to add product ", Toast.LENGTH_SHORT).show();
                            }
                            if (!(message.equals("Success")) || (message.equals("Failed"))) {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //

            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", productid);
                params.put("changed_price", edited_price);
                params.put("orginal_price", price2);
                //Log.d("orid_price","hjgh"+price2);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        String userinput = newText.toLowerCase();

        //  Log.d("inputt","user"+userinput);
        ArrayList<StoryPojo> storyPojos1 = new ArrayList<>();
        for (StoryPojo storyPojo : storyPojos) {
            if (storyPojo.getName().toLowerCase().contains(userinput)) {
                storyPojos1.add(storyPojo);
            }
            slidingContentAdapter.updateList(storyPojos1);
        }
        return true;
    }

    public void shareLongDynamicLink(String productid, String descript) {
//        String query = "";
//        try {
//            query = URLEncoder.encode(String.format("&%1s=%2s", "id", "1234"), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        final String path = "https://haahooapp.page.link/?" +
                "link=" + /*link*/
                "https://www.google.com/?" +
                "productid=" +
                productid + "+" + ApiClient.own_id +
                "&apn=" + /*getPackageName()*/
                "creo.com.haahooapp" +
                "&st=" + /*titleSocial*/
                "Download+Haahoo" +
                "&sd=" + /*description*/
                descript +
                "&utm_source=" + /*source*/
                "AndroidApp";
        //Log.d("referpath","ohh"+path);
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(path))
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            link = shortLink.toString();
                            Intent intent = new Intent();
//                            String path = buildDynamicLink();
                            //    Log.d("actual link","linkkk"+link);
                            newShare();
//                            String msg = "visit my awesome website: " + link;
//                            intent.setAction(Intent.ACTION_SEND);
//                            intent.setType("text/plain");
//                            intent.putExtra(Intent.EXTRA_TEXT, link);
//
//                            Intent shareintent = Intent.createChooser(intent, null);
//                            startActivity(shareintent);

                        } else {
                            // Error
                            // ...
                        }
                    }
                });

    }

    public void newShare() {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "Product Name : " + title.getText().toString() + "\n" + "Product Price : " + price.getText().toString() + "\n" + "Product URL : " + link);
                Picasso.with(context).load(img).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        File mydir = new File(Environment.getExternalStorageDirectory() + "/.HaaHoo");
                        if (!mydir.exists()) {
                            mydir.mkdirs();
                        }
                        try {
                            fileUri = mydir.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".jpg";
                            FileOutputStream outputStream = new FileOutputStream(fileUri);
                            Log.d("FILEEEURIII", "jkhgf" + fileUri.toString());
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                            outputStream.flush();
                            outputStream.close();
                            try {
//                                Uri uri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), BitmapFactory.decodeFile(fileUri), null, null));
                                Uri uri = Uri.parse(fileUri);
                                progressDialog.dismiss();
                                sharingIntent.setType("image/*");
                                sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
                                context.startActivity(Intent.createChooser(sharingIntent, "Share to"));
                            } catch (IllegalStateException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {

            }


        };
        TedPermission.with(context)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();

    }

    @Override
    public void onClick(String value) {

    }

    @Override
    public void variationClick(String value) {
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Fetching Variant Details..");
        progressDialog.show();

        ApiClient.variant_ids.clear();
        ApiClient.variant_ids.add(value);
//        Toast.makeText(context,"variation clicked",Toast.LENGTH_SHORT).show();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/view_variant/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            ApiClient.images.clear();
                            ApiClient.name.clear();
                            ApiClient.prices.clear();
                            Log.d("ghghghghg", "jsonarray" + jsonObject);
                            JSONArray data = jsonObject.optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject1 = data.optJSONObject(i);
                                if (name_pdt.equals(jsonObject1.optString("variant_name"))) {
                                    title.setText(name_pdt);
                                    ApiClient.name.add(name_pdt);
                                }
                                if (!(name_pdt.equals(jsonObject1.optString("variant_name")))) {
                                    title.setText(name_pdt + " " + jsonObject1.optString("variant_name"));
                                    ApiClient.name.add(title.getText().toString());

                                }
                                if (jsonObject1.optString("discount").length() != 0) {
                                    price.setText("Rs. " + jsonObject1.optString("discount").trim());
                                    ApiClient.prices.add(jsonObject1.optString("discount").trim());
                                    amount = jsonObject1.optString("discount").trim();
                                    ApiClient.price = jsonObject1.optString("discount").trim();
                                    if (jsonObject1.optString("fixed_price").length() > 0) {
                                        int val = Integer.parseInt(jsonObject1.optString("fixed_price")) - Integer.parseInt(jsonObject1.optString("discount"));
                                        offer.setVisibility(View.VISIBLE);
                                        offer.setText("(Rs. " + String.valueOf(val) + " OFF)");
                                    }
                                }
                                if (jsonObject1.optString("discount").length() == 0) {
                                    price.setText("Rs. " + jsonObject1.optString("fixed_price").trim());
                                    ApiClient.prices.add(jsonObject1.optString("fixed_price").trim());
                                    amount = jsonObject1.optString("fixed_price").trim();
                                    ApiClient.price = jsonObject1.optString("fixed_price").trim();
                                    offer.setVisibility(View.GONE);
                                }
                                ArrayList<String> imagess = new ArrayList<>();

                                imagess.add(ApiClient.BASE_URL + "media/" + jsonObject1.optString("variant_image").replace("[", "").replace("]", "").trim());
                                ApiClient.images.add(imagess.get(0));
//                                    img = ApiClient.BASE_URL+seperated[0].replace("[","").replace("]","").trim();

                                NUM_PAGES = imagess.size();
                                mPager.setAdapter(new ProductInShopAdapter(ProductDetailsShop.this, imagess));
                                indicator.setViewPager(mPager);
                                final float density = getResources().getDisplayMetrics().density;
                                indicator.setRadius(5 * density);
                                final Runnable Update = new Runnable() {
                                    public void run() {
                                        if (currentPage == NUM_PAGES) {
                                            currentPage = 0;
                                        }
                                        mPager.setCurrentItem(currentPage++, true);
                                    }
                                };
                                //Pager listener over indicator
                                indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                                    @Override
                                    public void onPageSelected(int position) {
                                        currentPage = position;

                                    }

                                    @Override
                                    public void onPageScrolled(int pos, float arg1, int arg2) {

                                    }

                                    @Override
                                    public void onPageScrollStateChanged(int pos) {

                                    }

                                });


                            }
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //

            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pdt_id", value);
                if (value.equals(productid)) {
                    params.put("variant", "0");
                }
                if (!(value.equals(productid))) {
                    params.put("variant", "1");
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void cartCount() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "cart_view/cart_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            Log.d("CCCOUNT", "HGDTRS" + response);
                            String total_count = jsonObject1.optString("total all count");
                            if (total_count.equals("")) {
                                total_count = "0";
                                ApiClient.count = Integer.valueOf(total_count);
                                count1.setText(total_count);
                            }
                            if (!(total_count.equals(""))) {
                                ApiClient.count = Integer.valueOf(total_count);
                                count1.setText(total_count);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("shop_id", shop_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}