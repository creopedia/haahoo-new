package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.viewpagerindicator.CirclePageIndicator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.Adapters.SlidingImage_Adapter;
import creo.com.haahooapp.Implementation.ProductDetailPresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.CartPresenter;
import creo.com.haahooapp.interfaces.ProductDetailCallback;
import creo.com.haahooapp.interfaces.ProductDetailPresenter;
import creo.com.haahooapp.interfaces.ProductDetailView;
import creo.com.haahooapp.interfaces.WishListPresenter;

public class ProductDetailsSell extends AppCompatActivity implements ProductDetailView, ProductDetailCallback {
    private static ViewPager mPager;
    private static int currentPage = 0;
    ProgressDialog progressDialog;
    ImageView fav,fav_fill;
    private static int NUM_PAGES = 0;
    TextView check, avail;
    TextInputEditText pincode;
    TextView addTocart,gotocart;
    Activity activity = this;
    RelativeLayout relativeLayout;
    JSONArray jsonArray1 = new JSONArray();
    ArrayList<String> imgs = new ArrayList<>();
    PopupWindow popupWindow;
    String id = null;
    CartPresenter cartPresenter;
    WishListPresenter wishListPresenter;
    Context context = this;
    ImageView whatsapp,back;
    TextView buynow,nametext,pricetxt,desctxt,whatsapptext;
    ProductDetailPresenter productDetailPresenter;
    private static final Integer[] IMAGES = {};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private ArrayList<String>images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details_sell);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back = findViewById(R.id.back);
        avail = findViewById(R.id.avail);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductDetailsSell.super.onBackPressed();
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        relativeLayout = findViewById(R.id.relative);
        check = findViewById(R.id.check);
        pincode = findViewById(R.id.pincode);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(ProductDetailsSell.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(ProductDetailsSell.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(ProductDetailsSell.this, SettingsActivity.class));
                                break;


                            case R.id.shop:

                                startActivity(new Intent(context,MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context,AllStories.class));
                                break;

                        }
                        return true;
                    }
                });
//        wishListPresenter = new WishListPresenterImpl(this);
//        cartPresenter = new CartPresenterImpl(this);
        progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);

        fav = findViewById(R.id.fav);
      //  addTocart = findViewById(R.id.addTocart);
        whatsapptext = findViewById(R.id.textwhatsapp);
        whatsapp = findViewById(R.id.whatsapp);
       // gotocart = findViewById(R.id.gotocart);
        fav_fill = findViewById(R.id.fav_fill);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wishListPresenter.addToWishList(getIntent().getExtras().getString("id"), token);
                fav.setVisibility(View.GONE);
                fav_fill.setVisibility(View.VISIBLE);
            }
        });
        fav_fill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wishListPresenter.removeFromWishList(getIntent().getExtras().getString("id"), token);
                fav_fill.setVisibility(View.GONE);
                fav.setVisibility(View.VISIBLE);

            }
        });
//        addTocart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                // cartPresenter.addToCart(getIntent().getExtras().getString("id"),token);
//
//                LayoutInflater layoutInflater = (LayoutInflater) ProductDetailsSell.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                View customView = layoutInflater.inflate(R.layout.popup, null);
//                Button closePopupBtn = (Button) customView.findViewById(R.id.closePopupBtn);
//                TextView decrease = customView.findViewById(R.id.decrease);
//                TextView increase = customView.findViewById(R.id.increase);
//                final TextView quantity = customView.findViewById(R.id.qty);
//                TextView apply = customView.findViewById(R.id.apply);
//                apply.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        cartPresenter.addToCart(getIntent().getExtras().getString("id"), quantity.getText().toString(), token);
//                    }
//                });
//                increase.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (Integer.parseInt(quantity.getText().toString()) >= 1) {
//
//                            String value = quantity.getText().toString();
//                            int value1 = Integer.parseInt(value) + 1;
//                            String new_value = String.valueOf(value1);
//                            quantity.setText(new_value);
//
//                        }
//                    }
//                });
//
//                decrease.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        if (Integer.parseInt(quantity.getText().toString()) >= 1) {
//
//                            String value = quantity.getText().toString();
//                            int value1 = Integer.parseInt(value) - 1;
//                            String new_value = String.valueOf(value1);
//                            quantity.setText(new_value);
//                        }
//
//                        if (quantity.getText().toString().equals("0")) {
//                            quantity.setText("1");
//
//                        }
//                    }
//                });

//                relativeLayout.setBackgroundResource(R.drawable.dim);
//                mPager.setBackgroundResource(R.drawable.dim);
                //instantiate popup window
//                popupWindow = new PopupWindow(customView, ViewPager.LayoutParams.MATCH_PARENT, 800);
//                popupWindow.setBackgroundDrawable(new ColorDrawable(
//                        android.graphics.Color.TRANSPARENT));
//                relativeLayout.setAlpha(0.2F);
//
//
//                //display the popup window
//                popupWindow.showAtLocation(relativeLayout, Gravity.CENTER, 0, 0);
//                closePopupBtn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        popupWindow.dismiss();
//                        relativeLayout.setAlpha(1.0F);
//                    }
//                });
//            }
//        });

        nametext = findViewById(R.id.name);
        pricetxt = findViewById(R.id.price);
        desctxt = findViewById(R.id.desc);
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        productDetailPresenter = new ProductDetailPresenterImpl(this);
        showProgressDialogWithTitle("Loading.. Please Wait");
        //productDetailPresenter.getProduct(token,id);
        getProductDetails();

//        check.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (pincode.getText().length() == 0) {
//                    pincode.setError("Enter valid pincode");
//                }
//                if (pincode.getText().length() < 6) {
//                    pincode.setError("Enter valid pincode");
//                } else if (pincode.getText().length() == 6) {
//                    checkavailability();
//                }
//            }
//        });


        buynow = findViewById(R.id.buynow);
        buynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductDetailsSell.this, OrderSummarySell.class));
            }
        });

//        whatsapp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                //sendIntent.putExtra(Intent.EXTRA_TEXT, desctxt.getText().toString()+"https://creo.page.link/?link=https://www.google.com/?id="+token+"&apn=creo.com.haahooapp&st="+nametext.getText().toString()+"&sd=''&utm_source=AndroidApp");
//
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "https://creo.page.link/");
//                sendIntent.setType("text/plain");
//
//                Intent receiver = new Intent(ProductDetailsSell.this, AppChooserReceiver.class);
//                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT);
//                Intent chooser = Intent.createChooser(sendIntent, null, pendingIntent.getIntentSender());
//                startActivityForResult(chooser, 0);
//            }
//        });

//        whatsapptext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setAction(Intent.ACTION_SEND);
//                intent.putExtra(Intent.EXTRA_TEXT, desctxt.getText().toString() + "https://play.google.com/store/apps/details?id=com.vsco.cam&hl=en");
//                intent.setType("text/plain");
//
//                Intent intent1 = new Intent(ProductDetailsSell.this, AppChooserReceiver.class);
//                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
//                Intent intent2 = Intent.createChooser(intent, null, pendingIntent.getIntentSender());
//                startActivityForResult(intent2, 0);
//
//            }
//        });

//        gotocart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                startActivity(new Intent(ProductDetailsSell.this, Cart.class));
//
//            }
//        });

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }


    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    private void init() {
        for(int i=0;i<IMAGES.length;i++)
            ImagesArray.add(IMAGES[i]);

        mPager =  findViewById(R.id.pager);

        mPager.setAdapter(new SlidingImage_Adapter(ProductDetailsSell.this,images));

        CirclePageIndicator indicator =
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle1 indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES =images.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
//        Timer swipeTimer = new Timer();
//        swipeTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, 0, 3000);

        //Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }

        });

    }

    void checkavailability(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"order_details/check_pin/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if(message.equals("Failed")){
                                avail.setVisibility(View.VISIBLE);
                                avail.setText("Delivery not available");
                                pincode.clearFocus();
                            }
                            if(message.equals("Success")){
                                avail.setVisibility(View.VISIBLE);
                                avail.setText("Delivery available");
                                pincode.clearFocus();
                            }
                            Log.d("jsonobjecttd","pbjj"+message);

                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProductDetailsSell.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("pin",pincode.getText().toString());
                params.put("product_id",id);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>params = new HashMap<String,String>();
                params.put("Authorization", "Token "+token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }

    void getProductDetails(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"product_details/product_view_id/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ApiClient.quantity.clear();
                            ApiClient.name.clear();
                            ApiClient.images.clear();
                            ApiClient.prices.clear();
                            JSONObject jsonObject1 = new JSONObject(response);

                            JSONArray jsonArray = jsonObject1.getJSONArray("data");
                            String s = jsonArray.getString(jsonArray.length()-1);
                            JSONArray jsonObject5 = new JSONArray(s);
                            JSONObject jsonObject6 =jsonObject5.getJSONObject(0);
                            String id = jsonObject6.getString("pk");
                            ApiClient.ids.clear();
                            ApiClient.ids.add(id);
                            String fields = jsonObject6.getString("fields");
                            JSONObject jsonObject7 = new JSONObject(fields);
                            String name = jsonObject7.getString("name");
                            nametext.setText(name);
                            String description = jsonObject7.getString("description");
                            desctxt.setText(description);
                            String price = jsonObject7.getString("price");
                            pricetxt.setText("₹ "+price);
                            ApiClient.name.add(name);
                            ApiClient.price = "₹ "+price;
                            ApiClient.prices.add("₹ "+price);
                            ApiClient.quantity.add("1");
                            hideProgressDialogWithTitle();
                            String wishlisted = jsonArray.getJSONObject(0).getString("wishlisted");
                            if(wishlisted.equals("0")){
                                fav.setVisibility(View.VISIBLE);
                                fav_fill.setVisibility(View.GONE);
                            }
                            if(wishlisted.equals("1")){
                                fav_fill.setVisibility(View.VISIBLE);
                                fav.setVisibility(View.GONE);
                            }
                            JSONObject jsonObject = new JSONObject();
                            int k =0;
                            for(int i = 1 ; i<jsonArray.length()-1; i++) {
//                                 jsonObject = jsonArray.getJSONObject(i);
//                                String jsonArray2 = jsonObject.optString(String.valueOf(i));
                                String image = jsonArray.getString(i);
                                JSONObject jsonObject2 = new JSONObject(image);
                                imgs.add(jsonObject2.toString());
                            }

                            for (int i=0 ; i <imgs.size();i++){
                                JSONObject jsonObject3 = new JSONObject(imgs.get(i));
                                String jsonObject2 = imgs.get(i);
                                images.add(ApiClient.BASE_URL+"media/"+jsonObject3.getString(String.valueOf(i)));

                            }
                            ApiClient.images.add(images.get(0));
                            init();

                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProductDetailsSell.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("id",id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    @Override
    public void success(JSONArray jsonArray) throws JSONException {
        ApiClient.quantity.clear();
        ApiClient.name.clear();
        ApiClient.images.clear();
        ApiClient.prices.clear();
        jsonArray1 = jsonArray;

        for(int i=0 ; i<jsonArray1.length()-1 ; i++){

            String image = jsonArray1.getString(i);
            JSONObject jsonObject = new JSONObject(image);
            images.add(ApiClient.BASE_URL+"media/"+jsonObject.optString(String.valueOf(i)));
        }
        ApiClient.images.add(images.get(0));
//        init();
        try {
            String name = null;
            String price = null;
            String description = null;
            for(int i = 0 ; i<=jsonArray1.length() ; i++) {
                //   JSONObject jsonObject = jsonArray1.getString(i);
//                JSONObject jsonObject = jsonArray1.getJSONObject(i);
                String jsonArray2 = jsonArray1.getString(jsonArray1.length()-1);
                JSONArray jsonObject1 = new JSONArray(jsonArray2);
                JSONObject jsonObject2 = jsonObject1.getJSONObject(0);
                JSONObject jsonObject3 = jsonObject2.getJSONObject("fields");
                name = jsonObject3.getString("name");
                price ="₹ "+jsonObject3.getString("price");
                description = jsonObject3.getString("description");


                //
                //String jsonObject1 = jsonObject.getString("fields");
            }
            ApiClient.name.add(name);
            ApiClient.price = price;
            ApiClient.prices.add(price);
            ApiClient.quantity.add("Quantity : 1");
            nametext.setText(name);
            pricetxt.setText(price);
            desctxt.setText(description);
            // hideProgressDialogWithTitle();
//            String name = jsonObject1.getString("name");
//            String description = jsonObject1.getString("description");
//            String price = jsonObject1.getString("price");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void failure(String data) {

    }

    @Override
    public void success123(JSONArray jsonArray) throws JSONException {

    }

    @Override
    public void failed(String data) {

    }
}
