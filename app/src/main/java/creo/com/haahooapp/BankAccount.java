package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputEditText;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.config.ApiClient;

public class BankAccount extends AppCompatActivity {
    ImageView back;
    TextInputEditText account,ifsc,name;
    Context context = this;
    ProgressDialog progressDialog;
    TextView save,bankname,branchname,distrcitname,statename;
    CheckBox checkBox;
    RelativeLayout ifscdet;
    Activity activity = this;
    String token = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_account);
        Window window = activity.getWindow();
        progressDialog = new ProgressDialog(this);

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        ifscdet = findViewById(R.id.ifscdet);
        back = findViewById(R.id.back);
        checkBox = findViewById(R.id.checkBox);
        bankname = findViewById(R.id.bankname);
        branchname = findViewById(R.id.branchname);
        distrcitname = findViewById(R.id.districtname);
        statename = findViewById(R.id.statename);

        account = findViewById(R.id.account);
        ifsc = findViewById(R.id.ifsc);
        ifsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ifscdet.setVisibility(View.GONE);
            }
        });

        ifsc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                fetchIFSC();
            }
        });
        name = findViewById(R.id.name);
        save = findViewById(R.id.save);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(checkBox.isChecked())){
                    Toast.makeText(BankAccount.this,"Please verify by clicking the above checkbox",Toast.LENGTH_SHORT).show();
                }
                if(ifsc.getText().toString().length()==0){
                    ifsc.setError("IFSC cannot be empty");
                }
                if(account.getText().toString().length() == 0){
                    account.setError("Account number cannot be empty");
                }
                if(name.getText().toString().length() == 0){
                    name.setError("Name cannot be empty");
                }
                if(ifsc.getText().toString().length() !=0 && account.getText().toString().length() !=0 && name.getText().toString().length() !=0 && checkBox.isChecked()){
                    //addBank();


                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Confirm Submission");
                    builder.setMessage("These details can't be changed once submitted. Confirm Submission ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressDialog.setTitle("Adding..");
                            progressDialog.setIcon(R.drawable.haahoologo);
                            progressDialog.setMessage("Adding Bank Account...");
                            progressDialog.show();
                            addBank();
                            // Toast.makeText(context1, "You've choosen to delete all records", Toast.LENGTH_SHORT).show();
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Toast.makeText(context1, "You've changed your mind to delete all records", Toast.LENGTH_SHORT).show();
                        }
                    });

                    builder.show();
//

//
//            }
//        });
                }

            }
        });
        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(BankAccount.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(BankAccount.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(BankAccount.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(BankAccount.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BankAccount.super.onBackPressed();
            }
        });
    }

    public void addBank(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"bank_details/bank_det/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            String message = jsonObject1.optString("message");
                            if(message.equals("success")){
                                progressDialog.dismiss();
                                Toast.makeText(BankAccount.this,"Successfully added Bank account",Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(BankAccount.this,MyAccount.class);
                                intent.putExtra("fromactivity","update");
                                startActivity(intent);
                            }
                            if(!(message.equals("success"))){
                                progressDialog.dismiss();
                                // Log.d("editaccountrespo","gfdfgh"+message);
                                Toast.makeText(BankAccount.this,message,Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.dismiss();

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                      //  Toast.makeText(BankAccount.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("acc_name",name.getText().toString());
                params.put("acc_no",account.getText().toString());
                params.put("ifsc",ifsc.getText().toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String,String> params = new HashMap<String,String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public void fetchIFSC(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://ifsc.razorpay.com/"+ifsc.getText().toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            Log.d("jhgdf","kjhgfd"+jsonObject1);
                            String bank = jsonObject1.optString("BANK");
                            String branch = jsonObject1.optString("BRANCH");
                            String district = jsonObject1.optString("DISTRICT");
                            String state = jsonObject1.optString("STATE");
                            Log.d("jhgfdifsc","ifsc"+bank);
                            ifscdet.setVisibility(View.VISIBLE);
                            bankname.setText(bank);
                            branchname.setText(branch);
                            distrcitname.setText(district);
                            statename.setText(state);
                          //

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ifsc.setError("Invalid IFSC");
                        //  Toast.makeText(BankAccount.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
}
