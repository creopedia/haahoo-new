package creo.com.haahooapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import creo.com.haahooapp.Adapters.CoinAdapter;
import creo.com.haahooapp.Modal.Downloadd;
import creo.com.haahooapp.config.ApiClient;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static creo.com.haahooapp.Downloads.PERMISSIONS_REQUEST_READ_CONTACTS;

public class CoinFragment extends Fragment {

        RecyclerView recyclerView;
        CoinAdapter coinAdapter;
        List<String> list = new ArrayList<>();
        List<String> name = new ArrayList<>();
        List<String> date = new ArrayList<>();
        List<String> number = new ArrayList<>();
        TextView invite;

    public CoinFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coin,container,false);
        recyclerView = view.findViewById(R.id.recyclerview);
        invite = view.findViewById(R.id.invite);
        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(contactPickerIntent, 1);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setNestedScrollingEnabled(false);
        getDownloads();

        return view;

    }



    public void getDownloads(){

        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/login_ref_count/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("refcount","refff"+jsonObject);
                            name.clear();
                            date.clear();
                            String message = jsonObject.optString("message");
                            if(message.equals("Success")){
                                JSONArray jsonArray = jsonObject.optJSONArray("data");
                                for(int i=0 ;i<jsonArray.length();i++){
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                    name.add(jsonObject1.optString("name"));
                                    date.add(jsonObject1.optString("join_date"));
                                    number.add(jsonObject1.optString("phone_no"));
                                }

                                final List<Downloadd> downloadPojos = new ArrayList<>();
                                for(int k=0;k<name.size();k++){
                                    Downloadd downloadPojo = new Downloadd(name.get(k),date.get(k),number.get(k));
                                    downloadPojos.add(downloadPojo);
                                }
                                coinAdapter = new CoinAdapter(downloadPojos,getContext());
                                recyclerView.setNestedScrollingEnabled(false);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                recyclerView.setAdapter(coinAdapter);
                            }
                            if(!(message.equals("Success"))){
                                Toast.makeText(getContext(),"No data found",Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Downloads.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Authorization","Token "+token);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }

    private void sendMessageToWhatsAppContact(String number) {
        PackageManager packageManager = getContext().getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            String url = "https://api.whatsapp.com/send?phone=" + number + "&text=" + URLEncoder.encode("Hii", "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                getContext().startActivity(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        if (resultCode == RESULT_OK) {
            // Check for the request code, we might be usign multiple startActivityForReslut
            switch (requestCode) {
                case 1:
                    Cursor cursor = null;
                    try {
                        String phoneNo = null ;
                        String name = null;
                        Uri uri = data.getData();
                        cursor = getContext().getContentResolver().query(uri, null, null, null, null);
                        cursor.moveToFirst();
                        int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        phoneNo = cursor.getString(phoneIndex);
                        if (phoneNo.length()>2){
                            sendMessageToWhatsAppContact("91"+phoneNo.replace("(","").replace(")","").trim());
                        }
                        Toast.makeText(getContext(),"num"+phoneNo.replace("(","").replace(")",""),Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }

}