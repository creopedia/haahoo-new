package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import creo.com.haahooapp.Adapters.Adapterr;
import creo.com.haahooapp.Adapters.SliderAdapterExample;

public class HomeAlter extends AppCompatActivity {

    ImageView first,second,third,fourth,fifth,six,one,two,topp;
    Context context = this;
    RecyclerView recyclerView,off;
    List<String> list = new ArrayList<>();
    SliderView sliderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage_alter_design);
        first = findViewById(R.id.first);
        second = findViewById(R.id.second);
        off = findViewById(R.id.off);
        third = findViewById(R.id.third);
        fourth = findViewById(R.id.fourth);
        fifth = findViewById(R.id.fifth);
        six = findViewById(R.id.six);
        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        sliderView = findViewById(R.id.slider1);
        SliderAdapterExample adapter = new SliderAdapterExample(this);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.BLACK);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(10); //set scroll delay in seconds :
        sliderView.startAutoCycle();
        list.add("https://grofers-prod-consumer-appimages.s3.amazonaws.com/images/orange-cash_no.jpg");
        list.add("https://indiancashback.com/blog/wp-content/uploads/2017/09/the-big-billion-days-2017-sale-cashback-rewards-discounts-offers.jpg");
        topp = findViewById(R.id.topp);
        recyclerView = findViewById(R.id.recycler);
        Adapterr adapterr = new Adapterr(list,context);
        //recyclerView.setAdapter(adapterr);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
        recyclerView.setAdapter(adapterr);
        Glide.with(context).load("https://lh3.googleusercontent.com/-ZepQghBinac/Xjuwt9QrRII/AAAAAAAAESI/CL0VIP7hWdY2LnIysjWsIheu7iw7payiQCK8BGAsYHg/s0/banner.jpg").into(topp);
        Glide.with(context).load("https://assets.myntassets.com/dpr_2,q_60,w_210,c_limit,fl_progressive/assets/images/4318138/2018/5/4/11525433792765-HERENOW-Men-Black-Printed-Round-Neck-T-shirt-2881525433792598-1.jpg").into(one);
        Glide.with(context).load("https://media.wired.com/photos/5cb8a587ef61de5f7bce668d/master/pass/Motorola-Moto-G7-front-and-back-SOURCE-Motorola.jpg").into(two);
        Glide.with(context).load("https://img.sitaphal.com/media/uploads/sitaphal-3314-1-1-1477302259.jpg").into(first);
        Glide.with(context).load("https://hungamadeal.co.in/wp-content/uploads/2016/07/other-offer-2.jpg").into(second);
        Glide.with(context).load("https://images.milled.com/2017-02-19/AsNYwpuGlKQlHMHr/Lw4tUluEH_ml.jpg").into(third);
        Glide.with(context).load("https://myindiacoupons.com/wp-content/uploads/2016/11/Yepme-Clothing-Loot-Offer-300x200.jpg").into(fourth);
        Glide.with(context).load("https://img.freepik.com/free-vector/sale-special-offer-banner-sunburst-50-off-discount_1588-739.jpg?size=338&ext=jpg").into(fifth);
        Glide.with(context).load("https://image.freepik.com/free-vector/crazy-deals-discount-banner-voucher-template-design_1017-13697.jpg").into(six);
    }
}