package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.NewHomeDeals;
import creo.com.haahooapp.Adapters.NewHomeGrocery;
import creo.com.haahooapp.Adapters.NewHomeHouseAdapter;
import creo.com.haahooapp.Modal.Grocery;
import creo.com.haahooapp.Modal.PopularPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MoreProduct extends AppCompatActivity {

    Activity activity = this;
    RecyclerView list;
    ProgressDialog progressDialog;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_product);
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        list = findViewById(R.id.list);
        Intent intent = getIntent();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Fetching Products...");

        if (intent.hasExtra("from")) {
            if (intent.getStringExtra("from").equals("fish")) {
                progressDialog.show();
                fishandmeat();
            }
            if (intent.getStringExtra("from").equals("fruits")) {
                progressDialog.show();
                fruits();
            }
            if (intent.getStringExtra("from").equals("grocery")) {
                progressDialog.show();
                grocery();
            }
        }
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.action_home:
                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:
                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:
                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:
                                startActivity(new Intent(context, AllStories.class));
                                break;

                            case R.id.shop:

//                                startActivity(new Intent(context, MyShopNew.class));
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
    }

    public void grocery() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        Call<ResponseBody> callregister = apiInterface.getProductsFish("grocery", "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    List<Grocery> trendingPojos = new ArrayList<>();
                    JSONArray jsonArray = obj.optJSONArray("data");
                    //Log.d("JSSSSS", "JJJJJ" + jsonArray);
                    if (jsonArray.length()>=10) {

                        for (int i = 0; i < 10; i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Grocery category = new Grocery();
                            category.setName(jsonObject.optString("product_name"));
                            String images1 = jsonObject.optString("pdt_image");
                            String[] seperated = images1.split(",");
                            category.setShop_id(jsonObject.optString("shop_id"));
                            category.setShop_name(jsonObject.optString("shop_name"));
                            category.setPrice("From Rs. " + jsonObject.optString("discount"));
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            trendingPojos.add(category);
                        }

                    }
                    if (jsonArray.length() <10 ){
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Grocery category = new Grocery();
                            category.setName(jsonObject.optString("product_name"));
                            String images1 = jsonObject.optString("pdt_image");
                            String[] seperated = images1.split(",");
                            category.setShop_id(jsonObject.optString("shop_id"));
                            category.setShop_name(jsonObject.optString("shop_name"));
                            category.setPrice("From Rs. " + jsonObject.optString("discount"));
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            trendingPojos.add(category);
                        }
                    }
                    list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                    list.setAdapter(new NewHomeGrocery(getApplicationContext(), trendingPojos));
                    list.setNestedScrollingEnabled(false);
                    progressDialog.dismiss();

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });


    }

    public void fruits() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        //NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        Call<ResponseBody> callregister = apiInterface.getProductsFish("fruits", "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    List<PopularPojo> trendingPojos = new ArrayList<>();
                    JSONArray jsonArray = obj.optJSONArray("data");
                    Log.d("JSSSSS", "JJJJJ" + jsonArray);
                    if (jsonArray.length() == 0) {
                        Toast.makeText(MoreProduct.this, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            PopularPojo category = new PopularPojo();
                            category.setName(jsonObject.optString("product_name"));
                            String images1 = jsonObject.optString("pdt_image");
                            category.setShop_name(jsonObject.optString("shop_name"));
                            category.setShop_id(jsonObject.optString("shop_id"));
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            trendingPojos.add(category);
                        }
                    }
                    list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                    list.setAdapter(new NewHomeHouseAdapter(getApplicationContext(), trendingPojos));
                    list.setNestedScrollingEnabled(false);
                    progressDialog.dismiss();
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });


    }

    public void fishandmeat() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        //NewProduct newProduct = new NewProduct(shop_id);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        Call<ResponseBody> callregister = apiInterface.getProductsFish("meat", "Token " + token);
        callregister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                ResponseBody x = response.body();
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    List<PopularPojo> trendingPojos = new ArrayList<>();
                    JSONArray jsonArray = obj.optJSONArray("data");
                    Log.d("JSSSSS", "JJJJJ" + jsonArray);
                    if (jsonArray.length() == 0) {
                        Toast.makeText(MoreProduct.this, "Sorry no products available right now", Toast.LENGTH_SHORT).show();
                    }
                    if (jsonArray.length() != 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            PopularPojo category = new PopularPojo();
                            category.setName(jsonObject.optString("product_name"));
                            String images1 = jsonObject.optString("pdt_image");
                            category.setPrice("Rs. " + jsonObject.optString("discount"));
                            String[] seperated = images1.split(",");
                            category.setShop_name(jsonObject.optString("shop_name"));
                            category.setShop_id(jsonObject.optString("shop_id"));
                            String split = seperated[0].replace("[", "").replace("]", "");
                            category.setImage(ApiClient.BASE_URL + split);
                            trendingPojos.add(category);
                        }
                    }
                    list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                    list.setAdapter(new NewHomeDeals(getApplicationContext(), trendingPojos));
                    list.setNestedScrollingEnabled(false);
                    progressDialog.dismiss();

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });


    }


}