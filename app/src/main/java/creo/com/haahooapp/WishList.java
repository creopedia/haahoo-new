package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import creo.com.haahooapp.Adapters.WishListAdapter;
import creo.com.haahooapp.Implementation.WishListPresenterImpl;
import creo.com.haahooapp.Modal.CartPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.WishListCallBack;
import creo.com.haahooapp.interfaces.WishListPresenter;
import creo.com.haahooapp.interfaces.WishListView;

public class WishList extends AppCompatActivity implements WishListCallBack,WishListView {

    RecyclerView recyclerView;
    WishListPresenter wishListPresenter;
    ImageView imageView,imageView2;
    Activity activity = this;
    ProgressDialog progressDialog;
    RelativeLayout relativeLayout;
    Context context = this;
    ImageView back;
    TextView textView, textView2, textView3,textView4,textView5,start;
    String token = null;
    ArrayList<String> ids = new ArrayList<>();
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String>image = new ArrayList<>();
    ArrayList<String>quantity = new ArrayList<>();
    ArrayList<String>price = new ArrayList<>();
    ArrayList<String> total = new ArrayList<>();
    public List<CartPojo> pjo = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        back = findViewById(R.id.back);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        imageView = findViewById(R.id.img);

        textView = findViewById(R.id.msg);
        relativeLayout = findViewById(R.id.relaa);
//        textView2 = findViewById(R.id.btn);
        start = findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WishList.this,NewHome.class));
            }
        });
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
         token = (pref.getString("token", ""));
        textView3 = findViewById(R.id.wishlist);
//        textView5 = findViewById(R.id.retry);
        recyclerView = findViewById(R.id.recyclerView);
        progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(WishList.this,NewHome.class));
            }
        });
//        showProgressDialogWithTitle("Loading.. Please Wait..");
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(WishList.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(WishList.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(WishList.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context,MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context,AllStories.class));
                                break;

                        }
                        return true;
                    }
                });
        wishListPresenter = new WishListPresenterImpl(this);

        boolean internet = isNetworkAvailable();

            progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
            wishListPresenter = new WishListPresenterImpl(this);

            token = (pref.getString("token", ""));
//            showProgressDialogWithTitle("Loading.. Please Wait..");


            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"wishlist/wishlist_show/",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {

                                JSONObject jsonObject1 = new JSONObject(response);
                                String message = jsonObject1.getString("message");
                               // Log.d("imagewish", "wish" +jsonObject1.optString("Total count"));
                                ApiClient.wish_count = jsonObject1.optString("Total count");
                                if(message.equals("Failed")){
                                    recyclerView.setVisibility(View.GONE);
                                    relativeLayout.setVisibility(View.VISIBLE);
                                }
                                if(!(message.equals("Failed"))) {
                                    relativeLayout.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    String jsonArray = jsonObject1.getString("data");
                                    JSONArray jsonObject11 = new JSONArray(jsonArray);
                                    String total_price = null;
                                    for (int i = 0; i < jsonObject11.length(); i++) {
                                        ids.add(jsonObject11.getJSONObject(i).getString("id"));
                                        name.add(jsonObject11.getJSONObject(i).getString("name"));
                                        String images1 = jsonObject11.getJSONObject(i).getString("image");
                                        String[] seperated = images1.split(",");
                                        String split = seperated[0].replace("[", "");
                                        image.add(ApiClient.BASE_URL+"media/" + split);
                                        quantity.add("Quantity : " + jsonObject11.getJSONObject(i).getString("count"));
                                        price.add("₹ " + jsonObject11.getJSONObject(i).getString("price"));
                                    }
                                    ApiClient.wishlist_ids = ids;
                                    for (int i = 0; i < name.size(); i++) {
                                        /*CartPojo productPojo = new CartPojo(name.get(i), price.get(i), image.get(i), quantity.get(i),"");
                                        pjo.add(productPojo);*/
                                    }

                                    WishListAdapter cartAdapter = new WishListAdapter(pjo, context);
                                    recyclerView.setHasFixedSize(true);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    recyclerView.setAdapter(cartAdapter);
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(WishList.this,error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("Authorization","Token "+token);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }


    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }


    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void onVerifySuccessWish(String data) {

    }

    @Override
    public void onVerifySuccessWishRemove(String data) {

    }

    @Override
    public void onVerifyFailedWish(String msg) {

    }

    @Override
    public void wishlistviewsuccess(String msg) {

    }

    @Override
    public void wishlistviewfailed(String data) {
     //   Toast.makeText(WishList.this,"Sever failure",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessWish(String response) {

    }

    @Override
    public void onRemove(String response) {

    }

    @Override
    public void onFailedWish(String response) {
//        Toast.makeText(WishList.this,"Sever failure",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void wishView(String response) {

    }

    @Override
    public void wishFailed(String response) {
//        hideProgressDialogWithTitle();
        imageView.setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);
        textView2.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        textView3.setVisibility(View.GONE);
        textView4.setVisibility(View.GONE);
        imageView2.setVisibility(View.GONE);
        textView5.setVisibility(View.GONE);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void noInternetConnection() {

    }

    public  void wishlist_remove(final String id1) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"wishlist/wishlist_remove/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            pjo.remove(ApiClient.wishlist_ids.indexOf(id1));
                            recyclerView.removeViewAt(ApiClient.wishlist_ids.indexOf(id1));
                            //cartAdapter.notifyDataSetChanged();
                            showProgressDialogWithTitle("Removing..");
                            startActivity(new Intent(WishList.this,WishList.class));
                            hideProgressDialogWithTitle();
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(WishList.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("productid",id1);
                params.put("virtual","0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


    public  void movetocart(final String id1) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"wishlist/wishlist_to_cart/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            pjo.remove(ApiClient.wishlist_ids.indexOf(id1));
                            recyclerView.removeViewAt(ApiClient.wishlist_ids.indexOf(id1));
                            //cartAdapter.notifyDataSetChanged();
                            showProgressDialogWithTitle("Moving to cart..");
                            startActivity(new Intent(WishList.this,WishList.class));
                            hideProgressDialogWithTitle();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(WishList.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("productid",id1);
                params.put("virtual","0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(WishList.this,NewHome.class));
    }
}