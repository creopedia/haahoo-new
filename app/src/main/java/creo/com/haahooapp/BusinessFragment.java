package creo.com.haahooapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.Adapters.BusinessAdapter;
import creo.com.haahooapp.Adapters.ProductsInShopAdapter;
import creo.com.haahooapp.Modal.BusinessPojo;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ItemClickSupport;

import static android.content.Context.MODE_PRIVATE;


public class BusinessFragment extends Fragment {
    RecyclerView recyclerView;
    BusinessAdapter businessAdapter;

    AlertDialog.Builder builder;
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    public BusinessFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business,container,false);
        recyclerView = view.findViewById(R.id.recyclerView);
        builder = new AlertDialog.Builder(getContext());

        getBusinessDetails();
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

               // Toast.makeText(getContext(),"Clicked"+position,Toast.LENGTH_SHORT).show();

//                builder.setMessage("") .setTitle("Confirm");
//
//                //Setting message manually and performing action on button click
//                builder.setMessage("Do you want to share this content to whatsapp ?")
//                        .setCancelable(false)
//                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                boolean status = isAppInstalled(getContext(),"com.whatsapp");
//                                if (!status){
//                                    Toast.makeText(getContext(),"Whatsapp not installed",Toast.LENGTH_SHORT).show();
//                                }
//                                if (status){
//                                    Toast.makeText(getContext(),"installed",Toast.LENGTH_SHORT).show();
//                                }
//                               dialog.dismiss();
//                            }
//                        })
//                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                //  Action for 'NO' Button
//                                dialog.cancel();
//                            }
//                        });
//                Creating dialog box
//                AlertDialog alert = builder.create();
//                alert.setTitle("Confirm ");
//                alert.show();
            }
        });
        return view;
    }


    public void getBusinessDetails(){
        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(getContext());

        //this is the url where you want to send the request

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"bussiness/bussiness_img_list/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("productsinshop","shopprods"+response);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            ArrayList<BusinessPojo>images = new ArrayList<>();

                            for (int i =0;i<jsonArray.length();i++){
                                BusinessPojo businessPojo = new BusinessPojo();
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                businessPojo.setImage(ApiClient.BASE_URL+"media/"+jsonObject.optString("image"));
                                businessPojo.setName(jsonObject.optString("name"));
                                businessPojo.setId(jsonObject.optString("id"));
                               // Log.d("businessid","idval"+jsonObject.optString("id"));
                                images.add(businessPojo);
                            }

                            businessAdapter = new BusinessAdapter(getContext(),images);
                            //recyclerView.setNestedScrollingEnabled(false);
                            recyclerView.setAdapter(businessAdapter);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {




            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }



}