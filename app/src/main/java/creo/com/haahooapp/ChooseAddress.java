package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.RecyclerAdapter;
import creo.com.haahooapp.Implementation.AddaddressPresenterImpl;
import creo.com.haahooapp.Modal.AddressPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.AddaddressCallback;
import creo.com.haahooapp.interfaces.AddaddressPresenter;
import creo.com.haahooapp.interfaces.AddaddressView;

public class ChooseAddress extends AppCompatActivity implements AddaddressCallback, AddaddressView {

    // ArrayList<RecyclerPojo>recyclerPojos;
    AddaddressPresenter addaddressPresenter;
    public List<AddressPojo> pjo = new ArrayList<>();
    private RecyclerAdapter recyclerAdapter;
    private RecyclerView recyclerView;
    Context context = this;
    Activity activity = this;
    TextView addnew;
    ImageView imageView;
    ArrayList<String> name = new ArrayList<>();
    ArrayList<String> ids = new ArrayList<>();
    ArrayList<String> address = new ArrayList<>();
    ArrayList<String> phone = new ArrayList<>();
    ArrayList<String> pincode = new ArrayList<>();
    ArrayList<String> defau = new ArrayList<>();
    String intentvalue = "null";
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_address);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading..");
        progressDialog.setMessage("Fetching Address..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.show();
// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        addnew = findViewById(R.id.addnew);
        imageView = findViewById(R.id.imageView3);
        addaddressPresenter = new AddaddressPresenterImpl(this);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        ApiClient.token = token;
        Intent intent = getIntent();
        if (intent.hasExtra("from")) {
            Bundle bundle = getIntent().getExtras();
            intentvalue = bundle.getString("from");

        }
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(ChooseAddress.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(ChooseAddress.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(ChooseAddress.this, SettingsActivity.class));
                                break;


                            case R.id.stories:

                                //startActivity(new Intent(ChooseAddress.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "booking/address_list/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            progressDialog.dismiss();
                            String jsonArray = jsonObject1.getString("data");
                            JSONArray jsonObject11 = new JSONArray(jsonArray);
                            for (int i = 0; i < jsonObject11.getJSONArray(0).length(); i++) {
                                ids.add(jsonObject11.getJSONArray(0).getJSONObject(i).getString("pk"));
                                name.add(jsonObject11.getJSONArray(0).getJSONObject(i).getJSONObject("fields").getString("name"));
                                address.add(jsonObject11.getJSONArray(0).getJSONObject(i).getJSONObject("fields").getString("house_no"));
                                phone.add(jsonObject11.getJSONArray(0).getJSONObject(i).getJSONObject("fields").getString("phone_no"));
                                defau.add(jsonObject11.getJSONArray(0).getJSONObject(i).getJSONObject("fields").getString("default"));
                                pincode.add(jsonObject11.getJSONArray(0).getJSONObject(i).getJSONObject("fields").getString("pincode"));
                            }
                            for (int i = 0; i < name.size(); i++) {
                                AddressPojo addressPojo = new AddressPojo(name.get(i), address.get(i), phone.get(i), pincode.get(i), defau.get(i));
                                pjo.add(addressPojo);
                            }
                            ApiClient.addressids = ids;
                            recyclerView = findViewById(R.id.recyclerView);
                            RecyclerAdapter recyclerAdapter = new RecyclerAdapter(pjo, context);
                            recyclerView.setAdapter(recyclerAdapter);
                            recyclerView.setNestedScrollingEnabled(false);
                            recyclerView.setLayoutManager(new LinearLayoutManager(context,RecyclerView.VERTICAL,false));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(ChooseAddress.this,error.toString(),Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        addaddressPresenter.viewAddress1(token);
//        RecyclerPojo[]recyclerPojo=new RecyclerPojo[]{
//                new RecyclerPojo("Shijin","Creopedia Business Intelligence Pvt Ltd","9089087678"),
//                new RecyclerPojo("Nithin","Kites softwares Pvt Ltd","6789878979"),
//                new RecyclerPojo("Akhil","AI Village ","8909878978"),
//
//        };


        addnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChooseAddress.this, AddAddress.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentvalue.equals("null")) {
                    startActivity(new Intent(ChooseAddress.this, NewHome.class));
                }
                if (intentvalue.equals("details")) {
                    ChooseAddress.super.onBackPressed();
                }
            }
        });
    }

    @Override
    public void onVerifySuccess(String data) {

    }

    @Override
    public void onVerifyFailed(String msg) {

    }

    @Override
    public void viewAddress(JSONArray data) {

        Log.d("jsonarray", "array" + data);

    }

    @Override
    public void viewFailed(String data) {

    }

    @Override
    public void onSuccess(String response) {

    }

    @Override
    public void onFailed(String response) {

    }

    @Override
    public void addressview(JSONArray data) {


        JSONArray jsonArray = data;
        try {
            JSONArray jsonObject1 = new JSONArray(jsonArray.getJSONArray(0));
            JSONObject jsonObject11 = jsonObject1.getJSONObject(0).getJSONObject("fields");
            Log.d("jsonarray", "array" + jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {

        if (intentvalue.equals("null")) {
            startActivity(new Intent(ChooseAddress.this, NewHome.class));
        }
        if (intentvalue.equals("details")) {
            super.onBackPressed();
        }
    }

    @Override
    public void addressfail(String data) {

    }

    @Override
    public void noInternetConnection() {

    }
}