package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Implementation.AddaddressPresenterImpl;
import creo.com.haahooapp.Modal.AddaddressPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.AddaddressCallback;
import creo.com.haahooapp.interfaces.AddaddressPresenter;
import creo.com.haahooapp.interfaces.AddaddressView;

public class Subscription_add_address extends AppCompatActivity implements AddaddressCallback, AddaddressView {

    TextInputEditText name,address,phone_no,landmark,city,state,pincode,area;
    TextView save;
    AddaddressPresenter addaddressPresenter;
    String device_id = null;
    ImageView imageView;
    Context context= this;
    Spinner spinner;
    Activity activity = this;
    ArrayList<String> areas = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        Window window = activity.getWindow();
        areas.add("Choose Area");

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, areas);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
//    spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//      @Override
//      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Toast.makeText(AddAddress.this,"selected"+areas.get(position),Toast.LENGTH_SHORT).show();
//      }
//    });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(AddAddress.this,"selected"+areas.get(position),Toast.LENGTH_SHORT).show();
                if (areas.get(position).equals("Choose Area")){
                    area.setText("");
                }
                if(!(areas.get(position).equals("Choose Area"))) {
                    area.setText(areas.get(position));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        name=findViewById(R.id.name);
        name.setText(ApiClient.username);
        address=findViewById(R.id.address);
        phone_no=findViewById(R.id.phone);
        landmark=findViewById(R.id.landmark);
        city=findViewById(R.id.city);
        state=findViewById(R.id.state);
        pincode=findViewById(R.id.pincode);
        save=findViewById(R.id.save);
        area = findViewById(R.id.area);
        imageView=findViewById(R.id.imageView3);

        device_id =  Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        addaddressPresenter=new AddaddressPresenterImpl(this);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        pincode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                areas.clear();
                getLocationDetails();
            }
        });
        pincode.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                areas.clear();

                return false;
            }
        });

        save.setText("Save & Continue");

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (name.getText().toString().length() < 3){
                    name.setError("Enter a valid name");
                }
                if(phone_no.getText().toString().length() < 10){
                    phone_no.setError("Enter a valid number");
                }
                if(pincode.getText().toString().length() <6 ){
                    pincode.setError("Pincode must be of six digits");
                }
                if(name.getText().toString().length() == 0){
                    name.setError("Name cannot be empty");
                }
                if(address.getText().toString().length() == 0 ){
                    address.setError("Address cannot be empty");
                }
                if (city.getText().toString().length() == 0){
                    city.setError("City cannot be empty");
                }
                if(state.getText().toString().length() == 0){
                    state.setError("State name cannot be empty");
                }
                if(name.getText().toString().length()>=3 && phone_no.getText().toString().length() == 10 && pincode.getText().toString().length() == 6
                        && address.getText().toString().length()!=0 && city.getText().toString().length() !=0 && state.getText().toString().length() !=0)
                {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                    String token = (pref.getString("token", ""));
//                    final AddaddressPojo addaddressPojo = new AddaddressPojo(name.getText().toString(), address.getText().toString(), phone_no.getText().toString(),pincode.getText().toString(),area.getText().toString(),landmark.getText().toString(),city.getText().toString(),state.getText().toString(), device_id);
//                    addaddressPresenter.addaddress(addaddressPojo,"Token "+""+token);
                }
            }
        });
    }

    @Override
    public void onVerifySuccess(String data) {

    }

    @Override
    public void onVerifyFailed(String msg) {

    }

    @Override
    public void viewAddress(JSONArray data) {

    }

    @Override
    public void viewFailed(String data) {

    }

    @Override
    public void onSuccess(String response) {

        if(!(response.equals("null"))) {
            Toast.makeText(context, "Successfully Added", Toast.LENGTH_LONG).show();
            startActivity(new Intent(context,SubscriptionAddress.class));
//            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//            SharedPreferences.Editor editor = pref.edit();
//            editor.putString("token", response);
//            editor.commit();
            // startActivity(new Intent(AddAddress.this, SetPin.class));
        }
        if (response.equals("null")) {
            Toast.makeText(context, "Failed to add", Toast.LENGTH_LONG).show();
        }

    }






    @Override
    public void onFailed(String response) {

    }

    @Override
    public void addressview(JSONArray data) {

    }

    @Override
    public void addressfail(String data) {

    }

    @Override
    public void noInternetConnection() {

    }

    public void getLocationDetails(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://form-api.com/api/geo/country/zip?key=snN7x4rWja5Jm3whmFwQ&country=IN&zipcode="+pincode.getText().toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            areas.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject results = jsonObject.getJSONObject("result");
                            String state1 = results.optString("state");
                            String city1 = results.optString("city");
                            String level4area = results.optString("level4area");
                            state.setText(state1);
                            city.setText(city1);
//                  area.setText(level4area);
                            String[] seperated = level4area.split(",");

//                  ArrayList<String> places = new ArrayList<>();
//                  for (int i=0;i<seperated.length;i++){
//                    places.add(seperated[i].replace("[",""));
//                  }
//                  String split = seperated[0].replace("[", "");

                            List<String> list = new ArrayList<String>(Arrays.asList(seperated));
                            ArrayList<String>places = new ArrayList<String>();
                            areas.add("Choose Area");
                            for (int i =0 ;i<list.size();i++){
                                areas.add(list.get(i).replace("[","").replace("\"", "").replace("]",""));
                            }


//                  Log.d("results","result222"+places.size()+places.get(0));

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(AddAddress.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                //params.put("search_key",searchView.getQuery().toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }
}
