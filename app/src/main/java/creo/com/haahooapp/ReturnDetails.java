package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

public class ReturnDetails extends AppCompatActivity {

    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_details);
        back = findViewById(R.id.back);
        back .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReturnDetails.super.onBackPressed();
            }
        });

    }
}