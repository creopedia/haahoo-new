package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.config.ApiClient;

public class MakePaymentEvents extends AppCompatActivity implements PaymentResultListener {
    Activity activity = this;
    String number = null;
    String email = null;
    String event_id = null;
    Context context = this;
    String data ="null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_payment_events);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        Bundle bundle = getIntent().getExtras();
        number = bundle.getString("number");
        email = bundle.getString("email");
        event_id = bundle.getString("event_id");
        data = bundle.getString("data");
        Checkout.preload(getApplicationContext());
        startPayment();
    }

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {

            JSONObject options = new JSONObject();
            options.put("name", "Haahoo");
            options.put("description", "Event Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("currency", "INR");
        //    String price[] = ApiClient.price.split("₹ ");
            //options.put("amount", ApiClient.event_price);
            options.put("amount",ApiClient.event_price);
            JSONObject preFill = new JSONObject();
            preFill.put("email", email);
            preFill.put("contact", number);
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {

        Log.d("asss","pay"+s);
        String payment_id = s;
        Toast.makeText(MakePaymentEvents.this,"Payment Success",Toast.LENGTH_SHORT).show();
        submitPaymentDetails(payment_id);
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(MakePaymentEvents.this,"Cancelled Payment",Toast.LENGTH_LONG).show();
        startActivity(new Intent(MakePaymentEvents.this,NewHome.class));
    }

    public void submitPaymentDetails(final String paymentid){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"events/event_ordering/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if (message.equals("Success")){
                                Toast.makeText(MakePaymentEvents.this,"Registered Successfully",Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(MakePaymentEvents.this,NewHome.class));
                            }
                            if(!(message.equals("Success"))){
                                //Toast.makeText(MakePaymentEvents.this,"//error",Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(MakePaymentEvents.this,NewHome.class));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(MakePaymentEvents.this, "//error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id",event_id);
                params.put("reg_id",data);
                params.put("payment_method","ONLINE");
                params.put("payment_status","completed");
                params.put("payment_id", paymentid);
                params.put("payment_amount",ApiClient.event_price);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
}
