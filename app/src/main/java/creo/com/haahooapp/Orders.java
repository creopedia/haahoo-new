package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.OrderAdapter;
import creo.com.haahooapp.Modal.OrderPojo;
import creo.com.haahooapp.config.ApiClient;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Orders extends AppCompatActivity {
    //private String URLstring = "http://haahoo.creopedia.com/order_details/view_order/";
    private static ProgressDialog mProgressDialog;
    ArrayList<OrderPojo> dataModelArrayList;
    private RecyclerView recyclerView;
    String token = null;
    RelativeLayout relativeLayout;
    ArrayList<String> image = new ArrayList<String>();
    PopupWindow popupWindow;
    private OrderAdapter orderAdapter;
    ProgressDialog progressDialog;
    TextView filters;
    String filter_mode = "null";
    ImageView back;
    Activity activity = this;
    Context context = this;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        Window window = activity.getWindow();
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmer();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        recyclerView = findViewById(R.id.recyclerView);
        back = findViewById(R.id.back);
        filters = findViewById(R.id.filters);
        relativeLayout = findViewById(R.id.base);
        filters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater) Orders.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.filter_orders, null);
                RadioButton recent = customView.findViewById(R.id.recent);
                RadioButton threemonths = customView.findViewById(R.id.threemonths);
                RadioButton sixmonths = customView.findViewById(R.id.sixmonths);
                RadioButton oneyear = customView.findViewById(R.id.oneyear);
                recent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        filter_mode = "recent";
                    }
                });
                threemonths.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        filter_mode = "3 months";
                    }
                });
                sixmonths.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        filter_mode = "6 months";
                    }
                });
                oneyear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        filter_mode = "12 months";
                    }
                });
                Button closePopupBtn = (Button) customView.findViewById(R.id.closePopupBtn);
                final TextView apply = customView.findViewById(R.id.apply);

                apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (filter_mode.equals("recent")) {
                            Log.d("fetching", "json" + filter_mode);
                            recent();
                        }
                        if (!(filter_mode.equals("recent"))) {
                            Log.d("rdddd", "json" + filter_mode);
                            filter_order(filter_mode);
                        }
                    }
                });
                popupWindow = new PopupWindow(customView, ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                popupWindow.setBackgroundDrawable(new ColorDrawable(
                        android.graphics.Color.TRANSPARENT));
                relativeLayout.setAlpha(0.2F);

                //display the popup window
                popupWindow.showAtLocation(relativeLayout, Gravity.CENTER, 0, 0);
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        relativeLayout.setAlpha(1.0F);
                    }
                });
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Orders.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Orders.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Orders.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(Orders.this, AllStories.class));
                                Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context, "Coming Soon", Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = getIntent().getExtras();
                String intent_value = bundle.getString("fromactivity");
                if (intent_value.equals("payment")) {
                    startActivity(new Intent(Orders.this, NewHome.class));
                }
                if (intent_value.equals("profile")) {
                    Orders.super.onBackPressed();
                }
                if (intent_value.equals("home")) {
                    Orders.super.onBackPressed();
                }
                if (intent_value.equals("details")) {
                    startActivity(new Intent(Orders.this, NewHome.class));
                }
            }
        });
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                listOrders();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));

    }


    private void filter_order(final String filter_mode) {

        RequestQueue queue = Volley.newRequestQueue(Orders.this);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "search/order_filters/";

        // Request a string response from the provided URL.


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            popupWindow.dismiss();
                            relativeLayout.setAlpha(1.0F);
                            Log.d("response", "resp" + response);
                            dataModelArrayList = new ArrayList<>();
                            JSONArray dataArray = obj.getJSONArray("data");

                            if (dataArray.length() == 0) {
                                Toast.makeText(Orders.this, "Nothing to display", Toast.LENGTH_SHORT).show();
                            }

                            for (int i = 0; i < dataArray.length(); i++) {

                                OrderPojo playerModel = new OrderPojo();
                                JSONObject dataobj = dataArray.getJSONObject(i);
                                String images1 = dataobj.getString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "").replace("]", "");

                                //image.add("http://haahoo.creopedia.com/media/" + split);
                                playerModel.setProductid(dataobj.getString("order_id"));
                                ApiClient.productids.add(dataobj.getString("order_id"));
                                ApiClient.shids.add(dataobj.optString("sh_order_id"));
                                ApiClient.virtual_order.add(dataobj.optString("virtual"));
                                playerModel.setName(dataobj.getString("name"));
                                playerModel.setPrice(dataobj.getString("status"));
                                playerModel.setQty(ApiClient.BASE_URL + "media/" + split);
                                playerModel.setShid(dataobj.optString("sh_order_id"));
                                // Log.d("imgggg","imgs"+"http://haahoo.creopedia.com/media/" + split);
                                //playerModel.setImgURL(dataobj.getString("imgURL"));

                                dataModelArrayList.add(playerModel);

                            }

                            setupRecycler();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(Orders.this,"//Error",Toast.LENGTH_LONG).show();


            }
        }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sub", filter_mode);


                return params;
            }
        };


        // Add the request to the RequestQueue.
        queue.add(stringRequest);

        //}


    }

    public void listOrders() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(Orders.this);
        String url = ApiClient.BASE_URL + "api_shop_app/list_customer_orders_page/";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject obj = new JSONObject(response);
                            dataModelArrayList = new ArrayList<>();
                            JSONArray dataArray = obj.getJSONArray("data");
                            if (dataArray.length() == 0) {
                                Toast.makeText(Orders.this, "Nothing to display", Toast.LENGTH_SHORT).show();
                            }
                            for (int i = 0; i < dataArray.length(); i++) {

                                OrderPojo playerModel = new OrderPojo();
                                JSONObject dataobj = dataArray.getJSONObject(i);
                                playerModel.setProductid(dataobj.getString("order_common_id"));
                                playerModel.setName(dataobj.optString("name"));
                                playerModel.setPrice("Order Status : "+dataobj.optString("status"));
                                playerModel.setDate("Ordered On : "+dataobj.optString("date"));
                                playerModel.setShop_name("Shop Name : "+dataobj.optString("shop_name"));
                                dataModelArrayList.add(playerModel);

                            }
                            int resId = R.anim.slide_right_to_left;
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            setupRecycler();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        queue.add(stringRequest);
    }


    private void fetchingJSON() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(Orders.this);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "order_details/view_order_all/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);

                            Log.d("response", "resp" + response);
                            dataModelArrayList = new ArrayList<>();
                            JSONArray dataArray = obj.getJSONArray("data");
//                            popupWindow.dismiss();
//                            relativeLayout.setAlpha(1.0F);
                            if (dataArray.length() == 0) {
                                Toast.makeText(Orders.this, "Nothing to display", Toast.LENGTH_SHORT).show();
                            }

                            for (int i = 0; i < dataArray.length(); i++) {

                                OrderPojo playerModel = new OrderPojo();
                                JSONObject dataobj = dataArray.getJSONObject(i);
                                String images1 = dataobj.getString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "").replace("]", "");

                                //   image.add("http://haahoo.creopedia.com/media/" + split);
                                playerModel.setProductid(dataobj.getString("order_id"));
                                ApiClient.productids.add(dataobj.getString("order_id"));
                                playerModel.setName(dataobj.getString("name"));
                                ApiClient.shids.add(dataobj.optString("sh_order_id"));
                                // ApiClient.sh_order_id = dataobj.optString("sh_order_id");
                                ApiClient.virtual_order.add(dataobj.optString("virtual"));
                                playerModel.setPrice(dataobj.getString("status"));
                                playerModel.setQty(ApiClient.BASE_URL + "media/" + split);
                                playerModel.setShid(dataobj.optString("sh_order_id"));
                                // Log.d("imgggg","imgs"+"http://haahoo.creopedia.com/media/" + split);
                                //playerModel.setImgURL(dataobj.getString("imgURL"));

                                dataModelArrayList.add(playerModel);

                            }
                            int resId = R.anim.slide_right_to_left;

//                            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, resId);
//                            recyclerView.setLayoutAnimation(animation);
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            setupRecycler();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(Orders.this,"//Error",Toast.LENGTH_LONG).show();


            }
        }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        queue.add(stringRequest);

    }

    private void recent() {

        RequestQueue queue = Volley.newRequestQueue(Orders.this);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "order_details/view_order_all/";

        // Request a string response from the provided URL.


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);

                            Log.d("response", "resp" + response);
                            dataModelArrayList = new ArrayList<>();
                            JSONArray dataArray = obj.getJSONArray("data");
                            popupWindow.dismiss();
                            relativeLayout.setAlpha(1.0F);
                            if (dataArray.length() == 0) {
                                Toast.makeText(Orders.this, "Nothing to display", Toast.LENGTH_SHORT).show();
                            }

                            for (int i = 0; i < dataArray.length(); i++) {

                                OrderPojo playerModel = new OrderPojo();
                                JSONObject dataobj = dataArray.getJSONObject(i);
                                String images1 = dataobj.getString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "").replace("]", "");
                                playerModel.setProductid(dataobj.getString("order_id"));
                                ApiClient.productids.add(dataobj.getString("order_id"));
                                ApiClient.shids.add(dataobj.optString("sh_order_id"));
                                ApiClient.virtual_order.add(dataobj.optString("virtual"));
                                playerModel.setName(dataobj.getString("name"));
                                playerModel.setPrice(dataobj.getString("status"));
                                playerModel.setQty(ApiClient.BASE_URL + "media/" + split);
                                playerModel.setShid(dataobj.optString("sh_order_id"));
                                // Log.d("imgggg","imgs"+"http://haahoo.creopedia.com/media/" + split);
                                //playerModel.setImgURL(dataobj.getString("imgURL"));

                                dataModelArrayList.add(playerModel);

                            }

                            setupRecycler();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(Orders.this,"//Error",Toast.LENGTH_LONG).show();


            }
        }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


    private void setupRecycler() {
        recyclerView.setVisibility(View.VISIBLE);
        orderAdapter = new OrderAdapter(this, dataModelArrayList);
        recyclerView.setAdapter(orderAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

    }

    @Override
    public void onBackPressed() {
        Bundle bundle = getIntent().getExtras();
        String intent_value = bundle.getString("fromactivity");
        if (intent_value.equals("payment")) {
            startActivity(new Intent(Orders.this, NewHome.class));
        }
        if (intent_value.equals("profile")) {
            super.onBackPressed();
        }
        if (intent_value.equals("home")) {
            super.onBackPressed();
        }
        if (intent_value.equals("details")) {
            startActivity(new Intent(Orders.this, NewHome.class));
        }
    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

}



