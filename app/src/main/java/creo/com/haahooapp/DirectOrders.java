package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.Adapters.DirectOrderAdapter;
import creo.com.haahooapp.Modal.DirectOrderPojo;
import creo.com.haahooapp.config.ApiClient;

public class DirectOrders extends AppCompatActivity {
    private static ProgressDialog mProgressDialog;
    ArrayList<DirectOrderPojo> dataModelArrayList;
    private RecyclerView recyclerView;
    TextView amount;
    String token = null;
    private DirectOrderAdapter orderAdapter;
    Activity  activity = this;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direct_orders);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        recyclerView = findViewById(R.id.recyclerView);
        back = findViewById(R.id.back);
        amount = findViewById(R.id.amount);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(DirectOrders.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(DirectOrders.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(DirectOrders.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(DirectOrders.this, MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(DirectOrders.this, AllStories.class));
                                break;

                        }
                        return true;
                    }
                });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DirectOrders.this,NewHome.class));
            }
        });
        fetchingJSON();
//        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
//            @Override
//            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                Toast.makeText(Orders.this,"orderid"+ApiClient.productids.get(position),Toast.LENGTH_SHORT).show();
//
//                Intent intent = new Intent(Orders.this,Orderdetails.class);
//                intent.putExtra("order_id",ApiClient.productids.get(position));
//                startActivity(intent);
//            }
//        });

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));

    }
    private void fetchingJSON() {

        RequestQueue queue = Volley.newRequestQueue(DirectOrders.this);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"direct_selling/direct_order_view/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("ssssd", "resp" + obj);
                            amount.setText(obj.optString("total"));
                            dataModelArrayList = new ArrayList<>();
                            JSONArray dataArray  = obj.getJSONArray("data");

                            if(dataArray.length() == 0){
                                Toast.makeText(DirectOrders.this,"Nothing to display",Toast.LENGTH_SHORT).show();
                            }

                            for (int i = 0; i < dataArray.length(); i++) {

                                DirectOrderPojo playerModel = new DirectOrderPojo();
                                JSONObject dataobj = dataArray.getJSONObject(i);
                                String images1 = dataobj.getString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "");
                                playerModel.setProductname(dataobj.optString("name"));
                                ApiClient.productids.add(dataobj.optString("id"));
                                playerModel.setName(dataobj.optString("customer_name"));
                                playerModel.setPrice(dataobj.optString("earnings"));
                                playerModel.setStatus(dataobj.optString("status"));
                                playerModel.setImg(ApiClient.BASE_URL+"media/" + split);

                                dataModelArrayList.add(playerModel);

                            }

                            setupRecycler();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Toast.makeText(DirectOrders.this,"//Error",Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


    private void setupRecycler(){

        orderAdapter = new DirectOrderAdapter(this,dataModelArrayList);
        recyclerView.setAdapter(orderAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(DirectOrders.this,NewHome.class));
    }
}
