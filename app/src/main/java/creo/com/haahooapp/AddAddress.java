package creo.com.haahooapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;
import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.example.applemacbookair.locationtracker.LocationTracker;
import com.example.applemacbookair.locationtracker.OnLocationChanged;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputEditText;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Implementation.AddaddressPresenterImpl;
import creo.com.haahooapp.Modal.AddaddressPojo;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.AddaddressCallback;
import creo.com.haahooapp.interfaces.AddaddressPresenter;
import creo.com.haahooapp.interfaces.AddaddressView;

public class AddAddress extends AppCompatActivity implements AddaddressCallback,AddaddressView {

  TextInputEditText name,address,phone_no,landmark,city,state,pincode,area;
  TextView save;
  AddaddressPresenter addaddressPresenter;
  String device_id = null;
  ImageView imageView;
  String addressval="null";
  Context context= this;
  Spinner spinner;
  Activity activity = this;
  ArrayList<String> areas = new ArrayList<String>();
  private FusedLocationProviderClient mFusedLocationClient;
  private int locationRequestCode = 1000;
  private LocationRequest locationRequest;
  private LocationCallback locationCallback;
  private LocationManager locationManager;
  private boolean isContinue = false;
  private LocationSettingsRequest mLocationSettingsRequest;
  private SettingsClient mSettingsClient;
  String latitude = "null";
  String longitude = "null";
  CardView mylocation;
  ProgressDialog progressDialog;
  String current_location_stat = "0";
  String lat = "0";
  String log = "0";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
    getSupportActionBar().hide();
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_address);
    Window window = activity.getWindow();
    areas.add("Choose Area");
    mylocation = findViewById(R.id.mylocation);
    progressDialog = new ProgressDialog(this,R.style.MyAlertDialogStyle);
    mylocation.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        req();
        Places.initialize(getApplicationContext(), "AIzaSyB66smBcn9YjDIWCdmhym66YJN7IGKdJII");
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
          ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                  locationRequestCode);
        }

        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

          ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Fetching your Location Details.. Please Wait...");
        progressDialog.show();
//        req();
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
        final LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        current_location_stat = "1";
        // This is the Best And IMPORTANT part
        final Looper looper = null;
        locationManager.requestSingleUpdate(criteria, mLocationListener, looper);
      }
    });
    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    locationRequest = LocationRequest.create();
    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    locationRequest.setInterval(10 * 1000); // 10 seconds
    locationRequest.setFastestInterval(5 * 1000); // 5 seconds
    locationCallback = new LocationCallback() {
      @Override
      public void onLocationResult(LocationResult locationResult) {
        List<Location> locationList = locationResult.getLocations();
        if (locationList.size() > 0) {
          Location location = locationList.get(locationList.size() - 1);
          //WORKING PORTION
          latitude = String.valueOf(location.getLatitude());
          longitude = String.valueOf(location.getLongitude());
          Log.d("GOTIT","hgdgf"+location.getLatitude());
          progressDialog.dismiss();
          locationManager.removeUpdates(mLocationListener);
          locationManager.removeUpdates(mLocationListener);
        }
      }
    };

//    if (checkLocationPermission()) {
//      locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 400,
//              1, mLocationListener);
//    }

// clear FLAG_TRANSLUCENT_STATUS flag:
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

    BottomNavigationView bottomNavigationView = (BottomNavigationView)
            findViewById(R.id.bottom_navigation);

    bottomNavigationView.setOnNavigationItemSelectedListener(
            new BottomNavigationView.OnNavigationItemSelectedListener() {
              @Override
              public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                  case R.id.action_home:

                    startActivity(new Intent(context, NewHome.class));
                    break;

                  case R.id.action_search:

                    startActivity(new Intent(context, Search.class));
                    break;

                  case R.id.action_account:

                    startActivity(new Intent(context, SettingsActivity.class));
                    break;

                  case R.id.stories:

                   // startActivity(new Intent(context, AllStories.class));
                    Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                    break;

                  case R.id.shop:

//                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                    String membershipval = (pref.getString("membership", "null"));
//                    if (membershipval.equals("1")) {
//                      startActivity(new Intent(context, MyShopNew.class));
//                    }
//                    if (!(membershipval.equals("1"))) {
//                      CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                              .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                              .setTitle("Become Our Premium Member")
//                              .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                              .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                startActivity(new Intent(context, MemberShipActivity.class));
//                              }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                dialog.dismiss();
//                              });
//// Show the alert
//                      builder.show();
//                    }
                    Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                    break;

                }
                return true;
              }
            });
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
    window.setStatusBarColor(activity.getResources().getColor(R.color.black));
    spinner = findViewById(R.id.spinner);
    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, areas);
    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinner.setAdapter(arrayAdapter);
//    spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//      @Override
//      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Toast.makeText(AddAddress.this,"selected"+areas.get(position),Toast.LENGTH_SHORT).show();
//      }
//    });

    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (areas.get(position).equals("Choose Area")){
          area.setText("");
        }
        if(!(areas.get(position).equals("Choose Area"))) {
          area.setText(areas.get(position));
        }
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    name=findViewById(R.id.name);
    name.setText(ApiClient.username);
    address=findViewById(R.id.address);
    phone_no=findViewById(R.id.phone);
    landmark=findViewById(R.id.landmark);
    city=findViewById(R.id.city);
    state=findViewById(R.id.state);
    pincode=findViewById(R.id.pincode);
    save=findViewById(R.id.save);
    area = findViewById(R.id.area);
    imageView=findViewById(R.id.imageView3);

    device_id =  Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    addaddressPresenter=new AddaddressPresenterImpl(this);

    imageView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });

//    pincode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//      @Override
//      public void onFocusChange(View v, boolean hasFocus) {
//        areas.clear();
//        getLocationDetails();
//      }
//    });
//   pincode.setOnTouchListener(new View.OnTouchListener() {
//     @Override
//     public boolean onTouch(View v, MotionEvent event) {
//       areas.clear();
//
//       return false;
//     }
//   });

    save.setText("Save & Continue");

    save.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        if (name.getText().toString().length() < 3){
          name.setError("Enter a valid name");
        }
        if(phone_no.getText().toString().length() < 10){
          phone_no.setError("Enter a valid number");
        }
        if(pincode.getText().toString().length() <6 ){
          pincode.setError("Pincode must be of six digits");
        }
        if(name.getText().toString().length() == 0){
          name.setError("Name cannot be empty");
        }
        if(address.getText().toString().length() == 0 ){
          address.setError("Address cannot be empty");
        }
        if (city.getText().toString().length() == 0){
          city.setError("City cannot be empty");
        }
        if(state.getText().toString().length() == 0){
          state.setError("State name cannot be empty");
        }
        if(name.getText().toString().length()>=3 && phone_no.getText().toString().length() == 10 && pincode.getText().toString().length() == 6
        && address.getText().toString().length()!=0 && city.getText().toString().length() !=0 && state.getText().toString().length() !=0)
        {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        final AddaddressPojo addaddressPojo = new AddaddressPojo(name.getText().toString(), address.getText().toString(), phone_no.getText().toString(),pincode.getText().toString(),area.getText().toString(),landmark.getText().toString(),city.getText().toString(),state.getText().toString(), device_id,current_location_stat,lat,log);
        addaddressPresenter.addaddress(addaddressPojo,"Token "+""+token);
        //getLocationFromAddress(context,address.getText().toString()+","+area.getText().toString()+","+city.getText().toString()+","+landmark.getText().toString()+","+pincode.getText().toString());
        }
      }
    });
  }

  private final LocationListener mLocationListener = new LocationListener() {
    @Override
    public void onLocationChanged(final Location location) {
      //your code here
      //WORKING PORTION
      Log.d("gcghcgh","HJBHJBHJB"+location.getLatitude()+"\n"+location.getLongitude());
      final List<Address>[] addresses = new List[]{new ArrayList<>()};
      Runnable runnable = new Runnable() {
        @Override
        public void run() {
          Geocoder geocoder = new Geocoder(context);
          try {
            addresses[0] = geocoder.getFromLocation(location.getLatitude(),location.getLongitude(),1);
            runOnUiThread(new Runnable() {
              @Override
              public void run() {
                try {
                  address.setText(addresses[0].get(0).getAddressLine(0));
                  pincode.setText(addresses[0].get(0).getPostalCode());
                  area.setText(addresses[0].get(0).getSubLocality());
                  city.setText(addresses[0].get(0).getLocality());
                  state.setText(addresses[0].get(0).getAdminArea());
                  lat = String.valueOf(location.getLatitude());
                  log = String.valueOf(location.getLongitude());
                }catch (Exception e){
                  e.printStackTrace();
                }
              }
            });
            progressDialog.dismiss();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      };
      Thread thread = new Thread(runnable);
      thread.start();
      locationManager.removeUpdates(mLocationListener);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
  };

  public boolean checkLocationPermission() {
    String permission = "android.permission.ACCESS_FINE_LOCATION";
    int res = this.checkCallingOrSelfPermission(permission);
    return (res == PackageManager.PERMISSION_GRANTED);
  }

  @SuppressLint("MissingPermission")
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    switch (requestCode) {
      case 1000: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

          if (isContinue) {
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
          } else {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(AddAddress.this, new OnSuccessListener<Location>() {
              @Override
              public void onSuccess(Location location) {
                if (location != null) {
                  //Toast.makeText(context, "JKBF" + location.getLongitude(), Toast.LENGTH_SHORT).show();
                } else {
                  mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                }
              }
            });
          }
        } else {
          Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
        break;
      }
    }
  }

  public void req() {
    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
    builder.addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY));
    builder.setAlwaysShow(true);
    mLocationSettingsRequest = builder.build();

    mSettingsClient = LocationServices.getSettingsClient(AddAddress.this);

    mSettingsClient
            .checkLocationSettings(mLocationSettingsRequest)
            .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
              @Override
              public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                // Success Perform Task Here
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (checkLocationPermission()) {
//                            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                  // Log.d("hereee","jgdgfdg");
                  if (ContextCompat.checkSelfPermission(AddAddress.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(AddAddress.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                  }
                  locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                  locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
                          0, mLocationListener);

                }

              }

            })
            .addOnFailureListener(new OnFailureListener() {
              @Override
              public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                  case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                      ResolvableApiException rae = (ResolvableApiException) e;
                      rae.startResolutionForResult(AddAddress.this, 1000);
                    } catch (IntentSender.SendIntentException sie) {
                      Log.e("GPS", "Unable to execute request.");
                    }
                    break;
                  case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Log.e("GPS", "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                }
              }
            })
            .addOnCanceledListener(new OnCanceledListener() {
              @Override
              public void onCanceled() {
                Log.e("GPS", "checkLocationSettings -> onCanceled");
              }
            });
  }

  public LatLng getLocationFromAddress(Context context,String strAddress) {

    Geocoder coder = new Geocoder(context);
    List<Address> address;
    LatLng p1 = null;

    try {
      // May throw an IOException
      address = coder.getFromLocationName(strAddress, 5);
      if (address == null) {
        return null;
      }
      Address location = address.get(0);
      p1 = new LatLng(location.getLatitude(), location.getLongitude() );
      Log.d("HGVBHVBV","hgVGHCHCGHC"+p1);

    } catch (IOException ex) {

      ex.printStackTrace();
    }

    return p1;
  }

  @Override
  public void onVerifySuccess(String data) {

  }

  @Override
  public void onVerifyFailed(String msg) {

  }

  @Override
  public void viewAddress(JSONArray data) {

  }

  @Override
  public void viewFailed(String data) {

  }

  @Override
  public void onSuccess(String response) {

    if(!(response.equals("null"))) {
      Toast.makeText(AddAddress.this, "Successfully Added", Toast.LENGTH_LONG).show();
      startActivity(new Intent(AddAddress.this,ChooseAddress.class));

//            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//            SharedPreferences.Editor editor = pref.edit();
//            editor.putString("token", response);
//            editor.commit();
      // startActivity(new Intent(AddAddress.this, SetPin.class));
    }
    if (response.equals("null")) {
      Toast.makeText(AddAddress.this, "Failed to add", Toast.LENGTH_LONG).show();
    }

  }

  @Override
  public void onFailed(String response) {

  }

  @Override
  public void addressview(JSONArray data) {

  }

  @Override
  public void addressfail(String data) {

  }

  @Override
  public void noInternetConnection() {

  }

  public void getLocationDetails(){

    StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://form-api.com/api/geo/country/zip?key=siKuAUf92Ooty9DdaCNU&country=IN&zipcode="+pincode.getText().toString(),
            new Response.Listener<String>() {
              @Override
              public void onResponse(String response) {

                try {
                  areas.clear();
                  JSONObject jsonObject = new JSONObject(response);
                  JSONObject results = jsonObject.getJSONObject("result");
                  String state1 = results.optString("state");
                  String city1 = results.optString("city");
                  String level4area = results.optString("level4area");
                  state.setText(state1);
                  city.setText(city1);
//                  area.setText(level4area);
                  String[] seperated = level4area.split(",");

//                  ArrayList<String> places = new ArrayList<>();
//                  for (int i=0;i<seperated.length;i++){
//                    places.add(seperated[i].replace("[",""));
//                  }
//                  String split = seperated[0].replace("[", "");

                  List<String>list = new ArrayList<String>(Arrays.asList(seperated));
                  ArrayList<String>places = new ArrayList<String>();
                  areas.add("Choose Area");
                  for (int i =0 ;i<list.size();i++){
                    areas.add(list.get(i).replace("[","").replace("\"", "").replace("]",""));
                  }


//                  Log.d("results","result222"+places.size()+places.get(0));

                }catch (JSONException e){
                  e.printStackTrace();
                }

              }
            },
            new Response.ErrorListener() {
              @Override
              public void onErrorResponse(VolleyError error) {
               // Toast.makeText(AddAddress.this,error.toString(),Toast.LENGTH_LONG).show();
              }
            }){
      @Override
      protected Map<String,String> getParams(){
        Map<String,String> params = new HashMap<String, String>();
        //params.put("search_key",searchView.getQuery().toString());
        return params;
      }

    };

    RequestQueue requestQueue = Volley.newRequestQueue(context);
    requestQueue.add(stringRequest);

  }

}