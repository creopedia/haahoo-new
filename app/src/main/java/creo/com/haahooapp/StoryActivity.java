package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashMap;
import creo.com.haahooapp.config.ApiClient;
import jp.shts.android.storiesprogressview.StoriesProgressView;

public class StoryActivity extends AppCompatActivity implements StoriesProgressView.StoriesListener  {

    private static final int PROGRESS_COUNT = 0;
    MediaController mediaController;
    View reverse;
    private StoriesProgressView storiesProgressView;
    private ImageView image;
    Uri uri;
    VideoView videoView;
    TextView user_story;
    private int counter = 0;
    Context context = this;


    long pressTime = 0L;
    long limit = 500L;

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);
        user_story = findViewById(R.id.user_story);
        Log.d("storyacti","vghghf"+ApiClient.dataModelArrayList.size());
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );

        storiesProgressView = (StoriesProgressView) findViewById(R.id.stories);
        storiesProgressView.setStoriesCount(ApiClient.dataModelArrayList.size());
        storiesProgressView.setStoryDuration(10000L);

        // or
        // storiesProgressView.setStoriesCountWithDurations(durations);
        storiesProgressView.setStoriesListener(this);
        storiesProgressView.startStories();
        videoView = findViewById(R.id.video);
        mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
      //  videoView.setMediaController(mediaController);
//        videoView.setVideoURI();
        reverse = findViewById(R.id.reverse);
        image = (ImageView) findViewById(R.id.image);
        reverse.setOnTouchListener(onTouchListener);
        View skip = findViewById(R.id.skip);
       // videoView.setVisibility(View.VISIBLE);
       // uri = Uri.parse("https://images.all-free-download.com/footage_preview/mp4/ripple_waves_water_liquid_clean_826.mp4");
       // videoView.setVideoURI(uri);
        if (ApiClient.dataModelArrayList.get(counter).getImage().contains(".jpeg")|| ApiClient.dataModelArrayList.get(counter).getImage().contains(".jpg")) {
            storiesProgressView.setStoryDuration(10000L);
            videoView.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
            Glide.with(context).load(ApiClient.dataModelArrayList.get(counter).getImage()).into(image);
        }
        if (ApiClient.dataModelArrayList.get(counter).getImage().contains(".mp4")){
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(ApiClient.dataModelArrayList.get(counter).getImage(), new HashMap<String, String>());
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long timeInMillisec = Long.parseLong(time);
            retriever.release();
            String duration=convertMillieToHMmSs(timeInMillisec); //use this duration
            Log.d("vidduration","duration"+timeInMillisec);
            storiesProgressView.setStoryDuration(timeInMillisec);
            image.setVisibility(View.GONE);
            uri = Uri.parse(ApiClient.dataModelArrayList.get(counter).getImage());
            videoView.setVisibility(View.VISIBLE);
            videoView.setVideoURI(uri);
            videoView.start();
        }
//        image.setImageResource(imgss);

        // bind reverse view

        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.reverse();
            }
        });

        // bind skip view

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.skip();
            }
        });
        skip.setOnTouchListener(onTouchListener);
    }
    @Override
    public void onNext() {
        String value = ApiClient.dataModelArrayList.get(++counter).getImage();
        Log.d("hgfdsghj","valueeee"+value);
if (value.contains(".jpeg")|| value.contains(".jpg")) {
    storiesProgressView.setStoryDuration(10000L);
    Glide.with(context).load(value).into(image);
    videoView.setVisibility(View.GONE);
    image.setVisibility(View.VISIBLE);
}
if (value.contains(".mp4")){

//    FFmpegMediaMetadataRetriever mFFmpegMediaMetadataRetriever = new FFmpegMediaMetadataRetriever();
//    mFFmpegMediaMetadataRetriever .setDataSource(value);
    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
    retriever.setDataSource(value, new HashMap<String, String>());
    String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
    long timeInMillisec = Long.parseLong(time);
    retriever.release();
    String duration=convertMillieToHMmSs(timeInMillisec); //use this duration
    Log.d("vidduration","duration"+timeInMillisec);
    storiesProgressView.setStoryDuration(timeInMillisec);
    image.setVisibility(View.GONE);
    uri = Uri.parse(value);
    videoView.setVisibility(View.VISIBLE);
    videoView.setVideoURI(uri);
    videoView.start();
}
       // image.setImageResource(resources[++counter]);
    }

    @Override
    public void onPrev() {
        if ((counter - 1) < 0) return;
       // image.setImageResource(resources[--counter]);
        String value = ApiClient.dataModelArrayList.get(--counter).getImage();
        Log.d("hgfdsghj","valueeee"+value);
        if (value.contains(".jpeg")|| value.contains(".jpg")) {
            storiesProgressView.setStoryDuration(10000L);
            Glide.with(context).load(value).into(image);
            videoView.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
        }
        if (value.contains(".mp4")){

            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(value, new HashMap<String, String>());
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long timeInMillisec = Long.parseLong(time);
            retriever.release();
            String duration=convertMillieToHMmSs(timeInMillisec); //use this duration
//            Log.d("vidduration","duration"+timeInMillisec);
            storiesProgressView.setStoryDuration(timeInMillisec);
            image.setVisibility(View.GONE);
            uri = Uri.parse(value);
            videoView.setVisibility(View.VISIBLE);
            videoView.setVideoURI(uri);
            videoView.start();
        }
    }
    @Override
    public void onComplete() {
        //send to home activity coz in whatsapp also after the last video goes back to listview

   super.onBackPressed();


    }
    @Override
    protected void onDestroy() {
        // Very important !
        storiesProgressView.destroy();
        super.onDestroy();
    }

    public static String convertMillieToHMmSs(long millie) {
        long seconds = (millie / 1000);
        long second = seconds % 60;
        long minute = (seconds / 60) % 60;
        long hour = (seconds / (60 * 60)) % 24;
        String result = "";
        if (hour > 0) {
            return String.format("%02d:%02d:%02d", hour, minute, second);
        }
        else {
            return String.format("%02d:%02d" , minute, second);
        }

    }
}