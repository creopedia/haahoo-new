package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import creo.com.haahooapp.config.ApiClient;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class SocialMakePayment extends AppCompatActivity implements PaymentResultListener {
    Activity activity = this;
    String payment_mode = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_make_payment);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Haahoo");
            options.put("description", "Social Entrepreneurship");
            //You can omit the image option to fetch the image from dashboard
            options.put("currency", "INR");
           // String price[] = ApiClient.price.split("₹ ");
            options.put("amount", "100000");
//            JSONObject preFill = new JSONObject();
//            preFill.put("email", ApiClient.email);
//            preFill.put("contact", ApiClient.mobile);
//            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment", Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }

    }

    @Override
    public void onPaymentSuccess(String s) {
      payment_mode = s;
      submmitDetails(s);
        // startActivity(new Intent(SocialMakePayment.this,NewHome.class));
    }

    public void submmitDetails(final String payment_id){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"events/club_place_order/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("mkepayment","payrazp"+response);
                            //payment_mode = payment_id;
                            Toast.makeText(SocialMakePayment.this,"You have successfully become a Social Entreprenuership Member",Toast.LENGTH_SHORT).show();
                            // startActivity(new Intent(MakePayment.this,OrderSuccess.class));

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       // Toast.makeText(S.this,token,Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("club_id","1");
               // params.put("address_id",ApiClient.address);
                params.put("payment_method","ONLINE");
                params.put("payment_status","1");
                params.put("payment_id",payment_mode);
                params.put("payment_amount","100000");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onPaymentError(int i, String s) {

        Toast.makeText(SocialMakePayment.this,"Failed making payment",Toast.LENGTH_SHORT).show();
        startActivity(new Intent(SocialMakePayment.this,SocialEntrepreneurship.class));

    }
}
