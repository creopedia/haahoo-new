package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.Implementation.ForgotOtpPresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.SMSReceiver;
import creo.com.haahooapp.interfaces.ForgotOtpCallback;
import creo.com.haahooapp.interfaces.ForgotOtpPresenter;
import creo.com.haahooapp.interfaces.ForgotOtpView;
import creo.com.haahooapp.interfaces.NumberVerifyPresenter;

public class ForgotOTP extends AppCompatActivity implements SMSReceiver.OTPReceiveListener, ForgotOtpView, ForgotOtpCallback {

    TextView verify, text;
    TextInputEditText otp;
    ForgotOtpPresenter forgotOtpPresenter;
    String phone_no = null;
    Activity activity = this;
    private SMSReceiver smsReceiver;
    CardView resend;
    Context context = this;
    ProgressDialog progressDialog;
    NumberVerifyPresenter numberVerify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_otp);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        resend = findViewById(R.id.resend);
        text = findViewById(R.id.text);
// finally change the color
        progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        verify = findViewById(R.id.verifyotp);
        otp = findViewById(R.id.otp);
        startSMSListener();
        Bundle bundle = getIntent().getExtras();
        phone_no = bundle.getString("phone_no");
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialogWithTitle("Please wait while we are sending the OTP");
//                forgotOtpPresenter.forgotOtp(phone_no);
                forgotresendOTP();
            }
        });
        forgotOtpPresenter = new ForgotOtpPresenterImpl(this);
        // click listener
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotOtpPresenter.verifyOtp(phone_no, otp.getText().toString());
            }
        });

    }

    private void startSMSListener() {
        try {
            smsReceiver = new SMSReceiver();
            smsReceiver.setOTPListener(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            this.registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(this);

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // API successfully started
//                    showToast("started" );
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Fail to start API
//                    showToast("failed");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void forgotresendOTP() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/forget_password/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            hideProgressDialogWithTitle();
                          resend.setVisibility(View.GONE);
                          new CountDownTimer(60000, 1) {

                            public void onTick(long millisUntilFinished) {
                              text.setVisibility(View.VISIBLE);
                              long remainedSecs = millisUntilFinished / 1000;
                              text.setText("You can resend otp after " +(remainedSecs)+" seconds");
                            }

                            public void onFinish() {
                              text.setVisibility(View.GONE);
                            }
                          }.start();
                          resend.postDelayed(new Runnable() {
                            public void run() {
                              resend.setVisibility(View.VISIBLE);
                            }
                          }, 60000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(MainUI.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("phone_no", phone_no);
                return params;
            }

            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onVerifySuccessOtp(String data) {
        //hideProgressDialogWithTitle();
    }

    @Override
    public void onVerifyFailedOtp(String msg) {
        Toast.makeText(ForgotOTP.this, "Invalid OTP", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onSuccessOtp(String response) {
        Toast.makeText(ForgotOTP.this, "Verified", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ForgotOTP.this, ForgotPassword.class);
        intent.putExtra("phone_no", phone_no);
        startActivity(intent);
    }

    @Override
    public void onFailedOtp(String response) {
        Toast.makeText(ForgotOTP.this, "Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onOTPReceived(String otp1) {
        otp.setText(otp1);
    }

    @Override
    public void onOTPTimeOut() {

    }

    @Override
    public void onOTPReceivedError(String error) {

    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }


}
