package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;

public class SkillQ extends AppCompatActivity {
    Activity activity = this;
    WebView webView;
    ProgressDialog progressDialog;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skill_q);
        Window window = activity.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        //showProgressDialogWithTitle("Taking you to SkillQ..");
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        webView = findViewById(R.id.webview);
        Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://skillqu.creopedia.com/home?token="+token));
        startActivity(browserIntent);

//        WebSettings webSettings = webView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
//        webView.setWebViewClient(new WebViewClient());
//        webSettings.setLoadWithOverviewMode(true);
//        webSettings.setUseWideViewPort(true);
//        //webSettings.setJavaScriptEnabled(true);
//
//        webSettings.setAppCacheEnabled(false);
//        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
//        webSettings.setDatabaseEnabled(false);
//        webSettings.setDomStorageEnabled(false);
//        webSettings.setGeolocationEnabled(false);
//        webSettings.setSaveFormData(false);
        //webView.setWebChromeClient(new WebChromeClient());
       // webView.setWebViewClient(new WebViewClient());
//        webView.setWebViewClient(new WebViewClient(){
//            public void onPageFinished(WebView view, String url) {
//                // do your stuff here
//                hideProgressDialogWithTitle();
//            }
//        });
       // webView.loadUrl("http://skillqu.creopedia.com");
    }
    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
          super.onBackPressed();
        }

    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(true);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }


    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();

    }
}
