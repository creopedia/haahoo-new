package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import creo.com.haahoo.CombinedCartActivity;
import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahoo.ViewCouponActivity;
import creo.com.haahooapp.Adapters.ConnectedShopsAdapter;
import creo.com.haahooapp.Adapters.EventAdapter;
import creo.com.haahooapp.Adapters.NewShopByCategory;
import creo.com.haahooapp.Adapters.ShopByCatgegoryAdapter;
import creo.com.haahooapp.Adapters.SliderAdapterExample;
import creo.com.haahooapp.Adapters.StoryAdapter;
import creo.com.haahooapp.Implementation.EventViewPresenterImpl;
import creo.com.haahooapp.Implementation.PinVerifyPresenterImpl;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.Modal.ConnectedShops;
import creo.com.haahooapp.Modal.ProductPojo;
import creo.com.haahooapp.Modal.StoryPojo;
import creo.com.haahooapp.camera.MainActivity;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.ItemClickSupport;
import creo.com.haahooapp.interfaces.EventCallBack;
import creo.com.haahooapp.interfaces.EventView;
import creo.com.haahooapp.interfaces.EventViewPresenter;
import creo.com.haahooapp.interfaces.PinVerifyCallback;
import creo.com.haahooapp.interfaces.PinVerifyPresenter;
import creo.com.haahooapp.interfaces.PinVerifyView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.github.juanlabrador.badgecounter.BadgeCounter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Navigation extends AppCompatActivity implements YouTubePlayer.OnInitializedListener, EventView, EventCallBack, PinVerifyCallback, PinVerifyView, NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    ActionBar actionBar;
    FloatingActionButton floatingActionButton;
    private int mNotificationCounter = 0;
    String link = null;
    ImageView imageView;
    String total_count = String.valueOf(ApiClient.count);
    RelativeLayout jobs, earnings;
    LinearLayout downloads, winners;
    JSONArray jsonObject = new JSONArray();
    RelativeLayout storyrel, cardrel, firstrel;
    public Menu globalMenuItem;
    private List<ProductPojo> pojo = new ArrayList<>();
    ArrayList<String> event_name = new ArrayList<>();
    ArrayList<String> event_description = new ArrayList<>();
    ArrayList<String> event_image = new ArrayList<>();
    PinVerifyPresenter pinVerifyPresenter;
    TextView name, email, phone, referralid, more_events, active;
    ImageView saree;
    ImageView socialimage, familyimage, knowledgeimage, skillqimage, styleqimage, moreimage;
    CardView first, second, third;
    ArrayList<String> names = new ArrayList<>();
    ViewPager viewPager;
    ArrayList<String> images = new ArrayList<>();
    RelativeLayout relativeLayout, thirdrel;
    ProgressDialog progressDialog;
    boolean doubleBackToExitPressedOnce = false;
    Context context = this;
    RecyclerView connectedshopos;
    Activity activity = this;
    LinearLayout linearLayout, fam, youlead;
    androidx.appcompat.widget.Toolbar toolbar;
    TextView textView;
    RecyclerView gridView;
    RecyclerView recyclerView;
    CardView fab;
    JSONArray jsonArray2 = new JSONArray();
    EventViewPresenter eventViewPresenter;
    ImageView firstImage, secondImage, thirdImage;
    CircularImageView couponImg;
    ArrayList<String> ids = new ArrayList<>();
    ArrayList<String> event_ids = new ArrayList<>();
    TextView firsttext, secondtext, thirdtext, out_msg, retry, msg, btn, namestory;
    ScrollView scrollView;
    NavigationView navigationView;
    String token = null;
    TextView cartcount, wishlistcount;
    RecyclerView recyclerView2;
    ArrayList<StoryPojo> dataModelArrayList = new ArrayList<>();
    ArrayList<StoryPojo> dataModelArrayList1 = new ArrayList<>();
    StoryAdapter storyAdapter;
    RelativeLayout flipkart, amazon, skillq, secondrel, fourthrel;
    ConnectedShopsAdapter connectedShopsAdapter;
    ArrayList<ConnectedShops> connectedShops = new ArrayList<>();
    LinearLayout coupon;
    String membershipval;
    ArrayList<Category> categories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Window window = activity.getWindow();
        recyclerView2 = findViewById(R.id.recyclerView2);
        imageView = findViewById(R.id.image);
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        storyrel = findViewById(R.id.storyrel);
        connectedshopos = findViewById(R.id.connectedshopos);
        cardrel = findViewById(R.id.cardrel);
        namestory = findViewById(R.id.namestory);
        firstrel = findViewById(R.id.firstrel);
        thirdrel = findViewById(R.id.thirdrel);
        fourthrel = findViewById(R.id.fourthrel);
        gridView = findViewById(R.id.gridview);
        getcategories();
        /*couponImg = findViewById(R.id.coupon1image);
        Glide.with(context).load(R.drawable.logo).into(couponImg);
        coupon = findViewById(R.id.coupon1);
        coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, CouponDetailsActivity.class));
            }
        });*/
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        membershipval = (pref.getString("membership", "null"));
        getConnectedShops();
        cartCount();
        fourthrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AllStories.class));
            }
        });
        secondrel = findViewById(R.id.secondrel);
        secondrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (membershipval.equals("1")) {
                    startActivity(new Intent(context, ResellingProducts.class));
                } else if (membershipval.equals("0") || membershipval.equals("null")) {
                    Toast.makeText(context, "You need to be a member to view products in Baazaar", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context, MemberShipActivity.class);
                    startActivity(intent);
                }
            }
        });
        thirdrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MyShopNew.class));
            }
        });
        firstrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ShopByCategory.class));
               // Toast.makeText(context, "Currently Unavailable", Toast.LENGTH_LONG).show();
            }
        });
        //StoryPojo storyPojo = new StoryPojo();
        getStories();
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        SliderView sliderView = findViewById(R.id.imageSlider);
        SliderAdapterExample adapter = new SliderAdapterExample(this);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.BLACK);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(10); //set scroll delay in seconds :
        sliderView.startAutoCycle();
        socialimage = findViewById(R.id.socialimage);
        familyimage = findViewById(R.id.familyimage);
        more_events = findViewById(R.id.moreevents);
        more_events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Navigation.this, MoreEvents.class));
            }
        });
        knowledgeimage = findViewById(R.id.knowledgeimage);
        Glide.with(context).load(ApiClient.BASE_URL + "media/files/events_add/extra_pic/social_logo.png").into(socialimage);
        Glide.with(context).load(ApiClient.BASE_URL + "media/files/events_add/extra_pic/doctor_logo.png").into(familyimage);
        Glide.with(context).load(ApiClient.BASE_URL + "media/files/events_add/extra_pic/knowledge_logo.png").into(knowledgeimage);
        skillqimage = findViewById(R.id.skillqimage);
        styleqimage = findViewById(R.id.styleqimage);
        moreimage = findViewById(R.id.moreimage);
        Glide.with(context).load("https://drlee.creopedia.com/admin/media/general/drlee_logo1.png").into(skillqimage);
        Glide.with(context).load("https://lh3.googleusercontent.com/-mSpe3pHTfkA/XsrsvTKJwUI/AAAAAAAAAo4/clAR3gvx3agO0NsP9WC8TB7yD7SiGYncACK8BGAsYHg/s0/2020-05-24.jpg").into(styleqimage);
        Glide.with(context).load(ApiClient.BASE_URL + "media/files/events_add/extra_pic/more_img.png").into(moreimage);
        floatingActionButton = findViewById(R.id.fabd);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(context, MainActivity.class));

                AlertDialog.Builder pictureDialog = new AlertDialog.Builder(context);
                pictureDialog.setTitle("Choose Action");
                String[] pictureDialogItems = {
                        "Upload Image As Story",
                        "Upload Video As Story"
                };
                pictureDialog.setItems(pictureDialogItems,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        startActivity(new Intent(context, UploadImageStory.class));
                                        break;
                                    case 1:
                                        startActivity(new Intent(context, MainActivity.class));
                                        break;

                                }
                            }
                        });
                pictureDialog.show();

//                startActivity(new Intent(Navigation.this,ChatBot.class));

            }
        });
        jobs = findViewById(R.id.jobs);
        linearLayout = findViewById(R.id.social2);
        fam = findViewById(R.id.fam);
        youlead = findViewById(R.id.youlead);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(Navigation.this,"Coming Soon..",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Navigation.this, MyShopNew.class));
            }
        });
        youlead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Navigation.this, YouLead.class));
            }
        });
        fam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Navigation.this, Healtho.class));
            }
        });
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Navigation.this, SocialEntrepreneurship.class));
                //  Toast.makeText(Navigation.this,"Coming soon...",Toast.LENGTH_SHORT).show();
            }
        });
        earnings = findViewById(R.id.earnings);
        downloads = findViewById(R.id.downloads);
        earnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Navigation.this, Earnings.class));
            }
        });
        jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Navigation.this, Jobs.class));
            }
        });
        downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Navigation.this, Downloads.class));
            }
        });
        winners = findViewById(R.id.winners);
        winners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(Navigation.this,"Coming Soon..",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(context, RewardsUI.class));
            }
        });
        drawerLayout = findViewById(R.id.drawer_layout);
        flipkart = findViewById(R.id.flipkart);
        amazon = findViewById(R.id.amazon);
        skillq = findViewById(R.id.skillq);
        skillq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = context.getPackageManager();
                boolean isInstalled = isPackageInstalled("creo.com.lee", pm);
                if (isInstalled) {


                    Intent i = context.getPackageManager().getLaunchIntentForPackage("creo.com.lee");
                    context.startActivity(i);


                }
                if (!isInstalled) {
                    Toast.makeText(context, "App Coming Soon..", Toast.LENGTH_SHORT).show();
                }

            }
        });
        flipkart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Navigation.this, CouponDetailsActivity.class);
                intent.putExtra("from", "navigation");
                startActivity(intent);
            }
        });
        amazon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Navigation.this, "More apps will be added soon..", Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(Navigation.this,Amazon.class));
            }
        });
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        actionBar = getSupportActionBar();
        actionBar.setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        cartcount = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.cart));
        wishlistcount = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.wishlist));
        initializeCountDrawer();
        View view = navigationView.getHeaderView(0);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyAccount.class);
                intent.putExtra("fromactivity", "home");
                startActivity(intent);
            }
        });
        name = view.findViewById(R.id.name);
        email = view.findViewById(R.id.email);
        phone = view.findViewById(R.id.phone);
        referralid = view.findViewById(R.id.referralid);
        active = view.findViewById(R.id.active);
        token = (pref.getString("token", ""));
        scrollView = findViewById(R.id.scrollview);
        firstImage = findViewById(R.id.firstImage);
        secondImage = findViewById(R.id.secondImage);
        thirdImage = findViewById(R.id.thirdImage);
        firsttext = findViewById(R.id.firsttext);
        secondtext = findViewById(R.id.secondtext);
        thirdtext = findViewById(R.id.thirdtext);
        first = findViewById(R.id.first);
        second = findViewById(R.id.second);
        eventViewPresenter = new EventViewPresenterImpl(this);
        pinVerifyPresenter = new PinVerifyPresenterImpl(this);
        third = findViewById(R.id.third);
        recyclerView = findViewById(R.id.recyclerView);
        getProfileDetails();
        getRefrralDetails();
        boolean internet = isNetworkAvailable();
        ApiClient.ids.clear();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.profile:
                        startActivity(new Intent(Navigation.this, ProfileNew.class));
                        break;

                    case R.id.cart:
                        startActivity(new Intent(Navigation.this, CombinedCartActivity.class));
                        break;

                    case R.id.wishlist:
                        startActivity(new Intent(Navigation.this, WishList.class));
                        break;

                    case R.id.orders:
                        Intent intent = new Intent(Navigation.this, Orders.class);
                        intent.putExtra("fromactivity", "home");
                        startActivity(intent);
                        break;

                    case R.id.address:
                        startActivity(new Intent(Navigation.this, ChooseAddress.class));
                        break;
                    case R.id.memebership:
                        startActivity(new Intent(Navigation.this, MemberShipActivity.class));
                        break;
                    case R.id.coupons:
                        startActivity(new Intent(Navigation.this, ViewCouponActivity.class));
                        break;
                    case R.id.events:
                        startActivity(new Intent(Navigation.this, Events.class));
                        break;

                    case R.id.about:
                        startActivity(new Intent(Navigation.this, AboutUs.class));
                        break;

                    case R.id.sell:
                        checkRole();
                        break;

                    case R.id.shop:
                        Intent inten = new Intent(context, MyShopNew.class);
                        startActivity(inten);
                        break;

                    case R.id.sell_list:
                        checkRoleList();
                        break;

                    case R.id.withdraw:
//                        startActivity(new Intent(Navigation.this,MyAccount.class));
                        Intent intent1 = new Intent(Navigation.this, MyAccount.class);
                        intent1.putExtra("fromactivity", "home");
                        startActivity(intent1);
                        break;

                    case R.id.shop_bycategory:
                        Intent intent2 = new Intent(Navigation.this, ShopByCategory.class);
                        startActivity(intent2);
                        break;

                    case R.id.active:
                        Intent intent3 = new Intent(context, SubscriptionList.class);
                        intent3.putExtra("fromactivity", "home");
                        startActivity(intent3);
                        break;


                    case R.id.leads:
                        Intent intent4 = new Intent(context, Leads.class);
                        startActivity(intent4);
                        break;

                    case R.id.notification:
                        Intent intent5 = new Intent(context, Notifications.class);
                        startActivity(intent5);
                        break;

                }
                return true;
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.action_home:
                                startActivity(new Intent(Navigation.this, NewHome.class));
                                break;

                            case R.id.action_search:
                                startActivity(new Intent(Navigation.this, Search.class));
                                break;

                            case R.id.action_account:
                                startActivity(new Intent(Navigation.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context, MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                        }
                        return true;
                    }
                });

        if (internet == true) {
            progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
            showProgressDialogWithTitle("Loading.. Please Wait ..");
            pinVerifyPresenter.getProducts(ApiClient.BASE_URL + "product_details/product_view/");
            eventViewPresenter.getEvents();
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            first.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Navigation.this, ProductDetails.class);
                    intent.putExtra("id", ids.get(0));
                    startActivity(intent);
                }
            });
            second.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Navigation.this, ProductDetails.class);
                    intent.putExtra("id", ids.get(1));
                    startActivity(intent);
                }
            });
            third.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Navigation.this, ProductDetails.class);
                    intent.putExtra("id", ids.get(2));
                    startActivity(intent);
                }
            });
            textView = findViewById(R.id.more);
            Bundle bundle = getIntent().getExtras();
            if (bundle == null) {
                jsonSUccess(jsonObject, "next");
            }
            if (bundle != null) {
                try {
                    JSONArray jsonArray = new JSONArray(bundle.getString("results"));
                    scrollView.setVisibility(View.VISIBLE);
                    jsonArray2 = jsonArray;
                    for (int i = 0; i < 3; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        ids.add(jsonObject.getString("pk"));
                        JSONObject jsonObject1 = jsonObject.getJSONObject("fields");
                        names.add(jsonObject1.getString("name"));

                        String images1 = jsonObject1.getString("image");
                        String[] seperated = images1.split(",");
                        String split = seperated[0].replace("[", "");
                        images.add(split);
                    }
                    firsttext.setText(names.get(0));
                    secondtext.setText(names.get(1));
                    thirdtext.setText(names.get(2));
                    Glide.with(context).load(ApiClient.BASE_URL + "media/" + images.get(0)).into(firstImage);
                    Glide.with(context).load(ApiClient.BASE_URL + "media/" + images.get(1)).into(secondImage);
                    Glide.with(context).load(ApiClient.BASE_URL + "media/" + images.get(2)).into(thirdImage);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            YouTubePlayerFragment youTubePlayerFragment =
                    (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
            youTubePlayerFragment.setRetainInstance(true);

            youTubePlayerFragment.initialize("AIzaSyCKVsZFjhEAMwC0hqPvsSWnR-g_bZgU2hk",
                    new YouTubePlayer.OnInitializedListener() {
                        @Override
                        public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                            YouTubePlayer youTubePlayer, boolean b) {
                            youTubePlayer.cueVideo("XxF3MWqhz1U");
                        }

                        @Override
                        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                        }
                    });

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Navigation.this, MoreProducts.class));
                }
            });
        }
    }

    public void getcategories() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/list_shop_cat/";

        // Request a string response from the provided URL.


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {
                            Log.d("RESPPP", "REDF" + response);
                            JSONObject obj = new JSONObject(response);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                Category category = new Category();
                                category.setId(jsonObject.optString("id"));
                                category.setName(jsonObject.optString("name"));
                                String images1 = jsonObject.optString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "").replace("]", "");
                                category.setImage(ApiClient.BASE_URL + split);
                                categories.add(category);

                            }
                            gridView.setLayoutManager(new GridLayoutManager(context, 4));
                            gridView.setAdapter(new ShopByCatgegoryAdapter(context, categories));
                            gridView.setNestedScrollingEnabled(false);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //   Toast.makeText(context,"//Error",Toast.LENGTH_LONG).show();


            }
        }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };


        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    @Override
    protected void onResume() {
        super.onResume();
        cartCount();
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void initializewishlist() {

        wishlistcount.setGravity(Gravity.CENTER_VERTICAL);
        wishlistcount.setTypeface(null, Typeface.BOLD);
        wishlistcount.setTextColor(getResources().getColor(R.color.theme));
        wishlistcount.setText(ApiClient.wish_count);

    }

    public void getStories() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "stories/story_list/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            JSONArray jsonArray = jsonObject1.optJSONArray("data");
                            if (jsonArray.length() == 0) {
                                storyrel.setVisibility(View.GONE);
                            }
                            if (jsonArray.length() > 0) {
                                storyrel.setVisibility(View.VISIBLE);
                                for (int i = 0; i < jsonArray.length(); i++) {


                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    if (jsonObject.optString("mine").equals("1")) {
                                        StoryPojo storyPojo = new StoryPojo();
                                        storyPojo.setStory_id(jsonObject.optString("user_id"));
                                        storyPojo.setName(jsonObject.optString("name"));
                                        storyPojo.setImage(ApiClient.BASE_URL + "media/" + jsonObject.optString("story"));
                                        dataModelArrayList.add(storyPojo);
                                    }
                                    if (jsonObject.optString("mine").equals("0")) {
                                        StoryPojo storyPojo = new StoryPojo();
                                        storyPojo.setName("My Story");
                                        storyPojo.setStory_id(jsonObject.optString("user_id"));
                                        storyPojo.setImage(ApiClient.BASE_URL + "media/" + jsonObject.optString("story"));
                                        dataModelArrayList.add(storyPojo);

                                    }
                                }
                            }
                            storyAdapter = new StoryAdapter(context, dataModelArrayList);
                            recyclerView2.setAdapter(storyAdapter);
                            recyclerView2.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void initializeCountDrawer() {
        //Gravity property aligns the text
        cartcount.setGravity(Gravity.CENTER_VERTICAL);
        cartcount.setTypeface(null, Typeface.BOLD);
        cartcount.setTextColor(getResources().getColor(R.color.theme));
        cartcount.setText(String.valueOf(ApiClient.count));
    }

    public void checkRole() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "direct_selling/check_role/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            Log.d("obaaaj", "json" + jsonObject1);
                            String message = jsonObject1.optString("message");
                            String type = jsonObject1.optString("types");

                            if ((message.equals("Failed"))) {

                                Intent intent = new Intent(Navigation.this, RegisterAsEmployee.class);
                                intent.putExtra("status", message);
                                startActivity(intent);

                            }
                            if (message.equals("Pending")) {
                                Intent intent = new Intent(Navigation.this, RegisterAsEmployee.class);
                                intent.putExtra("status", message);
                                startActivity(intent);

                            }
                            if (message.equals("Success")) {

                                if (!(type.equals("e-distributor"))) {
                                    startActivity(new Intent(Navigation.this, DirectSell.class));
                                }
                                if (type.equals("e-distributor")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("As an e-distributor you have no access to this page")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //do things
                                                    //startActivity(new Intent(context,NewHome.class));
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }
                                //startActivity(new Intent(Navigation.this,SellMode.class));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void cartCount() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "cart_view/cart_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            total_count = jsonObject1.optString("Total count");
                            if (total_count.equals("")) {
                                total_count = "0";
                                ApiClient.count = Integer.valueOf(total_count);
                            }
                            if (!(total_count.equals(""))) {
                                mNotificationCounter = Integer.parseInt(total_count);
                                ApiClient.count = Integer.valueOf(total_count);
                                // Log.d("qwertyu", "resp" + mNotificationCounter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public void checkRoleList() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "direct_selling/check_role/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            String message = jsonObject1.optString("message");
                            String type = jsonObject1.optString("types");
                            if (message.equals("Failed")) {
                                Toast.makeText(Navigation.this, "You are not authorized to view this page", Toast.LENGTH_SHORT).show();
                            }
                            if (message.equals("Success")) {
                                if (!(type.equals("e-distributor"))) {
                                    startActivity(new Intent(Navigation.this, DirectOrders.class));
                                }
                                if (type.equals("e-distributor")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("As an e-distributor you have no access to this page")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //do things
                                                    //startActivity(new Intent(context,NewHome.class));
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void shareLongDynamicLink() {

        final String path = "https://haahooapp.page.link/?" +
                "link=" + /*link*/
                "https://haahoo.in/?" +
                "id=" +
                referralid.getText().toString() +
                "&apn=" + /*getPackageName()*/
                "creo.com.haahooapp" +
                "&st=" + /*titleSocial*/
                "Download+Haahoo" +
                "&sd=" + /*description*/
                "Install+Haahoo+to+get+a+social+entrepreneurship+venture+experience" +
                "&utm_source=" + /*source*/
                "AndroidApp";
        Log.d("linkkkk", "linkpath" + path);
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(path))
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            link = shortLink.toString();
                            Intent intent = new Intent();

                            Log.d("actual link", "linkkk" + link);
                            String msg = "visit my awesome website: " + link;

                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT, link);

                            Intent shareintent = Intent.createChooser(intent, null);
                            startActivity(shareintent);

                        } else {

                        }
                    }
                });

    }


    public void getConnectedShops() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "virtual_shop/connected_shop_det/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("connectionstatus", "hhhh" + jsonObject);

                            JSONArray data = jsonObject.optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject1 = data.optJSONObject(i);
                                ConnectedShops connectedShops1 = new ConnectedShops();
                                connectedShops1.setId(jsonObject1.optString("shop_id"));
                                connectedShops1.setName(jsonObject1.optString("shop_name"));
                                connectedShops1.setRating(jsonObject1.optString("rating_count"));
                                connectedShops1.setImage(ApiClient.BASE_URL + jsonObject1.optString("shop_img").replace("[", "").replace("]", ""));
                                connectedShops.add(connectedShops1);
                            }

                            connectedShopsAdapter = new ConnectedShopsAdapter(context, connectedShops);
                            connectedshopos.setAdapter(connectedShopsAdapter);
                            connectedshopos.setNestedScrollingEnabled(false);
                            connectedshopos.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public void getRefrralDetails() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/login_count/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            // Log.d("jsonobjj","hhhh"+jsonObject);
                            String data = jsonObject.optJSONArray("data").optString(0);
                            JSONObject jsonObject1 = new JSONObject(data);
//                            referrals.setText(jsonObject1.optString("referal_count"));
//                            orders.setText(jsonObject1.optString("order_count"));
                            referralid.setText(jsonObject1.optString("id"));
                            ApiClient.own_id = jsonObject1.optString("id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            //globalMenuItem.findItem(R.id.refer).setVisible(false);
            return true;
        }
        int id = item.getItemId();

        switch (id) {

            case R.id.refer:
                shareLongDynamicLink();
                break;

            case R.id.cart2:
                startActivity(new Intent(Navigation.this, CombinedCartActivity.class));
                break;
            case R.id.profile:
                startActivity(new Intent(Navigation.this, Profile.class));
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.top_menu, menu);
        initializeCountDrawer();
        initializewishlist();
        BadgeCounter.update(this,
                menu.findItem(R.id.cart2),
                R.drawable.cart,
                BadgeCounter.BadgeColor.BLACK,
                ApiClient.count);

        return true;
    }


    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }


    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {

            } else {

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            finishAffinity();

            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(this, " Click back again to exit from the app", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;

            }
        }, 2000);
    }


    @Override
    public void success(JSONArray data) {

    }

    @Override
    public void failed(String msg) {

    }

    @Override
    public void onSuccess(JSONArray data) {

        try {
            final JSONArray jsonArray = data;
            JSONObject jsonObject = new JSONObject();
            for (int i = 0; i < 3; i++) {
                jsonObject = jsonArray.getJSONObject(i);
                event_ids.add(jsonObject.getString("pk"));
                JSONObject jsonObject1 = jsonObject.getJSONObject("fields");
                event_name.add(jsonObject1.getString("name"));
                event_description.add(jsonObject1.getString("date"));
                String images = jsonObject1.getString("image");
                String[] seperated = images.split(",");
                String split = seperated[0].replace("[", "");
                event_image.add(split);

            }
            ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    Intent intent = new Intent(Navigation.this, Details.class);
                    intent.putExtra("id", event_ids.get(position));
                    startActivity(intent);
                }
            });

            for (int i = 0; i < event_image.size(); i++) {
                ProductPojo productPojo = new ProductPojo(event_name.get(i), event_description.get(i), event_image.get(i), event_image.get(i), event_image.get(i), event_image.get(i));
                pojo.add(productPojo);
            }
            EventAdapter eventAdapter = new EventAdapter(pojo, this);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(eventAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onSuccess(String response, String mem) {

    }

    @Override
    public void onFailed(String data) {
        hideProgressDialogWithTitle();
        scrollView.setVisibility(View.GONE);

    }


    @Override
    public void jsonSUccess(JSONArray jsonObject1, String next) {
        try {
            jsonObject = jsonObject1;
            ApiClient.search_name.clear();
            for (int i = 0; i < 3; i++) {
                JSONObject jsonObject2 = jsonObject.getJSONObject(i);
                ids.add(jsonObject2.getString("pk"));
                JSONObject jsonObject3 = jsonObject2.getJSONObject("fields");
                names.add(jsonObject3.getString("name"));
                ApiClient.search_name.add(jsonObject3.optString("name"));
                String images1 = jsonObject3.getString("image");
                String[] seperated = images1.split(",");
                String split = seperated[0].replace("[", "").replace("]", "");
                // Log.d("oooooooooo","kkkkk"+split);
                images.add(split);
            }
            firsttext.setText(names.get(0));
            secondtext.setText(names.get(1));
            thirdtext.setText(names.get(2));
            Glide.with(context).load(ApiClient.BASE_URL + "media/" + images.get(0)).into(firstImage);
            Glide.with(context).load(ApiClient.BASE_URL + "media/" + images.get(1)).into(secondImage);
            Glide.with(context).load(ApiClient.BASE_URL + "media/" + images.get(2)).into(thirdImage);
            hideProgressDialogWithTitle();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onVerifySuccess(String data, String mem) {

    }

    @Override
    public void onVerifyFailed(String msg) {

    }

    @Override
    public void jsonObjectSuccess(JSONArray jsonObject, String next) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        return false;
    }

    void getProfileDetails() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL + "login/user_details/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("jkbjbb", "response" + response);
                            JSONObject jsonObject = new JSONObject(response);
                            String data = jsonObject.optString("data");
                            String activity_status = jsonObject.optString("activity_status");
                            JSONArray jsonObject1 = new JSONArray(data);
                            String fields = jsonObject1.optJSONObject(0).optString("fields");
                            JSONObject jsonObject2 = new JSONObject(fields);
                            String name1 = jsonObject2.optString("name");
                            String email1 = jsonObject2.optString("email");
                            String phone_no1 = jsonObject2.optString("phone_no");
                            ApiClient.mobile = phone_no1;
                            ApiClient.proof = jsonObject2.optString("proof_id");
                            ApiClient.activity_status = activity_status;
                            if (ApiClient.activity_status.equals("0")) {
                                active.setText("Inactive");
                                active.setTextColor(Color.parseColor("#FF0000"));
                            }
                            if (ApiClient.activity_status.equals("1")) {
                                active.setText("Active");
                                active.setTextColor(Color.parseColor("#00ff00"));

                            }
                            ApiClient.email = email1;
                            name.setText(name1);
                            ApiClient.username = name1;
                            ApiClient.user_contact_no = phone_no1;
                            email.setText(email1);
                            phone.setText(phone_no1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

}
