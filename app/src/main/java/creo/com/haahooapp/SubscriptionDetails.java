package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.config.ApiClient;
import de.hdodenhof.circleimageview.CircleImageView;

public class SubscriptionDetails extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    AlertDialog.Builder builder;
    Activity activity = this;
    Context context = this;
    Button choosehead,submit;
    String order_id = "null";
    String pdt_id = "null";
    String name = "null";
    String benefits =  "null";
    String days = "null";
    String image = "null";
    String until = "null";
    TextView choose,unsubscribe,sub_name,sub_until;
    String date = "null";
    TextView status,description;
    DatePickerDialog dialog;
    EditText reason;
    RelativeLayout relativeLayout;
    CircleImageView imageView;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_details);
        choosehead = findViewById(R.id.choosehead);
        relativeLayout = findViewById(R.id.relative);
        Bundle bundle = getIntent().getExtras();
        submit = findViewById(R.id.submit);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubscriptionDetails.super.onBackPressed();
            }
        });

        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context, AllStories.class));
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
        pdt_id = bundle.getString("pdt_id");
        sub_name = findViewById(R.id.name);
        reason = findViewById(R.id.reason);
        builder = new AlertDialog.Builder(this);
        sub_until = findViewById(R.id.until);
        status = findViewById(R.id.status);
        description = findViewById(R.id.description);
        imageView = findViewById(R.id.image);
        name = bundle.getString("name");
        order_id = bundle.getString("order_id");
        benefits = bundle.getString("benefits");
        days = bundle.getString("days");
        image = ApiClient.BASE_URL+bundle.getString("image");
        until = bundle.getString("until");
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        Glide.with(context).load(image).into(imageView);
        sub_name.setText(name);
        sub_until.setText(until);
        unsubscribe = findViewById(R.id.unsubscribe);
        choose = findViewById(R.id.choose);
        description.setText(benefits);
        if (days.equals("0")){
            unsubscribe.setVisibility(View.GONE);
            status.setText("Inactive");
            status.setTextColor(ContextCompat.getColor(context,R.color.red));
        }
        if (!(days.equals("0"))){
            unsubscribe.setVisibility(View.VISIBLE);
            status.setText("Active");
            status.setTextColor(ContextCompat.getColor(context,R.color.green));

        }
        unsubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayout.setVisibility(View.VISIBLE);
                unsubscribe.setVisibility(View.GONE);
            }
        });
        choosehead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
    submit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (date.equals("null")){
                Toast.makeText(context,"Please select date",Toast.LENGTH_SHORT).show();
            }
            if (!(date.equals("null"))){


                builder.setMessage("Opt out subscription") .setTitle("Opt out");

                //Setting message manually and performing action on button click
                builder.setMessage("Do you want to opt out subscription on "+date+" ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String token = (pref.getString("token", ""));
                                RequestQueue queue = Volley.newRequestQueue(context);

                                //this is the url where you want to send the request

                                String url = ApiClient.BASE_URL+"virtual_shop/subscription_cancel/";

                                // Request a string response from the provided URL.

                                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                // Display the response string.
                                                try {

                                                    JSONObject obj = new JSONObject(response);
                                                    dialog.dismiss();
                                                    String message = obj.optString("message");
                                                    if (message.equals("success")) {
                                                        Toast.makeText(context, "Request has been sent successfully", Toast.LENGTH_SHORT).show();
                                                        Intent intent = new Intent(context, SubscriptionList.class);
                                                        intent.putExtra("fromactivity", "payment");
                                                        startActivity(intent);
                                                    }
                                                    if (!(message.equals("success"))) {
                                                        Toast.makeText(context,"Already unsubscribed",Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                    //

                                    }
                                }) {

                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("order_id", order_id);
                                        params.put("date_cancel",date);
                                        if (reason.getText().toString().length()==0) {
                                            params.put("reason", "nothing");
                                        }
                                        if (!(reason.getText().toString().length()==0)) {
                                            params.put("reason", reason.getText().toString());
                                        }
                                        return params;
                                    }

                                    @Override
                                    public Map<String, String> getHeaders()  {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("Authorization", "Token " + token);
                                        return params;
                                    }
                                };

                                // Add the request to the RequestQueue.
                                queue.add(stringRequest);

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();

                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Opt out");
                alert.show();
            }


        }
    });
    }


    public void showDatePickerDialog(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        int val = month+1;
        date =  year + "-" + val + "-" + dayOfMonth;
        choose.setVisibility(View.VISIBLE);
        choose.setText("You choose to opt out the subscription on "+ date);
    }
}