package creo.com.haahooapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahooapp.Implementation.PinVerifyPresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.PinVerifyCallback;
import creo.com.haahooapp.interfaces.PinVerifyPresenter;
import creo.com.haahooapp.interfaces.PinVerifyView;

public class LoginWithPin extends AppCompatActivity implements PinVerifyCallback,PinVerifyView {
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    TextView skip,loginwithpwd,forgotpin;
    JSONArray jsonObject = new JSONArray();
    ProgressDialog progressDialog;
    Activity activity= this;
    PinVerifyPresenter pinVerifyPresenter;
    String device_id = null;
    private FirebaseAnalytics analytics;
    String total_count = "0";
    String refresh = null;
    ImageView logo;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with_pin);
        Window window = activity.getWindow();
        analytics = FirebaseAnalytics.getInstance(LoginWithPin.this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        refresh = refreshedToken;
        logo = findViewById(R.id.logo);
        Glide.with(context).load(ApiClient.BASE_URL+"media/files/events_add/extra_pic/haahoo_logo1.png").into(logo);
        Log.d("refreshloginwithpin","kjbhjb"+refresh);
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        loginwithpwd = findViewById(R.id.loginwithpwd);
//        Log.d("referttal_id","referr"+ApiClient.referal_id);
//        Log.d("prodrefff","prod"+ApiClient.product_referral_id);
        progressDialog = new ProgressDialog(this,R.style.MyAlertDialogStyle);
        loginwithpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginWithPin.this,MainUI.class));
            }
        });
        device_id =  Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        pinVerifyPresenter = new PinVerifyPresenterImpl(this);
        forgotpin = findViewById(R.id.forgotpin);
        forgotpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginWithPin.this,ForgotPinNumber.class));
            }
        });
        skip = findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pinVerifyPresenter.getProducts();
                startActivity(new Intent(LoginWithPin.this,NewHome.class));

            }
        });
        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        //attach lock view with dot indicator
        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLength(4);
        mPinLockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {

                //User input true code
                if (pin.length() == 4) {
                    showProgressDialogWithTitle("Verifying your account");
                    pinVerifyPresenter.verifyPin(device_id,pin,refresh);
                 //   pinVerifyPresenter.getProducts();

                } else {
                    Toast.makeText(LoginWithPin.this, "Failed code, try again!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onEmpty() {

            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {

            }
        });
    }
    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Loading");
        progressDialog.setIcon(R.drawable.haahoologo);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    @Override
    public void onVerifySuccess(String data,String mem) {

    }

    @Override
    public void onVerifyFailed(String msg) {

    }

    @Override
    public void jsonObjectSuccess(JSONArray jsonObject,String next) {

    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String response,String membership) {

        if(!(response.equals("failed"))) {

            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("token", response);
            editor.putString("membership", membership);
            editor.commit();
            ApiClient.user_token = response;
            loginToSkillq(device_id,response);
            cartCount(response);
            wishcount(response);


        }

        if(response.equals("failed")){
            mPinLockView.resetPinLockView();
            hideProgressDialogWithTitle();
            Toast.makeText(LoginWithPin.this,"Invalid PIN",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailed(String response) {

    }

    public void loginToSkillq(String number,String token1){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.haahoo.in/api_login/api_haahoo_user_update/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            Log.d("ffgh", "wish" +jsonObject1+number);
                            // ApiClient.wish_count = jsonObject1.optString("Total count");

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("phone_number","");
                params.put("token",token1);
                params.put("device_id",number);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


    public void wishcount(final String token){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"wishlist/wishlist_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);
                            String message = jsonObject1.getString("message");
                            Log.d("imagewish", "wish" +jsonObject1.optString("Total count"));
                            ApiClient.wish_count = jsonObject1.optString("Total count");

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       // Toast.makeText(LoginWithPin.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void jsonSUccess(JSONArray jsonObject1,String next) {

        jsonObject = jsonObject1;
        Log.d("productsXXX","prod"+jsonObject.toString());
        Intent intent = new Intent(LoginWithPin.this,NewHome.class);
        intent.putExtra("products",jsonObject.toString());
        startActivity(intent);
        hideProgressDialogWithTitle();

    }

    @Override
    public void noInternetConnection() {

    }

    public void cartCount(final String token){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"cart_view/cart_show/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            total_count = jsonObject1.optString("total all count");
                            if(total_count.equals("")){
                                total_count = "0";
                                ApiClient.count = Integer.valueOf(total_count);
                            }
                            if(!(total_count.equals(""))) {
                              //  mNotificationCounter = Integer.parseInt(total_count);
                                ApiClient.count = Integer.valueOf(total_count);
                            }
                            if (ApiClient.product_referral_id.equals("0")) {
                                startActivity(new Intent(LoginWithPin.this, NewHome.class));
                            }
//            if (!(ApiClient.product_referral_id.equals("0"))){
//                Intent intent = new Intent(LoginWithPin.this,ShareProductDetails.class);
//                intent.putExtra("id",ApiClient.product_referral_id);
//                startActivity(intent);
//            }
                            if (!(ApiClient.productinshopid.equals("0"))){
                                Intent intent = new Intent(LoginWithPin.this,ProductDetailsShop.class);
                                intent.putExtra("from","share");
                                intent.putExtra("shopid","1");
                                intent.putExtra("productid",ApiClient.productinshopid);
                                startActivity(intent);
                            }
                            if (!(ApiClient.shopidrefer.equals("0"))){
                                Intent intent = new Intent(LoginWithPin.this,ProductsInShop.class);
                                intent.putExtra("categoryid",ApiClient.shopidrefer);
                                startActivity(intent);
                            }
                            if (!(ApiClient.study_referal_id.equals("0"))){
                                Intent intent = new Intent(LoginWithPin.this, CouponDetailsActivity.class);
                                intent.putExtra("from","referal");
                                startActivity(intent);
                            }
                            Log.d("rrdddfgh","resss"+response);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      //  Toast.makeText(LoginWithPin.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
