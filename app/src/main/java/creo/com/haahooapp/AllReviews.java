package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahoo.CouponDetailsActivity;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.ReviewAdapter;
import creo.com.haahooapp.Modal.Reviewpojo;
import creo.com.haahooapp.config.ApiClient;

public class AllReviews extends AppCompatActivity {

    Activity activity = this;
    Context context = this;
    ImageView back;
    TextView add;
    String img="null";
    String productid = "null";
    String name="null";
    String price = "null";
    RecyclerView recyclerView;
    ArrayList<Reviewpojo> reviewpojos = new ArrayList<>();
    ReviewAdapter reviewAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_reviews);
        Window window = activity.getWindow();
        back = findViewById(R.id.back);
        add = findViewById(R.id.add);
        recyclerView = findViewById(R.id.recyclerview);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllReviews.super.onBackPressed();
            }
        });
        Bundle bundle = getIntent().getExtras();
        img = bundle.getString("image");
        productid = bundle.getString("productid");
        name = bundle.getString("name");
        price = bundle.getString("price");

        getAllReviews(productid);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,RateProduct.class);
                intent.putExtra("image",img);
                intent.putExtra("productid",productid);
                intent.putExtra("name",name);
                intent.putExtra("price",price);
                startActivity(intent);
            }
        });
        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(context, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(context,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(context, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(context, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                String membershipval = (pref.getString("membership", "null"));
                                if (membershipval.equals("1")) {
                                    startActivity(new Intent(context, MyShopNew.class));
                                }
                                if (!(membershipval.equals("1"))) {
                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                                            .setTitle("Become Our Premium Member")
                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                startActivity(new Intent(context, MemberShipActivity.class));
                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                                dialog.dismiss();
                                            });
// Show the alert
                                    builder.show();
                                }
                                break;

                        }
                        return true;
                    }
                });
    }

    public void getAllReviews(String productid){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"api_shop_app/pdt_rating_viewing/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("reviewss", "jsonarray" + jsonObject);
                            String message = jsonObject.optString("message");
                            String already_reviewed = jsonObject.optString("already");
                            if (jsonObject.optString("order_status").equals("0")){
                                add.setVisibility(View.GONE);
                            }
                            if (jsonObject.optString("order_status").equals("1") && already_reviewed.equals("0")){
                                add.setVisibility(View.VISIBLE);
                            }
                            JSONArray jsonArray = jsonObject.optJSONArray("data");
                            for (int i = 0;i<jsonArray.length();i++){
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                Reviewpojo reviewpojo = new Reviewpojo();
                                reviewpojo.setId(jsonObject1.optString("id"));
                                reviewpojo.setReview(jsonObject1.optString("review"));
                                reviewpojo.setRating(jsonObject1.optString("rating"));
                                reviewpojo.setName(jsonObject1.optString("user_name"));
                                reviewpojo.setOrder_status(jsonObject1.optString("order_status"));
                                reviewpojo.setDate(jsonObject1.optString("date"));
                                reviewpojo.setShop_rating(jsonObject1.optString("shop_rating"));
                                reviewpojo.setProductid(productid);
                                reviewpojo.setProductName(name);
                                reviewpojo.setProductImage(img);
                                reviewpojo.setPrice(price);
                                reviewpojos.add(reviewpojo);
                            }

                            reviewAdapter = new ReviewAdapter(context,reviewpojos);
                            recyclerView.setAdapter(reviewAdapter);
                            recyclerView.setNestedScrollingEnabled(false);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() throws AuthFailureError {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("pdt_id", productid);
                return params;
            }

            @Override
            public java.util.Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}