package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import creo.com.haahooapp.Adapters.ViewersAdapter;
import creo.com.haahooapp.Modal.ViewersPojo;
import creo.com.haahooapp.config.ApiClient;

public class StoryViewers extends AppCompatActivity {

    Activity activity = this;
    Context context = this;
    RecyclerView recyclerView;
    ArrayList<String>name = new ArrayList<>();
    ArrayList<String>time = new ArrayList<>();
    ImageView imageView;
    String image = "null";
    TextView count;
    String imageid = "null";
    ImageView delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_viewers);
        Bundle bundle = getIntent().getExtras();
        image = bundle.getString("image");
        String imgcount = bundle.getString("count");
        count = findViewById(R.id.count);
        delete = findViewById(R.id.delete);
        count.setText("Total Viewers "+imgcount);
        imageid = bundle.getString("imageid");
        getViewerDetails();
        recyclerView = findViewById(R.id.recyclerView);
        imageView = findViewById(R.id.image);


//        Glide.with(context)
//                .load(image)
//                .listener(new RequestListener< Drawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<String> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(String resource, Object model, Target<String> target, DataSource dataSource, boolean isFirstResource) {
//                        return false;
//                    }
//
//
//                })
//                .into(imageView)
//        ;
        Glide.with(context).load(image).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
              // Toast.makeText(context,"Loaded",Toast.LENGTH_SHORT).show();
                return false;
            }
        }).into(imageView);

//        Glide
//                .with( context )
//                .load( image)
//                .into( imageView );

        Window window = activity.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                String token = (pref.getString("token", ""));

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Confirm Delete");
                builder.setMessage("Do you really want to delete this Story ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"stories/story_del/",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject1 = new JSONObject(response);
                                            String message = jsonObject1.optString("message");
                                            if (message.equals("Success")){
                                                Toast.makeText(context,"Story deleted successfully",Toast.LENGTH_SHORT).show();
                                                startActivity(new Intent(context,NewHome.class));
                                            }
                                            if (message.equals("Failed")){
                                                Toast.makeText(context,"Failed to delete",Toast.LENGTH_SHORT).show();
                                            }
                                            Log.d("dsdfgfd","erws"+jsonObject1);



                                        }catch (JSONException e){
                                            e.printStackTrace();
                                        }

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(context,error.toString(),Toast.LENGTH_LONG).show();
                                    }
                                }){
                            @Override
                            protected Map<String,String> getParams(){
                                Map<String,String> params = new HashMap<String, String>();
                                params.put("id",imageid);

                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {

                                Map<String, String>  params = new HashMap<String, String>();
                                params.put("Authorization","Token "+token);
                                return params;
                            }
                        };

                        RequestQueue requestQueue = Volley.newRequestQueue(context);
                        requestQueue.add(stringRequest);
                        // Toast.makeText(context1, "You've choosen to delete all records", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(context1, "You've changed your mind to delete all records", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.show();

            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(StoryViewers.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(StoryViewers.this, Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(StoryViewers.this, SettingsActivity.class));
                                break;

                            case R.id.shop:

                                startActivity(new Intent(context,MyShopNew.class));
                                break;

                            case R.id.stories:

                                startActivity(new Intent(context,AllStories.class));
                                break;

                        }
                        return true;
                    }
                });


    }
    public void getViewerDetails(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        Log.d("jnjhjh","response"+imageid);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"stories/show_story_history/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject1 = new JSONObject(response);

                            JSONArray jsonArray = jsonObject1.optJSONArray("data");
                            if (jsonArray.length()==0){
                                Toast.makeText(activity, "No viewers yet..", Toast.LENGTH_SHORT).show();
                            }
                            for (int i = 0;i<jsonArray.length();i++){

                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                name.add(jsonObject.optString("name"));
                                try {
                                    DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                                    Date date = sdf.parse(jsonObject.optString("view_time"));
                                    DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                                    time.add(dateFormat.format(date));
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                            }
                            final List<ViewersPojo> viewersPojos = new ArrayList<>();
                            for(int k=0;k<name.size();k++){
                                ViewersPojo downloadPojo = new ViewersPojo(name.get(k),time.get(k));
                                viewersPojos.add(downloadPojo);
                            }
                            ViewersAdapter viewersAdapter = new ViewersAdapter(viewersPojos,context);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(context));
                            recyclerView.setAdapter(viewersAdapter);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(Navigation.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id",imageid);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
