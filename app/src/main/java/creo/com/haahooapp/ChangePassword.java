package creo.com.haahooapp;

import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import creo.com.haahooapp.Implementation.PasswordPresenterImpl;
import creo.com.haahooapp.interfaces.PasswordCallBack;
import creo.com.haahooapp.interfaces.PasswordPresenter;
import creo.com.haahooapp.interfaces.PasswordView;

public class ChangePassword extends AppCompatActivity implements PasswordCallBack,PasswordView {

    TextInputEditText current , newpass , confirmpass;
    TextView change;
    ImageView back;
    Activity activity = this;
    PasswordPresenter passwordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        back = findViewById(R.id.back);
        passwordPresenter = new PasswordPresenterImpl(this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePassword.super.onBackPressed();
            }
        });
        current = findViewById(R.id.currentpass);
        newpass = findViewById(R.id.newpass);
        confirmpass = findViewById(R.id.confirmpass);
        change = findViewById(R.id.change);

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(confirmpass.getText().toString().equals(newpass.getText().toString()))){
                    confirmpass.setError("Both passwords must be same");
                }

                if(confirmpass.getText().toString().equals(newpass.getText().toString())){
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                    String token = (pref.getString("token", ""));
                    passwordPresenter.change_password(current.getText().toString(),newpass.getText().toString(),token);
                }
            }
        });
    }

    @Override
    public void onVerifySuccesschangepass(String data) {
       // Toast.makeText(ChangePassword.this,data,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onVerifyFailedchangepass(String msg) {
        Toast.makeText(ChangePassword.this,msg,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccess(String response) {

        if(response.equals("Please enter the correct password")){
            current.setError(response);
            newpass.setText("");
            confirmpass.setText("");
        }
        if(response.equals("Password Changed")){
            Toast.makeText(ChangePassword.this,"Password changed successfully",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(ChangePassword.this,NewHome.class));
        }

    }

    @Override
    public void onFailed(String response) {

       // Toast.makeText(ChangePassword.this,response,Toast.LENGTH_LONG).show();
    }

    @Override
    public void noInternetConnection() {

    }
}
