package creo.com.haahooapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import creo.com.haahooapp.Adapters.ProfileOrderAdapter;
import creo.com.haahooapp.Modal.ProfileOrderPojo;
import creo.com.haahooapp.config.ApiClient;
import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {

    RecyclerView recyclerView;
    TextView order_count,referral_count,earnings_count,name,viewall,edit;
    ImageView imageView;
    ArrayList<ProfileOrderPojo>dataModelArrayList = new ArrayList<>();
    private ProfileOrderAdapter orderAdapter;
    String token = null;


    public ProfileFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile,container,false);
        recyclerView = view.findViewById(R.id.recyclerView);
        imageView = view.findViewById(R.id.img);
        order_count = view.findViewById(R.id.order_count);
        referral_count = view.findViewById(R.id.referral_count);
        earnings_count = view.findViewById(R.id.earnings_count);
        viewall = view.findViewById(R.id.viewall);
        edit = view.findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),EditProfile.class));
            }
        });
        viewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(),Orders.class);
                intent.putExtra("fromactivity","profile");
                startActivity(intent);
            }
        });
        name = view.findViewById(R.id.name);
        name.setText(ApiClient.username);
        Glide.with(getContext()).load(ApiClient.BASE_URL + "media/files/events_add/extra_pic/haahoo_logo1.png").into(imageView);
        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        fetchingJSON();
        getRefrralDetails();
        getEarnings();
        return view;
    }


    public void getEarnings(){

        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/login_ref_earnings/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String total = jsonObject.optString("total");

                            earnings_count.setText(total);
                            ApiClient.user_earnings = total;

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>params = new HashMap<String,String>();
                params.put("Authorization", "Token "+token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }


    public void getRefrralDetails(){

        SharedPreferences pref = getContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"login/login_count/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("jsonobjj","hhhh"+jsonObject);
                            String data = jsonObject.optJSONArray("data").optString(0);
                            JSONObject jsonObject1 = new JSONObject(data);
                            referral_count.setText(jsonObject1.optString("referal_count"));
                            order_count.setText(jsonObject1.optString("order_count"));

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization","Token "+token);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }

    private void fetchingJSON() {

        RequestQueue queue = Volley.newRequestQueue(getContext());
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL+"order_details/view_order_all/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            dataModelArrayList = new ArrayList<>();
                            Log.d("splitvalue","splitt"+response);
                            JSONArray dataArray  = obj.getJSONArray("data");
//                            popupWindow.dismiss();
//                            relativeLayout.setAlpha(1.0F);
                            if(dataArray.length() == 0){
//                                Toast.makeText(getContext(),"Nothing to display",Toast.LENGTH_SHORT).show();
                            }
                            if (dataArray.length() <= 4){

                                for (int i = 0; i < dataArray.length(); i++) {

                                    ProfileOrderPojo playerModel = new ProfileOrderPojo();
                                    JSONObject dataobj = dataArray.getJSONObject(i);
                                    String images1 = dataobj.getString("image");
                                    String[] seperated = images1.split(",");
                                    String split = seperated[0].replace("[", "").replace("]","");
                                    // image.add("http://haahoo.creopedia.com/media/" + split);
//                                  playerModel.setProductid(dataobj.getString("order_id"));
                                    ApiClient.virtual_order.add(dataobj.optString("virtual"));
                                    ApiClient.productids.add(dataobj.getString("order_id"));
                                    playerModel.setName(dataobj.getString("name"));
                                    playerModel.setId(dataobj.getString("order_id"));
                                    ApiClient.shids.add(dataobj.optString("sh_order_id"));
                                    // ApiClient.sh_order_id = dataobj.optString("sh_order_id");
//                                  playerModel.setPrice(dataobj.getString("status"));
                                    playerModel.setImage(ApiClient.BASE_URL+"media/"+split);
//                                  playerModel.setQty(ApiClient.BASE_URL+"media/" + split);
                                    //playerModel.setImgURL(dataobj.getString("imgURL"));
                                    // playerModel.setImage("https://haahoo.in/media/files/product_add/lbt.jpg");

                                    dataModelArrayList.add(playerModel);

                                }
                            }

                            if (dataArray.length() > 4){
                                for (int i = 0; i < 4; i++) {

                                    ProfileOrderPojo playerModel = new ProfileOrderPojo();
                                    JSONObject dataobj = dataArray.getJSONObject(i);
                                    String images1 = dataobj.getString("image");
                                    String[] seperated = images1.split(",");
                                    String split = seperated[0].replace("[", "").replace("]","");
//                                  playerModel.setProductid(dataobj.getString("order_id"));
                                    ApiClient.productids.add(dataobj.getString("order_id"));
                                    playerModel.setName(dataobj.getString("name"));
                                    playerModel.setId(dataobj.getString("order_id"));
                                    ApiClient.shids.add(dataobj.optString("sh_order_id"));
                                    // ApiClient.sh_order_id = dataobj.optString("sh_order_id");
//                                  playerModel.setPrice(dataobj.getString("status"));
                                    playerModel.setImage(ApiClient.BASE_URL+"media/"+split);
//                                  playerModel.setQty(ApiClient.BASE_URL+"media/" + split);
                                     //Log.d("imgggg","imgs"+"https://testapi.creopedia.com/media/" + split);
                                    //playerModel.setImgURL(dataobj.getString("imgURL"));
                                    // playerModel.setImage("https://haahoo.in/media/files/product_add/lbt.jpg");

                                    dataModelArrayList.add(playerModel);

                                }
                            }

                            orderAdapter = new ProfileOrderAdapter(getContext(),dataModelArrayList);
                            recyclerView.setAdapter(orderAdapter);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               //

            }
        }) {


            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        queue.add(stringRequest);

    }




}
