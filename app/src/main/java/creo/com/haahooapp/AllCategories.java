package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import creo.com.haahoo.MemberShipActivity;
import creo.com.haahooapp.Adapters.CategoryAdapter;
import creo.com.haahooapp.Modal.Category;
import creo.com.haahooapp.config.ApiClient;

public class AllCategories extends AppCompatActivity {

    RecyclerView list;
    Context context = this;
    Activity activity = this;
    ImageView imageView3;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);
        list = findViewById(R.id.list);
        Window window = activity.getWindow();
        imageView3 = findViewById(R.id.imageView3);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmer();
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllCategories.super.onBackPressed();
            }
        });
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        getcategories();
        BottomNavigationView bottomNavigationView =
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(AllCategories.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(AllCategories.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(AllCategories.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(AllCategories.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
    }

    public void getcategories() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/list_shop_cat/";

        // Request a string response from the provided URL.


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            List<Category> categories = new ArrayList<>();
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                Category category = new Category();
                                category.setId(jsonObject.optString("id"));
                                category.setName(jsonObject.optString("name"));
                                String images1 = jsonObject.optString("image");
                                String[] seperated = images1.split(",");
                                String split = seperated[0].replace("[", "").replace("]", "");

                                category.setImage(ApiClient.BASE_URL + split);
                                categories.add(category);
                            }
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            list.setVisibility(View.VISIBLE);
                            list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
                            list.setAdapter(new CategoryAdapter(getApplicationContext(), categories));
                            list.setNestedScrollingEnabled(false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //


            }
        }) {

            @Override
            public java.util.Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}