package creo.com.haahooapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import creo.com.haahooapp.Adapters.ProductAdapter;
import creo.com.haahooapp.Modal.ProdPojo;
import creo.com.haahooapp.config.ApiClient;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Orderdetails extends AppCompatActivity {

    ImageView back , image;
    String token = null;
    Context context=this;
    RelativeLayout cancel_relative,track,returnrelative;
    ProgressDialog progressDialog;
    RelativeLayout relativeLayout;
    PopupWindow popupWindow;
    Activity activity = this;
    String virtual = "null";
    TextView cancel,dateofdelivery;
    String dirpath;
    RecyclerView list;
    TextView date , id , price , paymentmode, name , quantity , shipname , address , pincode , status , phone,delhead,shopname,shopaddress,shoppincode,shopphone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetails);
        returnrelative = findViewById(R.id.returnrelative);
        returnrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,ReturnDetails.class));
            }
        });
        Window window = activity.getWindow();
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        dateofdelivery = findViewById(R.id.dateofdelivery);
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        list = findViewById(R.id.list);
// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        relativeLayout = findViewById(R.id.relative);
        shopname = findViewById(R.id.shopname);
        shopaddress = findViewById(R.id.shopaddress);
        shoppincode = findViewById(R.id.shoppincode);
        shopphone = findViewById(R.id.shopphone);
        delhead = findViewById(R.id.delhead);
        image = findViewById(R.id.image);
        date = findViewById(R.id.date);
        id = findViewById(R.id.id);
        price = findViewById(R.id.price);
        track = findViewById(R.id.trackrelative);
        paymentmode = findViewById(R.id.paymentmode);
        name = findViewById(R.id.name);
        shipname = findViewById(R.id.shipname);
        address = findViewById(R.id.address);
        cancel_relative = findViewById(R.id.cancel_relative);
        cancel = findViewById(R.id.cancel);
        pincode = findViewById(R.id.pincode);
        phone = findViewById(R.id.phonehead);
        status = findViewById(R.id.status);
        quantity = findViewById(R.id.quantity);
        Bundle bundle = getIntent().getExtras();
        final String order_id = getIntent().getStringExtra("order_id").toString();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        back = findViewById(R.id.back);
        track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Orderdetails.this,Track.class);
                intent.putExtra("orderid",order_id);
                intent.putExtra("virtual",virtual);
                startActivity(intent);
            }
        });
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:

                                startActivity(new Intent(Orderdetails.this, NewHome.class));
                                break;

                            case R.id.action_search:

                                startActivity(new Intent(Orderdetails.this,Search.class));
                                break;

                            case R.id.action_account:

                                startActivity(new Intent(Orderdetails.this, SettingsActivity.class));
                                break;

                            case R.id.stories:

                                //startActivity(new Intent(Orderdetails.this, AllStories.class));
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.shop:

//                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                String membershipval = (pref.getString("membership", "null"));
//                                if (membershipval.equals("1")) {
//                                    startActivity(new Intent(context, MyShopNew.class));
//                                }
//                                if (!(membershipval.equals("1"))) {
//                                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
//                                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
//                                            .setTitle("Become Our Premium Member")
//                                            .setMessage("Looks like you are not part of our Premium Membership , please click the below button to enjoy the full benefit of the app")
//                                            .addButton("BECOME PREMIUM", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                startActivity(new Intent(context, MemberShipActivity.class));
//                                            }).addButton("NO THANKS", Color.parseColor("#FFFFFF"), Color.parseColor("#FF0000"), CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
//                                                dialog.dismiss();
//                                            });
//// Show the alert
//                                    builder.show();
//                                }
                                Toast.makeText(context,"Coming Soon",Toast.LENGTH_SHORT).show();
                                break;

                        }
                        return true;
                    }
                });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Orderdetails.this,Orders.class);
                intent.putExtra("fromactivity","details");
                startActivity(intent);
//                startActivity(new Intent(Orderdetails.this,Orders.class));
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater layoutInflater = (LayoutInflater) Orderdetails.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.order_cancel, null);
                Button closePopupBtn = (Button) customView.findViewById(R.id.closePopupBtn);
                TextView apply = customView.findViewById(R.id.apply);
                apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showProgressDialogWithTitle("Cancelling Your Order....");
                        RequestQueue queue = Volley.newRequestQueue(Orderdetails.this);
                        //this is the url where you want to send the request

                        String url = ApiClient.BASE_URL+"order_details/order_cancel/";

                        // Request a string response from the provided URL.

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // Display the response string.
                                        try {

                                            JSONObject obj = new JSONObject(response);
                                            String message = obj.optString("message");
                                            if(message.equals("Success")){
                                                Toast.makeText(Orderdetails.this,"Successfully cancelled the order",Toast.LENGTH_SHORT).show();
                                                hideProgressDialogWithTitle();
                                                startActivity(new Intent(Orderdetails.this,Orders.class));
                                            }
                                            if(message.equals("Failed")){
                                                Toast.makeText(Orderdetails.this,"Failed to cancel the order",Toast.LENGTH_SHORT).show();
                                            }
                                            Log.d("ordrrrr","resp"+response);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
//order_details/order_cancel order_id
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Log.d("responseerror", "resp" + error.networkResponse.statusCode);
                              //  Toast.makeText(Orderdetails.this,"//Error",Toast.LENGTH_LONG).show();

                            }
                        }) {
                            //adding parameters to the request
                            @Override
                            protected Map<String, String> getParams()  {
                                Map<String, String> params = new HashMap<>();
                                params.put("order_id", order_id);
                                params.put("virtual",virtual);
                                return params;
                            }



                            @Override
                            public Map<String, String> getHeaders()  {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Authorization", "Token " + token);
                                return params;
                            }
                        };


                        queue.add(stringRequest);
                        // Add the request to the RequestQueue.

                    }
                });


                popupWindow = new PopupWindow(customView, ViewPager.LayoutParams.MATCH_PARENT, 1000);
                popupWindow.setBackgroundDrawable(new ColorDrawable(
                        android.graphics.Color.TRANSPARENT));
                relativeLayout.setAlpha(0.2F);
                //display the popup window
                popupWindow.showAtLocation(relativeLayout, Gravity.CENTER, 0, 0);
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        relativeLayout.setAlpha(1.0F);
                    }
                });
            }
        });
        //this is the url where you want to send the request
        String url = ApiClient.BASE_URL+"api_shop_app/detail_view_of_order_in_customer_page/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.d("VGHVHGVGHV","JHVHVJH"+response);
                            String name1 = obj.optString("name");
                            String images1 = obj.optString("image");
                            String[] seperated = images1.split(",");
                            String split = seperated[0].replace("[", "").replace("]","");
                            id.setText(obj.optString("order_common_id"));
                            price.setText("Rs. "+obj.optString("customer_ordered_amount"));
                            quantity.setText("Quantity : "+obj.optString("product_quantity"));
                            dateofdelivery.setText(obj.optString("expected_date"));
                            status.setText(obj.optString("status"));
                            if(status.getText().toString().equals("Processing")){
                                cancel_relative.setVisibility(View.VISIBLE);
                                track.setVisibility(View.VISIBLE);
                            }
                            if (status.getText().toString().equals("dispatched")){
                                cancel_relative.setVisibility(View.GONE);
                                track.setVisibility(View.VISIBLE);
                            }
                            if (status.getText().toString().equals("delivered")||(status.getText().toString().equals("Completed"))){
                                delhead.setText("Delivery Date");
                                track.setVisibility(View.VISIBLE);
                            }
                            address.setText(obj.optString("customer_address"));
                            shipname.setText(obj.optString("customer_name"));
                            pincode.setText("Pincode : "+obj.optString("customer_pincode"));
                            phone.setText("Phone Number : "+obj.optString("customer_contact"));
                            paymentmode.setText(obj.optString("payment_method"));
                            date.setText(obj.optString("date"));
                            Glide.with(context).load(ApiClient.BASE_URL+"media/" + split).into(image);
                            shopname.setText(obj.optString("shop_name"));
                            shopaddress.setText("Shop Address : "+obj.optString("shop_address"));
                            shoppincode.setText("Shop Pincode : "+obj.optString("shop_pincode"));
                            shopphone.setText("Shop Phone Number : "+obj.optString("shop_contact"));
                            List<ProdPojo> pojos = new ArrayList<>();
                            JSONArray array = obj.optJSONArray("product_array");
                            for (int i = 0;i<array.length();i++){
                                JSONObject jsonObject = array.optJSONObject(i);
                                ProdPojo productPojo = new ProdPojo();
                                productPojo.setName(jsonObject.optString("product_name"));
                                productPojo.setPrice("Rs. "+jsonObject.optString("haahoo_price"));
                                productPojo.setQty("Quantity : "+jsonObject.optString("product_quantity"));
                                productPojo.setImage(ApiClient.BASE_URL+jsonObject.optString("product_image").split(",")[0].replace("[","").replace("]",""));
                                productPojo.setStatus("Order Status : "+jsonObject.optString("status"));
                                pojos.add(productPojo);
                            }
                            ProductAdapter productAdapter = new ProductAdapter(pojos,context);
                            list.setNestedScrollingEnabled(false);
                            list.setLayoutManager(new LinearLayoutManager(context,RecyclerView.VERTICAL,false));
                            list.setAdapter(productAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            //adding parameters to the request
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();
                params.put("order_common_id", order_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Token " + token);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(1000,2,DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);
    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Orderdetails.this,Orders.class);
        intent.putExtra("fromactivity","details");
        startActivity(intent);
    }
}
