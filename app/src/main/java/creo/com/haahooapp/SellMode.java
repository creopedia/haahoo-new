package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;

public class SellMode extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_mode);

        new FancyAlertDialog.Builder(this)
                .setTitle("Direct Sell")
                .setBackgroundColor(Color.parseColor("#0078BC"))  //Don't pass R.color.colorvalue
                .setMessage("Choose the direct sell mode you want to sell the product ")
                .setNegativeBtnText("Individual")
                .setPositiveBtnBackground(Color.parseColor("#0078BC"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("B2B")
                .setNegativeBtnBackground(Color.parseColor("#0078BC"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.POP)
                .isCancellable(true)
                .setIcon(R.drawable.ic_star_border_black_24dp,Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                       startActivity(new Intent(SellMode.this,B2B.class));
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                       startActivity(new Intent(SellMode.this,DirectSell.class));
                    }
                })
                .build();
    }
}
