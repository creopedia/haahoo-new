package creo.com.haahooapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.timessquare.CalendarCellDecorator;
import com.squareup.timessquare.CalendarCellView;
import com.squareup.timessquare.CalendarPickerView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import creo.com.haahooapp.Implementation.EventViewPresenterImpl;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.interfaces.EventCallBack;
import creo.com.haahooapp.interfaces.EventView;
import creo.com.haahooapp.interfaces.EventViewPresenter;

public class SeatArrangement extends AppCompatActivity implements OnSelectDateListener, EventCallBack, EventView {
    Activity activity = this;
    CalendarView calendarView;
    EventViewPresenter eventViewPresenter;
    TextView number,title,location,date1,name,txt_seat_selected;
    CardView details;
    CalendarPickerView calendarPickerView;
    List<Calendar>disabled =new ArrayList<>();
    List<String> selected_date = new ArrayList<>();
    String selected_dates = "null";
    ImageView image;
    String proffession_name = "null";
    String proffession_type = "null";
    String event_id = "null";
    String number1 = "null";
    List<String>dates = new ArrayList<>();
    List<String>remainings = new ArrayList<>();
    List<String>disableddates = new ArrayList<>();
    List<String>ending = new ArrayList<>();
    Context context = this;
    String name1 = "null";
    String email1 = "null";
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seat_arrangement);
        Window window = activity.getWindow();
        progressDialog = new ProgressDialog(this);
        details = findViewById(R.id.details);
        title = findViewById(R.id.title);
        txt_seat_selected = findViewById(R.id.txt_seat_selected);
        location = findViewById(R.id.location);
        date1 =  findViewById(R.id.date);
        image = findViewById(R.id.image);
        calendarPickerView = findViewById(R.id.calendar_view);
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.DAY_OF_MONTH, 1);
        Calendar calendar = getCalendarForNow();
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date max = calendar.getTime();
        Date today = new Date();
        calendarPickerView.init(today, max).inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                .withSelectedDate(today);
        name = findViewById(R.id.name);
        number = findViewById(R.id.number);
        Bundle bundle = getIntent().getExtras();
        number1 = bundle.getString("number");
        name1 = bundle.getString("name");
        email1 = bundle.getString("email");
        name.setText(name1);
        number.setText(number1);
        event_id = bundle.getString("event_id");
        proffession_name = bundle.getString("proffession");
        proffession_type = bundle.getString("proffession_type");
        getAvailableSeats();
        calendarPickerView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
//                calendarPickerView.setBackgroundColor(Color.GREEN);
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String formattedDate = df.format(date);
                selected_date.add(formattedDate);
                Gson gson = new Gson();
                selected_dates = gson.toJson(selected_date);
               // Log.d("selecteddates","hdsf"+selected_dates);
//                if (selected_date.size() > 0) {
//                   // Toast.makeText(SeatArrangement.this, "selected" + selected_date.size(), Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onDateUnselected(Date date) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String formattedDate = df.format(date);
                selected_date.remove(formattedDate);
//                if (selected_date.size() > 0) {
//                    //Toast.makeText(SeatArrangement.this, "unselected" + selected_date.size(), Toast.LENGTH_SHORT).show();
//                }
            }
        });
        eventViewPresenter = new EventViewPresenterImpl(this);
        eventViewPresenter.getEventDetail(event_id);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        txt_seat_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                if (selected_date.size()==0){
                    progressDialog.dismiss();
                    Toast.makeText(SeatArrangement.this,"Please select atleast one date",Toast.LENGTH_SHORT).show();
                }
                if (!(selected_date.size()==0)){
                    registerDetails();
                }

            }
        });

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

    }

    @Override
    public void onSelect(List<Calendar> calendars) {
        Date date = new Date();
        for (int i = 0 ; i < calendars.size();i++){
            date = calendars.get(i).getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String formattedDate = df.format(date);
            //Log.d("dfghjsize","datesize"+formattedDate);
        }
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(date);
        details.setVisibility(View.VISIBLE);
        date1.setText(formattedDate);
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void success(JSONArray jsonArray) {

    }

    @Override
    public void failed(String msg) {

    }

    @Override
    public void onSuccess(JSONArray data) {
        try{

            JSONArray jsonArray = data;
            Log.d("dataaa","datas"+jsonArray);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            JSONObject jsonObject1  = jsonObject.getJSONObject("fields");
            String name = jsonObject1.getString("name");
            String imagea = jsonObject1.getString("image");
            String description1 = jsonObject1.getString("description");
            String date1 = jsonObject1.getString("date");
            String time1 = jsonObject1.getString("time");
            String place1 = jsonObject1.getString("location");
            String price1 = jsonObject1.optString("price");
            ApiClient.eventprice = price1;
            ApiClient.event_price = price1+"00";
            String[] seperated = imagea.split(",");
            String split = seperated[0].replace("[","");
            title.setText(name);
            location.setText(place1);
            Glide.with(context).load(ApiClient.BASE_URL+"media/" + split).into(image);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class EventDecorator implements CalendarCellDecorator {

        private Date evd;
        private int BackgroundColor;

        public EventDecorator(Date evd, int BackgroundColor) {
            this.evd = evd;
            this.BackgroundColor = BackgroundColor;
        }

        @Override
        public void decorate(CalendarCellView calendarCellView, Date date) {

            if (date.equals(evd)) {
                calendarCellView.setBackgroundColor(BackgroundColor);
            }

        }
    }

    public void registerDetails(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        Log.d("toke","token"+token);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiClient.BASE_URL+"events/event_reg/",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.optString("message");
                            if(message.equals("Success")){
                                progressDialog.dismiss();
                                String data = jsonObject.optString("data");

                                Intent intent = new Intent(SeatArrangement.this,MakePaymentEvents.class);
                                intent.putExtra("number",number1);
                                intent.putExtra("data",data);
                                intent.putExtra("email",email1);
                                intent.putExtra("event_id",event_id);
                                startActivity(intent);

                            }
                            if(!(message.equals("Success"))){
                                progressDialog.dismiss();
                                Toast.makeText(SeatArrangement.this,"Some error occured",Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                       // Toast.makeText(SeatArrangement.this, "//error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id",event_id);
                params.put("proffession",proffession_name);
                params.put("proffession_type",proffession_type);
                params.put("name", name1);
                params.put("date",selected_dates);
                params.put("phone_no", number1);
                params.put("email",email1);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);


                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onFailed(String data) {
        // Toast.makeText(Details.this,"Server failed",Toast.LENGTH_SHORT).show();
    }

    public void getAvailableSeats(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));

        RequestQueue queue = Volley.newRequestQueue(SeatArrangement.this);
        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"events/seat_availability/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.

                        try {

                            JSONObject obj = new JSONObject(response);
                            JSONArray jsonArray = obj.optJSONArray("data");
                            for (int i = 0 ; i<jsonArray.length() ; i++){

                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String date = jsonObject.optString("date");
                                String remaining = jsonObject.optString("remaining");
                                int remain_tickets = Integer.parseInt(remaining);
                                if (remain_tickets == 0){
                                    disableddates.add(date);
                                }
                                if (remain_tickets < 10 && remain_tickets!=0){
                                    ending.add(date);
                                }
                                if (remain_tickets > 10){
                                    dates.add(date);
                                    remainings.add(remaining);
                                }


                            }
                            if (remainings.size() == 0 && ending.size() == 0){
                                Toast.makeText(SeatArrangement.this,"Registrations for this event has been closed",Toast.LENGTH_SHORT).show();
                                txt_seat_selected.setVisibility(View.GONE);
                            }

                            SimpleDateFormat  format = new SimpleDateFormat("dd/MM/yyyy");
                            try {
                                Calendar calendar1 = Calendar.getInstance();
                                for (int j = 0 ; j < disableddates.size();j++){
                                Date date2 = format.parse(disableddates.get(j));

                                calendar1.setTime(date2);

                                }
                                disabled.add(calendar1);
                                DateFormat mydateformat = new SimpleDateFormat("dd/MM/yyyy");
                                List<CalendarCellDecorator> decoratorList = new ArrayList<>();
                                try {
//                                    for (int i = 0 ; i<ending.size();i++){
//                                        decoratorList.add(new EventDecorator(mydateformat.parse(ending.get(i)), Color.YELLOW));
//
//                                    }
                                    for (int i = 0;i<disableddates.size();i++){
                                        decoratorList.add(new EventDecorator(mydateformat.parse(disableddates.get(i)),Color.RED));
                                    }
                                    //Log.d("resppppjhioufydts","jhgfds"+calendarPickerView.getDecorators().get(0));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                calendarPickerView.setDecorators(decoratorList);

                            }catch (Exception e){

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SeatArrangement.this,"//Error",Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();
                params.put("event_id", event_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        queue.add(stringRequest);

    }

    private static Calendar getCalendarForNow() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        return calendar;
    }

}
