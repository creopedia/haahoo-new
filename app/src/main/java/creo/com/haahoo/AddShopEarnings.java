package creo.com.haahoo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import creo.com.haahooapp.Adapters.AddShopEarningsAdapter;
import creo.com.haahooapp.Modal.AddShopData;
import creo.com.haahooapp.Modal.EarningAddShop;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class AddShopEarnings extends Fragment {

    RecyclerView recyclerView;
    ArrayList<AddShopData> newArrayList;
    public AddShopEarnings() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_referral_earning,container,false);
//        TextView description = view.findViewById(R.id.description);
//        description.setText(ApiClient.product_description);
        getAddShopEarnings();
        recyclerView = view.findViewById(R.id.listview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//         names = view.findViewById(R.id.name);
//         price = view.findViewById(R.id.price);


        // Inflate the layout for this fragment
        return view;
    }

    public void getAddShopEarnings(){
        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        final String token = (pref.getString("token", ""));
        Call<EarningAddShop> call = RetrofitClient
                .getInstance()
                .getApi()
                .getAddShopEarnings("Token " + token);
        call.enqueue(new Callback<EarningAddShop>() {
            @Override
            public void onResponse(Call<EarningAddShop> call, Response<EarningAddShop> response) {
                if(response.isSuccessful()) {
                    EarningAddShop earningAddShop = response.body();
                    if(response.body()!=null){
                        String success = response.body().getMessage();
                        if (success != null && success.equals("success")) {
                            List<AddShopData> addShopData = earningAddShop.getAddShopData();
                            newArrayList =(ArrayList<AddShopData>) addShopData;
                           /* CombinedCartAdapter combinedCartAdapter = new CombinedCartAdapter((ArrayList<CombinedCartList>) combinedCartList, CombinedCartActivity.this, CombinedCartActivity.this);
                            cartListRecycler.setAdapter(combinedCartAdapter);*/
                            AddShopEarningsAdapter addShopEarningsAdapter = new AddShopEarningsAdapter((ArrayList<AddShopData>) addShopData,getContext());
                            recyclerView.setAdapter(addShopEarningsAdapter);
                            Toast.makeText(getContext(), "Successfull", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<EarningAddShop> call, Throwable t) {

            }
        });
    }
}
