package creo.com.haahoo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import creo.com.haahooapp.Adapters.CouponListAdapter;
import creo.com.haahooapp.Modal.Coupon;
import creo.com.haahooapp.Modal.ViewCoupon;
import creo.com.haahooapp.NewHome;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCouponActivity extends AppCompatActivity {

    Activity activity = this;
    RecyclerView couponListRecycler;
    ImageView back;
    String token;
    ArrayList<Coupon> newCouponList;
    Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_coupon);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,NewHome.class));
            }
        });
        couponListRecycler = findViewById(R.id.recyclerViewCoupon);
        couponListRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        getCoupons();
    }

    public void getCoupons() {
        Call<ViewCoupon> call = RetrofitClient
                .getInstance()
                .getApi()
                .getCoupons("Token " + token);
        call.enqueue(new Callback<ViewCoupon>() {
            @Override
            public void onResponse(Call<ViewCoupon> call, Response<ViewCoupon> response) {
                if (response.isSuccessful()) {
                    ViewCoupon viewCoupon = response.body();
                    if (response.body() != null) {
                        String success = response.body().getMessage();
                        if (success != null && success.equals("success")) {
                            List<Coupon> couponList = viewCoupon.getCoupon();
                            newCouponList = (ArrayList<Coupon>) couponList;
                            CouponListAdapter couponListAdapter = new CouponListAdapter(newCouponList, ViewCouponActivity.this);
                            couponListRecycler.setAdapter(couponListAdapter);
                            couponListRecycler.setNestedScrollingEnabled(false);
                            //Toast.makeText(ViewCouponActivity.this,"Successful",Toast.LENGTH_LONG).show();
                        }
                    }
                }
                //Toast.makeText(ViewCouponActivity.this,"Successful",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<ViewCoupon> call, Throwable t) {
                //Toast.makeText(ViewCouponActivity.this,"Failed",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, NewHome.class));
    }
}
