package creo.com.haahoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.Flipkart;
import creo.com.haahooapp.Modal.CombinedCart;
import creo.com.haahooapp.Modal.ShopPojo;
import creo.com.haahooapp.Navigation;
import creo.com.haahooapp.NewHome;
import creo.com.haahooapp.R;
import creo.com.haahooapp.ShopsList;
import creo.com.haahooapp.SocialEntrepreneurship;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.RetrofitClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class CouponDetailsActivity extends AppCompatActivity {

    Activity activity = this;
    ImageView imageView, back;
    TextView price, original, shareLink;
    Button submit, viewWeb;
    Context context = this;
    String membership;
    String link = null;
    String from = "null";
    String token;
    String referal_id;
    CardView share;
    ProgressDialog progressDialog;
    String stat = "null";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_details);
        imageView = findViewById(R.id.image);
        back = findViewById(R.id.back);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Checking User...");
        progressDialog.show();
        Bundle bundle = getIntent().getExtras();
        share = findViewById(R.id.share);
        shareLink = findViewById(R.id.shareLink);
        if (bundle.containsKey("from") || !ApiClient.study_referal_id.equals("0")) {
            from = bundle.getString("from");

        }
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((membership.equals("0")) || (membership.equals("null"))) {
                    AlertDialog.Builder builder
                            = new AlertDialog
                            .Builder(context);

                    // Set the message show for the Alert time
                    builder.setMessage("As you are not our premium customer , you will lose Rs. 400 on successfull referals. If you understand the risks , please click Proceed.");
                    // Set Alert Title
                    builder.setTitle("Alert !");
                    // Set Cancelable false
                    // for when the user clicks on the outside
                    // the Dialog Box then it will remain show
                    builder.setCancelable(true);

                    // Set the positive button with yes name
                    // OnClickListener method is use of
                    // DialogInterface interface.

                    builder
                            .setPositiveButton(
                                    "Proceed",
                                    new DialogInterface
                                            .OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            shareLongDynamicLink();
                                            // When the user click yes button
                                            // then app will close
                                            // finish();
                                        }
                                    });

                    // Set the Negative button with No name
                    // OnClickListener method is use
                    // of DialogInterface interface.
                    builder
                            .setNegativeButton(
                                    "Become Premium",
                                    new DialogInterface
                                            .OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            startActivity(new Intent(context, MemberShipActivity.class));
                                            // If user click no
                                            // then dialog box is canceled.
                                            dialog.cancel();
                                        }
                                    });

                    // Create the Alert dialog
                    AlertDialog alertDialog = builder.create();

                    // Show the Alert Dialog box
                    alertDialog.show();
                } else {


                    shareLongDynamicLink();
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, NewHome.class));
            }
        });
        referal_id = ApiClient.study_referal_id;
        if (referal_id.equals("0")) {
            progressDialog.dismiss();
        }
        if (!(referal_id.equals("0"))) {
            checkUser(referal_id);
            saveReferer(referal_id);
        }

        //Toast.makeText(context,"Refer"+referal_id,Toast.LENGTH_SHORT).show();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        membership = (pref.getString("membership", ""));
        submit = findViewById(R.id.submit);
        viewWeb = findViewById(R.id.viewWeb);
        price = findViewById(R.id.price);
        original = findViewById(R.id.original);
        Window window = activity.getWindow();

        Glide.with(context).load(ApiClient.BASE_URL+"media/android/studyfor.png").into(imageView);

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        if ((membership.equals("0") || membership.equals("null")) && ApiClient.study_referal_id.equals("0")) {
            original.setVisibility(View.GONE);
            price.setText("Price: Rs. 3500/-");
            // share.setVisibility(View.GONE);
        }
        if ((membership.equals("0") || membership.equals("null")) && !ApiClient.study_referal_id.equals("0")) {

            // share.setVisibility(View.GONE);
        }
        YouTubePlayerFragment youTubePlayerFragment =
                (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        youTubePlayerFragment.setRetainInstance(true);

        youTubePlayerFragment.initialize("AIzaSyCKVsZFjhEAMwC0hqPvsSWnR-g_bZgU2hk",
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean b) {
                        youTubePlayer.cueVideo("XxF3MWqhz1U");
                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                    }
                });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CouponDetailsActivity.this, CouponPaymentActivity.class);
                intent.putExtra("stat",stat);
                startActivity(intent);
            }
        });
        viewWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CouponDetailsActivity.this, Flipkart.class);
                startActivity(intent);

//                new FinestWebView.Builder(activity).webViewJavaScriptEnabled(true)
            }
        });
    }

    public void saveReferer(String referal_id) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "api_shop_app/set_referal_id/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject obj = new JSONObject(response);
                            Log.d("ssssd", "resp" + obj);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("referal_id", referal_id);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void checkUser(String user) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL + "login/check_membership_status/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject obj = new JSONObject(response);
                            String status = obj.optString("membership_status");
                            if (status.equals("0")){
                                price.setText("Price: Rs. 3500/-");
                                stat = "0";
                            }
                            if (status.equals("1")){
                                original.setVisibility(View.VISIBLE);
                                price.setText("Price: Rs. 3000/-");
                                stat = "1";
                            }
                            Log.d("USERCHECK", "resp" + obj);
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void shareLongDynamicLink() {

        final String path = "https://haahooapp.page.link/?" +
                "link=" + /*link*/
                "https://haahoo.in/?" +
                "studyid=" +
                ApiClient.own_id +
                "&apn=" + /*getPackageName()*/
                "creo.com.haahooapp" +
                "&st=" + /*titleSocial*/
                "Download+Haahoo+and+Get+discount+on+Study4career" +
                "&sd=" + /*description*/
                "Get+information+about+List+of+Courses+in+India+and+abroad.+|+Psychometric+test+|+Educational+Consulting+|Online+Learning+|+Career+placement+|" +
                "&utm_source=" + /*source*/
                "AndroidApp";
        Log.d("linkkkk", "linkpath" + path);
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(path))
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            link = shortLink.toString();
                            Intent intent = new Intent();

                            Log.d("actual link", "linkkk" + link);
                            String msg = "visit my awesome website: " + link;

                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            intent.putExtra(Intent.EXTRA_TEXT, link);


                            Intent shareintent = Intent.createChooser(intent, null);
                            startActivity(shareintent);

                        } else {

                        }
                    }
                });

    }

    @Override
    public void onBackPressed() {
        ApiClient.study_referal_id="0";
        startActivity(new Intent(context, NewHome.class));
    }
}
