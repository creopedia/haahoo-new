package creo.com.haahoo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import creo.com.haahooapp.Adapters.ReviewAdapter;
import creo.com.haahooapp.MakePayment;
import creo.com.haahooapp.Modal.CouponModel;
import creo.com.haahooapp.Modal.Membership;
import creo.com.haahooapp.Modal.Reviewpojo;
import creo.com.haahooapp.Navigation;
import creo.com.haahooapp.NewHome;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.RetrofitClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponPaymentActivity extends AppCompatActivity implements PaymentResultListener {
    Activity activity = this;
    String token,paymentId,paymentAmount,membership;
    Context context = this;
    String referal_id;
    String stat = "null";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_payment);
        Window window = activity.getWindow();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        membership = (pref.getString("membership", ""));
        referal_id=ApiClient.study_referal_id;
        Intent intent = getIntent();

        if(membership.equals("1")||!ApiClient.study_referal_id.equals("0")) {
            ApiClient.price = "₹ 3000";
        }else{
            ApiClient.price = "₹ 3500";
        }
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        if(membership.equals("1")||!ApiClient.study_referal_id.equals("0")||intent.getStringExtra("stat").equals("1")) {
            paymentAmount="300000";
        }else{
            paymentAmount="350000";;
        }
        startPayment();
    }
    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Haahoo");
            options.put("description", "Coupon Payment");
            //You can omit the image option to fetch the image from dashboard
            options.put("currency", "INR");
            String price[] = ApiClient.price.split("₹ ");
            options.put("amount", price[1]+"00");

            Log.d("amtt","price"+options.get("amount"));
            JSONObject preFill = new JSONObject();
            preFill.put("email", ApiClient.email);
            preFill.put("contact", ApiClient.mobile);
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }
    @Override
    public void onPaymentSuccess(String s) {
       create_payment(s);
        /*JSONObject jsonObject1 = new JSONObject();
        createPaymentNew(s);*/
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(CouponPaymentActivity.this,"Error while making payment",Toast.LENGTH_LONG).show();
        startActivity(new Intent(CouponPaymentActivity.this, NewHome.class));
    }

    public void create_payment(String payment_id) {
        paymentId=payment_id;
        Call<Membership> call = RetrofitClient
                .getInstance()
                .getApi()
                .getCouponCode("Token "+token,paymentId,paymentAmount);
        call.enqueue(new Callback<Membership>() {
            @Override
            public void onResponse(Call<Membership> call, Response<Membership> response) {
                if(response.isSuccessful()){
                    Membership membership = response.body();
                    String success = response.body().getMessage();
                    if(success != null&& success.equals("success")) {
                        if(!ApiClient.study_referal_id.equals("0")) {
                            getReferal();
                        }
                        Toast.makeText(CouponPaymentActivity.this, "Successful Payment", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(CouponPaymentActivity.this, ViewCouponActivity.class));
                    }
                }
                else{
                    Toast.makeText(CouponPaymentActivity.this,response.message(),Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Membership> call, Throwable t) {
                Toast.makeText(CouponPaymentActivity.this,"Cancelled Payment",Toast.LENGTH_LONG).show();
            }
        });

    }
    public void createPaymentNew(String paymentid){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String token = (pref.getString("token", ""));
        RequestQueue queue = Volley.newRequestQueue(context);

        //this is the url where you want to send the request

        String url = ApiClient.BASE_URL+"api_shop_app/generate_coupon/";

        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("reviewss", "jsonarray" + jsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Toast.makeText(context,"//Error",Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected java.util.Map<String, String> getParams() throws AuthFailureError {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("payment_data","");
                return params;
            }

            @Override
            public java.util.Map<String, String> getHeaders()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + token);
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
    public void getReferal(){
        Call<Membership> call = RetrofitClient
                .getInstance()
                .getApi()
                .getReferal("Token "+token,referal_id);
        call.enqueue(new Callback<Membership>() {
            @Override
            public void onResponse(Call<Membership> call, Response<Membership> response) {
                Log.d("REFDFF","hftyd"+response.body().getData()+response.body().getMessage());
                if(response.isSuccessful()) {
                    Membership membership = response.body();
                    if (response.body() != null) {
                        String success = membership.getMessage();
if(success!=null&&success.equals("Success")) {
    ApiClient.study_referal_id="0";
    //Toast.makeText(CouponPaymentActivity.this, "Success", Toast.LENGTH_LONG).show();
}
                    }
                }
            }

            @Override
            public void onFailure(Call<Membership> call, Throwable t) {
                Toast.makeText(CouponPaymentActivity.this,"Error while making payment",Toast.LENGTH_LONG).show();
            }
        });
    }

}
