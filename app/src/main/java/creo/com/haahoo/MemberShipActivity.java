package creo.com.haahoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import creo.com.haahooapp.Modal.Membership;
import creo.com.haahooapp.Navigation;
import creo.com.haahooapp.NewHome;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.RetrofitClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberShipActivity extends AppCompatActivity implements PaymentResultListener {

    Activity activity = this;
    ImageView back, image, image1;
    Button button;
    RelativeLayout rel;
    Context context = this;
    String token, paymentId, paymentAmount;
    String membershipval;
    TextView memStatus, memfee, membenefits;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership);
        rel = findViewById(R.id.rel);
        Window window = activity.getWindow();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading..");
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        image = findViewById(R.id.image);
        image1 = findViewById(R.id.image1);
        Glide.with(context).load("https://img.freepik.com/free-vector/people-check-money-accounts_74855-4452.jpg?size=626&ext=jpg&ga=GA1.2.94703687.1596348088").into(image);
        Glide.with(context).load("https://img.freepik.com/free-vector/hand-drawn-money-saving-concept-background_52683-6818.jpg?size=626&ext=jpg&ga=GA1.2.94703687.1596348088").into(image1);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        membershipval = (pref.getString("membership", "null"));
        paymentAmount = "25000";
        membenefits = findViewById(R.id.membenefits);
        memStatus = findViewById(R.id.memstatus);
        memfee = findViewById(R.id.memfee);
        back = findViewById(R.id.back);
        button = findViewById(R.id.btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (membershipval.equals("1")) {
            button.setVisibility(View.GONE);
            memStatus.setText("Premium Membership Status: Active");
            memfee.setVisibility(View.GONE);
            membenefits.setText("You will get the following benefits");
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MemberShipActivity.this, PremiumMemberActivity.class);
//                startActivity(intent);
                Checkout.preload(context);
                startPayment();
            }
        });
    }

    public void startPayment() {


        final Activity activity = this;

        final Checkout co = new Checkout();

        try {

            JSONObject options = new JSONObject();
            options.put("name", "Haahoo");
            options.put("description", "Premium Membership Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("currency", "INR");
            //    String price[] = ApiClient.price.split("₹ ");
            //options.put("amount", ApiClient.event_price);
            options.put("amount", "25000");
            JSONObject preFill = new JSONObject();
            preFill.put("email", ApiClient.email);
            preFill.put("contact", ApiClient.mobile);
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }

    }

    @Override
    public void onPaymentSuccess(String s) {
        /*Toast.makeText(context,"Payment Success"+s,Toast.LENGTH_SHORT).show();
        startActivity(new Intent(context, NewHome.class));*/
        progressDialog.show();

        create_payment(s);
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(context, "Error in payment" + s, Toast.LENGTH_SHORT).show();
    }

    public void create_payment(String payment_id) {
        paymentId = payment_id;
        Call<Membership> call = RetrofitClient
                .getInstance()
                .getApi()
                .getMembership("Token " + token, paymentId, paymentAmount);
        call.enqueue(new Callback<Membership>() {
            @Override
            public void onResponse(Call<Membership> call, Response<Membership> response) {
                if (response.isSuccessful()) {
                    Membership membership = response.body();
                    String success = response.body().getMessage();
                    if (success != null && success.equals("success")) {
                        String membershipval = membership.getData();
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("membership", membershipval);
                        editor.commit();
                        progressDialog.dismiss();
                        Toast.makeText(MemberShipActivity.this, "Successful Payment", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(MemberShipActivity.this, NewHome.class));
                    } else {
                        Toast.makeText(MemberShipActivity.this, success, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(MemberShipActivity.this, response.message(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Membership> call, Throwable t) {
                Toast.makeText(MemberShipActivity.this, "Cancelled Payment", Toast.LENGTH_LONG).show();
            }
        });

    }

}
