package creo.com.haahoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import creo.com.haahooapp.Adapters.CombinedCartAdapter;
import creo.com.haahooapp.Cart;
import creo.com.haahooapp.Modal.CombinedCart;
import creo.com.haahooapp.Modal.CombinedCartList;
import creo.com.haahooapp.NewHome;
import creo.com.haahooapp.R;
import creo.com.haahooapp.config.ApiClient;
import creo.com.haahooapp.config.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CombinedCartActivity extends AppCompatActivity implements CombinedCartAdapter.OnClickListener {

    Activity activity= this;
    RecyclerView cartListRecycler;
    ImageView back;
    String token;
    ArrayList<CombinedCartList> newArrayList;
    ProgressDialog progressDialog;
    Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combined_cart);
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        showProgressDialogWithTitle("Loading Products..");
        back = findViewById(R.id.back);
        cartListRecycler = findViewById(R.id.recyclerViewCart);
        cartListRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        token = (pref.getString("token", ""));
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(activity.getResources().getColor(R.color.black));

        getCartList();
    }

    public void getCartList(){
        Call<CombinedCart> call = RetrofitClient
                .getInstance()
                .getApi()
                .getCartList("Token "+token);
        call.enqueue(new Callback<CombinedCart>() {
            @Override
            public void onResponse(Call<CombinedCart> call, Response<CombinedCart> response) {
                if(response.isSuccessful()){
                    hideProgressDialogWithTitle();
                    CombinedCart combinedCart = response.body();
                    if(response.body()!=null){
                        String success = combinedCart.getMessage();
                        if(success != null&& success.equals("sucess")){
                            List<CombinedCartList> combinedCartList = combinedCart.getCombinedCartList();
                            newArrayList =(ArrayList<CombinedCartList>) combinedCartList;
                            CombinedCartAdapter combinedCartAdapter = new CombinedCartAdapter((ArrayList<CombinedCartList>) combinedCartList, CombinedCartActivity.this, CombinedCartActivity.this);
                            cartListRecycler.setAdapter(combinedCartAdapter);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CombinedCart> call, Throwable t) {
                hideProgressDialogWithTitle();
               // Toast.makeText(CombinedCartActivity.this,"Failed",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showProgressDialogWithTitle(String substring) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //Without this user can hide loader by tapping outside screen
        progressDialog.setCancelable(false);
        progressDialog.setIcon(R.drawable.haahoologo);
        progressDialog.setMessage(substring);
        progressDialog.show();
    }

    private void hideProgressDialogWithTitle() {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.dismiss();
    }

    @Override
    public void OnClickDetail(int position) {
        Intent intent = new Intent(CombinedCartActivity.this, Cart.class);
        intent.putExtra("from","cart");
        CombinedCartList combinedCartList = newArrayList.get(position);
        String shopId = combinedCartList.getShop_id();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        ApiClient.shop_id_del = shopId;
        editor.putString("shop_id", shopId);
        editor.commit();
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        if (intent.hasExtra("from")){
            if (intent.getStringExtra("from").equals("home")){
                startActivity(new Intent(context, NewHome.class));
            }
        }
        else{
            super.onBackPressed();
        }
    }

}